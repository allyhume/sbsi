\chapter{SBSI Data Format Specification}
\label{sec:sbsidataformat_start}
\section{General Comments}

Many parameter optimisations will require experimental data. This chapter describes the data format currently used within SBSI, called SBSI data format.

SBSI data format is a file format used for experimental and simulation data.  SBSI data format uses a bipartite file structure where the data is 
stored in essentially a tab-delimited format, and annotation is stored in a separate \emph{header} file. Data files refer to header files; a header file can be referenced by many data files.

In all files comments are indicated by line starting with a hash (�\#�) character.
Empty lines (pure white-space) are allowed to aid clarity.

The header file defines the columns in the data file so every data file must refer to an existing header file to be valid. Several data files can refer to the same header file.

A data file may refer to a subset of the columns defined in the header.

The header file can provide simple annotation of an experiment that the data files refer to.
Trailing tabs at ends of lines should also be avoided, in both files.
\section{Header File}

The header file is organised into seven sections as follows:
\begin{itemize}
\item *Annotation*
\item *InitialValues*
\item *Parameters*
\item *Columns*
\item *X2datasetconfig*  - optional, may be absent
\item *FFTdatasetconfig* - optional, may be absent
\item *StatesTable* - optional, may be absent. Mandatory if FFTdatasetconfig section has data in it.
\end{itemize}
where the * delimiters denote a section. The order of the sections is unimportant. The first four of these elements are part of the 'core' data format,
the latter three are used for the convenience of tools wishing to persist configuration information.

All the elements  in a header file are optional. A minimal header file is: 

\begin{verbatim}
*Annotation*
*InitialValues*
*Parameters*
*Column*
\end{verbatim}

\subsection{Annotation}
This enables the description of any experiment wide annotation. It is a single line of the form:

$<$ annotation field name $>$ $<$ description $>$

(In this and subsequent element descriptions, the $<$ and $>$ are just used to demarcate field names, but are not used in the files themselves).
The section is started by the following heading:

*Annotation*


\subsection{Initial Values}
This section can be empty and provides a set of initial values that may be used in a model associated with this experimental data. It is a single line of the form:

$<$species name$>$ $<$units$>$ $<$value$>$

Note that each item is delimited by white space.

The section is started by the following heading:

*Initial Values*

\subsection{Parameters}
This section can be empty and provides a set of initial parameter values to be used in model associated with the experimental data associated with this header. Each entry is defined on a single line.

$<$parameter name$>$ $<$units$>$ $<$value$>$

Each item is delimited by white space.

The section is started by the following header:

*Parameters*

\subsection{Column Definition}
This defines the columns present in the data file.

$<$column name$>$ $<$units$>$ $<$description$>$

Column names must be unique. Column names are compared in a case-insensitive manner � so, for example, cyclin\_A and CYCLIN\_A are considered duplicates.

The section is started by the following heading:

*Columns*

\subsection{X2DataSetConfig}
This defines values to be used in X2Costfunctions. Each row of data entered refers to a specific data file. Many rows of data may be present as many sbsidata files may share the same header. The values are as follows:
\begin{itemize}
\item Filename, start time, scale type, value type, use in setup, interval.
\item Values can be comma OR tab separated.
\item The File name must not be empty.
\item Start time and interval should be doubles, neither may be negative and interval may not be zero.
\item Scale type must be one  of :
      None, Average, AbsAverage, MinMax, Fix
\item Value type must be one  of: Normed, Direct
\item Use in setup must be true/false.
\end{itemize}

\subsection{FFTDataSetConfig}

This defines values to be used in FFTCostfunctions. Only 1 row of data can ever be present as a single FFT Cost Function is defined per header.

The values are as follows:

\element{use in setup}, \element{interval}.

Values can be comma OR tab separated.

\element{Use in setup} must be true/false.

\element{Interval} should be a double $>$ zero.

\subsection{StatesTable}

Every state/species in the SBML model must have an entry here if an FFTCostFunction is being configured. There may be multiple rows of data.

The values are as follows:

\element{Use}, \element{name}, \element{start time}, \element{period}.

Values can be comma OR tab separated.

\element{Use} must be true/false.

\element{Name} must not be empty.

\element{Start time} must be a positive double.

\element{Period} must be a positive double $>$ 0.

\section{Data File}

The data file must refer to its header and consists of the data to be used. It has the following structure:

Header Reference

Column Declaration

Data Values

\subsection{Header Reference}
This is a reference to the file containing the header definition. This should be given as a UNIX style file path (even on Windows) and can be either a relative or absolute path (relative is generally more portable and therefore preferred).

This consists of a line with the following format:

@!$<$header path$>$

Note that the �@!� must be the first 2 characters of the file and the path should follow without any space between it and its first character. Note that the path can contain spaces as the file path is read from character 3 until the last non-white space character on the line.

\subsection{Column Declaration}
This contains one or more column names defined in the header. They must be unique (duplicate column names cannot be used).  Column names are compared in a case-insensitive manner � e.g., cyclin\_A and CYCLIN\_A are considered duplicates. The column declaration can only span one line.

The format is:

$<$column name$>$�.

\subsection{Data Values}

These are one or more fields that correspond to the column declaration. The data values effectively define a table, and so there must be one value in every row for column that appears in the Column Declaration. Missing values should be  indicated by the value �null�.

$<$value$>$�..

\subsection{Lexical Notes}
Fields are separated by any white space characters (note that the Header reference is a special case).

Valid field definitions are defined below:
\begin{itemize}
\item Column Name	[A-Za-z\_][A-Za-z0-9\_-]*	``foo\_bar" is valid, as is ``\_foo\_bar", but ``foo bar" is not, nor is 1foo\_bar.
No spaces are allowed within the name.
\item Description	[A-Za-z]*.*\$	Can have spaces and is terminated by the end of the line.
\item Header Path.		See notes above
\item Units	[A-Za-z][A-Za-z0-9-()\_\^]*	This should conform to a controlled vocabulary. Examples are $ms^{-2}$.
\item Value	[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?	Matches integer, floating point and scientific notation in form
1.5e12 or 1.5E12
\item Species Name		as Column Name
\item Parameter Name		as Column Name
\end{itemize}
\subsection{Example}
This is an example of a header file
\begin{verbatim}
Header File
*Annotation*
Example header file for simple biochemical system (ABC)
synthetic data generated for Initial values A=950; B=20; C=30,
using abc_1.slv (abc_1.xml), all parameters equal 1, simulation time 10 s

# These initial values override those described in the SBML model.
*Initial values*
A	nm	950
B	nm	20
C	nm	30

# These parameters override those described in the SBML model.
*Parameters*
k1	n/a	1.5

#Annotation and units of columns
*Columns*
T	s	time
A	nm	substrate concentration
B	nm	intermediate product
C	nm	product
\end{verbatim}

and below is a sample of a data file  that refers to this header file. Note the agreement between column headings in the data file, and column names in the 'Columns' section of the header file.

\begin{verbatim}
@!ABC_9502030.sbsiheader
T	A	B	C
0	950	20	30
0.000387784	949.6396376	20.35247	30.00789235
0.000775567	949.2795514	20.70452733	30.01592123									
0.001551135	948.5599306	21.40781776	30.0322516

\end{verbatim}
