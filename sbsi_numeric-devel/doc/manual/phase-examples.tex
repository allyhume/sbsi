\chapter{Examples for Phase Shifting}
\label{sec:phase-examples:start}

\lstset{basicstyle=\small}

\section{Introduction}
This chapter is a tutorial on using the SBSI frameworks to
optimise a model in which deterministic discrete events occur.
Examples include the modelling of experiment conditions such as
light or temperature entrainment.

There are two ways in which we may model a change in the model
during the time course of the numerical analysis. The first uses
explicit events whilst the second uses our ability to define functions
which operate over the simulated time.

We begin with the trivial example model, which has a single reaction
at a simple rate, the full SBML source of this model is shown in
Figure~\ref{figure:phase-example-simple-model-source}.

\begin{verbatim}
k1 = 0.1
A -> B at rate A * k1
\end{verbatim}

\begin{figure}
\begin{lstlisting}
<?xml version="1.0" encoding="UTF-8"?>
<sbml xmlns="http://www.sbml.org/sbml/level2/version3" 
      level="2" version="3">
  <model id="singleReaction">
    <listOfCompartmentTypes>
      <compartmentType id="Compartment"/>
      <compartmentType id="Membrane"/>
    </listOfCompartmentTypes>
    <listOfCompartments>
      <compartment id="main" size="1.0"/>
    </listOfCompartments>
    <listOfSpecies>
      <species id="A" compartment="main" 
                      substanceUnits="item"
                      hasOnlySubstanceUnits="true"/>
      <species id="B" compartment="main" 
                      substanceUnits="item"
                      hasOnlySubstanceUnits="true"/>
    </listOfSpecies>
    <listOfParameters>
      <parameter id="k1" value="0.01"/>
        <parameter id="k2" value="0.001"/>
    </listOfParameters>
    <listOfInitialAssignments>
      <initialAssignment symbol="A">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <cn> 100.0 </cn>
        </math>
      </initialAssignment>
      <initialAssignment symbol="B">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <cn> 100.0 </cn>
        </math>
      </initialAssignment>
    </listOfInitialAssignments>
    <listOfReactions>
      <reaction id="a" reversible="false">
        <listOfReactants>
          <speciesReference species="A"/>
        </listOfReactants>
        <listOfProducts>
          <speciesReference species="B"/>
        </listOfProducts>
        <kineticLaw>
          <math xmlns="http://www.w3.org/1998/Math/MathML">
            <apply>
              <times/>
              <ci>A</ci>
              <ci>k1</ci>
            </apply>
          </math>
        </kineticLaw>
      </reaction>
    </listOfReactions>
  </model>
</sbml>
\end{lstlisting}
\caption{
\label{figure:phase-example-simple-model-source}
The full SBML source for our simple example model
}
\end{figure}

Our aim is to simulate three phases each of $20$ time units to a
final time of $60$. In the first phase we wish for our parameter
$k1$ to be equal to $0.1$, in the second this switches to $0.001$
and in the final phase we switch the value of $k1$ back to its
original value.

\section{Using Explicit Events}
Explicit events are supported in SBML, all events are defined within
a \element{listOfEvents} element using the \element{event} tag.
An \element{event} contains a \element{trigger} and a list of event
assignments. For more information please see the SBML
specification at: \url{www.sbml.org}.

Figure~\ref{figure:phases-list-of-events} depicts the additional
SBML which must be added to original base model shown in
Figure~\ref{figure:phase-example-simple-model-source}.
Note that there are two events for the three phases because
there are only two phase switches.
The list of events elements should be the sub-element of the `model'
element. It is important to note that in the definition of any parameter
altered by an event assignment `constant' attribute must be
explicitly set to `false'. So our original definition for the parameter
`k1' becomes:

\begin{lstlisting}
<listOfParameters>
  <parameter id="k1" value="0.01" constant="false" />
</listOfParameters>
\end{lstlisting}

However this represents the only \emph{modification} to the model
required in order to use explicit events. With this done the events
may added or subtracted from the model without otherwise modifying
the SBML model.

\begin{figure}
\begin{lstlisting}
<listOfEvents>
  <event>
    <trigger>
      <math xmlns="http://www.w3.org/1998/Math/MathML">
        <apply>
          <gt/>
          <csymbol encoding="text"
                   definitionURL="http://www.sbml.org/sbml/symbols/time"> t 
          </csymbol>
          <cn>20.0</cn>
        </apply>
      </math>
    </trigger>
    <listOfEventAssignments>
      <eventAssignment variable="k1">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <cn>0.001</cn>
        </math>
        </eventAssignment>
      </listOfEventAssignments>
  </event>
  <event>
    <trigger>
      <math xmlns="http://www.w3.org/1998/Math/MathML">
        <apply>
          <gt/>
          <csymbol encoding="text"
                   definitionURL="http://www.sbml.org/sbml/symbols/time"> t 
          </csymbol>
          <cn>40.0</cn>
        </apply>
      </math>
    </trigger>
    <listOfEventAssignments>
      <eventAssignment variable="k1">
        <math xmlns="http://www.w3.org/1998/Math/MathML">
          <cn>0.1</cn>
        </math>
        </eventAssignment>
      </listOfEventAssignments>
 </event>
</listOfEvents>
\end{lstlisting}
\caption{
Events SBML which is added to the simple SBML model in order to 
invoke the desired changes in rates during simulation.
\label{figure:phases-list-of-events}
}
\end{figure}

The results of evaluating this model are shown in
Figure~\ref{figure:phases-events-graph}.

\begin{figure}
\includegraphics{phases-events-graph}
\caption{
\label{figure:phases-events-graph} The results of numerical analysis
over a model containing phase switches at times 20 and 40.
}
\end{figure}
	
	
\section{Using Time-Dependent Rate Functions}
Often the model is not authored in SBML, but is authored in another
modelling framework such as \textrm{Bio-PEPA} and the SBML automatically
generated. In this situation it may be awkward, if the particular
framework involved does not support SBML events, to generate the SBML
and then manually add the required events. In this case it may be more
convenient to use a time-dependent rate function.

In \textrm{Bio-PEPA} to achieve the same scenario as that above we
would write the following rate function:

\begin{verbatim}
r = [ A * ( ( (H(20 - time) + H(time - 40)) * 0.1) +
            ( (H(time - 20) * H(40 - time)) * 0.001) )
    ];
\end{verbatim}

To understand this we must understand first of all the
$H$ function in \textrm{Bio-PEPA}. This function returns 1 if the
argument is greater than zero and 0 otherwise. So the first call
to the $H$ function, $H(20 -time)$ will be 1 if the current time is
less than 20. We add this to the result of calling $H(time - 40)$ which
checks that the current time is above 40. Hence the addition of these
will be 1 if we are in the first or third phase. This is then multiplied
by the rate at which we wish 'k1' to be, if we are in the first or third
phases. The next two calls are $H(time - 20)$ and $H(40 -time)$ check that
we are within the bounds of the second phase, since the first checks that
the time is above 20 and the second that it is below 40. Since the results
of these two calls are multiplied together the overall result will be
1 only if both of these hold true and we are in fact in the second phase.
The 1 or 0 is then multiplied by the rate which we wish the `k1' parameter
to take during the second phase. These two rates are then multiplied
together and the entire rate expression is equal to $A$ multiplied by
the desired value of `k1' depending on which phase we are in.

For other formalisms we can write the helper SBML function definitions
given in
Figure\ref{figure:phases-helper-functions}.


\begin{figure}
\begin{tabular}{ll}
\begin{lstlisting}[basicstyle=\tiny]
<functionDefinition id="phaseZeroToX">
<math xmlns="http://www.w3.org/1998/Math/MathML">
  <lambda>
    <bvar><ci>r</ci></bvar>
    <bvar><ci>X</ci></bvar>
    <bvar><ci>t</ci></bvar>
    <piecewise>
      <piece>
        <ci>r</ci>
        <apply>
          <gt/>
          <ci>X</ci>
          <ci>t</ci>
        </apply>
      </piece>
      <otherwise>
        <cn>0</cn>
      </otherwise>
    </piecewise>
  </lambda>
</math>  
</functionDefinition>
\end{lstlisting}
&
\begin{lstlisting}[basicstyle=\tiny]
<functionDefinition id="phaseXToInfinity">
<math xmlns="http://www.w3.org/1998/Math/MathML">
  <lambda>
    <bvar><ci>r</ci></bvar>
    <bvar><ci>X</ci></bvar>
    <bvar><ci>t</ci></bvar>
    <piecewise>
      <piece>
        <ci>r</ci>
        <apply>
          <gt/>
          <ci>t</ci>
          <ci>X</ci>
        </apply>
      </piece>
      <otherwise>
        <cn>0</cn>
      </otherwise>
    </piecewise>
  </lambda>
</math>  
</functionDefinition>
\end{lstlisting}
\\
\begin{lstlisting}[basicstyle=\tiny]

<functionDefinition id="phaseXToY">
<math xmlns="http://www.w3.org/1998/Math/MathML">
  <lambda>
    <bvar><ci>r</ci></bvar>
    <bvar><ci>X</ci></bvar>
    <bvar><ci>Y</ci></bvar>
    <bvar><ci>t</ci></bvar>
    <piecewise>
      <piece>
        <piecewise>
          <piece>
            <ci>r</ci>
            <apply>
              <gt/>
              <ci>t</ci>
              <ci>X</ci>
            </apply>
          </piece>
          <otherwise>
            <cn>0</cn>
          </otherwise>
        </piecewise>
        <apply>
          <lt/>
          <ci>t</ci>
          <ci>Y</ci>
        </apply>
      </piece>
      <otherwise>
        <cn>0</cn>
      </otherwise>
    </piecewise>
  </lambda>
</math>  
</functionDefinition>
\end{lstlisting}
& \\
\end{tabular}
\caption{
\label{figure:phases-helper-functions}.
Helper functions for SBML time-dependent rate functions
}
\end{figure}

% todo, I've not *quite* worked out how to do this yet.
% \section{Periodical Phase Switches}

% This section is left as a todo
% \section{Optimisation}
% The previous two sections have shown how the numerical analysis of
% an SBML model can react to events which alter parameters within the
% model. In this section we combine this with optimisation to optimise
% for two parameters which control the rate of a reaction in different
% phases of the time course.

