\chapter{Installation}
  This chapter explains how to set up SBSI-Numerics to run on your system. 
  SBSI-Numerics can run on any Unix-based machine that has an MPI library installed.
  {\bf This includes the Windows operating system if appropriate POSIX interfaces are installed, such as the CygWin package.}

\section{Quick start automatic installation}
The following sections detail the dependencies on which the SBSI numerics
framework depends and how to obtain and install them on Mac and Linux based
systems. However there is an automatic installation script which will on
most systems take care of this for you. The script is named setup.py and
is in the root folder of the SBSI distribution or can be obtained using the
following link: 
\url{https://sourceforge.net/projects/sbsi/files/SBSINumerics/setup.py/download}

The script is written in the \emph{python} programming language and hence
requires that python is installed to run. If this is not already installed
it can be obtained from \url{www.python.org}.
This is a python version 2 script.

To invoke the script run the command:

\begin{verbatim}
$ python setup.py
\end{verbatim}

This will print the help information which should guide you through
installation of the SBSI numerics framework and dependencies.

An important point however is that the installation directory for SBSI
and dependencies can be set using the option \texttt{install=/some/directory}
as in the command:

\begin{verbatim}
$ python setup.py all install=/usr/
\end{verbatim}

The default is to install to a directory named ``install'' under the current
working directory. If this, or whatever installation directory is not
a default location then in order to use the SBSI framework the installation
directory must be added to various system paths and compiler search paths.
This can be using the script found at:
\url{https://sourceforge.net/projects/sbsi/files/SBSINumerics/set-vars.sh/download}

This can be invoked with the command:

\begin{verbatim}
$ source set-vars.sh
\end{verbatim}
or
\begin{verbatim}
$ chmod u+x set-vars.sh
$ ./set-vars.sh
\end{verbatim}

In either case the installation directory is assumed to be a directory
named ``install'' under the current directory but can be specified as the
only command line argument as in:

\begin{verbatim}
$ source set-vars.sh /my/install/directory
\end{verbatim}


\section {General requirements}
  Version 4 or later of the GNU C++ compiler and GNU make are recommended if it is available. 
  The "make install" command installs libraries in standard places, and typically 
  requires root privileges, unless you specify the place with the --prefix flag to configure.  
E.g., to install into a non-standard location:

\begin{verbatim}
  ./configure  --prefix=/my/preferred/install/folder
\end{verbatim}

  SBSI-Numerics depends on  the following libraries.  The versions needed and where to obtain them are explained in more detail in future sections.
    \begin{enumerate}
      \item {\bf libsbml} At least level 2 version 3 support, 3.4.0 or greater.\\
           This  is optional. If you do not have to generate C++ code for SBSI-Numeric on your  system, 
           SBSI-Numeric will be built without libsbml with the flag "--with-sbml=no" to configure. 
      \item {\bf libxml2} \\ This is an  XML parsing library required by libsbml. Most Linux distributions include this package.
      \item {\bf MPI}  Open MPI, MPICH or LAM/MPI. \\
             Since MPI-2 standard has a bug for definition of SEEK\_SET, SEEK\_END, SEEK\_CUR,  
             if you use MPICH, you need to add a flag, "CXXFLAGS=-DMPICH\_IGNORE\_CXX\_SEEK" to configure.  
% allan - Okay I didn't even know about this when I took over the code
% because it appears to be documented exactly nowhere other than here.
% I was working from the README attempting to get SBSI-Numerics to compile
% and the only way to do that was with Sundials 2.4.
%       \item{\bf  Sundials Library v2.4.0} differential equation solver. \\
%             The newest version of Sundials is v2.4.0, however SBSI-Numeric uses a modified v2.3.0 with prefetch optimisations added by SBSI-Numeric author. 
%             v.2.4.0 is not compatible.
      \item {\bf Sundials Library} differential equation solver,
the currently required version is 2.4.0.
      \item {\bf PGAPack}  A parallelized optimisation library for Genetic Algorithm. \\
It was developed by David Levine of the Mathematics and Computer Science Division at Argonne National Laboratory. 
We have fixed bugs and changed its header files to run with C++ compiler, so use PGAPack included in SBSI-Numeric distribution.
These fixes however have been incorporated back into the main distribution
of the pgapack library which is available from:
\url{http://code.google.com/p/pgapack/}
       \item {\bf FFTW} Fast Fourier Transform library used for FFT cost functions.\\
 There are two FFTW version SBSI-Numeric supports, although the API of FFTW 3.x and  that of FFTW 2.x are not compatible. FFTW 2.x supports MPI for distributed-memory, and FFTW 3.x does have support for Cell processors but not MPI.
      \item {\bf CppUnit} This is optional. If you want to run unit tests, Installation of libcppunit is required.
   \end{enumerate}

\section{Installation on MacOSX}

 To install SBSI-Numeric, your system has to have the Apple developer tools installed.
  
 MacOS system needs \begin{verbatim} -Wl,-search_paths_first \end{verbatim} to use newly created
 libraries instead of system libraries.
 If you wish to override system libraries with new versions, pass this flag to configure.
 
  \subsection* {Installing libFFTW}
  
  For FFTW v3.x,
  \begin{verbatim}
   ./configure --disable-fortran CC=gcc CXX=g++ CFLAGS=-O3 CXXFLAGS=-Wall
    make
    make install
  \end{verbatim}

 For FFTW v2.x, 
  \begin{verbatim}
   ./configure --disable-fortran --enable-type-prefix CC=gcc CXX=g++ CFLAGS=-O3 CXXFLAGS=-Wall
    make
    make install
  \end{verbatim}


  \subsection*{Installing libxml2}

  The standard simple commands are enough.
  \begin{verbatim}
   ./configure 
    make
    make install
  \end{verbatim}

  The minimal requirement for SBSI-Numeric for libxml2 are available to be built with the command below.
  \begin{verbatim}
  ./configure CC=gcc CXX=g++ CFLAGS=-O3 --without-zlib --without-python --without-readline --without-ftp --without-http --without-pattern --without-catalog --without-docbook --without-iconv --without-schemas --with-schematron --without-threads
    make
    make install
  \end{verbatim}

  \subsection*{Installing libSBML}

 As the same as libxml2, the standard commands will build the library.
  \begin{verbatim}
   ./configure 
    make
    make install
  \end{verbatim}

 This configure command will find the libxml2 in standard places, /usr/local/lib or /usr/lib .
 If you want to specify the place of the libxml2 to build the libSBML, the flag --with-libxml2 to configure will find that one.

 The minimal requirement for SBSI-Numeric is available to build with,
  \begin{verbatim}
   ./configure  CXX=g++ CFLAGS=-O3 CXXFLAGS=-Wall --without-zlib --without-bzip2 
    make
    make install
  \end{verbatim}

 \subsection*{Installing PGAPack}

  \begin{verbatim}
    ./configure CC=mpicc CFLAGS="-O3 -lm" 
    make
    make install
  \end{verbatim}
  
  \subsection*{Installing Sundials}

 As the same as libxml2, the standard commands will build the library.
  \begin{verbatim}
   ./configure 
    make
    make install
  \end{verbatim}

  If your system does not support Fortran, use --disable-f77 flag to configure.
  
  \subsection*{Installing SBSI-Numeric}
 Now we have installed all the dependent libraries and can install SBSI-Numeric!
  \begin{verbatim}
  ./configure CXX=mpic++ CFLAGS=-O3 CXXFLAGS=-Wall
    make
    make install
  \end{verbatim}

  The configure command will find the libxml2, libsbml, libFFTW v3.x, libsundials, libpgapack under the standard directory, /usr or /usr/local.
  If you specify the libraries place, use --with-libxml2, --with-sundials, --with-pgapack --with-fftw3 flags to configure.
  SBSI-Numeric try to find FFTW v3.x as the standard FFTW library. If you want to use FFTW v2.x, the flags --with-fftw3=no --with-fftw2 will make available to link fftw v2.x instead of v3.x. 
   If the system does not have libSBML, SBSI-Numeric will skip to codes which has dependency of SBML.

 E.g., here is an example configure invocation where all the installed libraries have their locations specified.
\begin{verbatim}
./configure CXX=mpic++ CXXFLAGS="-O3 -Wall " --with-libxml2=/Desktop/SBSINumReleases/install
--with-sbml=/Desktop/SBSINumReleases/install  \
--with-sundials=/usr/local \
--with-pgapack=/Desktop/SBSINumReleases/install \
--with-fftw3=/Desktop/SBSINumReleases/install \
--prefix=/Desktop/SBSINumReleases/install \
\end{verbatim}


   We recommend to "make check" after built SBSI-Numeric before make install.
  
  
   
  \subsection*{Checking your installation works}

  \begin{verbatim}
    make check
  \end{verbatim}
 
%  There are four type of test, cppunit, abc_test, def\_Test and opt\_Test.
%  The cppunit test is option. On the system which does not install the cppunit library, the cppunit test will skip.
%  The abc_test is 
  

\section {Installation on Linux}

  \subsection* {Installing libFFTW}
   As well as installing from source, the packages from Debian and Redhat are available from their home page, \begin{verbatim} http://www.fftw.org/install/linux.html.  \end{verbatim}
  
  \subsection*{Installing libxml2}

  The standard simple commands are enough.
  \begin{verbatim}
   ./configure
    make
    make install
  \end{verbatim}

  The minimal requirement for SBSI-Numeric for libxml2 are available to be built with the command below.
  \begin{verbatim}
  ./configure CC=gcc CXX=g++ CFLAGS=-O3 --without-zlib --without-python --without-readline --without-ftp --without-http --without-pattern --without-catalog --without-docbook --without-iconv --without-schemas --with-schematron --without-threads
    make
    make install
  \end{verbatim}

 \subsection*{Installing libSBML}
 As the same as libxml2, the standard commands will build the library.
  \begin{verbatim}
   ./configure
    make
    make install
  \end{verbatim}

 This configure command will find the libxml2 in standard places, /usr/local/lib or /usr/lib .
 If you want to specify the place of the libxml2 to build the libSBML, the flag --with-libxml2 to configure will find that one.

 The minimal requirement for SBSI-Numeric is available to build with,
  \begin{verbatim}
   ./configure  CXX=g++ CFLAGS=-O3 CXXFLAGS=-Wall --without-zlib --without-bzip2
    make
    make install
  \end{verbatim}
 
 \subsection*{Installing PGAPack}

 \begin{verbatim}
    ./configure CC=mpicc CFLAGS="-O3 -lm"
    make
    make install
  \end{verbatim}
  
  \subsection*{Installing Sundials}

 As the same as libxml2, the standard commands will build the library.
  \begin{verbatim}
   ./configure
    make
    make install
  \end{verbatim}

  If your system does not support Fortran, use --disable-f77 flag to configure.

  \subsection*{Installing SBSI-Numeric}

  \begin{verbatim}
  ./configure CXX=mpic++ CFLAGS=-O3 CXXFLAGS=-Wall
    make
    make install
  \end{verbatim}

  The configure command will find the libxml2, libsbml, libFFTW v3.x, libsundials, libpgapack under the standard directory, /usr or /usr/local.\\
  If you specify the libraries place, use --with-libxml2, --with-sundials, --with-pgapack --with-fftw3 flags to configure.\\
  SBSI-Numeric tries to find FFTW v3.x as the standard FFTW library.\\ If you want to use FFTW v2.x, the flags --with-fftw3=no --with-fftw2 will make available to link fftw v2.x instead of v3.x.
   If the system does not have libSBML, SBSI-Numeric will skip to codes which has dependency of SBML.
   We will recommend to "make check" after building SBSI-Numeric before make install.

   Now we have installed all the dependent libraries and can install SBSI-Numeric!
   
  \subsection*{Checking your installation works}

  \begin{verbatim}
    make check
  \end{verbatim}

%\section{Installation on HECToR supercomputer}
%
% Since shared libraries are not supported on HECToR compute node, all libraries have to be built as static libraries.
% To compile MPI code, We use the Cray wrappers cc or CC.
% Modules for SBSINumeric is 
%  \begin{verbatim}
%  1) modules/3.1.6.5                       12) PrgEnv-pgi/2.2.48B
%  2) cray/MySQL/5.0.64-1.0202.2899.21.1    13) xtpe-target-cnl
%  3) pbs/10.0.1.83201                      14) xt-service/2.2.48B
%  4) packages                              15) xt-os/2.2.48B
%  5) pgi/9.0.4                             16) xt-boot/2.2.48B
%  6) totalview-support/1.0.6               17) xt-lustre-ss/2.2.48B_1.6.5
%  7) xt-totalview/8.6.0                    18) cray/job/1.5.5-0.1_2.0202.19481.53.6
%  8) xt-libsci/10.4.1                      19) cray/csa/3.0.0-1_2.0202.19602.75.1
%  9) xt-mpt/3.5.1                          20) cray/account/1.0.0-2.0202.19482.49.3
% 10) xt-pe/2.2.48B                         21) cray/projdb/1.0.0-1.0202.19483.52.1
% 11) xt-asyncpe/3.6                        22) Base-opts/2.2.48B
% 23) fftw/2.1.5.1
%  \end{verbatim}
%
%  An User does not have root authority for HECToR machine, you have to specify the directory path to install all libraries.
%
%  \subsection* {Installing libFFTW}
%   Installation of FFTW is not required. User load module command for FFTW.
%
%  \subsection*{Installing libxml2}
%
%  User -prefix flag for the install directory.
%
%  \begin{verbatim}
%   ./configure --without-zlib --without-python --without-readline --without-ftp --without-http --without-pattern --without-catalog --without-docbook --without-iconv --without-schemas --with-schematron --enable-shared=no --enable-static=yes --without-bzip2 CC=cc
%   make 
%   make install
%  \end{verbatim}
%
%  \subsection*{Installing libSBML}
%   On HECToR machine, libSBML does not need.
%
%  \subsection*{Installing PGAPack}
%  User -prefix flag for the install directory.
%  \begin{verbatim}
% ./configure CC=cc CFLAGS="-O3 -lm"
%   make 
%   make install
%  \end{verbatim}
%
%  \subsection*{Installing Sundials}
%
%  User -prefix flag for the install directory.
%  \begin{verbatim}
%  ./configure CC=cc CFLAGS="-O3 -lm"
%./configure --disable-fortran --enable-shared=no CC=cc CXX=CC CFLAGS="-fast -Munroll=n:4 -Minfo -Mneginfo -Mipa=fast,inline"
%   make 
%   make install
%  \end{verbatim}
%
%  \subsection*{Installing SBSI-Numeric}
%%
%  libxml2, PGAPack and Sundials were installed to  directories which specified with --prefix flag. Declare those paths for SBSINumeric with flags, --with-libxml2, --with-pgapack and  --with-sundials.
%  \begin{verbatim}
%./configure cc=cc CXX=CC CFLAGS="-DMPICH_IGNORE_CXX_SEEK -fast CXXFLAGS=-DMPICH_IGNORE_CXX_SEEK" --with-fftw3=no --with-fftw2=/opt/fftw/2.1.5/cnos/ --with-sbml=no
%   make 
%   make install
%  \end{verbatim}
%
%  \subsection*{Checking your installation works}
%
%  On HECToR, the parallel job for the front end is not supported. If you have to check, use job submit command for abc\_Test, def\_Test and opt\_Test.
%
%\section{Installation on ECDF cluster}
%
%  ECDF, Eddie is a standard compute cluster with 128 nodes with two dual core CPUs and 118 worker nodes with two quad core CPUs. There are two MPI on ECDF, OpenMPI and QLogic MPI. SBSI-Numeric use OpenMPI with infiniband support.
%  The compiler GNU and Intell are both work for SBSI-Numeric.
%
	
