\documentclass[11pt]{report}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{a4paper}                   % ... or a4paper or a5paper or ... 
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{amsmath}
\usepackage{hyperref}

\usepackage{listings}

\newcommand{\element}[1]{\texttt{#1}}
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\lstset{language=XML}

\begin{document}
\begin{titlepage}

\begin{center}


% Upper part of the page
\includegraphics[width=0.40\textwidth]{./SBSIsplash.png}\\[1cm]    






% Title
\HRule \\[0.4cm]
{ \huge \bfseries SBSI Numerics User manual }\\[0.4cm]
\HRule \\[1.5cm]

% \textsc{\LARGE Version 1.0.0}\\[1.5cm]
% This line is now in this input file, such that we can easily
% automatically update the version number during packaging.
\textsc{\LARGE Version 
\input{SBSI-NumericsVersion}
}\\[1.5cm]

% Author and supervisor
% \begin{minipage}{0.6\textwidth}
% \begin{flushleft} \large
\emph{Authors:} Richard \textsc{Adams}, Azusa \textsc{Yamaguchi} and Allan \textsc{Clark}
% \end{flushleft}
% \end{minipage}
\vfill
{\large \today}

\end{center}

% Bottom of the page
\begin{minipage}{0.6\textwidth}
\begin{flushleft}
\includegraphics[width=0.9\textwidth]{./csbe.png}\\[1cm] 
\end{flushleft}
\end{minipage}
\begin{minipage}{0.4\textwidth}
\begin{flushright}
\includegraphics[width=1\textwidth]{./bbsrc.png}\\[1cm] 
\end{flushright}
\end{minipage}


\end{titlepage}
\tableofcontents
\newpage
%This is the top level document, which includes chapters from other files
\section*{Acknowledgements}

\subsection*{Funding}
This work was supported by funding awarded to the Centre for Systems Biology at Edinburgh, a Centre for Integrative Systems Biology (CISB), by the BBSRC and EPSRC reference BB/D019621/1
\subsection*{Contributors}

This software was developed initially by {\bf Azusa Yamaguchi} and currently by  {\bf Allan Clark}. Many other people have contributed to this project, most of whom are listed below. If anyone who should be on is missing, we apologize, and please let us know so we can rectify the omission.
\\
\\
\noindent {{\bf Anatoly Sorokin} helped with the design and early development of the project.}
\\
\\
\noindent{ {\bf Kevin Stratford}, {\bf  Richard Adams},  {\bf Carl Troein},  {\bf Nikos Tsorman},  {\bf Neil Hanlon} and  {\bf Shakir Ali}  also made  contributions to the 
code and documentation.}
\\
\\
\noindent{  {\bf Galina Lebedeva}  provided example models and helped with testing the software.}
\\
\\
\noindent{ Acknowledgments are also due to  {\bf Andrew Millar} and members of the Circadian Clock modelling group for providing
direction, support and requirements to the project. }
\\
\\
\noindent{ {\bf Liz Elliot} and the CSBE admin team have provided a great deal of behind-the-scenes support to the project.
\\
\\  
\noindent{ This project was led firstly by  {\bf Igor Goryanin} and currently by  {\bf Stephen Gilmore}.}
\\
\\
For up-to-date information about help and contacts, please see our website:
\\
\\
\url{http://www.sbsi.ed.ac.uk}
\\
\\
\noindent{  {\bf Copyright Centre for Systems Biology Edinburgh, 2011.}}




\chapter{Introduction}
\section{ Scope and aim of SBSI optimization framework}
 SBSI-Numerics is a software library and application designed to fit parameters for biological models to experimental data.
  As models become larger, optimization becomes increasingly unsuitable for running on desktop machines.  SBSI-Numerics is  parallelized and is 
   designed to run on supercomputers or other parallel machines. So far SBSI-Numerics has been successfully installed on IBM BlueGene, HECTOR (the UK national
      supercomputer (Cray) )and the Edinburgh Compute Data Facility (ECDF). However SBSI-Numerics can also be installed on Unix-based desktop machines - such as MacOSX,   and Redhat scientific, Mandrake and Ubuntu flavours of Linux.  SBSI-Numerics uses entirely open source third-party libraries, and is itself open-source.
 SBSI-Numerics accepts models in SBML level 2 and data in a standard data format (SBSI data format, described later in this document). 
 
   This document describes how to install the optimization framework and  how to run the example files. The next chapters are devoted to the input file formats required. Finally, an in-depth description of the configuration is presented, along with some worked examples.
       
    SBSI-Numerics is under  continuous development and we will be interested to hear of your comments, bug reports and feature requests.
    
 \section{ Optimization strategies supported by SBSI-Numerics}
  
   Optimization is the process of attempting to find the best possible solution amongst all those available. In the domain of this application, optimization attempts 
   to find the best possible parameter values for a biological model to reproduce a given set of experimental data. The success of optimization depends on the choice 
   of optimization algorithm, and also on the ability of the evaluation (a.k.a objective, cost) function  to guide the optimization process towards a satisfactory solution.
   
   The optimization framework seeks global optima for parameter values. During a \emph{Setup} phase, all parameter space is searched using a quasi-random
   selection algorithm which endeavours to search  the parameter space uniformly to produce a starting parameter set. This starting  set is then refined during the
   main optimization phase, which can use either simulated annealing or genetic algorithm techniques.
   
   \subsection { Genetic algorithms}
      Genetic algorithms (GAs) are search methods based on the principles of natural selection and genetics. GAs encode the decision variables of a search problem
      into strings of alphabets which represent candidate solutions to the problem. The strings are referred to as \emph{chromosomes}, the alphabets are \emph{genes} and the values of genes are termed \emph{alleles}. In this application, a chromosome would represent a candidate set of parameter values to be
      evaluated. GAs rely on a \emph{population} of chromosomes, which,  over a number of generations undergo selection and mutation, where the evaluation function provides the selection force. The evolution proceeds through the following steps:
      \begin{enumerate}
      \item Initialization - an initial population of candidate solutions is generated (this is accomplished in SBSI-Numerics by the set up phase described in section \ref{sec:conf_setup}).
      \item Evaluation - the fitness values of candidate solutions are evaluated.
      \item Selection - the best solutions are propagated to the next generation.
      \item Recombination/mutation -these two processes allow the creation of novel parameter sets which may be better than the parental sets.
      \item The novel sets replace the original parental population.
      \item Steps 2-5 are repeated until some terminating condition is reached.
       \end{enumerate}
       
       In SBSI-Numerics, this process is configurable. Configuration is described in detail in chapter \ref{sec:config:start} , but the major factors influencing outcome are:
        \begin{enumerate}
        	\item The population size: Larger populations will cover more  of parameter space, but will take longer to evaluate.  Unnecessarily large populations may also duplicate search space and be wasteful.
	\item The number of generations: More generations will give more opportunity for  the search algorithm to reach the best, or target solution. 
	\item The number of parameter sets selected  to seed the next generation will influence how the parameter sets evolve.
	\item The mutation frequency will influence the search - high rates of mutation will produce different solutions, low mutation rates will produce new 
	  solutions more similar to previous solutions.
        \end{enumerate} 
    \subsection{ Simulated annealing}
     Simulated annealing techniques are based on an analogy with the physical annealing process of solids during cooling, where state transitions are effected by small perturbations, as the temperature is gradually lowered. In its usage in optimization, candidate solutions are equivalent to states of the physical system, 
     and the cost of a solution is equivalent to the energy of a state. 
     
     Simulated annealing, as well as accepting improvements in cost, also accepts deterioration with  a certain probability. This feature allows escape from
     local optima while still allowing iterative improvement. 
     
     In SBSI-Numerics's implementation, the major control variable for simulated annealing is the  parameter \element{Mu} which controls how likely a worse solution is to be accepted.
     Increasing values of  \element{Mu} decrease the probability of worse solutions being accepted to the next generation.
     
    \subsection {Objective (cost) functions}
    
    \subsubsection{Chi-squared}
     
      The basic cost function supplied with the framework is the chi-squared function, which is evaluated for each data point in the experimental data:

     \begin{equation}
     (cal_y - dat_y)^{2} * \frac{normFactor}  { weight}
       \end{equation}
     where $cal_y$ is the simulation at that time point, and $dat_y$ is the
experimental measurement at that time point.  For a data set, these
individual costs are summed over all data points and then divided through
by the number of data points, thus the cost for a data set is the average
cost per data point. Taking the average avoids one data set dominating the
cost function value due to having many more data points.
     
     By default, normalization and weighting factors are 1. Future releases of SBSI-Numerics will allow encoding of weighting factors for experimental data points.
     
     
      
    
\include{installation}
\include{inputformat}
\include{sbsidataformat}
\include{running}
\include{config}
\include{phase-examples}
\include{glossary}

\end{document}
