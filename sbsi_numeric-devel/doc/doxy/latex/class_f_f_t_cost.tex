\hypertarget{class_f_f_t_cost}{
\section{FFTCost Class Reference}
\label{class_f_f_t_cost}\index{FFTCost@{FFTCost}}
}
{\tt \#include $<$FFTCost.h$>$}

Inheritance diagram for FFTCost::\begin{figure}[H]
\begin{center}
\leavevmode
\includegraphics[height=3cm]{class_f_f_t_cost}
\end{center}
\end{figure}
\subsection*{Public Member Functions}
\begin{CompactItemize}
\item 
virtual void \hyperlink{class_f_f_t_cost_143c4453e4d6222a965f7cc36acc2302}{SetDataSet} (size\_\-t d\_\-i)
\item 
virtual double \hyperlink{class_f_f_t_cost_7753041d5b888a7d47c2dd78450c21d5}{GetCost} ()
\begin{CompactList}\small\item\em A virtual function for a top level function to calculate cost values. Should be implemented by all cost functions. \item\end{CompactList}\item 
virtual double \hyperlink{class_f_f_t_cost_9551317b1182e945f410b934de4b34b8}{getcost} ()
\item 
size\_\-t \hyperlink{class_f_f_t_cost_bd574c4a3f96841ba1d301ede0d03dbd}{GetNumFFTTarget} ()
\item 
vector$<$ string $>$ \hyperlink{class_f_f_t_cost_cbecaee2637138d6e20752b1c7187e47}{GetFFTTargets} ()
\item 
string \hyperlink{class_f_f_t_cost_47dbb516592a63cf8ca6371667031d94}{GetAFFTTarget} (int a)
\item 
size\_\-t \hyperlink{class_f_f_t_cost_f43aa0e4ad09cb9e33b5ef0ba211af76}{GetNumFFTPeriod} ()
\item 
vector$<$ double $>$ \hyperlink{class_f_f_t_cost_6276e72026482b9479b9234239a324c3}{GetFFTPeriods} ()
\item 
double \hyperlink{class_f_f_t_cost_e5f480ab1a8475df8d0da02d8d43f8e3}{GetAFFTPeriod} (int a)
\item 
size\_\-t \hyperlink{class_f_f_t_cost_a32fa16966b69f7896f7a12c7f76dbaf}{GetNumFFTTstart} ()
\item 
vector$<$ double $>$ \hyperlink{class_f_f_t_cost_7441b75ac6c6bd395768ad5cc1f9bd59}{GetFFTTstart} ()
\item 
double \hyperlink{class_f_f_t_cost_c988d9e848ccb71711a9ec0335e9ed3d}{GetAFFTTstart} (int a)
\item 
size\_\-t \hyperlink{class_f_f_t_cost_7fc982cf446b6e19e84f8b91df3cd79f}{GetNumFFTTend} ()
\item 
vector$<$ double $>$ \hyperlink{class_f_f_t_cost_c4111dcc4cade6f99d64d3ec8a1170cc}{GetFFTTend} ()
\item 
double \hyperlink{class_f_f_t_cost_8f7440e14373ea3cf14031e9c6344ebf}{GetAFFTTend} (int a)
\item 
vector$<$ double $>$ \hyperlink{class_f_f_t_cost_edd64d588c3bb1896efabe2afed1173a}{GetResultPeriods} ()
\item 
double \hyperlink{class_f_f_t_cost_e8ea4f34a6f89e20749a9888166e038f}{GetAPeriod} (int a)
\item 
double \hyperlink{class_f_f_t_cost_b7733f0bfc493a1941755dc16aab6392}{GetAPhase} (int a)
\item 
void \hyperlink{class_f_f_t_cost_bd6766f0f108332c40202ed9460378f2}{GetFFT3} (vector$<$ double $>$ vec)
\item 
void \hyperlink{class_f_f_t_cost_4484ec3ffc8bf3d7090869beb0497058}{GetFFT2} (vector$<$ double $>$ vec)
\end{CompactItemize}
\subsection*{Protected Attributes}
\begin{CompactItemize}
\item 
vector$<$ double $>$ \hyperlink{class_f_f_t_cost_bb26c44047156d7775925aeacb289b7d}{Period}
\item 
vector$<$ double $>$ \hyperlink{class_f_f_t_cost_17f1c9c7b86ec0bb1f28ac17c6f5c311}{Phase}
\item 
vector$<$ double $>$ \hyperlink{class_f_f_t_cost_c5441e65c3fb58b444caee3b18114d45}{power\_\-spect}
\item 
vector$<$ double $>$ \hyperlink{class_f_f_t_cost_a54c953829472743b954152df617fd73}{phase}
\item 
fftw\_\-plan \hyperlink{class_f_f_t_cost_3aef15e2030782bbd5e434860f8f8618}{p}
\end{CompactItemize}


\subsection{Member Function Documentation}
\hypertarget{class_f_f_t_cost_143c4453e4d6222a965f7cc36acc2302}{
\index{FFTCost@{FFTCost}!SetDataSet@{SetDataSet}}
\index{SetDataSet@{SetDataSet}!FFTCost@{FFTCost}}
\subsubsection[SetDataSet]{\setlength{\rightskip}{0pt plus 5cm}virtual void FFTCost::SetDataSet (size\_\-t {\em d\_\-i})\hspace{0.3cm}{\tt  \mbox{[}virtual\mbox{]}}}}
\label{class_f_f_t_cost_143c4453e4d6222a965f7cc36acc2302}




Reimplemented from \hyperlink{class_model_cost_function_dbf1082c3472380afd4b23ca5a5d5deb}{ModelCostFunction}.\hypertarget{class_f_f_t_cost_7753041d5b888a7d47c2dd78450c21d5}{
\index{FFTCost@{FFTCost}!GetCost@{GetCost}}
\index{GetCost@{GetCost}!FFTCost@{FFTCost}}
\subsubsection[GetCost]{\setlength{\rightskip}{0pt plus 5cm}virtual double FFTCost::GetCost ()\hspace{0.3cm}{\tt  \mbox{[}virtual\mbox{]}}}}
\label{class_f_f_t_cost_7753041d5b888a7d47c2dd78450c21d5}


A virtual function for a top level function to calculate cost values. Should be implemented by all cost functions. 

\begin{Desc}
\item[Returns:]a cost value \end{Desc}


Implements \hyperlink{class_model_cost_function_d818dc05cf7f25365ffdc643dd3955e1}{ModelCostFunction}.\hypertarget{class_f_f_t_cost_9551317b1182e945f410b934de4b34b8}{
\index{FFTCost@{FFTCost}!getcost@{getcost}}
\index{getcost@{getcost}!FFTCost@{FFTCost}}
\subsubsection[getcost]{\setlength{\rightskip}{0pt plus 5cm}virtual double FFTCost::getcost ()\hspace{0.3cm}{\tt  \mbox{[}virtual\mbox{]}}}}
\label{class_f_f_t_cost_9551317b1182e945f410b934de4b34b8}




Implements \hyperlink{class_model_cost_function_833d8e097168ae930e644640f641c9fd}{ModelCostFunction}.\hypertarget{class_f_f_t_cost_bd574c4a3f96841ba1d301ede0d03dbd}{
\index{FFTCost@{FFTCost}!GetNumFFTTarget@{GetNumFFTTarget}}
\index{GetNumFFTTarget@{GetNumFFTTarget}!FFTCost@{FFTCost}}
\subsubsection[GetNumFFTTarget]{\setlength{\rightskip}{0pt plus 5cm}size\_\-t FFTCost::GetNumFFTTarget ()\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_bd574c4a3f96841ba1d301ede0d03dbd}


\hypertarget{class_f_f_t_cost_cbecaee2637138d6e20752b1c7187e47}{
\index{FFTCost@{FFTCost}!GetFFTTargets@{GetFFTTargets}}
\index{GetFFTTargets@{GetFFTTargets}!FFTCost@{FFTCost}}
\subsubsection[GetFFTTargets]{\setlength{\rightskip}{0pt plus 5cm}vector$<$string$>$ FFTCost::GetFFTTargets ()\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_cbecaee2637138d6e20752b1c7187e47}


\hypertarget{class_f_f_t_cost_47dbb516592a63cf8ca6371667031d94}{
\index{FFTCost@{FFTCost}!GetAFFTTarget@{GetAFFTTarget}}
\index{GetAFFTTarget@{GetAFFTTarget}!FFTCost@{FFTCost}}
\subsubsection[GetAFFTTarget]{\setlength{\rightskip}{0pt plus 5cm}string FFTCost::GetAFFTTarget (int {\em a})\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_47dbb516592a63cf8ca6371667031d94}


\hypertarget{class_f_f_t_cost_f43aa0e4ad09cb9e33b5ef0ba211af76}{
\index{FFTCost@{FFTCost}!GetNumFFTPeriod@{GetNumFFTPeriod}}
\index{GetNumFFTPeriod@{GetNumFFTPeriod}!FFTCost@{FFTCost}}
\subsubsection[GetNumFFTPeriod]{\setlength{\rightskip}{0pt plus 5cm}size\_\-t FFTCost::GetNumFFTPeriod ()\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_f43aa0e4ad09cb9e33b5ef0ba211af76}


\hypertarget{class_f_f_t_cost_6276e72026482b9479b9234239a324c3}{
\index{FFTCost@{FFTCost}!GetFFTPeriods@{GetFFTPeriods}}
\index{GetFFTPeriods@{GetFFTPeriods}!FFTCost@{FFTCost}}
\subsubsection[GetFFTPeriods]{\setlength{\rightskip}{0pt plus 5cm}vector$<$double$>$ FFTCost::GetFFTPeriods ()\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_6276e72026482b9479b9234239a324c3}


\hypertarget{class_f_f_t_cost_e5f480ab1a8475df8d0da02d8d43f8e3}{
\index{FFTCost@{FFTCost}!GetAFFTPeriod@{GetAFFTPeriod}}
\index{GetAFFTPeriod@{GetAFFTPeriod}!FFTCost@{FFTCost}}
\subsubsection[GetAFFTPeriod]{\setlength{\rightskip}{0pt plus 5cm}double FFTCost::GetAFFTPeriod (int {\em a})\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_e5f480ab1a8475df8d0da02d8d43f8e3}


\hypertarget{class_f_f_t_cost_a32fa16966b69f7896f7a12c7f76dbaf}{
\index{FFTCost@{FFTCost}!GetNumFFTTstart@{GetNumFFTTstart}}
\index{GetNumFFTTstart@{GetNumFFTTstart}!FFTCost@{FFTCost}}
\subsubsection[GetNumFFTTstart]{\setlength{\rightskip}{0pt plus 5cm}size\_\-t FFTCost::GetNumFFTTstart ()\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_a32fa16966b69f7896f7a12c7f76dbaf}


\hypertarget{class_f_f_t_cost_7441b75ac6c6bd395768ad5cc1f9bd59}{
\index{FFTCost@{FFTCost}!GetFFTTstart@{GetFFTTstart}}
\index{GetFFTTstart@{GetFFTTstart}!FFTCost@{FFTCost}}
\subsubsection[GetFFTTstart]{\setlength{\rightskip}{0pt plus 5cm}vector$<$double$>$ FFTCost::GetFFTTstart ()\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_7441b75ac6c6bd395768ad5cc1f9bd59}


\hypertarget{class_f_f_t_cost_c988d9e848ccb71711a9ec0335e9ed3d}{
\index{FFTCost@{FFTCost}!GetAFFTTstart@{GetAFFTTstart}}
\index{GetAFFTTstart@{GetAFFTTstart}!FFTCost@{FFTCost}}
\subsubsection[GetAFFTTstart]{\setlength{\rightskip}{0pt plus 5cm}double FFTCost::GetAFFTTstart (int {\em a})\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_c988d9e848ccb71711a9ec0335e9ed3d}


\hypertarget{class_f_f_t_cost_7fc982cf446b6e19e84f8b91df3cd79f}{
\index{FFTCost@{FFTCost}!GetNumFFTTend@{GetNumFFTTend}}
\index{GetNumFFTTend@{GetNumFFTTend}!FFTCost@{FFTCost}}
\subsubsection[GetNumFFTTend]{\setlength{\rightskip}{0pt plus 5cm}size\_\-t FFTCost::GetNumFFTTend ()\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_7fc982cf446b6e19e84f8b91df3cd79f}


\hypertarget{class_f_f_t_cost_c4111dcc4cade6f99d64d3ec8a1170cc}{
\index{FFTCost@{FFTCost}!GetFFTTend@{GetFFTTend}}
\index{GetFFTTend@{GetFFTTend}!FFTCost@{FFTCost}}
\subsubsection[GetFFTTend]{\setlength{\rightskip}{0pt plus 5cm}vector$<$double$>$ FFTCost::GetFFTTend ()\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_c4111dcc4cade6f99d64d3ec8a1170cc}


\hypertarget{class_f_f_t_cost_8f7440e14373ea3cf14031e9c6344ebf}{
\index{FFTCost@{FFTCost}!GetAFFTTend@{GetAFFTTend}}
\index{GetAFFTTend@{GetAFFTTend}!FFTCost@{FFTCost}}
\subsubsection[GetAFFTTend]{\setlength{\rightskip}{0pt plus 5cm}double FFTCost::GetAFFTTend (int {\em a})\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_8f7440e14373ea3cf14031e9c6344ebf}


\hypertarget{class_f_f_t_cost_edd64d588c3bb1896efabe2afed1173a}{
\index{FFTCost@{FFTCost}!GetResultPeriods@{GetResultPeriods}}
\index{GetResultPeriods@{GetResultPeriods}!FFTCost@{FFTCost}}
\subsubsection[GetResultPeriods]{\setlength{\rightskip}{0pt plus 5cm}vector$<$double$>$ FFTCost::GetResultPeriods ()\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_edd64d588c3bb1896efabe2afed1173a}


\hypertarget{class_f_f_t_cost_e8ea4f34a6f89e20749a9888166e038f}{
\index{FFTCost@{FFTCost}!GetAPeriod@{GetAPeriod}}
\index{GetAPeriod@{GetAPeriod}!FFTCost@{FFTCost}}
\subsubsection[GetAPeriod]{\setlength{\rightskip}{0pt plus 5cm}double FFTCost::GetAPeriod (int {\em a})\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_e8ea4f34a6f89e20749a9888166e038f}


\hypertarget{class_f_f_t_cost_b7733f0bfc493a1941755dc16aab6392}{
\index{FFTCost@{FFTCost}!GetAPhase@{GetAPhase}}
\index{GetAPhase@{GetAPhase}!FFTCost@{FFTCost}}
\subsubsection[GetAPhase]{\setlength{\rightskip}{0pt plus 5cm}double FFTCost::GetAPhase (int {\em a})\hspace{0.3cm}{\tt  \mbox{[}inline\mbox{]}}}}
\label{class_f_f_t_cost_b7733f0bfc493a1941755dc16aab6392}


\hypertarget{class_f_f_t_cost_bd6766f0f108332c40202ed9460378f2}{
\index{FFTCost@{FFTCost}!GetFFT3@{GetFFT3}}
\index{GetFFT3@{GetFFT3}!FFTCost@{FFTCost}}
\subsubsection[GetFFT3]{\setlength{\rightskip}{0pt plus 5cm}void FFTCost::GetFFT3 (vector$<$ double $>$ {\em vec})}}
\label{class_f_f_t_cost_bd6766f0f108332c40202ed9460378f2}


\hypertarget{class_f_f_t_cost_4484ec3ffc8bf3d7090869beb0497058}{
\index{FFTCost@{FFTCost}!GetFFT2@{GetFFT2}}
\index{GetFFT2@{GetFFT2}!FFTCost@{FFTCost}}
\subsubsection[GetFFT2]{\setlength{\rightskip}{0pt plus 5cm}void FFTCost::GetFFT2 (vector$<$ double $>$ {\em vec})}}
\label{class_f_f_t_cost_4484ec3ffc8bf3d7090869beb0497058}




\subsection{Member Data Documentation}
\hypertarget{class_f_f_t_cost_bb26c44047156d7775925aeacb289b7d}{
\index{FFTCost@{FFTCost}!Period@{Period}}
\index{Period@{Period}!FFTCost@{FFTCost}}
\subsubsection[Period]{\setlength{\rightskip}{0pt plus 5cm}vector$<$double$>$ {\bf FFTCost::Period}\hspace{0.3cm}{\tt  \mbox{[}protected\mbox{]}}}}
\label{class_f_f_t_cost_bb26c44047156d7775925aeacb289b7d}


\hypertarget{class_f_f_t_cost_17f1c9c7b86ec0bb1f28ac17c6f5c311}{
\index{FFTCost@{FFTCost}!Phase@{Phase}}
\index{Phase@{Phase}!FFTCost@{FFTCost}}
\subsubsection[Phase]{\setlength{\rightskip}{0pt plus 5cm}vector$<$double$>$ {\bf FFTCost::Phase}\hspace{0.3cm}{\tt  \mbox{[}protected\mbox{]}}}}
\label{class_f_f_t_cost_17f1c9c7b86ec0bb1f28ac17c6f5c311}


\hypertarget{class_f_f_t_cost_c5441e65c3fb58b444caee3b18114d45}{
\index{FFTCost@{FFTCost}!power\_\-spect@{power\_\-spect}}
\index{power\_\-spect@{power\_\-spect}!FFTCost@{FFTCost}}
\subsubsection[power\_\-spect]{\setlength{\rightskip}{0pt plus 5cm}vector$<$double$>$ {\bf FFTCost::power\_\-spect}\hspace{0.3cm}{\tt  \mbox{[}protected\mbox{]}}}}
\label{class_f_f_t_cost_c5441e65c3fb58b444caee3b18114d45}


\hypertarget{class_f_f_t_cost_a54c953829472743b954152df617fd73}{
\index{FFTCost@{FFTCost}!phase@{phase}}
\index{phase@{phase}!FFTCost@{FFTCost}}
\subsubsection[phase]{\setlength{\rightskip}{0pt plus 5cm}vector$<$double$>$ {\bf FFTCost::phase}\hspace{0.3cm}{\tt  \mbox{[}protected\mbox{]}}}}
\label{class_f_f_t_cost_a54c953829472743b954152df617fd73}


\hypertarget{class_f_f_t_cost_3aef15e2030782bbd5e434860f8f8618}{
\index{FFTCost@{FFTCost}!p@{p}}
\index{p@{p}!FFTCost@{FFTCost}}
\subsubsection[p]{\setlength{\rightskip}{0pt plus 5cm}fftw\_\-plan {\bf FFTCost::p}\hspace{0.3cm}{\tt  \mbox{[}protected\mbox{]}}}}
\label{class_f_f_t_cost_3aef15e2030782bbd5e434860f8f8618}




The documentation for this class was generated from the following file:\begin{CompactItemize}
\item 
include/\hyperlink{_f_f_t_cost_8h}{FFTCost.h}\end{CompactItemize}
