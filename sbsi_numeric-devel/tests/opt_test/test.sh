#!/bin/sh

np=3 

mkdir -p UserModel/results  UserModel/ckpoint UserModel/SA_results UserModel/SA_ckpoint
echo Test for test optimisation with user defined costfuncion withs GA using mpi np=$np
mpirun -np $np ./optTest UserModel config_PGA.xml

mpirun -np $np ./optTest UserModel config_PSO.xml


./optTest UserModel config_SA.xml
