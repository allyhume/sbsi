#include <sys/time.h>
#include <mpi.h>          //MAXPATHLEN

#include <Setup.h>
#include <Optimiser.h>
#include <MultiObjCost.h>
#include <CommandLine.h>
#include <ParseConfigFile.h>
#include <ERR.h>

using namespace std;

#include "UserModel/UserCostFunction.h"


double getnow()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + 1e-6 * tv.tv_usec;
}

void writeBestCostResults( const string fname, const double cost, cModel *model);

int main(int argc, char **argv)
{
  /* usage Model_name logfilename loglevel xmlfile */
  if(argc < 3) {
    cout << "Not right usage. Usage: " << argv[0] << " [Model ID] [config.xml file]" << endl;
    exit(8);
  }

  MPI_Init(&argc, &argv);
  int my_rank(0);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  double tstart = getnow();

  CommandLine::is(argc, argv, my_rank);

  /* usage Model_name logfilename loglevel xmlfile */ // CT: Really??
  const string MODEL_NAME = CommandLine::arg();
  const string xmlfilename = CommandLine::arg();


  string modeldir = MODEL_NAME + "/";
  string exp_dir = modeldir + "exp_data/";
  string results_dir = modeldir + "results/";
  string ckpoint_dir = modeldir + "ckpoint/";

  /* prepare args */
  cModelArg ModelArg;
  OptArg opt_arg;

  /* set default values */
  opt_arg.setCkpointStartNum(0);
  opt_arg.setPrintBestPop(true);
  opt_arg.MyMutation = NULL;
  opt_arg.MyCrossOver = NULL;
  opt_arg.MyEndOfGen = NULL;

  opt_arg.setResultDir(results_dir);
  opt_arg.setCkpointDir(ckpoint_dir);

  ParseConfigFile parser(modeldir);

  CostArgSet setup_cost_args;
  CostArgSet optimiser_cost_args;
  CostArg GlbCostArg;

  parser.parseConfig(xmlfilename, opt_arg, GlbCostArg, setup_cost_args,
    optimiser_cost_args, ModelArg);
  parser.loadData(setup_cost_args);
  parser.loadData(optimiser_cost_args);


/* EXEC */
  if(my_rank == 0)
    cout << modeldir + parser.Init_Param_File << endl;


  /* read param info file  and set paraminfo */
  FileReader &paraminfo_freader = FileReader::SetFileReader(TabDelimitedColumnParamInfo);
  paraminfo_freader.ClearSets();
  paraminfo_freader.Read(modeldir + parser.Init_Param_File);
  ParamInfo paraminfo = paraminfo_freader.paraminfo;

  /*creat setup for the first parameter sets */
  Setup &setup_param = Setup::SetupParam(opt_arg.getSetupType());

  ModelCostFunction &mcf_x2c = ModelCostFunction::SetModelCostFunction(X2Cost);
  ModelCostFunction &mcf_fft = ModelCostFunction::SetModelCostFunction(FFT);

  /* set CostFunction  */
  /* if there is no costfunctions set to the setup, use the same costfunctions with opt_arg */

  /* create the model */
  /* NOTE THIS CASE NO MODEL */
  /*
   *static cModel *model = getModel();
   *model->setModelArg(ModelArg);
   *model->inputModel();
   *
   *model->setTFinal(std::max(model->getTFinal(), setup_cost_args.maxtime()));
   *TimeSeries t_series;
   *t_series.SetModel(model);
  */

  MultiObjCost *cf = new MultiObjCost();

  if(!setup_cost_args.X2C_cost_arg.empty() || !setup_cost_args.FFT_cost_arg.empty() || !parser.UserCostFunctionNames.empty() )
    cf->Init();

  /*
   *if(!setup_cost_args.X2C_cost_arg.empty()) {
   * mcf_x2c.Init(model, &setup_cost_args.X2C_cost_arg, paraminfo.GetParamName());
   * if(!paraminfo.IsUnOptimiseParamNameEmpty()) 
   *     mcf_x2c.SetUnOptimiseParameters(paraminfo.GetUnOptimiseParamName(),paraminfo.GetUnOptimiseParamValue());
   * mcf_x2c.SetDataSets(&setup_cost_args.X2C_dataset);
   * mcf_x2c.SetGlbCostArg(GlbCostArg);
   * mcf_x2c.SetTimeSeries(&t_series);
   * mcf_x2c.SetSolver();
   * cf->AddObjective(mcf_x2c);
   * }
   * if(!setup_cost_args.FFT_cost_arg.empty()) {
   *   mcf_fft.Init(model, &setup_cost_args.FFT_cost_arg, paraminfo.GetParamName());
   * if(!paraminfo.IsUnOptimiseParamNameEmpty()) 
   *   mcf_fft.SetUnOptimiseParameters(paraminfo.GetUnOptimiseParamName(),paraminfo.GetUnOptimiseParamValue());
   *  mcf_fft.SetDataSets(&setup_cost_args.FFT_dataset);
   *  mcf_fft.SetGlbCostArg(GlbCostArg);
   *  mcf_fft.SetTimeSeries(&t_series);
   *  mcf_fft.SetSolver();
   *  cf->AddObjective(mcf_fft);
   * }
   */
  if(!parser.UserCostFunctionNames.empty()) {
   for(size_t i=0; i<parser.UserCostFunctionNames.size(); i++){
   UserCostFunction &ucf=UserCostFunction::setUserCostFunction(parser.UserCostFunctionNames.at(i));
    vector<string> pname;
    vector<string> allpname = paraminfo.GetParamName();
    for(int j=0; j<20; j++) pname.push_back(allpname.at(i*20+j)); 
    ucf.SetParamNames(pname);
    cf->AddObjective(ucf);
   }
  }

  if(!paraminfo.IsParamNameEmpty())  cf->SetParamNames(paraminfo.GetParamName());

  setup_param.Init(paraminfo, &opt_arg);

  /* Global CostArg to CostFunction */
    cf->SetGlbCostArg(GlbCostArg);

  if(opt_arg.getSetupType() == ReadinFile) {
    ostringstream paramreadinfile;
    if(opt_arg.getCkpointStartNum() == 0) {
      paramreadinfile << opt_arg.getCkpointDir() << "/" << parser.paramreadinfile;
    }
    else {
      paramreadinfile << opt_arg.getCkpointDir() << "/BestPop." << opt_arg.getCkpointStartNum() << ".ga";
    }
    setup_param.SetFileName(paramreadinfile.str());
  }

  setup_param.SetCostFunction(cf);
  setup_param.run();

  delete cf;
  cf = new MultiObjCost();
  cf->Init();

  /*optimiser */
  /*
   *model->setTFinal(std::max(model->getTFinal(), optimiser_cost_args.maxtime()));
   */

  vector<ParamSet> paramset = setup_param.paramset;

  Optimiser &myoptimiser = Optimiser::SetOptimiser(opt_arg.getOptimiserType());

  myoptimiser.Init(&opt_arg, paraminfo, &paramset);

  cf->SetGlbCostArg(GlbCostArg);

/*
 * if(!optimiser_cost_args.X2C_cost_arg.empty()) {
 *   mcf_x2c.Init(model, &optimiser_cost_args.X2C_cost_arg, paraminfo.GetParamName());
 *   if(!paraminfo.IsUnOptimiseParamNameEmpty()) 
 *        mcf_x2c.SetUnOptimiseParameters(paraminfo.GetUnOptimiseParamName(),paraminfo.GetUnOptimiseParamValue());
 *   mcf_x2c.SetDataSets(&optimiser_cost_args.X2C_dataset);
 *   mcf_x2c.SetGlbCostArg(GlbCostArg);
 *   mcf_x2c.SetTimeSeries(&t_series);
 *   mcf_x2c.SetSolver();
 *   cf->AddObjective(mcf_x2c);
 * } else if(!setup_cost_args.X2C_cost_arg.empty()) {
 *   mcf_x2c.Init(model, &setup_cost_args.X2C_cost_arg, paraminfo.GetParamName());
 *   if(!paraminfo.IsUnOptimiseParamNameEmpty()) 
 *        mcf_x2c.SetUnOptimiseParameters(paraminfo.GetUnOptimiseParamName(),paraminfo.GetUnOptimiseParamValue());
 *   mcf_x2c.SetDataSets(&setup_cost_args.X2C_dataset);
 *   mcf_x2c.SetGlbCostArg(GlbCostArg);
 *   mcf_x2c.SetTimeSeries(&t_series);
 *   mcf_x2c.SetSolver();
 *   cf->AddObjective(mcf_x2c);
 * }
 * if(!optimiser_cost_args.FFT_cost_arg.empty()) {
 *   mcf_fft.Init(model, &optimiser_cost_args.FFT_cost_arg, paraminfo.GetParamName());
 *   if(!paraminfo.IsUnOptimiseParamNameEmpty()) 
 *        mcf_fft.SetUnOptimiseParameters(paraminfo.GetUnOptimiseParamName(),paraminfo.GetUnOptimiseParamValue());
 *   mcf_fft.SetDataSets(&optimiser_cost_args.FFT_dataset);
 *   mcf_fft.SetGlbCostArg(GlbCostArg);
 *   mcf_fft.SetTimeSeries(&t_series);
 *   mcf_fft.SetSolver();
 *   cf->AddObjective(mcf_fft);
 * }else if(!setup_cost_args.FFT_cost_arg.empty()) {
 *   mcf_fft.Init(model, &setup_cost_args.FFT_cost_arg, paraminfo.GetParamName());
 *   if(!paraminfo.IsUnOptimiseParamNameEmpty()) 
 *        mcf_fft.SetUnOptimiseParameters(paraminfo.GetUnOptimiseParamName(),paraminfo.GetUnOptimiseParamValue());
 *   mcf_fft.SetDataSets(&setup_cost_args.FFT_dataset);
 *   mcf_fft.SetGlbCostArg(GlbCostArg);
 *   mcf_fft.SetTimeSeries(&t_series);
 *   mcf_fft.SetSolver();
 *   cf->AddObjective(mcf_fft);
 * }
 */
 if(!parser.UserCostFunctionNames.empty()) {
   for(size_t i=0; i<parser.UserCostFunctionNames.size(); i++){
   UserCostFunction &ucf=UserCostFunction::setUserCostFunction(parser.UserCostFunctionNames.at(i));
   vector<string> pname;
   vector<string> allpname = paraminfo.GetParamName();
   for(int j=0; j<10; j++) pname.push_back(allpname.at(i*10+j));
   ucf.SetParamNames(pname);
   cf->AddObjective(ucf);
   }
  }


  cf->SetParamNames(paraminfo.GetParamName());
  myoptimiser.SetCostFunction(cf);


  double opt_tstart = getnow();

  /* Optimiser Run */
  int iter = myoptimiser.Run();

  /* Finish Run */

  if(my_rank == 0) {

    double opt_elapsed = getnow() - opt_tstart;

    cout << "elapsed time for Optimiser " << opt_elapsed << "sec" << endl;

    int cNumGen = myoptimiser.optarg->getCkpointStartNum() + iter;

    string result_dir = myoptimiser.optarg->getResultDir();

    vector<double> b_param = myoptimiser.GetTheBest();

    CostArgSet cost_args;

    if(optimiser_cost_args.X2C_cost_arg.empty() && optimiser_cost_args.FFT_cost_arg.empty()) {
     cost_args=setup_cost_args;
    }else cost_args=optimiser_cost_args;

    if(!cost_args.X2C_dataset.empty()){
     mcf_x2c.SetDataSets(&cost_args.X2C_dataset);
    }else mcf_fft.SetDataSets(&cost_args.FFT_dataset);

    if(!cost_args.X2C_cost_arg.empty()){
      mcf_x2c.SetParameters(b_param);
    }else {
    if(!cost_args.FFT_cost_arg.empty())
      mcf_fft.SetParameters(b_param);
    }

    for(size_t i = 0; i < cost_args.X2C_dataset.size(); i++) {

      mcf_x2c.SetDataSet(i);

      string dataSetname=cost_args.X2C_dataset[i].GetSetName();

      dataSetname.resize(dataSetname.rfind("."));
#ifdef SBSI_TEST
      cout.precision(8);
      cout << "========================= Result for the dataset: " << dataSetname << 
              " =========================" << endl;
      cout << "\tCostValue is " << mcf_x2c.getcost() << endl;
#else
      ostringstream b_cost_fname;
      b_cost_fname << result_dir << "BestCost_" << dataSetname << ".txt." << cNumGen;
      writeBestCostResults( b_cost_fname.str(), mcf_x2c.getcost(), model);
     
      ostringstream t_series_fname;
      t_series_fname << parser.timeseries_dir <<  parser.timeseries_fname << "_" << dataSetname << "_" <<  cNumGen << ".dat";

      t_series.WriteToSBSIDataFormat(t_series_fname.str());
#endif
    }

    for(size_t i = 0; i < cost_args.FFT_dataset.size(); i++) {

      mcf_fft.SetDataSet(i);
		
      string dataSetname = cost_args.FFT_dataset[i].GetSetName();

      dataSetname.resize(dataSetname.rfind("."));
#ifdef SBSI_TEST
      cout << "========================= Result for the dataset: " << dataSetname << 
              " =========================" << endl;
      cout << "\tCostValue is " << mcf_fft.getcost() << endl;
#else
      ostringstream b_cost_fname;
      b_cost_fname << result_dir << "BestCost_FFT_" << dataSetname << ".txt." << cNumGen;
     
      writeBestCostResults( b_cost_fname.str(), mcf_fft.getcost(), model);

      ostringstream t_series_fname;
      t_series_fname << parser.timeseries_dir <<  "FFT_" << parser.timeseries_fname << "_" << dataSetname << "_" <<  cNumGen << ".dat";

      t_series.WriteToSBSIDataFormat(t_series_fname.str());
#endif
    }


  }

  int stat(0);
  MPI_Initialized(&stat);
  if(stat)
    MPI_Finalize();

  if(my_rank == 0)
  {
    double elapsed = getnow() - tstart;
    cout << "finish jobs; elapsed " << elapsed << "sec" << endl;

  }

  return 0;
}

void writeBestCostResults(  const string fname , const double cost,  cModel *model) {

	ofstream bfp(fname.c_str());

	if(!bfp) ERR.FileW("Result Out", __func__, fname.c_str());
	
	bfp.precision(8);
	
	bfp << "CostValue is " <<cost << endl;

        vector<string> paramNames = model->getParamNames();
	
	for(unsigned int p_i = 0; p_i < paramNames.size(); p_i++) 
        bfp << paramNames[p_i] << ":\t" << model->getParamByName(paramNames[p_i]) << ";\n" ;
	
	
}
