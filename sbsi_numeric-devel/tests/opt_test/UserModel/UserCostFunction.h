//#include <iostream>
//#include <fstream>
//#include <sstream>
//#include <string>
//#include <vector>
//#include <exception>
#include <cmath>
using namespace std;

#include<CostFunction.h>

#ifndef __UserCostFunction_h__
#define __UserCostFunction_h__

#define SHIFT 1
#define BASE_COST 0.001

#define PI M_PI



class UserCostFunction: public CostFunction
{
public:

    static UserCostFunction& setUserCostFunction(const string name);

};

class Rosenbrock: public UserCostFunction
{

public:

	Rosenbrock(){};

	virtual double GetCost() {
		vector<double> x=ParamValues;
		double xid,f(0);
		for(vector<double>::iterator d=x.begin();d!=x.end()-1;d++) {
			xid=1-*d;
			f+=xid*xid;
			xid= *d - *(d+1);
			f+=100*xid*xid;
		}
		return f;

	};

};

class Rastrigin: public UserCostFunction
{

public:

	Rastrigin() {};

	virtual double GetCost() {
		vector<double> x=ParamValues;
		double f=BASE_COST,k=10;
		for(vector<double>::iterator d=x.begin();d!=x.end();d++) {
			f+= *d * *d - k* cos(2*PI* *d);
		}
		f+=k*x.size();
		return f;
	};


};

class Griewank: public UserCostFunction
{

public:

	Griewank() {};

	virtual double GetCost() {
		vector<double> x=ParamValues;
		double f=BASE_COST;
                int count(1);
		for(vector<double>::iterator d=x.begin();d!=x.end();d++) {
                   f+=0.00025 * *d * *d ;
                   f-=cos(*d/sqrt((double)count));
                   count ++;
		}
		return f;

	};


};

class DD_sum: public UserCostFunction
{

public:

	DD_sum() {};

	virtual double GetCost() {
		vector<double> x=ParamValues;
		double f(0);
		for(vector<double>::iterator d=x.begin();d!=x.end();d++) {
			f += *d * *d;
		}
		return f;

	};


};

/*
 * Easom
 * Bounds [-100.0,100.0]
 * Global minimum 0 position (PI,PI,...,PI)
 */
class Easom: public UserCostFunction
{

public:

	Easom(){};


	virtual double GetCost() {
		vector<double> x=ParamValues;
		double f=1,xid=0;
		for(vector<double>::iterator d=x.begin();d!=x.end();d++) {
			f*=cos(*d);
			xid+=(*d-PI)*(*d-PI);
		}
		return 1-f*exp(-1*xid)+BASE_COST;

	};


};

#endif
