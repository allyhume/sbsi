#!/bin/sh

np=2 

mkdir -p UserModel/SA_results UserModel/SA_ckpoint UserModel/ckpoint UserModel/results
echo Test for abc Model with X2C costfuncion with GA using mpi np=$np
mpirun -np $np ./abcTest UserModel config_X2C.xml

echo 

echo Test for abc Model with X2C costfuncion with SA
./abcTest UserModel config_SA.xml

