
/*****************************************************************************
 *
 *  TestTDCFileHeaderReader.cpp
 *
 *  See TestTDCFileHeaderReader.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestTDCFileReader.h"

#define TOLERANCE DBL_EPSILON


void TestTDCFileReader::testTDCHeaderFileReader(void) {

        FileReader *hfr;
        DataSet dataset;
        hfr = new TDCHeaderFileReader();
        hfr->ClearSets();
        hfr->SetDataSet(&dataset);
        string filename="CppunitTestData/ABC.hdr";
        hfr->Read(filename);

        CPPUNIT_ASSERT(dataset.GetNumInitialStateValues() == 2);
        CPPUNIT_ASSERT(dataset.GetNumInitialStateNames() == 2);
        string EXPECTED="I1";
        CPPUNIT_ASSERT_EQUAL(EXPECTED,dataset.GetAInitialStateName(0));
        CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetAInitialStateValue(1),0.2,TOLERANCE);

        CPPUNIT_ASSERT(dataset.GetNumInitialParamValues() == 2);
        CPPUNIT_ASSERT(dataset.GetNumInitialParamNames() == 2);
        EXPECTED="P2";
        CPPUNIT_ASSERT_EQUAL(EXPECTED,dataset.GetAInitialParamName(1));
        CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetAInitialParamValue(0),0.15,TOLERANCE);

  return;
}

