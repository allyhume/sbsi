
/*****************************************************************************
 *
 *  TestcModel.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <cModel.h>
#include <cModelArg.h>

#ifndef TESTCMODEL_H
#define TESTCMODEL_H

class TestcModel : public CppUnit::TestFixture {

 public:

  TestcModel(){};
  virtual ~TestcModel(){};

  void setUp(void);
  void tearDown(void);
  void testinitModelArg();
  void testinputModel();
  void testgetRHS();
  void testcalVarVec();
  void testcalCoef();
  void testgetNumParameters();
  void testsetParameters();
  void testgetParameters();
  void testgetParamNames();
  void testsetParamByName();
  void testgetParamByName();
  void testgetParamIndexByName();
  void testgetParamNameByIndex();
  void testsetParamNameByIndex();
  void testgetNumStates();
  void testsetStates();
  void testgetStates();
  void testgetStateNames();
  void testsetStateByName();
  void testgetStateByName();
  void testgetStateNameByIndex();
  void testgetStateIndexByName();
  void testsetVars();
  void testgetVars();
  void testgetVarNames();
  void testsetVarByName();
  void testgetVarByName();
  void testgetVarIndexByName();
  void testgetNumVars();
  void testsetExpTInit();
  void testgetExpTInit();
  void testsetOutInterval();
  void testgetOutInterval();
  void testsetInterval();
  void testgetInterval();
  void testgetFinal();
  void testsetFinal();
  void testgetInit();
  void testsetInit();
  void testsetAtol();
  void testsetReltol();
  void testgetAtol();
  void testgetReltol();
  void testgetMaxTimes();
  void testsetMaxTimes();
  void testwriteResults();

  CPPUNIT_TEST_SUITE(TestcModel);
  CPPUNIT_TEST(testinitModelArg);
  CPPUNIT_TEST(testinputModel);
  CPPUNIT_TEST(testgetRHS);
  CPPUNIT_TEST(testcalVarVec);
  CPPUNIT_TEST(testcalCoef);
  CPPUNIT_TEST(testgetNumParameters);
  CPPUNIT_TEST(testsetParameters);
  CPPUNIT_TEST(testgetParameters);
  CPPUNIT_TEST(testgetParamNames);
  CPPUNIT_TEST(testsetParamByName);
  CPPUNIT_TEST(testgetParamByName);
  CPPUNIT_TEST(testgetParamIndexByName);
  CPPUNIT_TEST(testgetParamNameByIndex);
  CPPUNIT_TEST(testsetParamNameByIndex);
  CPPUNIT_TEST(testgetNumStates);
  CPPUNIT_TEST(testsetStates);
  CPPUNIT_TEST(testgetStates);
  CPPUNIT_TEST(testgetStateNames);
  CPPUNIT_TEST(testsetStateByName);
  CPPUNIT_TEST(testgetStateByName);
  CPPUNIT_TEST(testgetStateNameByIndex);
  CPPUNIT_TEST(testgetStateIndexByName);
  CPPUNIT_TEST(testsetVars);
  CPPUNIT_TEST(testgetVars);
  CPPUNIT_TEST(testgetVarNames);
  CPPUNIT_TEST(testsetVarByName);
  CPPUNIT_TEST(testgetVarByName);
  CPPUNIT_TEST(testgetVarIndexByName);
  CPPUNIT_TEST(testgetNumVars);
  CPPUNIT_TEST(testsetExpTInit);
  CPPUNIT_TEST(testgetExpTInit);
  CPPUNIT_TEST(testsetOutInterval);
  CPPUNIT_TEST(testgetOutInterval);
  CPPUNIT_TEST(testsetInterval);
  CPPUNIT_TEST(testgetInterval);
  CPPUNIT_TEST(testgetFinal);
  CPPUNIT_TEST(testsetFinal);
  CPPUNIT_TEST(testgetInit);
  CPPUNIT_TEST(testsetInit);
  CPPUNIT_TEST(testsetAtol);
  CPPUNIT_TEST(testsetReltol);
  CPPUNIT_TEST(testgetAtol);
  CPPUNIT_TEST(testgetReltol);
  CPPUNIT_TEST(testgetMaxTimes);
  CPPUNIT_TEST(testsetMaxTimes);
  CPPUNIT_TEST_SUITE_END();

  cModel *usermodel;
  cModelArg ModelArg;

};

#endif

