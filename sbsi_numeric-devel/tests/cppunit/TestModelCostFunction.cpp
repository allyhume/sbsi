
/*****************************************************************************
 *
 *  TestModelCostFunction.cpp
 *
 *  See TestModelCostFunction.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include <CostArg.h>
#include <cModel.h>
#include "TestModelCostFunction.h"
#include "CppunitTestData/UserModel/UserModel.h"

#define TOLERANCE DBL_EPSILON

using namespace std;


CPPUNIT_TEST_SUITE_REGISTRATION(TestModelCostFunction);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestModelCostFunction::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestModelCostFunction::tearDown(void) {

  return;
}

void TestModelCostFunction::testInit(){

 vector<CostArg> u_costargs;
 CostArg u_costarg;

 u_costarg.ExpT0 = 1.0; //childDouble(doc, d_node, "DataFileStartTime");
 u_costarg.OutputInterval = 0.1;//childDouble(doc, d_node, "Interval");
 u_costarg.scaletype = None;
 u_costarg.costvaluetype = Normed;

 u_costargs.push_back(costarg);

  cModel *UModel= new UserModel();
  cModelArg ModelArg;

  UModel->setModelArg(ModelArg);
  UModel->initModelArg();
  UModel->inputModel();

  vector<string> tmp=UModel->getParamNames();
  vector<string> pname;
  pname.push_back(tmp.at(0));
  pname.push_back(tmp.at(1));
  pname.push_back(tmp.at(2));

  Init(UModel, u_costargs, pname);
  CPPUNIT_ASSERT(Model == UModel);
  CPPUNIT_ASSERT(u_costargs.size() == costargs.size());
  CPPUNIT_ASSERT(pname.size() == ParamNames.size());

  u_costarg=GetACostArg(0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(u_costarg.ExpT0,costarg.ExpT0, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(GetDataStartTime(),u_costarg.ExpT0, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(GetOutputInterval(),u_costarg.OutputInterval, TOLERANCE);

  tmp = GetParamNames();
  CPPUNIT_ASSERT_EQUAL(pname.at(0), tmp.at(0));
  CPPUNIT_ASSERT_EQUAL(pname.back(), tmp.back());

}
void TestModelCostFunction::testSetModel(){

  cModel *UModel= new UserModel();
  cModelArg ModelArg;

  UModel->setModelArg(ModelArg);
  UModel->initModelArg();
  UModel->inputModel();

  SetModel(UModel);
  CPPUNIT_ASSERT(Model == UModel);

}
void TestModelCostFunction::testSetGlbCostArg(){

 CostArg glbcostarg;

 glbcostarg.MAXCOST = 50.0;
 glbcostarg.solvertype = CVODE;

 SetGlbCostArg(glbcostarg);

 CPPUNIT_ASSERT_DOUBLES_EQUAL(glbcostarg.MAXCOST, GlbCostArg.MAXCOST, TOLERANCE);
 CPPUNIT_ASSERT(glbcostarg.solvertype == GlbCostArg.solvertype);

}
void TestModelCostFunction::testSetTimeSeries(){

 TimeSeries ts;
 SetTimeSeries(&ts);
 CPPUNIT_ASSERT(&ts == TSeries);
}

void TestModelCostFunction::testSetSolver(){


 CostArg glbcostarg;

 glbcostarg.MAXCOST = 50.0;
 glbcostarg.solvertype = CVODE;

 SetGlbCostArg(glbcostarg);

 TimeSeries ts;
 SetTimeSeries(&ts);
 CPPUNIT_ASSERT(&ts == TSeries);

 cModel *UModel= new UserModel();
 cModelArg ModelArg;

 UModel->setModelArg(ModelArg);
 UModel->initModelArg();
 UModel->inputModel();

 SetModel(UModel);
 CPPUNIT_ASSERT(Model == UModel);

 SetSolver();
 CPPUNIT_ASSERT(solver != NULL);
 CPPUNIT_ASSERT(solver->t_series!= NULL);

 solver =NULL;

 Solver &sol=Solver::SetSolver(CVODE);
 SetSolver(&sol);
 CPPUNIT_ASSERT(solver != NULL);

}
void TestModelCostFunction::testSetModelCostFunction(){
 ModelCostFunction &mcf_x2c = SetModelCostFunction(X2Cost);
 CPPUNIT_ASSERT(&mcf_x2c != NULL);
 ModelCostFunction &mcf_fft = SetModelCostFunction(X2Cost);
 CPPUNIT_ASSERT(&mcf_fft != NULL);
 
}

void TestModelCostFunction::testSetParameters(){
  vector<CostArg> costargs;
  CostArg costarg;
 
  costarg.ExpT0 = 1.0; //childDouble(doc, d_node, "DataFileStartTime");
  costarg.OutputInterval = 0.1;//childDouble(doc, d_node, "Interval");
  costarg.scaletype = None;
  costarg.costvaluetype = Normed;

  costargs.push_back(costarg);

  cModel *UModel= new UserModel();
  cModelArg ModelArg;

  UModel->setModelArg(ModelArg);
  UModel->initModelArg();
  UModel->inputModel();

  vector<string> tmp=UModel->getParamNames();
  vector<string> pname;
  pname.push_back(tmp.at(0));
  pname.push_back(tmp.at(1));
  pname.push_back(tmp.at(2));

  Init(UModel, costargs, pname);

  vector<double> values;
  values.push_back(10);
  values.push_back(20);
  values.push_back(30);

  SetParameters(values);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName(pname.front()),values.front(),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName(pname.back()),values.back(),TOLERANCE);
}
void TestModelCostFunction::testSolverInit(){}
void TestModelCostFunction::testSolveRHS(){}
void TestModelCostFunction::testGetCosts(){
  Costs.push_back(10);
  Costs.push_back(20);
  vector<double> tmp = GetCosts();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Costs.front(),tmp.front(),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Costs.back(),tmp.back(),TOLERANCE);
}
void TestModelCostFunction::testGetResultVec(){

 cModel *UModel= new UserModel();
 cModelArg ModelArg;

 UModel->setModelArg(ModelArg);
 UModel->initModelArg();
 UModel->inputModel();

 SetModel(UModel);

 TimeSeries ts;
 SetTimeSeries(&ts);

 vector<vector<double> > res;
  res.resize(4);
  for(int i=0;i<10; i++){
      res.at(0).push_back((i+1)*0.01);
      res.at(1).push_back((i+1)*0.1);
      res.at(2).push_back((i+1)*1.0);
      res.at(3).push_back((i+1)*2.0);
  }
  ts.SetTimeSeries(&res);

  CPPUNIT_ASSERT(TSeries->IsTimeSeriesEmpty() != true);
  vector<double> tmp =GetResultVec(Model->getStateNameByIndex(0));
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.front(),res.at(1).front(),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.back(),res.at(1).back(),TOLERANCE);
}

