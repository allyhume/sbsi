/*****************************************************************************
 *
 *  TestOptArg.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <OptArg.h>

#ifndef TESTOptArg_H
#define TESTOptArg_H

class TestOptArg : public CppUnit::TestFixture, public OptArg {

 public:

  TestOptArg(){};
  virtual ~TestOptArg(){};

  void setUp(void);
  void tearDown(void);

  void testSobolSeed();
  void testOptimiserType();
  void testSetupType();
  void testSeed();
  void testNumGen();
  void testNumBest();
  void testPopSize();
  void testMaxNoChange();
  void testMinCost();
  void testMaxSimilarity();
  void testCkpointFreq();
  void testCkpointStartNum();
  void testPrintFreq();
  void testRestartFreq();
  void testMutProb();
  void testVerbose();
  void testPrintBestPop();
  void testResultDir();
  void testCkpointDir();
  void testMu();

  CPPUNIT_TEST_SUITE(TestOptArg);
  CPPUNIT_TEST_SUITE_END();

};

#endif

