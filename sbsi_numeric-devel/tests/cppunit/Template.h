/*****************************************************************************
 *
 *  TestXXX.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <XXX.h>

#ifndef TESTXXX_H
#define TESTXXX_H

class TestXXX : public CppUnit::TestFixture {

 public:

  TestXXX();
  virtual ~TestXXX();

  void setUp(void);
  void tearDown(void);


  CPPUNIT_TEST_SUITE(TestXXX);
  CPPUNIT_TEST_SUITE_END();

};

#endif

