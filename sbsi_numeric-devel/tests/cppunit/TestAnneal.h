
/*****************************************************************************
 *
 *  TestAnneal.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestAnnealCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <Anneal.h>

#ifndef TESTANNEAL_H
#define TESTANNEAL_H

class TestAnneal : public CppUnit::TestFixture {

 public:

  TestAnneal();
  virtual ~TestAnneal();

  void setUp(void);
  void tearDown(void);

  void testRun();
  void testCalcEnergy();
  void testStopCondition();
  void testPrintPop();

  CPPUNIT_TEST_SUITE(TestAnneal);
  CPPUNIT_TEST_SUITE_END();

};

#endif

