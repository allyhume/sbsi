/*****************************************************************************
 *
 *  TestDeCodeSBML.cpp
 *
 *  Unit tests for DeCodeSBML class.
 *
 *  $Id: TestDeCodeSBML.cpp,v 1.1.2.1 2010/05/20 14:36:25 ayamaguc Exp $
 *
 *  Millar Laboratory, Centre for Systems Biology at Edinburgh
 *  (c) The University of Edinburgh (2009)
 *  Kevin Stratford (kevin@epcc.ed.ac.uk)
 *
 *****************************************************************************/
using namespace std;
#include "TestDeCodeSBML.h"
#include <sbml/SBMLTypes.h>
#include <sbml/Constraint.h>
#include <sbml/FunctionDefinition.h>
#include <DeCodeSBML.h>
#include <cModelArg.h>
#include <cModel.h>
#include <SBMLModel.h>
#include <cfloat>

#define TOLLERANCE DBL_EPSILON

/*****************************************************************************
 *
 *  Constructor
 *
 *****************************************************************************/

TestDeCodeSBML::TestDeCodeSBML() {
}


TestDeCodeSBML::~TestDeCodeSBML() {
}



ASTNode * createInequality(void);

FunctionDefinition * createValidFunctionDefinition(void);
FunctionDefinition * createFunctionDefinitionWithNoMath(void);
Rule * createValidAssignmentRule(void);
Rule * createValidAlgebraicRule(void);
Rule * createValidRateRule(void);
void populateRule(Rule * rule);
bool outputContainsString(ostringstream &oss, string testString);
cModel * makeEmptyModel(void) ;
Event * createComplexInequalityMathForEvent(void);


const string VDERPOL_STANDARD("CppunitTestData/VderPol.xml");

const string BIOMD5("CppunitTestData/BIOMD0000000005.xml");

const string BIOMD55_FOR_EVENT_ASSIGNMENTS("CppunitTestData/BIOMD055sbsi.xml");

const string ABC1_MODEL("CppunitTestData/abc_1.xml");

CPPUNIT_TEST_SUITE_REGISTRATION(TestDeCodeSBML);


void TestDeCodeSBML::setUp(void) {

  return;
}


void TestDeCodeSBML::tearDown(void) {

  return;
}

void TestDeCodeSBML::testCheckParameter(void) {

        string filename = VDERPOL_STANDARD;
	SBMLDocument* document = readSBML(filename.c_str());

        Model* model = document->getModel();
        CPPUNIT_ASSERT(model != NULL);

        size_t PARAM_NUM = model->getNumParameters() + model->getNumCompartments();

	cModel * usermodel =  getParsedModel();
	DeCode(usermodel,filename);
	size_t size = usermodel->getNumParameters();
 	CPPUNIT_ASSERT(size == PARAM_NUM );

}

void TestDeCodeSBML::testCheckParametersNestedInKineticLaws(void) {
         string filename=BIOMD5;
	 readSBML(filename.c_str());
	 size_t EXPECTED_PARAM_NUM(11);
	 cModel * usermodel =  getParsedModel();
	 DeCode(usermodel,filename);
	 CPPUNIT_ASSERT(usermodel->getNumParameters() == EXPECTED_PARAM_NUM );
}

void TestDeCodeSBML::testSetConstraint(void) {

	Constraint *c=new Constraint(3,1);
	c->setId("constraint1");
	CPPUNIT_ASSERT(c->isSetMath() != true);
	
	// a > = 4 && a <= 10 
	ASTNode* andN = createInequality();
	
	CPPUNIT_ASSERT(2 == andN->getNumChildren());
	c->setMath(andN);
	CPPUNIT_ASSERT(c->isSetMath() == true);
	setConstraintIntoParamList(c);

	CPPUNIT_ASSERT_DOUBLES_EQUAL(4.0, getParamLowerByName("a", 5.0),DBL_EPSILON);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(2.0, getParamLowerByName("a", 2.0),DBL_EPSILON);
	
	CPPUNIT_ASSERT_DOUBLES_EQUAL(10.0, getParamUpperByName("a", 5.0),DBL_EPSILON);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(20.0, getParamUpperByName("a", 20.0),DBL_EPSILON);
	delete c;
}

void TestDeCodeSBML::testPrintFunctionDefinition(void) {
	FunctionDefinition* fd = createValidFunctionDefinition();
	ostringstream oss;
	
        printFunctionDefinition(1, fd, oss);
	CPPUNIT_ASSERT(outputContainsString(oss,"double fd1( double a)"));
	CPPUNIT_ASSERT(outputContainsString(oss,"4 + a"));
	delete fd;
	
}

void TestDeCodeSBML::testPrintInvalidFunctionDefinitionWithNoMathPrintsNothing(void) {
	FunctionDefinition* fd = createFunctionDefinitionWithNoMath();
	ostringstream oss;
	printFunctionDefinition(1, fd, oss);
        string EXPECTED="";
	CPPUNIT_ASSERT_EQUAL(EXPECTED , oss.str());
	delete fd;
}

void TestDeCodeSBML::testPrintAssignmentRule(void) {
    cModelArg ModelArg;
    cModel * usermodel;
    usermodel  = new SBMLModel();
    usermodel->setModelArg(ModelArg);
    setModel(usermodel);

    Rule* rule = createValidAssignmentRule();
	ostringstream oss;
	printRuleMath( rule, oss);
	CPPUNIT_ASSERT(outputContainsString(oss, "sp1=4 + a"));
	delete rule;
}

void TestDeCodeSBML::testPrintAlgebraicRule(void) {
    cModelArg ModelArg;
    cModel * usermodel;
    usermodel  = new SBMLModel();
    usermodel->setModelArg(ModelArg);
    setModel(usermodel);

    Rule* rule = createValidAlgebraicRule();
	CPPUNIT_ASSERT(rule->isAlgebraic());
	ostringstream oss;
	printRuleMath( rule, oss);
	CPPUNIT_ASSERT(outputContainsString(oss, "4 + a = 0"));
	delete rule;
}

void TestDeCodeSBML::testPrintRateRule(void) {
	Rule* rule = createValidRateRule();
	CPPUNIT_ASSERT(rule->isRate());
	ostringstream oss;
	
	cModelArg ModelArg;
	cModel * usermodel;
	usermodel  = new SBMLModel();
        usermodel->setModelArg(ModelArg);
 	usermodel->setStateByName("sp1",4.0);
	setModel(usermodel);
	printRuleMath( rule, oss);
}

void TestDeCodeSBML::testPrintReactionMathHandleError(void) {
	Reaction* r = new Reaction(3,1);
	r->setId("r1");
	ostringstream oss;
	printReactionMath( r, oss);
	CPPUNIT_ASSERT(outputContainsString(oss, "// No kinetic law defined"));
	delete r;
}

void TestDeCodeSBML::testPrintReactionMathHandleErrorIfKineticLawHasNoMath(void) {
	Reaction* r = new Reaction(3,1);
	r->setId("r1");
	KineticLaw* lk1 =new KineticLaw(3,1); // no mat h added
	r->setKineticLaw(lk1);
	ostringstream oss;
	printReactionMath( r, oss);
	CPPUNIT_ASSERT(outputContainsString(oss, "// No math defined"));
	delete r;
}

void TestDeCodeSBML::testPrintReactionMathUsingVderPolModel (void){
        string filename   = ABC1_MODEL;
        SBMLDocument* document = readSBML(filename.c_str());
        Model *m = document->getModel();
        Reaction* r = m->getReaction(0);
        ostringstream oss;

           cModel * usermodel = makeEmptyModel();

        for (size_t n = 0; n < m->getNumSpecies(); ++n) {
                Species *sp=m->getSpecies(n);
                string statename = sp->getId();
                double statevalue =(double) sp->getInitialConcentration();
                usermodel->setStateByName(statename,statevalue);
        }
        setModel(usermodel);
        printReactionMath( r, oss);

        CPPUNIT_ASSERT(outputContainsString(oss, "double Reac_1=V_flow_1_;"));
        delete document;
        delete usermodel;

}
void TestDeCodeSBML::testPrintEventMathUsingBioMD55 (void){
        string filename   = BIOMD55_FOR_EVENT_ASSIGNMENTS;
        SBMLDocument* document = readSBML(filename.c_str());
        Model *m = document->getModel();
        CPPUNIT_ASSERT(m != 0);
        Event* ev = m->getEvent(0);
        ostringstream oss;
        cModel * usermodel = makeEmptyModel();
        setModel(usermodel);
        printEventMath( ev, oss);

        //trigger
        CPPUNIT_ASSERT(outputContainsString(oss, "t <= num_cycles_e * cyclePeriod_e"));
        // event assigment
        CPPUNIT_ASSERT(outputContainsString(oss, "light = LightFunction(t, lightOffset, lightAmplitude, phase, photoPeriod_e, cyclePeriod_e);"));
        delete document;
        delete usermodel;
}

void TestDeCodeSBML::testhandleNoEventsPrintsNothing (void){
	
	Event* ev = new Event(3,1);
	ostringstream oss;

	cModel * usermodel = makeEmptyModel();
	setModel(usermodel);
        printEventMath( ev, oss);
	
	//trigger 
	CPPUNIT_ASSERT("" == oss.str());
	
	delete ev;
	delete usermodel;
}

cModel * makeEmptyModel(void) {
	cModelArg ModelArg;
	cModel * usermodel;
	usermodel  = new SBMLModel();
	usermodel->setModelArg(ModelArg);
	return usermodel;
}

bool outputContainsString(ostringstream &oss, string testString) {
       
	return (oss.str().find(testString)!= string::npos);
}

//creates algebraic rule: 'sp1 = 4 + a'
Rule * createValidAlgebraicRule(void) {
	AlgebraicRule * rule = new AlgebraicRule(3,1);
	populateRule(rule);
	return rule;
	
}

//creates assignment rule: 'sp1 = 4 + a'
Rule * createValidAssignmentRule(void) {
	AssignmentRule * pRule = new AssignmentRule(3,1);
	pRule->setVariable("sp1");
	populateRule(pRule);
	return pRule;
}

//creates rate rule: 'sp1 = 4 + a'
Rule * createValidRateRule(void) {
	RateRule * rule = new RateRule(3,1);
	rule->setVariable("sp1");
	populateRule(rule);
	return rule;	
}

void populateRule (Rule * rule) {
	// 4 + a
 	char * xml =(char *)"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> " 
	" <apply> "
	   "<plus/>"
	   " <cn>4.0</cn>  "
	   "<ci> a </ci>  "
	" </apply>  "
	"</math>";
	ASTNode * plus = readMathMLFromString(xml);
	rule->setMath(plus);
	
}
FunctionDefinition * createFunctionDefinitionWithNoMath (void) {
	FunctionDefinition* fd = new FunctionDefinition(3,1);
	fd->setId("fd1");
	fd->setName("name");
	return fd;
	
}

FunctionDefinition * createValidFunctionDefinition(void) {
	// math is ' 4 + a'
	FunctionDefinition* fd = new FunctionDefinition(3,1);
	fd->setId("fd1");
	fd->setName("name");
	ASTNode* lambda = new ASTNode();
	lambda->setType(AST_LAMBDA);
	ASTNode* bvar1 = new ASTNode();
	bvar1->setType(AST_NAME);
	bvar1->setName("a");
	ASTNode* plus = new ASTNode();
	plus->setType(AST_PLUS);
	ASTNode* c1 = new ASTNode();
	c1->setValue(4.0);
	ASTNode* c2 = new ASTNode();
	c2->setType(AST_NAME);
	c2->setName("a");
	plus->addChild(c1);
	plus->addChild(c2);
	lambda->addChild(bvar1);
	lambda->addChild(plus);
	fd->setMath(lambda);
	return fd;
}

ASTNode * createInequality(void) {
// a > 4 && a < 10
char * xml =(char *)"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> " 
	       " <apply> "
	         "<and/>"
			" <apply> "
		       "   <geq/>  "
		        "  <ci>a</ci>  "
				 " <cn>4.0</cn>  "
		      " </apply>  "
	        " <apply> "
	        "   <leq/>  "
	        "  <ci> a </ci>  "
	         " <cn> 10.0 </cn>  "
 	        " </apply>  "
			" </apply>  "
		    "</math>";
	ASTNode *n = readMathMLFromString(xml);
	Event * e = createComplexInequalityMathForEvent ();
	delete e;
	return n;
}

Event * createComplexInequalityMathForEvent (){
	
	// event where triger is 't > 2000 && trigger_value == 0'
	Event * evt = new Event(3,1);
	char * xml = (char *)"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> \
	<apply> \
	<and/> \
	<apply> \
	<geq/> \
	<csymbol encoding=\"text\" definitionURL=\"http://www.sbml.org/sbml/symbols/time\"> t </csymbol> \
	<cn type=\"integer\"> 2000 </cn> \
	</apply> \
	<apply> \
	<eq/> \
	<ci> trigger_value </ci> \
	<cn type=\"integer\"> 0 </cn> \
	</apply> \
	</apply> \
	</math> ";
	ASTNode * math = readMathMLFromString(xml);
	Trigger * trig= new Trigger(3,1);
	trig->setMath(math);
	evt->setTrigger(trig);
	
	EventAssignment * assign = new EventAssignment(3,1);
	assign->setVariable("trigger_value");
	char * nullxml = (char *)"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> </math>";
	
	assign->setMath(readMathMLFromString(nullxml));
	evt->addEventAssignment(assign);
	return evt;
	
}

// utility method for callin debug method
cModel * TestDeCodeSBML::getParsedModel() {
		  cModelArg ModelArg;
		  cModel *usermodel;
		  usermodel  = new SBMLModel();
		  usermodel->setModelArg(ModelArg);
		  return usermodel;

}







