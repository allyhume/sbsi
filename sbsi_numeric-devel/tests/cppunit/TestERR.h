
/*****************************************************************************
 *
 *  TestERR.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <ERR.h>

#ifndef TESTERR_H
#define TESTERR_H

class TestERR : public CppUnit::TestFixture {

 public:

  TestERR();
  virtual ~TestERR();

  void setUp(void);
  void tearDown(void);


  CPPUNIT_TEST_SUITE(TestERR);
  CPPUNIT_TEST_SUITE_END();

};

#endif

