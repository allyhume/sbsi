
/*****************************************************************************
 *
 *  TestMultiObjCost.cpp
 *
 *  See TestMultiObjCost.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestMultiObjCost.h"
#include "CppunitTestData/UserCostFunction.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestMultiObjCost);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestMultiObjCost::setUp(void) {

  mcf= new MultiObjCost();
  CPPUNIT_ASSERT(mcf !=NULL);

  ucf1=new DD_sum();
  CPPUNIT_ASSERT(ucf1 !=NULL);

  ucf2=new DD_product();
  CPPUNIT_ASSERT(ucf2 !=NULL);

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestMultiObjCost::tearDown(void) {
  mcf =NULL;
  ucf1 =NULL;
  ucf2 =NULL;
  return;
}

void TestMultiObjCost::testAddObjective(){
  int i=mcf->AddObjective(*ucf1);
  CPPUNIT_ASSERT(i == 0);
  i=mcf->AddObjective(*ucf2);
  CPPUNIT_ASSERT(i == 1);
}
void TestMultiObjCost::testSetObjectives(){
  vector<CostFunction*> mucf;
  mucf.push_back(ucf1);
  mucf.push_back(ucf2);
  int num=mcf->SetObjectives(mucf);
  CPPUNIT_ASSERT(num == 2);
}

void TestMultiObjCost::testGetNumObjectives(){
  vector<CostFunction*> mucf;
  mucf.push_back(ucf1);
  mucf.push_back(ucf2);
  int num=mcf->SetObjectives(mucf);
  int numobj=mcf->GetNumObjectives();
  CPPUNIT_ASSERT(num == numobj);
}

void TestMultiObjCost::testInit(){
  mcf->Init();
  size_t num=mcf->multiobjcost_val.size();
  CPPUNIT_ASSERT(num == 0);
}
void TestMultiObjCost::testSetParameters(){

  mcf->AddObjective(*ucf1);
  mcf->AddObjective(*ucf2);

  vector<double> paramA;
  for(int i=0;i<20;i++) paramA.push_back((double)i*0.01);

  mcf->SetParameters(paramA);

  vector<double> tmp0 = mcf->MCFun.at(0)->GetParameters();
  vector<double> tmp1 = mcf->MCFun.at(1)->GetParameters();

  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp0.front(),0,TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp1.back(),0.19,TOLERANCE);
  
  for(int i=0;i<20;i++) paramA.at(i)=double(i+1)*0.01;

  mcf->SetParameters(paramA,0);
  tmp0 = mcf->MCFun.at(0)->GetParameters();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp0.back(),0.20,TOLERANCE);

  for(int i=0;i<20;i++) paramA.at(i)+=double(i+1)*0.01;

  mcf->SetParameters(paramA,1);
  tmp1 = mcf->MCFun.at(1)->GetParameters();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.40,tmp1.back(),TOLERANCE);

}
void TestMultiObjCost::testSetParamNames(){

  mcf->AddObjective(*ucf1);
  mcf->AddObjective(*ucf2);

  vector<string> names;
  names.push_back("A");
  names.push_back("B");
  names.push_back("C");

  mcf->SetParamNames(names);
  string EXPECTED = "A";
  vector<string> tmp = mcf->GetParamNames();
  CPPUNIT_ASSERT_EQUAL(EXPECTED, tmp.front());
  EXPECTED = "C";
  CPPUNIT_ASSERT_EQUAL(EXPECTED, tmp.back());

  names.at(0)="D";
  names.at(1)="E";
  names.at(2)="F";

  mcf->SetParamNames(names,0);
  EXPECTED = "F";
  tmp = mcf->MCFun.at(0)->GetParamNames();
  CPPUNIT_ASSERT_EQUAL(EXPECTED, tmp.back());

  vector<string> namesA;
  namesA.push_back("P1");
  namesA.push_back("P2");
  namesA.push_back("P3");
  mcf->SetParamNames(namesA,1);
  tmp = mcf->MCFun.at(1)->GetParamNames();
  EXPECTED = "P1";
  CPPUNIT_ASSERT_EQUAL(EXPECTED, tmp.front());
}
void TestMultiObjCost::testGetParamNames(){
  mcf->AddObjective(*ucf1);
  mcf->AddObjective(*ucf2);

  vector<string> names;
  names.push_back("A");
  names.push_back("B");
  names.push_back("C");

  mcf->SetParamNames(names);
 
  vector<string> tmp;
  tmp=mcf->GetParamNames();
  CPPUNIT_ASSERT_EQUAL(names.front(), tmp.front());

  vector<string> tmp0;
  tmp0=mcf->GetParamNames(1);
  CPPUNIT_ASSERT_EQUAL(tmp0.back(),names.back());

}
void TestMultiObjCost::testGetCost(){
  mcf->AddObjective(*ucf1);
  mcf->AddObjective(*ucf2);
  vector<double> paramA;
  for(int i=1;i<5;i++) paramA.push_back((double)i*0.01);
  mcf->SetParameters(paramA);
  double cost=mcf->GetCost(); // 
  CPPUNIT_ASSERT_DOUBLES_EQUAL(cost,0.10100024,TOLERANCE);
  vector<double> costs;
  costs=mcf->GetCosts();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(costs.back(),2.4e-07,TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(costs.front(),0.101,TOLERANCE);
}
