
/*****************************************************************************
 *
 *  TestSetup.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <Setup.h>

#ifndef TESTSetup_H
#define TESTSetup_H

class TestSetup : public CppUnit::TestFixture, public Setup {

 public:

  TestSetup(){};
  virtual ~TestSetup(){};

  void run(){};

  void setUp(void);
  void tearDown(void);

  void testInit();
  void testSetupParam();
  void testSetFileName();
  void testSetCostFunction();
  void testWriteStart();

  CPPUNIT_TEST_SUITE(TestSetup);
  CPPUNIT_TEST(testInit);
  CPPUNIT_TEST(testSetupParam);
  CPPUNIT_TEST(testSetFileName);
  CPPUNIT_TEST(testSetCostFunction);
  CPPUNIT_TEST(testWriteStart);
  CPPUNIT_TEST_SUITE_END();

};

#endif

