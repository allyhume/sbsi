
/*****************************************************************************
 *
 *  TestAdditiveMultiObjCos.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <AdditiveMultiObjCos.h>

#ifndef TESTADDITIVEMULTIOBJCOS_H
#define TESTADDITIVEMULTIOBJCOS_H

class TestAdditiveMultiObjCos : public CppUnit::TestFixture {

 public:

  TestAdditiveMultiObjCos();
  virtual ~TestAdditiveMultiObjCos();

  void setUp(void);
  void tearDown(void);


  CPPUNIT_TEST_SUITE(TestAdditiveMultiObjCos);
  CPPUNIT_TEST_SUITE_END();

};

#endif

