/*****************************************************************************
 *
 *  TestSobol64Bit.cpp
 *
 *  See TestSobol64Bit.h for a description.
 *
 *  $Id: TestSobol64Bit.cpp,v 1.1.2.1 2010/05/20 14:36:25 ayamaguc Exp $
 *
 *  Millar Laboratory, Centre for Systems Biology at Edinburgh
 *  (c) The University of Edinburgh (2009)
 *  Kevin Stratford (kevin@epcc.ed.ac.uk) 
 *
 *****************************************************************************/

#include <float.h>
#include "TestSobol64Bit.h"

#define TOLERANCE DBL_EPSILON

/*****************************************************************************
 *
 *  Constructor
 *
 *****************************************************************************/

TestSobol64Bit::TestSobol64Bit() {
}

/*****************************************************************************
 *
 *  Destructor
 *
 *****************************************************************************/

TestSobol64Bit::~TestSobol64Bit() {
}

/*****************************************************************************
 *
 *  Register the tests
 *
 *****************************************************************************/

CPPUNIT_TEST_SUITE_REGISTRATION(TestSobol64Bit);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestSobol64Bit::setUp(void) {

  CPPUNIT_ASSERT_MESSAGE("Sobol64Bit sizeof(long long int)",
			 sizeof(long long int) >= 8);
  CPPUNIT_ASSERT_MESSAGE("Sobol64Bit DIM_MAX", SOBOL_DIM_MAX == 1111);
  CPPUNIT_ASSERT_MESSAGE("Sobol64Bit LOG_MAX", SOBOL_LOG_MAX == 62);

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestSobol64Bit::tearDown(void) {

  return;
}

/*****************************************************************************
 *
 *   testLowZeroBit
 *
 *   Some test values are:
 *
 *   value      in binary     position of rightmost zero bit
 *   -----      ---------     ------------------------------
 *      0               0     1
 *      1               1     2
 *      2              10     1
 *      3              11     3
 *
 *     14            1110     1
 *     15            1111     5
 *     16           10000     1
 *     17           10001     2
 *
 *   1023      1111111111    11
 *   1024      1000000000     1
 *   1025      1000000001     2
 *
 *****************************************************************************/

void TestSobol64Bit::testLowZeroBit(void) {

  Sobol64Bit * test = new Sobol64Bit(2);
  long long int ivalue, lowbit;

  ivalue = 0;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 1);

  ivalue = 1;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 2);

  ivalue = 2;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 1);

  ivalue = 3;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 3);

  ivalue = 14;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 1);

  ivalue = 15;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 5);

  ivalue = 16;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 1);

  ivalue = 17;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 2);

  ivalue = 1023;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 11);

  ivalue = 1024;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 1);

  ivalue = 1025;
  lowbit = test->lowZeroBit(ivalue);
  CPPUNIT_ASSERT(lowbit == 2);

  delete test;

  return;
}

/*****************************************************************************
 *
 *  testDimension2
 *
 *****************************************************************************/

void TestSobol64Bit::testDimension2(void) {

  double sequence[2];

  Sobol64Bit * test = new Sobol64Bit(2);

  /* Start at seed = 0 */

  test->setSeed(0);
  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.000000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.000000, sequence[1], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.500000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.500000, sequence[1], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.750000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.250000, sequence[1], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.2500000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.7500000, sequence[1], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.3750000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.3750000, sequence[1], TOLERANCE);

  /* Now skip up to seed = 100 and test a few more */

  test->setSeed(100);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.4140625, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.2578125, sequence[1], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.9140625, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.7578125, sequence[1], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.6640625, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0078125, sequence[1], TOLERANCE);

  /* Jump back a few places to seed = 95 */

  test->setSeed(95);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0546875, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.9296875, sequence[1], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0390625, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.1328125, sequence[1], TOLERANCE);


  delete test;

  return;
}

/*****************************************************************************
 *
 *  testDimension3
 *
 *****************************************************************************/

void TestSobol64Bit::testDimension3(void) {

  Sobol64Bit * test = new Sobol64Bit(3);
  double sequence[3];

  test->setSeed(0);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0000000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0000000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0000000, sequence[2], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.5000000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.5000000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.5000000, sequence[2], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.7500000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.2500000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.7500000, sequence[2], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.2500000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.7500000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.2500000, sequence[2], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.3750000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.3750000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.6250000, sequence[2], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.8750000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.8750000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.1250000, sequence[2], TOLERANCE);

  /* Jump forward to seed = 100 */

  test->setSeed(100);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.4140625, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.2578125, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.3046875, sequence[2], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.9140625, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.7578125, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.8046875, sequence[2], TOLERANCE);

  delete test;

  return;
}

/*****************************************************************************
 *
 *  testDimension500
 *
 *  Here, we only test the first two, and the last two values returned.
 *
 *****************************************************************************/

void TestSobol64Bit::testDimension500(void) {

  Sobol64Bit * test = new Sobol64Bit(500);
  double sequence[500];

  test->setSeed(0);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0000000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0000000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0000000, sequence[498], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0000000, sequence[499], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.5000000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.5000000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.5000000, sequence[498], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.5000000, sequence[499], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.7500000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.2500000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.2500000, sequence[498], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.7500000, sequence[499], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.2500000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.7500000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.7500000, sequence[498], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.2500000, sequence[499], TOLERANCE);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.3750000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.3750000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.6250000, sequence[498], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.8750000, sequence[499], TOLERANCE);

  /* Skip forward to seed = 10000 */

  test->setSeed(10000);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.09832763671875000, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.39019775390625000, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.53094482421875000, sequence[498], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.35858154296875000, sequence[499], TOLERANCE);

  /* Skip forward to seed = 100000 */
  /* Note the last 3 digits here are beyond 64 bit range to check */

  test->setSeed(100000);

  test->reap(sequence);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.061073303222656250, sequence[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.107582092285156250, sequence[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.967597961425781250, sequence[498], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.142341613769531250, sequence[499], TOLERANCE);

  delete test;

  return;
}
