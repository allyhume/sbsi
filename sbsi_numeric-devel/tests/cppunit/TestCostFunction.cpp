
/*****************************************************************************
 *
 *  TestCostFunction.cpp
 *
 *  See TestCostFunction.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include <SingleObjCost.h>
#include <MultiObjCost.h>
#include "TestCostFunction.h"
#include "CppunitTestData/UserCostFunction.h"

#define TOLERANCE DBL_EPSILON

CPPUNIT_TEST_SUITE_REGISTRATION(TestCostFunction);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestCostFunction::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestCostFunction::tearDown(void) {
  return;
}

void TestCostFunction::testSetCostFunction(){
 CostFunctionType type;

 type=Multi;
 CostFunction &mcf=CostFunction::SetCostFunction(type);
 CPPUNIT_ASSERT(&mcf !=NULL);

 type=Single;
 CostFunction &scf=CostFunction::SetCostFunction(type);
 CPPUNIT_ASSERT(&scf !=NULL);
 
 CostFunction *ucf=new Easom();
 CPPUNIT_ASSERT(ucf !=NULL);
 
}

void TestCostFunction::testSetParameters(){
  vector<double> param;
  for(int i=0;i<20;i++) param.push_back((double)i*0.01);

  CostFunction *cfun= new Rosenbrock();
  cfun->SetParameters(param);
  vector<double> tmp = cfun->GetParameters();

  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.front(),0,TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.back(),0.19,TOLERANCE);

};

void TestCostFunction::testSetParamNames(){
  vector<string> names;
  names.push_back("A");
  names.push_back("B");
  names.push_back("C");
  CostFunction *cfun = new Rastrigin();

  cfun->SetParamNames(names);

  vector<string> tmp= cfun->GetParamNames();
  CPPUNIT_ASSERT_EQUAL(names.front(),tmp.front());
  CPPUNIT_ASSERT_EQUAL(names.back(),tmp.back());
};

void TestCostFunction::testGetParamNames(){
  vector<string> names;
  names.push_back("A");
  names.push_back("B");
  names.push_back("C");
  CostFunction *cfun = new Rastrigin();
  cfun->SetParamNames(names);
  vector<string> rtn=cfun->GetParamNames();
  CPPUNIT_ASSERT_EQUAL(rtn.front(),names.front());
  CPPUNIT_ASSERT_EQUAL(rtn.back(),names.back());
};

void TestCostFunction::testGetCost(){
  vector<double> param;
  for(int i=0;i<20;i++) param.push_back((double)i*0.01);

  CostFunction *cfun= new DD_sum();
  cfun->SetParameters(param);
  double cost=cfun->GetCost();

  CPPUNIT_ASSERT_DOUBLES_EQUAL(cost,1.901,TOLERANCE);
};

void TestCostFunction::testGlbCostArg(){

 CostArg GlbCostArg;
 GlbCostArg.solvertype=CVODE;
 GlbCostArg.MAXCOST=50.0;
 GlbCostArg.MAX_RandomNumGen=5;

  CostFunction *cfun = new Rosenbrock();
  cfun->SetGlbCostArg(GlbCostArg);
  CPPUNIT_ASSERT(cfun->GetMAXNumRandomGen() == 5);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(cfun->GetMAXCost(),GlbCostArg.MAXCOST,TOLERANCE);
  CPPUNIT_ASSERT(cfun->GetSolverType() == GlbCostArg.solvertype);

};
