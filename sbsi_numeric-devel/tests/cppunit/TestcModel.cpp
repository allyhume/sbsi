
/*****************************************************************************
 *
 *  TestcModel.cpp
 *
 *  See TestcModel.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include <cmath>
#include "TestcModel.h"

#include "CppunitTestData/UserModel/UserModel.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestcModel);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestcModel::setUp(void) {
  usermodel= new UserModel();
  usermodel->setModelArg(ModelArg);
  usermodel->initModelArg();
  usermodel->inputModel();
  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestcModel::tearDown(void) {
  delete usermodel;
  return;
}
void TestcModel::testinitModelArg(){
   usermodel->initModelArg();
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getFinal(), TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getInit(), TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getInterval(), TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getOutInterval(), TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getExpTInit(), TOLERANCE);
   CPPUNIT_ASSERT(0 == usermodel->getNumStates());
   CPPUNIT_ASSERT(0 == usermodel->getNumParameters());
   CPPUNIT_ASSERT(0 == usermodel->getNumVars());
}

void TestcModel::testinputModel(){
   usermodel->initModelArg();
   usermodel->inputModel();
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getStateByName("A"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getStateByName("B"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getStateByName("C"),TOLERANCE);
   
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getVarByName("V_flow_1_"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getVarByName("V_flow_2_"),TOLERANCE);
   
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getParamByName("P_flow_1_"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getParamByName("P_flow_2_"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getParamByName("k1"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getParamByName("k2"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getParamByName("k_1"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getParamByName("Default"),TOLERANCE);
   
}
void TestcModel::testgetRHS(){
   vector<double> y_in=usermodel->getStates();
   vector<double> params=usermodel->getParameters();
   vector<double> y_out(usermodel->getNumStates());
   double t(1.0);

   usermodel->getRHS( &y_in[0], t,  &y_out[0]);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(-1.0, y_out.at(0),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, y_out.at(1),TOLERANCE);
}

void TestcModel::testcalVarVec(){
//    double out=usermodel->calVarVec(0);
//    CPPUNIT_ASSERT_DOUBLES_EQUAL(out, 1.0, TOLERANCE);
//    CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getVarByName("V_flow_1_"),TOLERANCE);
//    CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getVarByName("V_flow_2_"),TOLERANCE);
}
void TestcModel::testcalCoef(){
    usermodel->calCoef();
    CPPUNIT_ASSERT_DOUBLES_EQUAL(exp(1), usermodel->getParamByName("exp_k2"),TOLERANCE);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(2.0, usermodel->getParamByName("k1_p_k2"),TOLERANCE);
}

void TestcModel::testgetNumParameters(){
    CPPUNIT_ASSERT(usermodel->getNumParameters()==8);
}

void TestcModel::testsetParameters(){
   vector<double> tmp(usermodel->getNumParameters(),20.0);
   usermodel->setParameters(tmp);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(20.0, usermodel->getParamByName("k1"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(20.0, usermodel->getParamByName("P_flow_1_"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(20.0, usermodel->getParamByName("Default"),TOLERANCE);
   

}
void TestcModel::testgetParameters(){
   vector<double> tmp;
   tmp=usermodel->getParameters();
   CPPUNIT_ASSERT(tmp.size() == 8);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, tmp.at(0),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, tmp.at(1),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, tmp.at(2),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, tmp.at(3),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, tmp.at(4),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, tmp.at(5),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, tmp.at(6),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, tmp.at(7),TOLERANCE);
}
void TestcModel::testgetParamNames(){
   vector<string> tmp;
   string EXPECTED;
   tmp=usermodel->getParamNames();
   for(size_t i=0; i<tmp.size();i++){
   EXPECTED=tmp.at(i);
   CPPUNIT_ASSERT_EQUAL(EXPECTED,usermodel->getParamNameByIndex(i));
   }
}
void TestcModel::testsetParamByName(){
   /* test push_back*/
   size_t before= usermodel->getNumParameters();
   usermodel->setParamByName("XYZ",1.0);
   size_t after= usermodel->getNumParameters();
   CPPUNIT_ASSERT(after == before +1);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getParamByName("XYZ"),TOLERANCE);
  /* change value */
   usermodel->setParamByName("XYZ",10.0);
   size_t after_0= usermodel->getNumParameters();
   CPPUNIT_ASSERT(after_0 == before +1);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(10.0, usermodel->getParamByName("XYZ"),TOLERANCE);
}
void TestcModel::testgetParamByName(){
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getParamByName("P_flow_1_"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, usermodel->getParamByName("P_flow_2_"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getParamByName("k1"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getParamByName("k2"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getParamByName("k_1"),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getParamByName("Default"),TOLERANCE);
}
void TestcModel::testgetParamIndexByName(){
   CPPUNIT_ASSERT(usermodel->getParamIndexByName("k1")==0);
   CPPUNIT_ASSERT(usermodel->getParamIndexByName("k2")==1);
   CPPUNIT_ASSERT(usermodel->getParamIndexByName("k_1")==2);
   CPPUNIT_ASSERT(usermodel->getParamIndexByName("P_flow_1_")==3);
   CPPUNIT_ASSERT(usermodel->getParamIndexByName("P_flow_2_")==4);
   CPPUNIT_ASSERT(usermodel->getParamIndexByName("Default")==5);
}
void TestcModel::testgetParamNameByIndex(){
   string EXPECTED;
   EXPECTED="k2";
   CPPUNIT_ASSERT_EQUAL(EXPECTED,usermodel->getParamNameByIndex(1));
   EXPECTED="Default";
   CPPUNIT_ASSERT_EQUAL(EXPECTED,usermodel->getParamNameByIndex(5));

}
void TestcModel::testsetParamNameByIndex(){
   string EXPECTED="XYZ";
   size_t tmp(0);
   usermodel->setParamNameByIndex(tmp,EXPECTED);
   CPPUNIT_ASSERT_EQUAL(EXPECTED,usermodel->getParamNameByIndex(0));
   CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, usermodel->getParamByName("XYZ"),TOLERANCE);
}

void TestcModel::testgetNumStates(){
   CPPUNIT_ASSERT(usermodel->getNumStates()==3);
}
void TestcModel::testsetStates(){
   vector<double> tmp(usermodel->getNumStates(),20.0);
   usermodel->setStates(tmp);
   CPPUNIT_ASSERT(usermodel->getNumStates()==3);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getStateByName("A"),20.0,TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getStateByName("B"),20.0,TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getStateByName("C"),20.0,TOLERANCE);
}
void TestcModel::testgetStates(){
   vector<double> tmp;
   tmp=usermodel->getStates();
   CPPUNIT_ASSERT(tmp.size()==3);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getStateByName("A"),tmp.at(0),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getStateByName("B"),tmp.at(1),TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getStateByName("C"),tmp.at(2),TOLERANCE);
}
void TestcModel::testgetStateNames(){
   vector<string> tmp;
   tmp=usermodel->getStateNames();
   CPPUNIT_ASSERT(tmp.size()==3);
   CPPUNIT_ASSERT(usermodel->getStateIndexByName(tmp.at(0))==0);
   CPPUNIT_ASSERT(usermodel->getStateIndexByName(tmp.at(1))==1);
   CPPUNIT_ASSERT(usermodel->getStateIndexByName(tmp.at(2))==2);
}
void TestcModel::testsetStateByName(){
  size_t before=usermodel->getNumStates();
  /* test pushback*/
   string EXPECTED="XYZ";
   usermodel->setStateByName(EXPECTED,2.0);
    size_t after=usermodel->getNumStates();
    CPPUNIT_ASSERT(after == before +1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(2.0, usermodel->getStateByName("XYZ"),TOLERANCE);
  /* change value */
   usermodel->setStateByName("XYZ",10.0);
   size_t after_0= usermodel->getNumStates();
   CPPUNIT_ASSERT(after_0 == before +1);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(10.0, usermodel->getStateByName("XYZ"),TOLERANCE);
}

void TestcModel::testgetStateByName(){
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getStateByName("A"),1.0,TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getStateByName("B"),0.0,TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getStateByName("C"),0.0,TOLERANCE);
}

void TestcModel::testgetStateNameByIndex(){
   vector<string> tmp;
   tmp=usermodel->getStateNames();
   string EXPECTED;
   for(size_t i=0; i<tmp.size();++i){
   EXPECTED=tmp.at(i);
   CPPUNIT_ASSERT_EQUAL(EXPECTED,usermodel->getStateNameByIndex(i));
   }
}
void TestcModel::testgetStateIndexByName(){
   vector<string> tmp;
   tmp=usermodel->getStateNames();
   for(size_t i=0; i<tmp.size();i++) CPPUNIT_ASSERT(usermodel->getStateIndexByName(tmp.at(i))== i);
}
void TestcModel::testgetNumVars(){
   CPPUNIT_ASSERT(usermodel->getNumVars()==2);
}
void TestcModel::testsetVars(){
  vector<double> tmp(usermodel->getNumVars(),2.0);
  usermodel->setVars(tmp);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getVarByName("V_flow_1_"),2.0,TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getVarByName("V_flow_2_"),2.0,TOLERANCE);
}
void TestcModel::testgetVars(){
  vector<double> tmp; 
  tmp=usermodel->getVars();
  CPPUNIT_ASSERT(tmp.size()==2);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getVarByName("V_flow_1_"),tmp.at(0),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getVarByName("V_flow_2_"),tmp.at(1),TOLERANCE);
}
void TestcModel::testgetVarNames(){
   vector<string> tmp;
   tmp=usermodel->getVarNames();
   CPPUNIT_ASSERT(tmp.size()==2);
   CPPUNIT_ASSERT("V_flow_1_"==tmp.at(0));
   CPPUNIT_ASSERT("V_flow_2_"==tmp.at(1));
}
void TestcModel::testsetVarByName(){
  size_t before=usermodel->getNumVars();
  /* test pushback*/
   string EXPECTED="XYZ";
   usermodel->setVarByName(EXPECTED,2.0);
    size_t after=usermodel->getNumVars();
    CPPUNIT_ASSERT(after == before +1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(2.0, usermodel->getVarByName("XYZ"),TOLERANCE);
  /* change value */
   usermodel->setVarByName("XYZ",10.0);
   size_t after_0= usermodel->getNumVars();
   CPPUNIT_ASSERT(after_0 == before +1);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(10.0, usermodel->getVarByName("XYZ"),TOLERANCE);
}
void TestcModel::testgetVarByName(){
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getVarByName("V_flow_1_"),0.0,TOLERANCE);
   CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getVarByName("V_flow_2_"),0.0,TOLERANCE);
}
void TestcModel::testgetVarIndexByName(){
   vector<string> tmp;
   tmp=usermodel->getVarNames();
   string EXPECTED;
   for(size_t i=0; i<tmp.size();i++){
   EXPECTED=tmp.at(i);
   CPPUNIT_ASSERT(i==usermodel->getVarIndexByName(EXPECTED));
   }
}

void TestcModel::testgetExpTInit(){
  usermodel->initModelArg();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getExpTInit(),0.0,TOLERANCE);
}

void TestcModel::testsetExpTInit(){
  usermodel->setExpTInit(1.0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getExpTInit(),1.0,TOLERANCE);
}

void TestcModel::testgetOutInterval(){
  usermodel->initModelArg();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getOutInterval(),0.0,TOLERANCE);
}
void TestcModel::testsetOutInterval(){
  usermodel->setOutInterval(1.0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getOutInterval(),1.0,TOLERANCE);
}
void TestcModel::testgetInterval(){
  usermodel->initModelArg();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getInterval(),0.0,TOLERANCE);
}
void TestcModel::testsetInterval(){
  usermodel->setInterval(1.0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getInterval(),1.0,TOLERANCE);
}
void TestcModel::testgetFinal(){
  usermodel->initModelArg();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getFinal(),0.0,TOLERANCE);
}
void TestcModel::testsetFinal(){
  usermodel->setFinal(1.0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getFinal(),1.0,TOLERANCE);
}
void TestcModel::testgetInit(){
  usermodel->initModelArg();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getInit(),0.0,TOLERANCE);
}
void TestcModel::testsetInit(){
  usermodel->setInit(1.0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getInit(),1.0,TOLERANCE);
}
void TestcModel::testgetAtol(){
  usermodel->initModelArg();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getAtol(),1.0e-14,TOLERANCE);
}
void TestcModel::testsetAtol(){
  usermodel->setAtol(1.0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getAtol(),1.0,TOLERANCE);
}
void TestcModel::testgetReltol(){
  usermodel->initModelArg();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getReltol(),1.0e-04,TOLERANCE);
}
void TestcModel::testsetReltol(){
  usermodel->setReltol(1.0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getReltol(),1.0,TOLERANCE);

}
void TestcModel::testgetMaxTimes(){
  usermodel->initModelArg();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(usermodel->getMaxTimes(),0.0,TOLERANCE);
}
void TestcModel::testsetMaxTimes(){
  usermodel->setMaxTimes(10);
  CPPUNIT_ASSERT(usermodel->getMaxTimes()==10);
}
