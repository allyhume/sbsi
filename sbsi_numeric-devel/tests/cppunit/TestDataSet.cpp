/*****************************************************************************
 *
 *  TestDataSet.cpp
 *
 *  See TestDataSet.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestDataSet.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestDataSet);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestDataSet::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestDataSet::tearDown(void) {

  return;
}

void TestDataSet::testDataSet(){
    DataSet dataset;
    CPPUNIT_ASSERT(dataset.IsSetNameEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsDataNamesEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsInitialStateNamesEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsInitialStateValuesEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsInitialParamNamesEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsInitialParamValuesEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsDataValuesEmpty() == true);
}
void TestDataSet::testSetName(){
    DataSet dataset;
    string Name="XXX";
    dataset.SetSetName(Name);
    CPPUNIT_ASSERT(dataset.IsSetNameEmpty() != true);
    CPPUNIT_ASSERT(dataset.IsDataNamesEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsInitialStateNamesEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsInitialStateValuesEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsInitialParamNamesEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsInitialParamValuesEmpty() == true);
    CPPUNIT_ASSERT(dataset.IsDataValuesEmpty() == true);
    CPPUNIT_ASSERT_EQUAL(Name, dataset.GetSetName());
}
void TestDataSet::testDataNames(){
    DataSet dataset;
    CPPUNIT_ASSERT(dataset.IsDataNamesEmpty() == true);
    CPPUNIT_ASSERT(dataset.GetNumDataNames() == 0);
    string time = "TIME";
    string d1 = "Var1";
    string d2 = "State1";
    string d3 = "State2";
    dataset.AddADataName(time);
    dataset.AddADataName(d1);
    dataset.AddADataName(d2);
    dataset.AddADataName(d3);
    CPPUNIT_ASSERT(dataset.IsDataNamesEmpty() != true);
    CPPUNIT_ASSERT(dataset.GetNumDataNames() ==4);
    vector<string> tmp = dataset.GetDataNames();
    CPPUNIT_ASSERT(tmp.size() == dataset.GetNumDataNames());
    CPPUNIT_ASSERT_EQUAL(time, tmp.front() );
    CPPUNIT_ASSERT_EQUAL(d3, tmp.back() );
    CPPUNIT_ASSERT_EQUAL(dataset.GetADataName(0), tmp.front() );
    CPPUNIT_ASSERT_EQUAL(dataset.GetADataName(3), tmp.back() );
    string d4 = "State0";
    dataset.SetADataName(1,d4);
    CPPUNIT_ASSERT_EQUAL(dataset.GetADataName(1), d4);
}
void TestDataSet::testInitialStateNames(){
    DataSet dataset;
    CPPUNIT_ASSERT(dataset.IsInitialStateNamesEmpty() == true);
    CPPUNIT_ASSERT(dataset.GetNumInitialStateNames() == 0);
    string s1="State1";
    string s2="State2";
    string s3="State3";
    dataset.AddAInitialStateName(s1);
    dataset.AddAInitialStateName(s2);
    dataset.AddAInitialStateName(s3);
    CPPUNIT_ASSERT(dataset.IsInitialStateNamesEmpty() != true);
    CPPUNIT_ASSERT(dataset.GetNumInitialStateNames() ==3);
    vector<string> tmp = dataset.GetInitialStateNames();
    CPPUNIT_ASSERT(tmp.size() == dataset.GetNumInitialStateNames());
    CPPUNIT_ASSERT_EQUAL(s1, tmp.front() );
    CPPUNIT_ASSERT_EQUAL(s3, tmp.back() );
    CPPUNIT_ASSERT_EQUAL(dataset.GetAInitialStateName(0), tmp.front() );
    CPPUNIT_ASSERT_EQUAL(dataset.GetAInitialStateName(1), tmp.at(1) );
    string s4 = "State0";
    dataset.SetAInitialStateName(0,s4);
    CPPUNIT_ASSERT_EQUAL(dataset.GetAInitialStateName(0), s4);
}

void TestDataSet::testInitialStateValues(){
    DataSet dataset;
    CPPUNIT_ASSERT(dataset.IsInitialStateValuesEmpty() == true);
    CPPUNIT_ASSERT(dataset.GetNumInitialStateValues() == 0);
    double v1=0.01;
    double v2=0.02;
    double v3=0.1;
    dataset.AddAInitialStateValue(v1);
    dataset.AddAInitialStateValue(v2);
    dataset.AddAInitialStateValue(v3);
    CPPUNIT_ASSERT(dataset.IsInitialStateValuesEmpty() != true);
    CPPUNIT_ASSERT(dataset.GetNumInitialStateValues() ==3);
    vector<double> tmp = dataset.GetInitialStateValues();
    CPPUNIT_ASSERT(tmp.size() == dataset.GetNumInitialStateValues());
    CPPUNIT_ASSERT_DOUBLES_EQUAL(v1, tmp.front(),TOLERANCE );
    CPPUNIT_ASSERT_DOUBLES_EQUAL(v3, tmp.back(),TOLERANCE );
    CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetAInitialStateValue(0), tmp.front(),TOLERANCE );
    CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetAInitialStateValue(1), tmp.at(1),TOLERANCE );
    double v4 = 10.0;
    dataset.SetAInitialStateValue(0,v4);
    CPPUNIT_ASSERT_EQUAL(dataset.GetAInitialStateValue(0), v4);
}
void TestDataSet::testInitialParamNames(){
    DataSet dataset;
    CPPUNIT_ASSERT(dataset.IsInitialParamNamesEmpty() == true);
    CPPUNIT_ASSERT(dataset.GetNumInitialParamNames() == 0);
    string p1="Param1";
    string p2="Param2";
    string p3="Param3";
    dataset.AddAInitialParamName(p1);
    dataset.AddAInitialParamName(p2);
    dataset.AddAInitialParamName(p3);
    CPPUNIT_ASSERT(dataset.IsInitialParamNamesEmpty() != true);
    CPPUNIT_ASSERT(dataset.GetNumInitialParamNames() ==3);
    vector<string> tmp = dataset.GetInitialParamNames();
    CPPUNIT_ASSERT(tmp.size() == dataset.GetNumInitialParamNames());
    CPPUNIT_ASSERT_EQUAL(p1, tmp.front() );
    CPPUNIT_ASSERT_EQUAL(p3, tmp.back() );
    CPPUNIT_ASSERT_EQUAL(dataset.GetAInitialParamName(0), tmp.front() );
    CPPUNIT_ASSERT_EQUAL(dataset.GetAInitialParamName(1), tmp.at(1) );
    string p4 = "Param0";
    dataset.SetAInitialParamName(0,p4);
    CPPUNIT_ASSERT_EQUAL(dataset.GetAInitialParamName(0), p4);
}
void TestDataSet::testInitialParamValues(){
    DataSet dataset;
    CPPUNIT_ASSERT(dataset.IsInitialParamValuesEmpty() == true);
    CPPUNIT_ASSERT(dataset.GetNumInitialParamValues() == 0);
    double v1=0.01;
    double v2=0.02;
    double v3=0.1;
    dataset.AddAInitialParamValue(v1);
    dataset.AddAInitialParamValue(v2);
    dataset.AddAInitialParamValue(v3);
    CPPUNIT_ASSERT(dataset.IsInitialParamValuesEmpty() != true);
    CPPUNIT_ASSERT(dataset.GetNumInitialParamValues() ==3);
    vector<double> tmp = dataset.GetInitialParamValues();
    CPPUNIT_ASSERT(tmp.size() == dataset.GetNumInitialParamValues());
    CPPUNIT_ASSERT_DOUBLES_EQUAL(v1, tmp.front(),TOLERANCE );
    CPPUNIT_ASSERT_DOUBLES_EQUAL(v3, tmp.back(),TOLERANCE );
    CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetAInitialParamValue(0), tmp.front(),TOLERANCE );
    CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetAInitialParamValue(1), tmp.at(1),TOLERANCE );
    double v4 = 10.0;
    dataset.SetAInitialParamValue(0,v4);
    CPPUNIT_ASSERT_EQUAL(dataset.GetAInitialParamValue(0), v4);
}
void TestDataSet::testDataValues(){
    DataSet dataset;
    CPPUNIT_ASSERT(dataset.IsDataValuesEmpty() == true);
    CPPUNIT_ASSERT(dataset.GetNumDataValues() == 0);
    vector<double> v0;
    for(int i=0;i<10;i++) v0.push_back(0.1*i);
    dataset.AddADataValues(v0);
    v0.clear();
    for(int i=0;i<10;i++) v0.push_back(0.2*i+0.3);
    dataset.AddADataValues(v0);
    v0.clear();
    for(int i=0;i<9;i++) v0.push_back(0.3*i);
    dataset.AddADataValues(v0);
    CPPUNIT_ASSERT(dataset.GetNumDataValues() == 3);
    CPPUNIT_ASSERT(dataset.IsDataTimeEmpty() != true);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetEndDataTime(), 0.9, TOLERANCE);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetADataTime(1), 0.1, TOLERANCE);
    CPPUNIT_ASSERT(dataset.GetNumDataTimes() == 10);
    CPPUNIT_ASSERT(dataset.GetNumADataValues(0) == 10);
    CPPUNIT_ASSERT(dataset.GetNumADataValues(2) == 9);
    double value=0.3*9.0;
    dataset.AddADataValue(2,value);
    CPPUNIT_ASSERT(dataset.GetNumADataValues(2) == 10);
    vector<double> tmp = dataset.GetADataValues(1);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.front(), 0.3, TOLERANCE);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.back(), 2.1, TOLERANCE);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetADataValue(2,1), 0.3, TOLERANCE);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetAEndDataValue(2), value, TOLERANCE);
    dataset.SetADataValue(1,0,0.55);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetADataValue(1,0), 0.55, TOLERANCE);

    DataSet dtmp;
    dtmp.SetSizeDataValues(4);
    CPPUNIT_ASSERT(dtmp.GetNumDataValues() == 4);
}
