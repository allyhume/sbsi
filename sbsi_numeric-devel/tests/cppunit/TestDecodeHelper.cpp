/*****************************************************************************
 *
 *  TestDecoderHelper.cpp
 *
 *  Unit tests for DecoderHelper class.
 *
 *  $Id: TestDecodeHelper.cpp,v 1.1.2.1 2010/05/20 14:36:25 ayamaguc Exp $
 *
 *   Centre for Systems Biology at Edinburgh
 *  (c) The University of Edinburgh (2010)
 *  Richard Adams 
 *
 *****************************************************************************/
using namespace std;
#include "TestDecodeHelper.h"
#include <sbml/SBMLTypes.h>
#include <sbml/Constraint.h>
#include <sbml/FunctionDefinition.h>
#include <cModel.h>
#include <cModelArg.h>
#include <DecodeHelper.h>
#include <SBMLModel.h>
#include <iostream>
#include <sstream>
#include <fstream>

/*
  Utility functions to create various test snippets of ASTNodes.
 */
ASTNode * createInequality2(void);

ASTNode * createComplexInequality(void) ;

ASTNode * createTwoNestedLogicalLevelInequality ();

ASTNode * createMathsToTestIDChange();

Model * createReactionForTestingIdReplacement (void);

ASTNode* createInequalityToTestIDChange();

ASTNode * createPiecewise();

CPPUNIT_TEST_SUITE_REGISTRATION(TestDecodeHelper);


void TestDecodeHelper::setUp(void) {
  return;
}


void TestDecodeHelper::tearDown(void) {
  return;
}

void TestDecodeHelper::testGetInequality (){
	// tests correct creation of basic inequality 
	ASTNode * ineq = createInequality2();
	ostringstream oss;
	DecodeHelper helper;
	helper.createInequality(oss, ineq);
         string EXPECTED= "a  ==  22" ;
	CPPUNIT_ASSERT_EQUAL(EXPECTED,oss.str());
	
}

void TestDecodeHelper::testPopulateBottomLogicalNodes (){
	map<ASTNode* , ASTNode*> c2Parent;
	vector<ASTNode * > children;
	ASTNode * ineq = createTwoNestedLogicalLevelInequality();
	ineq->setName("ID");
	DecodeHelper helper;
	helper.populateBottomLogicalNodes( ineq, children, c2Parent);
	CPPUNIT_ASSERT_EQUAL ((int)c2Parent.size() , 7); // 3 nodes : 'a', '<=', '103'
	CPPUNIT_ASSERT(c2Parent[ineq]==NULL); // root node has no parent.
	CPPUNIT_ASSERT_EQUAL (c2Parent[ineq->getChild(0)] , ineq); //  node has  parent.
	CPPUNIT_ASSERT_EQUAL (c2Parent[ineq->getChild(1)] , ineq); //  node has  parent.
	CPPUNIT_ASSERT_EQUAL ((int)children.size(),2); //  node has  parent.
	delete ineq;
}

void TestDecodeHelper::testPopulateList (){
	string EXPECTED = "((a  ==  103 && b  >  40)  ||  (a  ==  103 && b  >  40))";
	ASTNode * node = createTwoNestedLogicalLevelInequality();
 	DecodeHelper helper;
	CPPUNIT_ASSERT_EQUAL(EXPECTED, helper.getString(node));
		
	
	delete node;
}

void TestDecodeHelper::testComplexInequality () {
	string EXPECTED = "(a  ==  103 && b  >  40)";
	ASTNode * node = createComplexInequality();
	DecodeHelper helper;
	CPPUNIT_ASSERT_EQUAL(EXPECTED, helper.getString(node));
	delete node;
}

void TestDecodeHelper::testComplexNestedInequality () {
	string EXPECTED = "((a  ==  103 && b  >  40)  ||  (a  ==  103 && b  >  40))";
	ASTNode * node = createTwoNestedLogicalLevelInequality();
	DecodeHelper helper;
	CPPUNIT_ASSERT_EQUAL(EXPECTED, helper.getString(node));
	delete node;
}

// fix for e.g.,  biomodels 69 which has 'default' as cmpartment identifier
void TestDecodeHelper::testCheckModelForReservedWords(){
	Model *m = new Model(3,1);
	Species *s = m->createSpecies();
	s->setId("default");
	DecodeHelper helper;
	CPPUNIT_ASSERT_EQUAL(true, helper.searchAndReplaceReservedWords(m));
        string EXPECTED = "default_MODEL";
	CPPUNIT_ASSERT_EQUAL(EXPECTED, m->getSpecies(0)->getId());
	
	// now try normal ID
        EXPECTED = "OK";
	s->setId(EXPECTED);
	CPPUNIT_ASSERT_EQUAL (false , helper.searchAndReplaceReservedWords(m));
	CPPUNIT_ASSERT_EQUAL (EXPECTED ,s->getId());
	Compartment * c =m->createCompartment();
	c->setId("while");
	s->setCompartment("while");
        EXPECTED="while_MODEL";
	CPPUNIT_ASSERT_EQUAL (true , helper.searchAndReplaceReservedWords(m));
	CPPUNIT_ASSERT_EQUAL (EXPECTED ,s->getCompartment());	
}

// fix for e.g.,  biomodels 69 which has 'default' as cmpartment identifier
void TestDecodeHelper::testCheckCompartmentIDsAreChangedInSpecies(){
	Model *m = new Model(3,1);
	Species *s = m->createSpecies();
        s->setId("default");
	Compartment * c =m->createCompartment();
	c->setId("while");
	s->setCompartment("while");
	DecodeHelper helper;
        string EXPECTED = "while_MODEL";
	CPPUNIT_ASSERT_EQUAL (true , helper.searchAndReplaceReservedWords(m));
	CPPUNIT_ASSERT_EQUAL (EXPECTED ,s->getCompartment());	
}

// fix for e.g.,  biomodels 69 which has 'default' as cmpartment identifier
void TestDecodeHelper::testMathsInKineticLawsAreChangedInSpecies(){
	Model *m = createReactionForTestingIdReplacement();
	DecodeHelper helper;
	//before transform 
        string EXPECTED="default * continue";

	CPPUNIT_ASSERT_EQUAL(EXPECTED, string((char*)SBML_formulaToString(m->getReaction(0)->getKineticLaw()->getMath()))) ;

	CPPUNIT_ASSERT_EQUAL(true,helper.searchAndReplaceReservedWords(m));

	string formula = string((char *)SBML_formulaToString(m->getReaction(0)->getKineticLaw()->getMath()));

        EXPECTED="default_MODEL * continue_MODEL";
	CPPUNIT_ASSERT_EQUAL(EXPECTED, formula) ;

        EXPECTED="default_MODEL";
	// after transform
	CPPUNIT_ASSERT_EQUAL(EXPECTED,m->getSpecies("default_MODEL")->getId());
	CPPUNIT_ASSERT_EQUAL(EXPECTED,m->getReaction(0)->getReactant(0)->getSpecies() );
}

// fix for e.g.,  biomodels 69 which has 'default' as cmpartment identifier
void TestDecodeHelper::testMathsInKineticLawsParametersAreChanged(){
	Model *m = createReactionForTestingIdReplacement();
	KineticLaw *kl = m->getReaction(0)->getKineticLaw();
	Parameter *p = kl->createParameter(); //create local parameter
	p->setId("continue"); // reserved
	DecodeHelper helper;
        string EXPECTED = "continue_MODEL" ;
	CPPUNIT_ASSERT_EQUAL(true,helper.searchAndReplaceReservedWords(m));
	CPPUNIT_ASSERT_EQUAL(EXPECTED, p->getId() );
}

// 
void TestDecodeHelper::testGlobalParameterChangeID(){
	Model *m = new Model(3,1);
	m->setId("ID");
	Parameter *p = m->createParameter();
	p->setId("break"); // reserved word.
	DecodeHelper helper;
        string EXPECTED = "break_MODEL" ;
	CPPUNIT_ASSERT_EQUAL(true,helper.searchAndReplaceReservedWords(m));
	CPPUNIT_ASSERT_EQUAL(EXPECTED , p->getId() );
}

void TestDecodeHelper::testAssignmentRuleChangeID(){
	Model *m = new Model(3,1);
	m->setId("ID");
	AssignmentRule *r = m->createAssignmentRule();
	r->setVariable("continue"); // reserved word.
	r->setMath(createMathsToTestIDChange());
	DecodeHelper helper;

	CPPUNIT_ASSERT_EQUAL(true,helper.searchAndReplaceReservedWords(m));
        string EXPECTED = "continue_MODEL" ;
	CPPUNIT_ASSERT_EQUAL(EXPECTED,r->getVariable());
        EXPECTED = "default_MODEL * continue_MODEL";
	CPPUNIT_ASSERT_EQUAL(EXPECTED,(string)SBML_formulaToString(r->getMath()));
}

void TestDecodeHelper::testConstraintChangeID(){
	Model *m = new Model(3,1);
	m->setId("ID");
	Constraint *c = m->createConstraint();
	c->setMath(createInequalityToTestIDChange());
	DecodeHelper helper;
		CPPUNIT_ASSERT_EQUAL(true,helper.searchAndReplaceReservedWords(m));
	string EXPECTED="eq(default_MODEL, continue_MODEL)";
	CPPUNIT_ASSERT_EQUAL(EXPECTED,(string)SBML_formulaToString(c->getMath()));
}

void TestDecodeHelper::testInitialAssignmentChangeID(){
	Model *m = new Model(3, 1);
	m->setId("ID");
	InitialAssignment *r = m->createInitialAssignment();
	r->setSymbol("new"); // reserved word.
	r->setMath(createMathsToTestIDChange());
	DecodeHelper helper;
	CPPUNIT_ASSERT_EQUAL(true,helper.searchAndReplaceReservedWords(m));
        string EXPECTED = "new_MODEL" ;
	CPPUNIT_ASSERT_EQUAL(EXPECTED,r->getSymbol());
        EXPECTED="default_MODEL * continue_MODEL" ;
	CPPUNIT_ASSERT_EQUAL(EXPECTED, (string)SBML_formulaToString(r->getMath()));
}

void  TestDecodeHelper::testFunctionDefinitionChangeID () {
	Model *m = new Model(3,1);
	m->setId("ID");
	FunctionDefinition *r = m->createFunctionDefinition();
	r->setId("new"); // reserved word.
	r->setMath(createMathsToTestIDChange());
	DecodeHelper helper;
	CPPUNIT_ASSERT_EQUAL(true,helper.searchAndReplaceReservedWords(m));
        string EXPECTED = "new_MODEL" ;
	CPPUNIT_ASSERT_EQUAL(EXPECTED,r->getId());
        EXPECTED="default_MODEL * continue_MODEL" ;
	CPPUNIT_ASSERT_EQUAL(EXPECTED, (string)SBML_formulaToString(r->getMath()));
	
}

void TestDecodeHelper::testEventAssignmentChangeID (){
	Model *m = new Model(3,1);
	Event *e = m->createEvent();
	Trigger *t = e->createTrigger();
	Delay *d = e->createDelay();
	EventAssignment *ea = e->createEventAssignment();
	t->setMath(createInequalityToTestIDChange());
	d->setMath(createInequalityToTestIDChange());
	ea->setVariable("new");
	ea->setMath(createMathsToTestIDChange());
	DecodeHelper helper;
	CPPUNIT_ASSERT_EQUAL(true,helper.searchAndReplaceReservedWords(m));
	CPPUNIT_ASSERT_EQUAL(strcmp(SBML_formulaToString(ea->getMath()), "default_MODEL * continue_MODEL") ,  0) ;
	CPPUNIT_ASSERT_EQUAL(strcmp(SBML_formulaToString(d->getMath()), "eq(default_MODEL, continue_MODEL)") ,  0) ;
	CPPUNIT_ASSERT_EQUAL(strcmp(SBML_formulaToString(t->getMath()), "eq(default_MODEL, continue_MODEL)") ,  0) ;
        string EXPECTED="new_MODEL";
	CPPUNIT_ASSERT_EQUAL(EXPECTED,ea->getVariable()) ;
}

void TestDecodeHelper::testPiecewiseInAssignmentRule() {
		ASTNode *math = createPiecewise();
	
	DecodeHelper helper;
	string assign(helper.getPiecewiseString(math, string("var")));
	CPPUNIT_ASSERT(assign.find("if (a  ==  3) {") != string::npos);
	CPPUNIT_ASSERT(assign.find("var = 2;") != string::npos);
	CPPUNIT_ASSERT(assign.find("}else if (PIP_PM  <  PIP_basal_PIPSyn) {") != string::npos);
	CPPUNIT_ASSERT(assign.find("var = 0.581 * kBasalSynPIP_PIPSyn * (-1 + exp((PIP_basal_PIPSyn + -PIP_PM) * (1 / PIP_basal_PIPSyn)));") != string::npos);
	CPPUNIT_ASSERT(assign.find("}else {") != string::npos);
	CPPUNIT_ASSERT(assign.find("var = 0;") != string::npos);

}

Model * createReactionForTestingIdReplacement (void) {
	Model *m = new Model(3,1);
	Species *r1 = m->createSpecies();
	r1->setId("default");
	Species *p1 = m->createSpecies();
	p1->setId("OK");
	Reaction *r =m->createReaction();
	r->createReactant()->setSpecies(r1->getId());
	r->createProduct()->setSpecies(p1->getId());
	ASTNode *math = createMathsToTestIDChange();
	KineticLaw *kl =  r->createKineticLaw();
	kl->setMath(math);
	return m;
	
	
}

ASTNode* createMathsToTestIDChange(){
	// 'a <= 103'
	char * xml =(char*)"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> " 
	" <apply> "
	"   <times/>  "
	" <ci> default </ci>  "
	" <ci> continue </ci>  "
	" </apply>  "
	"</math>";
	ASTNode *n = readMathMLFromString(xml);
	return n;
}

ASTNode * createInequalityToTestIDChange(){
	// 'a <= 103'
	char * xml =(char *)"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> " 
	" <apply> "
	"   <eq/>  "
	" <ci> default </ci>  "
	" <ci> continue </ci>  "
	" </apply>  "
	"</math>";
	ASTNode *n = readMathMLFromString(xml);
	return n;
}


ASTNode * createInequality2(void) {
// 'a <= 103'
char * xml =(char *)"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> " 
	       " <apply> "
	        "   <eq/>  "
	        "  <ci> a </ci>  "
	         " <cn> 22 </cn>  "
 	        " </apply>  "
		    "</math>";
	ASTNode *n = readMathMLFromString(xml);
	return n;
}

//piecewise elements are represented as a flat list in pairs - odd child at end will be optional otherwise.
ASTNode * createPiecewise() {
// this is :
//  if (a > 3) {
//	var =2;
//	else if (PIP_PM < PIP_basal_PIPSyn) { //condition = child2
//		var = 0.581 * kBasalSynPIP_PIPSyn * (-1 + exp((PIP_basal_PIPSyn + -PIP_PM) * (1 / PIP_basal_PIPSyn))); //child1
//	} else  {
//		var = 0; //otherwise part
//	}
	
char * xml = (char *)
	"	<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> "
	"<piecewise>"
	"	<piece>"
			"<cn>2</cn>"
	        "	<apply>"
	     "<eq/>"
	       "<ci>a</ci>"
		   "<cn>3</cn>"
	"	</apply>"
	"	</piece>"
	"	<piece>"
	"<cn>2</cn>"
	"	<apply>"
	"<eq/>"
	"<ci>a</ci>"
	"<cn>3</cn>"
	"	</apply>"
	"	</piece>"
	"	<piece>"
		"	<apply>"
			"	<times/>"
			"	<cn> 0.581 </cn>"
			"	<ci> kBasalSynPIP_PIPSyn </ci>"
			"	<apply>"
				"	<plus/>"
				"	<cn> -1 </cn>"
				"	<apply>"	
					"	<exp/>"
					"	<apply>"
						"	<times/>"
							"	<apply>"
								"	<plus/>"
									"	<ci> PIP_basal_PIPSyn </ci>"
										"	<apply>"
											"	<minus/>"
											"	<ci> PIP_PM </ci>"
										"	</apply>"
							"	</apply>"
							"	<apply>"
								"	<divide/>"
								"	<cn> 1 </cn>"
								"	<ci> PIP_basal_PIPSyn </ci>"
							"	</apply>"
					"	</apply>"
				"	</apply>"
			"	</apply>"
		"	</apply>"
		"	<apply>"
			"	<lt/>"
			"	<ci> PIP_PM </ci>"
			"	<ci> PIP_basal_PIPSyn </ci>"
		"	</apply>"
	"	</piece>"
	"	<otherwise>"
		"	<cn> 0 </cn>"
	"	</otherwise>"
"	</piecewise>"
"	</math>";
	ASTNode *n = readMathMLFromString(xml);
	return n;
}
	
	

ASTNode * createTwoNestedLogicalLevelInequality (){
	ASTNode * branch1 =  createComplexInequality();
	ASTNode * branch2 = createComplexInequality();
	ASTNode * topOr = new ASTNode();
	topOr->setType(AST_LOGICAL_OR);
	topOr->addChild(branch1);
	topOr->addChild(branch2);
	

	return topOr;
}



ASTNode * createComplexInequality(void) {
	// 'a == 103 && b > 40'
	char * xml =(char *)"<math xmlns=\"http://www.w3.org/1998/Math/MathML\"> " 
	"<apply>"
	" <and/>"
	" <apply> "
	"   <eq/>  "
	"   <ci> a </ci>  "
	"    <cn> 103 </cn>  "
	" </apply>  "
	" <apply> "
	"   <gt/>  "
	"   <ci> b </ci>  "
	"    <cn> 40 </cn>  "
	" </apply>  "
	 "</apply>"
	"</math>";
	ASTNode *n = readMathMLFromString(xml);
	return n;
}

