/*****************************************************************************
 *
 *  TestParamInfo.cpp
 *
 *  See TestParamInfo.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestParamInfo.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestParamInfo);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestParamInfo::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestParamInfo::tearDown(void) {

  return;
}

void TestParamInfo::testClear(){
  int numparam(10);
  ParamInfo pi;
  vector<double> u;
  vector<double> d;
  vector<double> is;
  vector<string> pn;
  for(int i=0;i<numparam;i++){
   u.push_back(10.0*i);
   d.push_back(0.001*i);
   is.push_back(0.02);
   pn.push_back("A");
  }

   pi.SetUpper(u);
   pi.SetLower(d);
   pi.SetInitialStep(is);
   pi.SetParamName(pn);

   CPPUNIT_ASSERT(pi.GetNumLower() ==  (size_t)numparam);
   CPPUNIT_ASSERT(pi.GetNumUpper() ==  (size_t)numparam);
   CPPUNIT_ASSERT(pi.GetNumInitialStep() ==  (size_t)numparam);
   CPPUNIT_ASSERT(pi.GetNumParamName() ==  (size_t)numparam);

   pi.Clear();

   CPPUNIT_ASSERT(pi.GetNumLower() ==  0);
   CPPUNIT_ASSERT(pi.GetNumUpper() ==  0);
   CPPUNIT_ASSERT(pi.GetNumInitialStep() ==  0);
   CPPUNIT_ASSERT(pi.GetNumParamName() ==  0);
}
void TestParamInfo::testUpper(){

  ParamInfo pi;
  CPPUNIT_ASSERT(pi.GetNumUpper() ==  0);
  CPPUNIT_ASSERT(pi.IsUpperEmpty() ==  true);

  int numparam(10);
  vector<double> u;
  for(int i=0;i<numparam;i++){
   u.push_back(10.0*i);
  }

   pi.SetUpper(u);

  CPPUNIT_ASSERT(pi.IsUpperEmpty() ==  false);
  CPPUNIT_ASSERT(pi.GetNumUpper() ==  (size_t)numparam);
  vector<double> tmp = pi.GetUpperVec();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.front() ,  u.front(), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.back() ,  u.back(), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(pi.GetAUpper(1) ,  u.at(1), TOLERANCE);
  pi.SetAUpper(1,3.0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(pi.GetAUpper(1) , 3.0, TOLERANCE);
  pi.AddAUpper(5.0);
  CPPUNIT_ASSERT(pi.GetNumUpper()== (size_t)numparam+1);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(pi.GetAUpper(numparam) , 5.0, TOLERANCE);
  
}
void TestParamInfo::testLower(){
  ParamInfo pi;
  CPPUNIT_ASSERT(pi.GetNumLower() ==  0);
  CPPUNIT_ASSERT(pi.IsLowerEmpty() ==  true);

  int numparam(10);
  vector<double> l;
  for(int i=0;i<numparam;i++){
   l.push_back(10.0*i);
  }

   pi.SetLower(l);

  CPPUNIT_ASSERT(pi.IsLowerEmpty() ==  false);
  CPPUNIT_ASSERT(pi.GetNumLower() ==  (size_t)numparam);
  vector<double> tmp = pi.GetLowerVec();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.front() ,  l.front(),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.back() ,  l.back(),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(pi.GetALower(1) ,  l.at(1),TOLERANCE);
  pi.SetALower(1,3.0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(pi.GetALower(1) , 3.0,TOLERANCE);
  pi.AddALower(5.0);
  CPPUNIT_ASSERT(pi.GetNumLower() == (size_t)numparam+1);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(pi.GetALower(numparam) , 5.0,TOLERANCE);

}
void TestParamInfo::testInitialStep(){
  ParamInfo pi;
  CPPUNIT_ASSERT(pi.GetNumInitialStep() ==  0);
  CPPUNIT_ASSERT(pi.IsInitialStepEmpty() ==  true);

  int numparam(10);
  vector<double> is;
  for(int i=0;i<numparam;i++){
   is.push_back(10.0*i);
  }

   pi.SetInitialStep(is);

  CPPUNIT_ASSERT(pi.IsInitialStepEmpty() ==  false);
  CPPUNIT_ASSERT(pi.GetNumInitialStep() ==  (size_t)numparam);
  vector<double> tmp = pi.GetInitialStep();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.front() ,  is.front(),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.back() ,  is.back(),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(pi.GetAInitialStep(1) ,  is.at(1),TOLERANCE);
  pi.SetAInitialStep(1,3.0);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(pi.GetAInitialStep(1) , 3.0,TOLERANCE);
  pi.AddAInitialStep(5.0);
  CPPUNIT_ASSERT(pi.GetNumInitialStep() ==  (size_t)numparam+1);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(pi.GetAInitialStep(numparam) , 5.0,TOLERANCE);

}
void TestParamInfo::testParamName(){
  ParamInfo pi;
  CPPUNIT_ASSERT(pi.GetNumParamName() ==  0);
  CPPUNIT_ASSERT(pi.IsParamNameEmpty() ==  true);

  int numparam(7);
  vector<string> np;
  np.push_back("P");
  np.push_back("P1");
  np.push_back("P2");
  np.push_back("P3");
  np.push_back("P4");
  np.push_back("P5");
  np.push_back("P6");

   pi.SetParamName(np);

  CPPUNIT_ASSERT(pi.IsParamNameEmpty() ==  false);
  CPPUNIT_ASSERT(pi.GetNumParamName() ==  (size_t)numparam);
  vector<string> tmp = pi.GetParamName();
  CPPUNIT_ASSERT_EQUAL(tmp.front() , np.front());
  CPPUNIT_ASSERT_EQUAL(tmp.back() , np.back());
  CPPUNIT_ASSERT_EQUAL(pi.GetAParamName(1) ,  np.at(1));
  pi.SetAParamName(1,"P0");
  string EXPECTED="P0";
  CPPUNIT_ASSERT_EQUAL(EXPECTED, pi.GetAParamName(1));
  pi.AddAParamName("P7");
  CPPUNIT_ASSERT(pi.GetNumParamName() ==  (size_t)numparam+1);
  EXPECTED="P7";
  CPPUNIT_ASSERT_EQUAL(EXPECTED,pi.GetAParamName(numparam) );

}
