
/*****************************************************************************
 *
 *  TestMultiObjCost.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <MultiObjCost.h>

#ifndef TESTMultiObjCost_H
#define TESTMultiObjCost_H

class TestMultiObjCost : public CppUnit::TestFixture {

 public:

  TestMultiObjCost(){};
  virtual ~TestMultiObjCost(){};

  void setUp(void);
  void tearDown(void);
  void testInit();
  void testGetCost();
  void testSetParameters();
  void testSetParamNames();
  void testGetParamNames();
  void testGetNumObjectives();
  void testAddObjective();
  void testSetObjectives();


  CPPUNIT_TEST_SUITE(TestMultiObjCost);
  CPPUNIT_TEST(testAddObjective);
  CPPUNIT_TEST(testSetObjectives);
  CPPUNIT_TEST(testGetNumObjectives);
  CPPUNIT_TEST(testInit);
  CPPUNIT_TEST(testSetParameters);
  CPPUNIT_TEST(testSetParamNames);
  CPPUNIT_TEST(testGetParamNames);
  CPPUNIT_TEST(testGetCost);
  CPPUNIT_TEST_SUITE_END();

  MultiObjCost *mcf;
  CostFunction *ucf1;
  CostFunction *ucf2;


};

#endif

