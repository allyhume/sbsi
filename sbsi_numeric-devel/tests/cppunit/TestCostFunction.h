
/*****************************************************************************
 *
 *  TestCostFunction.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <CostFunction.h>

#ifndef TESTCostFunction_H
#define TESTCostFunction_H

class TestCostFunction : public CppUnit::TestFixture {

 public:

  TestCostFunction(){};
  virtual ~TestCostFunction(){};

  void setUp(void);
  void tearDown(void);

  void testGlbCostArg();
  void testSetCostFunction();
  void testSetParameters();
  void testGetParamNames();
  void testSetParamNames();
  void testGetCost();

  CPPUNIT_TEST_SUITE(TestCostFunction);
  CPPUNIT_TEST(testSetCostFunction);
  CPPUNIT_TEST(testGlbCostArg);
  CPPUNIT_TEST(testSetParameters);
  CPPUNIT_TEST(testGetParamNames);
  CPPUNIT_TEST(testSetParamNames);
  CPPUNIT_TEST(testGetCost);
  CPPUNIT_TEST_SUITE_END();

};

#endif

