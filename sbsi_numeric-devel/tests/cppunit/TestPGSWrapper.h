
/*****************************************************************************
 *
 *  TestPGSWrapper.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <PGSWrapper.h>

#ifndef TESTPGSWrapper_H
#define TESTPGSWrapper_H

class TestPGSWrapper : public CppUnit::TestFixture, public PGSWrapper {

 public:

  TestPGSWrapper(){};
  virtual ~TestPGSWrapper(){};

  void setUp(void);
  void tearDown(void);

  void testInitialise();
  void testDestroy();
  void testN_InitString();
  void testsetParamSet();
  void testendOfGen();
  void testStopCond();
  void testgetTheBest();
  void testgetContext();
  void testSetLogfile();
  void testgetScore();
  void testsetVerbose();
  void testsetFixedSeed();
  void testsetRestartFreq();
  void testsetPrintBestPop();
  void testisPrintBestPop();
  void testsetPopulationFile();
  void testsummarise();

  CPPUNIT_TEST_SUITE(TestPGSWrapper);
  CPPUNIT_TEST(testInitialise);
  CPPUNIT_TEST(testDestroy);
  CPPUNIT_TEST(testN_InitString);
  CPPUNIT_TEST(testsetParamSet);
  CPPUNIT_TEST_SUITE_END();

  PGSWrapper *pgs;
  vector<ParamSet> ps;
  OptArg u_optarg;
  ParamInfo u_paraminfo;

  CostFunction *cfun;



};

#endif

