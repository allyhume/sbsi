/*****************************************************************************
 *
 *  TestOptimiser.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <Optimiser.h>

#ifndef TESTOptimiser_H
#define TESTOptimiser_H

class TestOptimiser: public CppUnit::TestFixture, public Optimiser{

 public:

  TestOptimiser(){};
  virtual ~TestOptimiser(){};

  int Run(){return 0;};

  void setUp(void);
  void tearDown(void);

  void testInit();
  void testSetOptimiser();
  void testSetCostFunction();
  void testGetTheBestCost();
  void testWriteStopInfo_MAXITER();
  void testWriteStopInfo_TOOSIMILAR();
  void testWriteStopInfo_NOIMPROVEMENT();
  void testWriteStopInfo_REACHED();
  void testWriteGeneralInfo();

  CPPUNIT_TEST_SUITE(TestOptimiser);
  CPPUNIT_TEST(testInit);
  CPPUNIT_TEST(testSetOptimiser);
  CPPUNIT_TEST(testSetCostFunction);
  CPPUNIT_TEST(testGetTheBestCost);
  CPPUNIT_TEST(testWriteStopInfo_MAXITER);
  CPPUNIT_TEST(testWriteStopInfo_TOOSIMILAR);
  CPPUNIT_TEST(testWriteStopInfo_NOIMPROVEMENT);
  CPPUNIT_TEST(testWriteStopInfo_REACHED);
  CPPUNIT_TEST(testWriteGeneralInfo);
  CPPUNIT_TEST_SUITE_END();

};

#endif

