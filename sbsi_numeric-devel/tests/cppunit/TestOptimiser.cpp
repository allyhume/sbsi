/*****************************************************************************
 *
 *  TestOptimiser.cpp
 *
 *  See TestOptimiser.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include <MultiObjCost.h>
#include "TestOptimiser.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestOptimiser);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestOptimiser::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestOptimiser::tearDown(void) {

  return;
}

void TestOptimiser::testInit(){
 OptArg u_optarg;
 ParamInfo u_paraminfo;
 vector<ParamSet> paramset;
 Init(&u_optarg, paraminfo, &paramset);
 CPPUNIT_ASSERT(&u_optarg == optarg);
 CPPUNIT_ASSERT(&paramset == paramsets);
 CPPUNIT_ASSERT(u_paraminfo.IsUpperEmpty() == paraminfo.IsUpperEmpty());
}
void TestOptimiser::testSetOptimiser(){
 OptimiserType otype =PGA;
 Optimiser &pga=SetOptimiser(otype);
 CPPUNIT_ASSERT(&pga != NULL);
 otype =DirectSearch;
 Optimiser &sa=SetOptimiser(otype);
 CPPUNIT_ASSERT(&sa != NULL);
 otype =PGAPSO;
 Optimiser &pso=SetOptimiser(otype);
 CPPUNIT_ASSERT(&pso != NULL);
}
void TestOptimiser::testSetCostFunction(){
   MultiObjCost *cf = new MultiObjCost();
   SetCostFunction(cf);
   CPPUNIT_ASSERT(cf == costfunction);
}
void TestOptimiser::testGetTheBestCost(){

  vector<ParamSet> ps;

  for(int j=1; j<10;j++) {

  double cost=1.0-0.1*j;
  vector<double> tmp;
  for(int i=0; i<3;i++) tmp.push_back(j+i*0.1);
  ps.push_back(ParamSet(tmp, cost));
  }

 OptArg u_optarg;
 ParamInfo u_paraminfo;
 Init(&u_optarg, paraminfo, &ps);

 double bc=GetTheBestCost();
 CPPUNIT_ASSERT_DOUBLES_EQUAL(bc, 0.1, TOLERANCE);

 vector<double> best = GetTheBest();
 CPPUNIT_ASSERT_DOUBLES_EQUAL(best.front(),9,TOLERANCE);
 CPPUNIT_ASSERT_DOUBLES_EQUAL(best.back(),9.2,TOLERANCE);

 double ac=GetTheCost(2);
 CPPUNIT_ASSERT_DOUBLES_EQUAL(ac, 0.7, TOLERANCE);

 vector<ParamSet> *ret=GetParamSets();
 CPPUNIT_ASSERT_DOUBLES_EQUAL(ret->front().cost, ps.front().cost, TOLERANCE);
 
 
}
void TestOptimiser::testWriteStopInfo_MAXITER(){}
void TestOptimiser::testWriteStopInfo_TOOSIMILAR(){}
void TestOptimiser::testWriteStopInfo_NOIMPROVEMENT(){}
void TestOptimiser::testWriteStopInfo_REACHED(){}
void TestOptimiser::testWriteGeneralInfo(){}
