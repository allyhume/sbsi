
/*****************************************************************************
 *
 *  TestSetup.cpp
 *
 *  See TestSetup.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include <ModelCostFunction.h>
#include "TestSetup.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestSetup);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestSetup::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestSetup::tearDown(void) {

  return;
}

void TestSetup::testInit(){

  ParamInfo InParamInfo;
  OptArg InOptArg;
  Init(&InParamInfo,&InOptArg);

  CPPUNIT_ASSERT(paramset.size() == 0);
  CPPUNIT_ASSERT(paraminfo->GetNumUpper() == 0);
  CPPUNIT_ASSERT(paraminfo->GetNumLower() == 0);
  CPPUNIT_ASSERT(paraminfo->GetNumParamName() == 0);
  CPPUNIT_ASSERT(optarg == &InOptArg);

  CPPUNIT_ASSERT(GetParamSetNum() == 0);
}
void TestSetup::testSetFileName(){
  string filename="Populataion.ga";
  SetFileName(filename);
  CPPUNIT_ASSERT_EQUAL(filename,InFileName);
}
void TestSetup::testSetupParam(){
  SetupType type;
  type=SobolSelect;
  Setup &setup_ss=SetupParam(type);
  CPPUNIT_ASSERT(&setup_ss !=NULL);
  type=SobolUnselect;
  Setup &setup_su=SetupParam(type);
  CPPUNIT_ASSERT(&setup_su !=NULL);
  type=ReadinFile;
  Setup &setup_rf=SetupParam(type);
  CPPUNIT_ASSERT(&setup_rf !=NULL);
}
void TestSetup::testSetCostFunction(){
  ModelCostFunction &mcf_x2c = ModelCostFunction::SetModelCostFunction(X2Cost);
  SetCostFunction(&mcf_x2c);
  CPPUNIT_ASSERT(costfunction == &mcf_x2c);
}
void TestSetup::testWriteStart(){}
