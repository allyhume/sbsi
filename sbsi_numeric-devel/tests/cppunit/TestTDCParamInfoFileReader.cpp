
/*****************************************************************************
 *
 *  TestFileReadParamInfo.cpp
 *
 *  See TestFileReadParamInfo.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include <cModel.h>
#include "TestTDCFileReader.h"
#include "CppunitTestData/UserModel/UserModel.h"

#define TOLERANCE DBL_EPSILON


void TestTDCFileReader::testTDCParamInfoFileReader(void) {

  string filename="CppunitTestData/UserModel/abc_1_InitParam.dat";
  FileReader &paraminfo_freader = FileReader::SetFileReader(TabDelimitedColumnParamInfo);
  paraminfo_freader.ClearParamInfo();
  paraminfo_freader.Read(filename);


  CPPUNIT_ASSERT( paraminfo_freader.paraminfo.GetNumParamName()== 5);
  CPPUNIT_ASSERT( paraminfo_freader.paraminfo.GetNumLower()== 5);
  CPPUNIT_ASSERT( paraminfo_freader.paraminfo.GetNumUpper()== 5);
  CPPUNIT_ASSERT( paraminfo_freader.paraminfo.GetNumInitialStep()== 1);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(paraminfo_freader.paraminfo.GetAInitialStep(0),0.01,TOLERANCE);

  string EXPECTED="IS";
  CPPUNIT_ASSERT_EQUAL(EXPECTED, paraminfo_freader.paraminfo.GetAParamName(4));
  CPPUNIT_ASSERT_DOUBLES_EQUAL(paraminfo_freader.paraminfo.GetALower(4),1.0,TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(paraminfo_freader.paraminfo.GetAUpper(4),10,TOLERANCE);

  EXPECTED="P2";
  CPPUNIT_ASSERT_EQUAL(EXPECTED,paraminfo_freader.paraminfo.GetAUnOptimiseParamName(0));
  CPPUNIT_ASSERT_DOUBLES_EQUAL(paraminfo_freader.paraminfo.GetAUnOptimiseParamValue(0),15,TOLERANCE);

  EXPECTED="P1";
  CPPUNIT_ASSERT_EQUAL(EXPECTED, paraminfo_freader.paraminfo.GetAParamName(0));
  CPPUNIT_ASSERT_DOUBLES_EQUAL(paraminfo_freader.paraminfo.GetALower(0),1.0e-8,TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(paraminfo_freader.paraminfo.GetAUpper(0),20,TOLERANCE);

  EXPECTED="k_1";
  CPPUNIT_ASSERT_EQUAL(EXPECTED, paraminfo_freader.paraminfo.GetAParamName(3));
  CPPUNIT_ASSERT_DOUBLES_EQUAL(paraminfo_freader.paraminfo.GetALower(3),1.0e-8,TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(paraminfo_freader.paraminfo.GetAUpper(3),3,TOLERANCE);

  return;
}

