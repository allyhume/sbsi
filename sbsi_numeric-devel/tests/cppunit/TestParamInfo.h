
/*****************************************************************************
 *
 *  TestParamInfo.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <ParamInfo.h>

#ifndef TESTParamInfo_H
#define TESTParamInfo_H

class TestParamInfo : public CppUnit::TestFixture {

 public:

  TestParamInfo(){};
  virtual ~TestParamInfo(){};

  void setUp(void);
  void tearDown(void);
  void testClear();
  void testUpper();
  void testLower();
  void testInitialStep();
  void testParamName();


  CPPUNIT_TEST_SUITE(TestParamInfo);
  CPPUNIT_TEST(testClear);
  CPPUNIT_TEST(testUpper);
  CPPUNIT_TEST(testLower);
  CPPUNIT_TEST(testInitialStep);
  CPPUNIT_TEST(testParamName);
  CPPUNIT_TEST_SUITE_END();

};

#endif

