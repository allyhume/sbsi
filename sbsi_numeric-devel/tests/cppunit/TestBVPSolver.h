
/*****************************************************************************
 *
 *  TestBVPSolver.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <BVPSolver.h>

#ifndef TESTBVPSolver_H
#define TESTBVPSolver_H

class TestBVPSolver : public CppUnit::TestFixture {

 public:

  TestBVPSolver();
  virtual ~TestBVPSolver();

  void setUp(void);
  void tearDown(void);


  CPPUNIT_TEST_SUITE(TestBVPSolver);
  CPPUNIT_TEST_SUITE_END();

};

#endif

  void testBVPSolver();
  void testsolve();
  void testsolve_input();
  void testsolve_create();
