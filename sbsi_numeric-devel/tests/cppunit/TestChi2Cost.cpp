
/*****************************************************************************
 *
 *  TestChi2Cost.cpp
 *
 *  See TestChi2Cost.h for a description.
 *
 *
 *****************************************************************************/

#include <iostream>
#include <cfloat>
#include <cmath>
#include <cModel.h>
#include <CostArg.h>
#include <SBMLModel.h>

#include "TestChi2Cost.h"
#include "CppunitTestData/UserModel/UserModel.h"

using namespace std;

#define TOLERANCE 1.0e-13
#define pi M_PI


CPPUNIT_TEST_SUITE_REGISTRATION(TestChi2Cost);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestChi2Cost::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestChi2Cost::tearDown(void) {

  return;
}

void TestChi2Cost::testSetDataSets(){
    cModelArg ModelArg;
    cModel * usermodel;
    usermodel  = new SBMLModel();
    usermodel->setModelArg(ModelArg);
    SetModel(usermodel);

    vector<DataSet> datasets;
    SetDataSets(&datasets);
    CPPUNIT_ASSERT(&datasets == dataset_s);
}

void TestChi2Cost::testGetNorm(){
  cModel *UModel = new UserModel();
  cModelArg ModelArg;
  UModel->setModelArg(ModelArg);
  UModel->initModelArg();
  UModel->inputModel();

  vector<DataSet> datasets;
  vector<CostArg> costargs;

  for(int j=1; j<11 ; j++){

    CostArg costarg;
    costarg.ExpT0 = 0.1*j; //childDouble(doc, d_node, "DataFileStartTime");
    costarg.OutputInterval = 0.1;//childDouble(doc, d_node, "Interval");
    if(j % 2) {costarg.costvaluetype = Normed; } else{costarg.costvaluetype = Direct;}

    costargs.push_back(costarg);

    DataSet dataset;

    vector<double>tmp;
    for (int i=1;i<11;i++) tmp.push_back(i*0.1+j*0.01);
    dataset.AddADataValues(tmp);

    for(int k=0; k<3 ;k++){
      for (vector<double>:: iterator i=tmp.begin();i!=tmp.end();i++) *i *= 10.0;
      dataset.AddADataValues(tmp);
    }
  datasets.push_back(dataset);
  }

  vector<string> pname;

  /* case 1 all values */
  UModel->setFinal(1.1);

  Init(UModel,costargs, pname);
  SetDataSets(&datasets);
  CPPUNIT_ASSERT(GetACostValueType(0) == Normed);
  CPPUNIT_ASSERT(GetACostValueType(1) == Direct);
  GetNorm();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Norms.front().front(), 1.0/(5.6*5.6), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Norms.at(1).back(), 1.0, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Norms.at(2).back(), 1.0/(580*580), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Norms.back().back(), 1.0, TOLERANCE);

  /* case 2 Final < dataset time values */

  UModel->setFinal(0.5);
  Init(UModel,costargs, pname);
  GetNorm();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Norms.front().front(), 1.0/(2.6*2.6), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Norms.at(1).back(), 1.0, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Norms.at(2).back(), 1.0/(280*280), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Norms.back().back(), 1.0, TOLERANCE);
}

void TestChi2Cost::testSetDataSet(){

  cModel *UModel = new UserModel();
  cModelArg ModelArg;
  UModel->setModelArg(ModelArg);
  UModel->initModelArg();
  UModel->inputModel();

  vector<DataSet> datasets;

  DataSet dataset;
  dataset.SetSetName("example1");
  dataset.AddAInitialStateName("A");
  dataset.AddAInitialStateValue(0.5);
  dataset.AddAInitialStateName("B");
  dataset.AddAInitialStateValue(0.75);
  dataset.AddAInitialStateName("C");
  dataset.AddAInitialStateValue(75);

  dataset.AddAInitialParamName("k1");
  dataset.AddAInitialParamValue(2);
  dataset.AddAInitialParamName("k2");
  dataset.AddAInitialParamValue(5);
  dataset.AddAInitialParamName("k1_p_k2");
  dataset.AddAInitialParamValue(15);

  vector<double>tmp;
   /*time*/
   for (int i=1;i<11;i++) tmp.push_back(i*0.1+ 0.01);
   dataset.AddADataValues(tmp);

  /*data*/
   for(int k=0; k<2 ;k++){
     for (vector<double>:: iterator i=tmp.begin();i!=tmp.end();i++) *i *= 10.0;
     dataset.AddADataValues(tmp);
   }
  datasets.push_back(dataset);


  DataSet dataset2;
  dataset2.SetSetName("example2");
  dataset2.AddAInitialStateName("A");
  dataset2.AddAInitialStateValue(0.15);
  dataset2.AddAInitialStateName("B");
  dataset2.AddAInitialStateValue(0.55);

  dataset2.AddAInitialParamName("k_1");
  dataset2.AddAInitialParamValue(2.5);
  dataset2.AddAInitialParamName("Default");
  dataset2.AddAInitialParamValue(5);

   /*time*/
   double t(1);
   for (vector<double>::iterator i=tmp.begin();i!=tmp.end();i++) {
              *i=(t*0.1+0.02);
              ++t;
    }
    dataset2.AddADataValues(tmp);

  /*data*/
    for(int k=0; k<2 ;k++){
      for (vector<double>:: iterator i=tmp.begin();i!=tmp.end();i++) *i *= 10.0;
      dataset2.AddADataValues(tmp);
   }

  datasets.push_back(dataset2);

  vector<CostArg> costargs;
  for(int j=1; j<3 ; j++){

    CostArg costarg;
    costarg.ExpT0 = 0.1*j; //childDouble(doc, d_node, "DataFileStartTime");
    costarg.OutputInterval = 0.1;//childDouble(doc, d_node, "Interval");
    if(j==1)costarg.scaletype = None;
    if(j==2){ costarg.scaletype = Fix;
              costarg.FixScale.push_back(0.1);
              costarg.FixScale.push_back(0.2);
             }
              
    costarg.costvaluetype = Normed;

    costargs.push_back(costarg);
  }
  vector<string> pname;
  pname.push_back("k1");
  pname.push_back("k2");
  pname.push_back("k_1");

  vector<double> pvalue;
  pvalue.push_back(0.1);
  pvalue.push_back(0.2);
  pvalue.push_back(0.3);
  Init(UModel,costargs, pname);

  SetDataSets(&datasets);

  SetParameters(pvalue);

  SetDataSet(0);

  CPPUNIT_ASSERT(GetScaleType() == None);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getStateByName("A"), 0.5, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getStateByName("B"), 0.75, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getStateByName("C"), 75, TOLERANCE);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName("k1"), 2, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName("k2"), 5, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName("k_1"), 0.3, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName("k1_p_k2"), 15, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName("Default"), 1, TOLERANCE);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getExpTInit(),0.1+0.11, TOLERANCE);
  
  
  SetDataSet(1);
  CPPUNIT_ASSERT(GetScaleType() == Fix);
  CPPUNIT_ASSERT(GetNumFixScale() == 2);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(GetAFixScale(1),  0.1, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(GetAFixScale(2),  0.2, TOLERANCE);
  vector<double> fscale = GetFixScale();
  CPPUNIT_ASSERT(fscale.size()  == 2);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(fscale.front(),  0.1, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(fscale.back(),  0.2, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getStateByName("A"), 0.15, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getStateByName("B"), 0.55, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getStateByName("C"), 0, TOLERANCE);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName("k1"), 0.1, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName("k2"), 0.2, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName("k_1"), 2.5, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName("k1_p_k2"), 0, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getParamByName("Default"), 5, TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(Model->getExpTInit(),0.2+0.12, TOLERANCE);

}
void TestChi2Cost::testUpdateScale(){

  Model = new UserModel();
  cModelArg ModelArg;
  Model->setModelArg(ModelArg);
  Model->initModelArg();
  Model->inputModel();


  vector<vector<double> > results;

    /* time */
  vector<double> tmp; 
  for(int i=0; i<10;i++){
   tmp.push_back(0.1*i +0.01);
  }
  results.push_back(tmp);
   
    /*data 1*/
  tmp.clear();
  for(int i=0; i<10;i++){
   tmp.push_back(pow(-2.0,i));
   }
  results.push_back(tmp);
  double max1=pow(-2.0,8);
  double min1=pow(-2.0,9);
  double avg1=4*(1-pow(-2.0,8))/24;
  double absavg1=4*(pow(2.0,8)-1)/8;
  
  /*data 2*/
  tmp.clear();
  for(int i=0; i<10;i++){
   tmp.push_back(pow(-3.0,i));
   }
  results.push_back(tmp);
  double max2=pow(-3.0,8);
  double min2=pow(-3.0,9);
  double avg2=9*(1-pow(-3.0,8))/32;
  double absavg2=9*(pow(3.0,8)-1)/16;

  TimeSeries ts;
  ts.SetTimeSeries(&results);

  SetTimeSeries(&ts);

  DataSet ds;
  ds.AddADataName("Time");
  ds.AddADataName("A");
  ds.AddADataName("B");

  vector<double> zero(3,0);
  vector<double> one(3,1);

  tmp.clear();
  for (int i=1;i<11;i++) tmp.push_back(i*0.1); 
  ds.AddADataValues(tmp);

  for(int k=0; k<2 ;k++){
    for (vector<double>:: iterator i=tmp.begin();i!=tmp.end();i++) *i *= 10.0;
    ds.AddADataValues(tmp);
  }


  dataset=&ds;

  costarg.ExpT0 = 0.1; 
  for(int j=0; j<5 ; j++) {
   Base = zero;
   Scale = one;
   ScaleType type;
   if(j==0) type = None;
   if(j==1) type = MaxMin;
   if(j==2) {type = Fix;
             costarg.FixScale.push_back(0.1);
             costarg.FixScale.push_back(0.2);
             }
   if(j==3) type = Average;
   if(j==4) type = AbsAverage;

   UpdateScale(type);

   if(j==0) {
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Scale.at(1), 1.0, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Scale.back(), 1.0, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Base.at(1), 0.0, TOLERANCE);
   }
   if(j==1){
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Base.at(1), min1, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Base.back(), min2, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Scale.at(1), max1-min1, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Scale.back(), max2-min2, TOLERANCE);
   }
   if(j==2) {
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Scale.at(1), 0.1, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Scale.back(), 0.2, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Base.at(1), 0.0, TOLERANCE);
   }
   if(j==3) {
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Scale.at(1), avg1, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Scale.back(), avg2, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Base.at(1), 0.0, TOLERANCE);
   }
   if(j==4) {
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Scale.at(1), absavg1, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Scale.back(), absavg2, TOLERANCE);
      CPPUNIT_ASSERT_DOUBLES_EQUAL(Base.at(1), 0.0, TOLERANCE);
   }
 
  }
}

void TestChi2Cost::testgetcost() {

    Model = new UserModel();
    cModelArg ModelArg;
    Model->setModelArg(ModelArg);
    Model->initModelArg();
    Model->inputModel();

    Model->setFinal(1.2);

    vector < vector<double> > results;

    /* time */
    vector<double> tmp;
    for (int i = 1; i < 11; i++) {
        tmp.push_back(0.1 * i);
    }
    results.push_back(tmp);

    /*data 1*/
    tmp.clear();
    for (int i = 1; i < 11; i++) {
        tmp.push_back(0.5 * pi * i * sin(0.5 * i * pi));
    }
    results.push_back(tmp);

    /*data 2*/
    tmp.clear();
    for (int i = 1; i < 11; i++) {
        tmp.push_back(0.5 * pi * i * cos(0.5 * i * pi));
    }
    results.push_back(tmp);

    TimeSeries ts;
    ts.SetTimeSeries(&results);

    DataSet ds;
    ds.AddADataName("Time");
    ds.AddADataName("A");
    ds.AddADataName("B");

    vector<double> zero(3, 0);
    vector<double> one(3, 1);

    tmp.clear();
    for (int i = 1; i < 11; i++)
        tmp.push_back(i * 0.1);
    ds.AddADataValues(tmp);

    tmp.clear();
    double sum_diff_squared1 = 0;
    // normalisation factor is square of average (absolute) value
    double norm1 = 0;
    for (int i = 1; i < 11; i++) {
        tmp.push_back(0.25 * pi * i * sin(0.5 * i * pi));
        double diff = tmp.back() - results.at(1).at(i - 1);
        sum_diff_squared1 += diff * diff;
        norm1 += abs(tmp.back());
    }
    norm1 /= 10;     // take the average
    norm1 *= norm1;  // squared
    ds.AddADataValues(tmp);

    tmp.clear();
    double sum_diff_squared2 = 0;
    // normalisation factor is square of average (absolute) value
    double norm2 = 0;
    for (int i = 1; i < 11; i++) {
        tmp.push_back(0.25 * pi * i * cos(0.5 * i * pi));
        double diff = tmp.back() - results.at(2).at(i - 1);
        sum_diff_squared2 += diff * diff;
        norm2 += abs(tmp.back());
    }
    norm2 /= 10;     // take the average
    norm2 *= norm2;  // squared
    ds.AddADataValues(tmp);

    vector < DataSet > DataSets;
    DataSets.push_back(ds);

    vector < string > pname;
    pname.push_back("k1");
    pname.push_back("k2");
    pname.push_back("k_1");

    vector<double> pvalue;
    pvalue.push_back(0.1);
    pvalue.push_back(0.2);
    pvalue.push_back(0.3);

    for (int j = 0; j < 5; j++) {

        vector < CostArg > costargs;
        CostArg costarg;
        costarg.ExpT0 = 0.0; //childDouble(doc, d_node, "DataFileStartTime");
        costarg.OutputInterval = 0.1; //childDouble(doc, d_node, "Interval");
        costarg.costvaluetype = Normed;

        CostArg GlbCostArg;
        GlbCostArg.solvertype = CVODE;
        GlbCostArg.MAXCOST = 50;

        if (j == 0)
            costarg.scaletype = None;
        if (j == 1)
            costarg.scaletype = MaxMin;
        if (j == 2) {
            costarg.scaletype = Fix;
            costarg.FixScale.push_back(2.0);
            costarg.FixScale.push_back(2.0);
        }
        if (j == 3)
            costarg.scaletype = Average;
        if (j == 4)
            costarg.scaletype = AbsAverage;

        costargs.push_back(costarg);

        Init(Model, costargs, pname);

        SetDataSets (&DataSets);

        SetTimeSeries(&ts);

        SetParameters(pvalue);

        SetDataSet(0);

        double c = getcost();

        if (j == 0) {
            double EXPECTED = ((sum_diff_squared1 / 10.0) / norm1) +
                              ((sum_diff_squared2 / 10.0) / norm2);

            CPPUNIT_ASSERT_DOUBLES_EQUAL(EXPECTED, c, TOLERANCE * 2);
        }

        // TODO: Not currently testing the other scale types
    }
}

