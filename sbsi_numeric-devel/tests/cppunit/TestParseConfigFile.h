
/*****************************************************************************
 *
 *  TestParseConfigFile.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <ParseConfigFile.h>

#ifndef TESTParseConfigFile_H
#define TESTParseConfigFile_H

class TestParseConfigFile : public CppUnit::TestFixture{

 public:

  TestParseConfigFile(){};
  virtual ~TestParseConfigFile(){};

  void setUp(void);
  void tearDown(void);
  void testParseConfig();
  void testparseSetup();
  void testparseOptimiser();
  void testparseCostFunctions();
  void testparseFFT();
  void testparseState();
  void testparseX2Cost();
  void testparseSolver();
  void testparseResults();
  void testloadData();

  CPPUNIT_TEST_SUITE(TestParseConfigFile);
  CPPUNIT_TEST_SUITE_END();


};

#endif

