
/*****************************************************************************
 *
 *  TestFileReader.cpp
 *
 *  See TestFileReader.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include <vector>
#include <string>

#include <cModelArg.h>
#include <cModel.h>
#include <SBMLModel.h>
#include <SBMLModel.h>
#include "TestFileReader.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestFileReader);

void TestFileReader::setUp(void) {
 return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestFileReader::tearDown(void) {
  return;
}

void TestFileReader::testClearSets(){
  ClearSets();
  CPPUNIT_ASSERT(Sets.size() == 0);
}
 /* this is for param Info */
/*
void TestFileReader::testSetModelArg(){

    cModel *umodel=new SBMLModel();
    cModelArg ModelArg;
    umodel->setModelArg(ModelArg);
    umodel->initModelArg();

    FileReaderType frt=TabDelimitedColumnData;
    FileReader &file_reader=SetFileReader(frt);
    file_reader.SetModelArg(ModelArg);

    CPPUNIT_ASSERT(file_reader.ModelArg->ParamNames.size() == 0);
    CPPUNIT_ASSERT(file_reader.ModelArg->StateNames.size() == 0);
    CPPUNIT_ASSERT(file_reader.ModelArg->VarNames.size() == 0);
    CPPUNIT_ASSERT(file_reader.ModelArg->States.size() == 0);
}
*/
 /* this is for hdr file reader  */
void TestFileReader::testSetDataSet(){
     FileReaderType frt=TabDelimitedColumnHeader;
     //FileReader &file_reader=FileReader::SetFileReader(frt);
}

void TestFileReader::testSetFileReader(){

     FileReaderType frt=TabDelimitedColumnData;
     FileReader &tdfile_reader=FileReader::SetFileReader(frt);
     CPPUNIT_ASSERT(&tdfile_reader != NULL);

     frt=TabDelimitedColumnPop;
     FileReader &popfile_reader=FileReader::SetFileReader(frt);
     CPPUNIT_ASSERT(&popfile_reader != NULL);

     frt=TabDelimitedColumnParamInfo;
     FileReader &pifile_reader=FileReader::SetFileReader(frt);
     CPPUNIT_ASSERT(&pifile_reader != NULL);

     frt=TabDelimitedColumnHeader;
     FileReader &hdrfile_reader=FileReader::SetFileReader(frt);
     CPPUNIT_ASSERT(&hdrfile_reader != NULL);
}
void TestFileReader::testGetLine(){
      fstream f;
      string filename="CppunitTestData/ABC.dat";
      f.open(filename.c_str(),ios::in);
      CPPUNIT_ASSERT(f != false);
      string line = GetLine(f);
      string EXPECTED="@!ABC.hdr";
      CPPUNIT_ASSERT_EQUAL(EXPECTED,line);
      /* testTokenize ()*/
      line = GetLine(f);
      SetTokenDelimiters("\t");
      SetWhitespaces(" \r");
      vector<string> columns=Tokenize(line);
      CPPUNIT_ASSERT(columns.size() == 4);
}
void TestFileReader::testIsDelimiter(){
      SetTokenDelimiters("\t");
      const char *c= "\t";
      CPPUNIT_ASSERT_EQUAL(true, IsDelimiter(*c));
      c="A";
      CPPUNIT_ASSERT_EQUAL(false,IsDelimiter(*c));
}
void TestFileReader::testIsWhitespace(){
      SetWhitespaces(" \r");
      const char *c = " \r";
      CPPUNIT_ASSERT_EQUAL(true, IsWhitespace(*c));
      c="A";
      CPPUNIT_ASSERT_EQUAL(false,IsWhitespace(*c));
}
void TestFileReader::testIsWord(){
      string str="ABC";
      CPPUNIT_ASSERT_EQUAL(true, IsWord(str));
      str="200";
      CPPUNIT_ASSERT_EQUAL(false, IsWord(str));
      str="20.0";
      CPPUNIT_ASSERT_EQUAL(false, IsWord(str));
}
void TestFileReader::testIsNum(){
      string str="ABC";
      CPPUNIT_ASSERT_EQUAL(false,IsNum(str));
      str="200";
      CPPUNIT_ASSERT_EQUAL(true,IsNum(str));
      str="20.0";
      CPPUNIT_ASSERT_EQUAL(true,IsNum(str));
}
void TestFileReader::testToDouble(){
      string str="20.0";
      CPPUNIT_ASSERT_DOUBLES_EQUAL(ToDouble(str),20.0,TOLERANCE);
}
void TestFileReader::testStripWhitespace(){
     SetWhitespaces(" \r");
     string str="ABC DEF GHI JKL";
     string EXPECTED="ABCDEFGHIJKL";
     CPPUNIT_ASSERT_EQUAL(EXPECTED,StripWhitespace(str));
     str="ABC \rDEF GHI JKL";
     EXPECTED="ABCDEFGHIJKL";
     CPPUNIT_ASSERT_EQUAL(EXPECTED,StripWhitespace(str));
}
void TestFileReader::testSetTokenDelimiters(){
     SetTokenDelimiters("\t");
     const char *c = "\t";
     CPPUNIT_ASSERT_EQUAL(true, IsDelimiter(*c));
     c=" ";
     CPPUNIT_ASSERT_EQUAL(false, IsDelimiter(*c));
     SetTokenDelimiters(" ");
     c="\t";
     CPPUNIT_ASSERT_EQUAL(false, IsDelimiter(*c));
     c=" ";
     CPPUNIT_ASSERT_EQUAL(true, IsDelimiter(*c));
}
