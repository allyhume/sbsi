/*****************************************************************************
 *
 *  TestDataSet.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <DataSet.h>

#ifndef TESTDataSet_H
#define TESTDataSet_H

class TestDataSet : public CppUnit::TestFixture {

 public:

  TestDataSet(){};
  virtual ~TestDataSet(){};

  void setUp(void);
  void tearDown(void);

  void testDataSet();
  void testSetName();
  void testDataNames();
  void testInitialStateNames();
  void testInitialStateValues();
  void testInitialParamNames();
  void testInitialParamValues();
  void testDataValues();

  CPPUNIT_TEST_SUITE(TestDataSet);
  CPPUNIT_TEST(testDataSet);
  CPPUNIT_TEST(testSetName);
  CPPUNIT_TEST(testDataNames);
  CPPUNIT_TEST(testInitialStateNames);
  CPPUNIT_TEST(testInitialStateValues);
  CPPUNIT_TEST(testInitialParamNames);
  CPPUNIT_TEST(testInitialParamValues);
  CPPUNIT_TEST(testDataValues);
  CPPUNIT_TEST_SUITE_END();

};

#endif

