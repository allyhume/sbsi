/*****************************************************************************
 *
 *  TestTDCFileReader.cpp
 *
 *  See TestTDCFileReader.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestTDCFileReader.h"

#define TOLERANCE DBL_EPSILON

/*****************************************************************************
 *
 *  Constructor
 *
 *****************************************************************************/

TestTDCFileReader::TestTDCFileReader() {
}

/*****************************************************************************
 *
 *  Destructor
 *
 *****************************************************************************/

TestTDCFileReader::~TestTDCFileReader() {
}

/*****************************************************************************
 *
 *  Register the tests
 *
 *****************************************************************************/

CPPUNIT_TEST_SUITE_REGISTRATION(TestTDCFileReader);


void TestTDCFileReader::testTDCFileReader() {

 FileReaderType expfilereadtype=TabDelimitedColumnData;
 FileReader &file_reader=FileReader::SetFileReader(expfilereadtype);
 string filename="CppunitTestData/ABC.dat";
 file_reader.Read(filename);

 CPPUNIT_ASSERT(file_reader.Sets.size() == 1);

 DataSet dataset = file_reader.Sets.front();
 size_t size = dataset.GetNumDataValues();

 CPPUNIT_ASSERT(size == 4);

 CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, dataset.GetADataTime(0), TOLERANCE);
 CPPUNIT_ASSERT_DOUBLES_EQUAL(10.0, dataset.GetEndDataTime(), TOLERANCE);

 CPPUNIT_ASSERT_DOUBLES_EQUAL(0.000001, dataset.GetADataValue(size-1,0), TOLERANCE);
 CPPUNIT_ASSERT_DOUBLES_EQUAL(0.974315743, dataset.GetAEndDataValue(size-1), TOLERANCE);

 string EXPECTED="ABC.hdr";
 CPPUNIT_ASSERT_EQUAL(EXPECTED,dataset.GetSetName());

  return;
}

