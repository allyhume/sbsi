
/*****************************************************************************
 *
 *  TestSolver.cpp
 *
 *  See TestSolver.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include <CostArg.h>
#include "TestSolver.h"
#include "CppunitTestData/UserModel/UserModel.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestSolver);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestSolver::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestSolver::tearDown(void) {

  return;
}

void TestSolver::testSetSolver(){

 SolverType type=CVODE;
 Solver &solver=SetSolver(type);
 CPPUNIT_ASSERT(&solver != NULL);
  
}
void TestSolver::testSetModel(){
 cModel *umodel = new UserModel();
 SetModel(umodel);
 CPPUNIT_ASSERT(umodel == model);
  
}
void TestSolver::testSetTimeSeries(){
 TimeSeries ts;
 SetTimeSeries(&ts);
 CPPUNIT_ASSERT(&ts == t_series);
  
}

void TestSolver::testCheckFlag(){
  int flag(0);

/* case opt ==0 memory check */
  void *mem;
  mem=NULL;
  CPPUNIT_ASSERT(CheckFlag(mem,"Checkflag case 1 ",0) == 1); // error
/* case opt ==1 flag check */
  flag=0;
  CPPUNIT_ASSERT(CheckFlag(&flag,"Checkflag case 2",1) == 0); // OK
  flag=-4;
  CPPUNIT_ASSERT(CheckFlag(&flag,"Checkflag case 3",1) == 1); // error
/* case opt ==2 flag check */
  CPPUNIT_ASSERT(CheckFlag(mem,"Checkflag case 4",2) == 1); // error

}

