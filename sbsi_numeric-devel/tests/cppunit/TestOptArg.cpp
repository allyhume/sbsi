/*****************************************************************************
 *
 *  TestOptArg.cpp
 *
 *  See TestOptArg.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestOptArg.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestOptArg);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestOptArg::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestOptArg::tearDown(void) {

  return;
}

void TestOptArg::testSobolSeed(){
  setSobolSeed(101);
  int seed = getSobolSeed();
  CPPUNIT_ASSERT(101 == seed);
}
void TestOptArg::testOptimiserType(){
  OptimiserType otype=PGA;
  setOptimiserType(otype);
  CPPUNIT_ASSERT(otype == getOptimiserType());
}
void TestOptArg::testSetupType(){
  SetupType otype=SobolSelect;
  setSetupType(otype);
  CPPUNIT_ASSERT(otype == getSetupType());
}
void TestOptArg::testSeed(){
  setSeed(101);
  CPPUNIT_ASSERT(101 == getSeed());
}
void TestOptArg::testNumGen(){
  setNumGen(10);
  CPPUNIT_ASSERT(10 == getNumGen());
}
void TestOptArg::testNumBest(){
  setNumBest(2);
  CPPUNIT_ASSERT(2 == getNumBest());
}
void TestOptArg::testPopSize(){
  setPopSize(2);
  CPPUNIT_ASSERT(2 == getPopSize());
}
void TestOptArg::testMaxNoChange(){
  setMaxNoChange(2);
  CPPUNIT_ASSERT(2 == getMaxNoChange());
}
void TestOptArg::testMinCost(){
  setMinCost(0.0010);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.001,getMinCost(),TOLERANCE);
}
void TestOptArg::testMaxSimilarity(){
  setMaxSimilarity(95);
  CPPUNIT_ASSERT(95==getMaxSimilarity());
}
void TestOptArg::testCkpointFreq(){
  setCkpointFreq(10);
  CPPUNIT_ASSERT(10==getCkpointFreq());
}
void TestOptArg::testCkpointStartNum(){
  setCkpointStartNum(95);
  CPPUNIT_ASSERT(95==getCkpointStartNum());
}
void TestOptArg::testPrintFreq(){
  setPrintFreq(5);
  CPPUNIT_ASSERT(5==getPrintFreq());
}
void TestOptArg::testRestartFreq(){
  setRestartFreq(5);
  CPPUNIT_ASSERT(5==getRestartFreq());
}
void TestOptArg::testMutProb(){
 setMutProb(0.9);
 CPPUNIT_ASSERT_DOUBLES_EQUAL(0.9,getMutProb(), TOLERANCE);
}
void TestOptArg::testVerbose(){
  setVerbose(2);
  CPPUNIT_ASSERT(2==getVerbose());
}
void TestOptArg::testPrintBestPop(){
  setPrintBestPop(false);
  CPPUNIT_ASSERT(false ==getPrintBestPop());
}
void TestOptArg::testResultDir(){
 string dir="DIR";
 CPPUNIT_ASSERT_EQUAL(dir, getResultDir());
}
void TestOptArg::testCkpointDir(){
 string dir="DIR";
 CPPUNIT_ASSERT_EQUAL(dir, getCkpointDir());
}
void TestOptArg::testMu(){
 setMu(0.9);
 CPPUNIT_ASSERT_DOUBLES_EQUAL(0.9,getMu(), TOLERANCE);
}
