/*****************************************************************************
 *
 *  TestSobol64Bit.h
 *
 *  These tests are based on the tabulated values produced by
 *  the Fortran implementation of John Burkardt. See
 *
 *  http://www.scs.fsu.edu/~burkardt/cpp.src/sobol/sobol.html
 *
 *  $Id: TestSobol64Bit.h,v 1.1.2.1 2010/05/20 14:36:25 ayamaguc Exp $
 *
 *  Millar Laboratory, Centre for Systems Biology at Edinburgh
 *  (c) The University of Edinburgh (2009)
 *  Kevin Stratford (kevin@epcc.ed.ac.uk) 
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <Sobol64Bit.h>

#ifndef TESTSOBOL64BIT_H
#define TESTSOBOL64BIT_H

class TestSobol64Bit : public CppUnit::TestFixture {

 public:

  TestSobol64Bit();
  virtual ~TestSobol64Bit();

  void setUp(void);
  void tearDown(void);

  void testLowZeroBit(void);
  void testDimension2(void);
  void testDimension3(void);
  void testDimension500(void);

  CPPUNIT_TEST_SUITE(TestSobol64Bit);
  CPPUNIT_TEST(testLowZeroBit);
  CPPUNIT_TEST(testDimension2);
  CPPUNIT_TEST(testDimension3);
  CPPUNIT_TEST(testDimension500);
  CPPUNIT_TEST_SUITE_END();

};

#endif

