
/*****************************************************************************
 *
 *  TestFFTCost.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <FFTCost.h>

#ifndef TESTFFTCOST_H
#define TESTFFTCOST_H

class TestFFTCost : public CppUnit::TestFixture, public FFTCost {

 public:

  TestFFTCost(){};
  virtual ~TestFFTCost(){};

  void setUp(void);
  void tearDown(void);

  void testSetDataSet();
  void testCostUsingPeriodOnly();
  void testCostUsingPeriodAndAmpFncFunctional();
  void testCostUsingPeriodAndAmpFncMax();
  void testCostUsingPeriodAndAmpFncFunctionalNone();

  CPPUNIT_TEST_SUITE(TestFFTCost);
  CPPUNIT_TEST(testSetDataSet);
  CPPUNIT_TEST(testCostUsingPeriodOnly);
  CPPUNIT_TEST(testCostUsingPeriodAndAmpFncFunctional);
  CPPUNIT_TEST(testCostUsingPeriodAndAmpFncMax);
  CPPUNIT_TEST(testCostUsingPeriodAndAmpFncFunctionalNone);
  CPPUNIT_TEST_SUITE_END();

 private:

  double calculateCost(
          double targetPeriod,
          AmpFncType costFunctionType,
          double duration,
          double interval,
          double period,
          double amp,
          double offset,
          double dampening);

  vector<double> createPeriodVector(
          int numValues, double interval, double period,
          double amp, double offset, double dampening);

  vector<double> createTimeVector(int numValues, double interval);

  double calcCoefficientOfVariation(
          int numValues, double interval, double period,
          double amp, double offset, double dampening);

};

#endif

