
/*****************************************************************************
 *
 *  TestFFTCost.cpp
 *
 *  See TestFFTCost.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include <math.h>
#include <cModel.h>
#include <CostArg.h>
#include "TestFFTCost.h"
#include "CppunitTestData/UserModel/UserModel.h"

#define TOLERANCE DBL_EPSILON

#define pi M_PI


CPPUNIT_TEST_SUITE_REGISTRATION(TestFFTCost);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestFFTCost::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestFFTCost::tearDown(void) {

  return;
}

void TestFFTCost::testSetDataSet(){

  cModel *Model = new UserModel();
  cModelArg ModelArg;
  Model->setModelArg(ModelArg);
  Model->initModelArg();
  Model->inputModel();
  Model->setFinal(100);
  Model->setInit(0);
  Model->setInterval(0.1);
  Model->setMaxTimes(10000);
  Model->setAtol(1.0e-14);
  Model->setReltol(1.0e-04);


  vector<DataSet> datasets;

  DataSet dataset;
  dataset.SetSetName("example1");
  datasets.push_back(dataset);

  vector<CostArg> costargs;

  CostArg costarg;
  costarg.TStart.push_back(1.0);
  costarg.TStart.push_back(1.0);
  costarg.TStart.push_back(0.5);

  costarg.TEnd.push_back(15.0);
  costarg.TEnd.push_back(15.0);
  costarg.TEnd.push_back(90.0);

  costarg.FFTTarget.push_back("A");
  costarg.FFTTarget.push_back("B");
  costarg.FFTTarget.push_back("C");

  costarg.Period.push_back(1.0);
  costarg.Period.push_back(2.0);
  costarg.Period.push_back(3.0);

  costarg.MaxAmplitude.push_back(1.0e+08);
  costarg.MaxAmplitude.push_back(1.0e+08);
  costarg.MaxAmplitude.push_back(1.0e+08);

  costarg.MinAmplitude.push_back(1.0e-08);
  costarg.MinAmplitude.push_back(1.0e-08);
  costarg.MinAmplitude.push_back(1.0e-08);

  // Start with AmpFncNone - later on we will need to test more
  costarg.AmpFnc.push_back(AmpFncNone);
  costarg.AmpFnc.push_back(AmpFncNone);
  costarg.AmpFnc.push_back(AmpFncNone);

  costarg.OutputInterval=0.1;
  costargs.push_back(costarg);

  vector<string> pname;
  pname.push_back("k1");
  pname.push_back("k2");
  pname.push_back("k_1");

  vector<double> pvalue;
  pvalue.push_back(0.1);
  pvalue.push_back(0.2);
  pvalue.push_back(0.3);

  Init(Model, costargs, pname);
  SetParameters(pvalue);
  SetDataSets(&datasets);

  SetDataSet(0);

  CPPUNIT_ASSERT(GetNumFFTTarget() == 3);

  vector<string> target = GetFFTTargets();
  string EXPECTED="A";
  CPPUNIT_ASSERT_EQUAL(EXPECTED, target.front());
  EXPECTED="C";
  CPPUNIT_ASSERT_EQUAL(EXPECTED, target.back());
  EXPECTED="B";
  CPPUNIT_ASSERT_EQUAL(EXPECTED, GetAFFTTarget(1));

  vector<double> periods = GetFFTPeriods();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(1, periods.front(),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(3, periods.back(),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(2, GetAFFTPeriod(1),TOLERANCE);

  vector<vector<double> > results;

    // time 
  int datanum(1000);
  vector<double> tmp;
  for(int i=1; i<datanum;i++){
   tmp.push_back(0.1*i);
  }
  results.push_back(tmp);

  //data 1
  tmp.clear();
  for(int i=1; i<datanum;i++){
    tmp.push_back(0.1*i*pi*sin(0.1*i*pi*2)); // T=1
  }
  results.push_back(tmp);

  //data 2
  tmp.clear();
  for(int i=1; i<datanum;i++){
    tmp.push_back(0.1*i*pi*cos((0.1*i+0.1)*pi)); //T=2
   }
  results.push_back(tmp);


  //data 2
  tmp.clear();
  for(int i=1; i<datanum;i++){
    tmp.push_back(0.5*pi*i*cos(0.1*i*pi*2/3)); //T=3
   }
  results.push_back(tmp);

  TimeSeries ts;
  ts.SetTimeSeries(&results);
  SetTimeSeries(&ts);

  CPPUNIT_ASSERT(&ts == TSeries);
  
  getcost();

  CPPUNIT_ASSERT_DOUBLES_EQUAL(GetAPeriod(0),1.0, 0.1);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(GetAPeriod(1),2.0, 0.1);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(GetAPeriod(2),3.0, 0.1);
}


/**
 * Tests calculating cost using only the difference between the target and
 * measured periods.  Creates a time series with period 24 and calculates
 * a FFT cost with a target period of 24.5.  The cost should therefore be
 * (24.5-24.0)^2 = 0.25.
 *
 * Note that we never measure the period with full accuracy this can make
 * the result significantly different from the theortical one. For example,
 * if we measure the period as 24.14 rather than 24.0 then we cost is
 * (24.5-24.14)^2 = 0.1296. Hence we need a reasonable tollerance on this
 * test.
 */
void TestFFTCost::testCostUsingPeriodOnly(){

    // User specified target period - cost is low when measured period is
    // close to this target
    double targetPeriod = 24.5;
    AmpFncType costFunctionType = AmpFncNone;  // Use period only
    double duration = 90.0;             // 90 hours
    double interval = 0.1;              // 6 min intervals
    double period = 24.0;               // time series period
    double amp = 0.1;                   // time series amplitude
    double offset = 0.35;               // time series offset
    double dampening = 0;               // dampening factor

    // Get the cost
    double cost = calculateCost(
            targetPeriod,
            costFunctionType,
            duration,
            interval,
            period,
            amp,
            offset,
            dampening);

    double expectedCost = pow( targetPeriod-period, 2.0);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(expectedCost, cost, 0.2);
}


/**
 * Tests calculating cost period differences and amplitude based
 * weigthing that applies a factor of 1 / sqrt(CV) where
 * CV is the coefficient of variation.
 */
void TestFFTCost::testCostUsingPeriodAndAmpFncFunctional(){

    // User specified target period - cost is low when measured period is
    // close to this target
    double targetPeriod = 24.5;
    AmpFncType costFunctionType = AmpFncFunctional;
    double duration = 90.0;             // 90 hours
    double interval = 0.1;              // 6 min intervals
    double period = 24.0;               // time series period
    double amp = 0.1;                   // time series amplitude
    double offset = 0.35;               // time series offset
    double dampening = 0;               // dampening factor

    // Get cost from the period - other tests will test this and
    // it requires a high tolerance. To check that the rest of
    // the calculation is correct we accept the period based cost
    // as being accurate an ensure given this that the code does
    // the correct thing after that.
    double periodBasedCost = calculateCost(
            targetPeriod,
            AmpFncNone,
            duration,
            interval,
            period,
            amp,
            offset,
            dampening);

    // Get the cost
    double cost = calculateCost(
            targetPeriod,
            costFunctionType,
            duration,
            interval,
            period,
            amp,
            offset,
            dampening);

    double coeffOfVariation = calcCoefficientOfVariation(
            duration/interval, interval, period, amp, offset, dampening);

    double expectedCost = periodBasedCost;
    expectedCost *= 1.0/sqrt(coeffOfVariation);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(expectedCost, cost, 0.0001);
}

/**
 * Tests calculating cost period differences and amplitude based
 * weigthing that applies a factor of 1 / sqrt(1 / log(CV+1)) where
 * CV is the coefficient of variation.
 */
void TestFFTCost::testCostUsingPeriodAndAmpFncMax(){

    // User specified target period - cost is low when measured period is
    // close to this target
    double targetPeriod = 24.5;
    AmpFncType costFunctionType = AmpFncMax;
    double duration = 90.0;             // 90 hours
    double interval = 0.1;              // 6 min intervals
    double period = 24.0;               // time series period
    double amp = 0.1;                   // time series amplitude
    double offset = 0.35;               // time series offset
    double dampening = 0;               // dampening factor

    // Get cost from the period - other tests will test this and
    // it requires a high tolerance. To check that the rest of
    // the calculation is correct we accept the period based cost
    // as being accurate an ensure given this that the code does
    // the correct thing after that.
    double periodBasedCost = calculateCost(
            targetPeriod,
            AmpFncNone,
            duration,
            interval,
            period,
            amp,
            offset,
            dampening);

    // Get the cost
    double cost = calculateCost(
            targetPeriod,
            costFunctionType,
            duration,
            interval,
            period,
            amp,
            offset,
            dampening);

    double coeffOfVariation = calcCoefficientOfVariation(
            duration/interval, interval, period, amp, offset, dampening);

    double expectedCost = periodBasedCost;
    expectedCost *= 1.0/sqrt(log(coeffOfVariation + 1.0));

    CPPUNIT_ASSERT_DOUBLES_EQUAL(expectedCost, cost, 0.0001);
}

/**
 * Tests calculating cost period differences and amplitude based
 * weigthing that applies a factor of 1 + 1 / sqrt(CV) where
 * CV is the coefficient of variation.
 */
void TestFFTCost::testCostUsingPeriodAndAmpFncFunctionalNone(){

    // User specified target period - cost is low when measured period is
    // close to this target
    double targetPeriod = 24.5;
    AmpFncType costFunctionType = AmpFncFunctionalNone;
    double duration = 90.0;             // 90 hours
    double interval = 0.1;              // 6 min intervals
    double period = 24.0;               // time series period
    double amp = 0.1;                   // time series amplitude
    double offset = 0.35;               // time series offset
    double dampening = 0;               // dampening factor

    // Get cost from the period - other tests will test this and
    // it requires a high tolerance. To check that the rest of
    // the calculation is correct we accept the period based cost
    // as being accurate an ensure given this that the code does
    // the correct thing after that.
    double periodBasedCost = calculateCost(
            targetPeriod,
            AmpFncNone,
            duration,
            interval,
            period,
            amp,
            offset,
            dampening);

    // Get the cost
    double cost = calculateCost(
            targetPeriod,
            costFunctionType,
            duration,
            interval,
            period,
            amp,
            offset,
            dampening);

    double coeffOfVariation = calcCoefficientOfVariation(
            duration/interval, interval, period, amp, offset, dampening);

    double expectedCost = periodBasedCost;
    expectedCost *= 1.0 + 1.0/sqrt(coeffOfVariation);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(expectedCost, cost, 0.0001);
}

/**
 * Calculates the FFTCost by creating a time series graph with a specified
 * period, amplitude, offset and dampening. The FFTCost is then calculated
 * against this graph using the specified target period and cost function
 * type.
 *
 * @param targetPeriod       the target period - input to cost function
 * @param costFunctionType   cost function type - input to cost function
 * @param duration           duration of the time series
 * @param interval           interval of the time series
 * @param period             period of the time series
 * @param amp                amplitude of the time series
 * @param offset             offset of the time series (in y dimension)
 * @param dampening          time series is dampened at time t by factor of (1-t*dampening)
 *
 * @return the FFT cost.
 */
double TestFFTCost::calculateCost(
        double targetPeriod,
        AmpFncType costFunctionType,
        double duration,
        double interval,
        double period,
        double amp,
        double offset,
        double dampening
        ) {

    // Create a mode to get the initial and final times and interval etc.
    cModel *Model = new UserModel();
    cModelArg ModelArg;
    Model->setModelArg(ModelArg);
    Model->initModelArg();
    Model->inputModel();
    Model->setFinal(duration);
    Model->setInit(0);
    Model->setInterval(interval);

    // Create the arguments used to calculate FFT cost
    vector<CostArg> costargs;
    CostArg costarg;
    costarg.TStart.push_back(interval);
    costarg.TEnd.push_back(duration);
    costarg.FFTTarget.push_back("A");
    costarg.Period.push_back(targetPeriod);
    costarg.MaxAmplitude.push_back(1.0e+08);
    costarg.MinAmplitude.push_back(1.0e-08);
    costarg.AmpFnc.push_back(costFunctionType);
    costarg.OutputInterval=0.1;
    costargs.push_back(costarg);

    // Create some dummy parameters - I don't think these are used
    vector<string> pname;
    pname.push_back("k1");
    vector<double> pvalue;
    pvalue.push_back(0.1);

    // Create a data set. FFTCost does not use any observed data so we
    // are just setting one empty data set just now
    vector<DataSet> datasets;
    DataSet dataset;
    dataset.SetSetName("emptyDataSet");
    datasets.push_back(dataset);

    // Initialise the model and set parameters and data sets. Note that
    // this test class inherits from the FFTCost class
    Init(Model, costargs, pname);
    SetParameters(pvalue);
    SetDataSets(&datasets);
    SetDataSet(0);

    // Now we create some time series data
    vector<vector<double> > results;

    int numValues = duration/interval;
    results.push_back(createTimeVector(numValues, interval));

    results.push_back(createPeriodVector(
            numValues, interval, period, amp, offset, dampening ));

    // Configure FFTCost for the time series
    TimeSeries ts;
    ts.SetTimeSeries(&results);
    SetTimeSeries(&ts);

    // Get the cost
    return getcost();
}

/**
 * Create a vector of time values starting from interval and incrementing by interval
 * for the specified number of values.
 *
 * @param numValues  number of values to include in the result vector
 * @param interval   interval between each value in the result vector
 *
 * @return vector of incrementing time values
 */
vector<double> TestFFTCost::createTimeVector(int numValues, double interval) {

    vector<double> result;
    for(int i=1; i<=numValues;i++){
        result.push_back(interval*i);
    }
    return result;
}

/**
 * Create a vector of values of a curve with a given period.
 *
 * @param numValues  number of values to include in the result vector
 * @param interval   time interval between each value in the result vector.
 * @param amp        peak amplitude
 * @param offset     offset of each value from zero
 * @param dampening  dampens the signal by a factor of (1-t*dampening)
 *
 * @return vector containing a curve of the specified period, amplitude and offset.
 */
vector<double> TestFFTCost::createPeriodVector(
        int numValues, double interval, double period,
        double amp, double offset, double dampening) {
    vector<double> result;
    for(int i=1; i<=numValues;i++){
        double t = interval*i;
        result.push_back((1-t*dampening) * amp * sin(t/period * 2.0 * pi) + offset);
    }
    return result;
}

/**
 * Calculates the coefficient of variation for a time series with the
 * given characteristics. The coefficient of variation is defined as
 * standard_deviation/mean.
 *
 * @param numValues  number of values to include in the time series
 * @param interval   time interval between each value in the time series
 * @param amp        peak amplitude
 * @param offset     offset of each value from zero
 * @param dampening  dampens the signal by a factor of (1-t*dampening)
 *
 * @return coefficient of variation for the time series.
 */
double TestFFTCost::calcCoefficientOfVariation(
        int numValues, double interval, double period,
        double amp, double offset, double dampening) {
    double total = 0;
    double count = 0;
    vector<double> data;

    for(int i=1; i<=numValues;i++){
        double t = interval*i;
        data.push_back((1-t*dampening) * amp * sin(t/period * 2.0 * pi) + offset);
        total += data.back();
        count += 1.0;
    }
    double mean = total/count;

    double std_dev = 0.0;
    for (int i=0; i<data.size(); i++){
        double diff = data.at(i) - mean;
        std_dev += diff*diff;
    }
    std_dev = std_dev / (data.size()-1);
    std_dev = sqrt(std_dev);

    return std_dev/mean;
}
