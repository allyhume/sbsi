/*****************************************************************************
 *
 *  TestTDCFileReader.h
 *
 *  CPP Unit Tests for Tab delimeter file readers.
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <FileReader.h>

#ifndef TESTFILEREADER_H
#define TESTFILEREADER_H

class TestTDCFileReader : public CppUnit::TestFixture {

 public:

  TestTDCFileReader();
  virtual ~TestTDCFileReader();

  void setUp(void){}; // Do nothing
  void tearDown(void){};

  void testTDCFileReader(void);   
  void testTDCHeaderFileReader(void); 
  void testTDCPopFileReader(void);   
  void testTDCParamInfoFileReader(void);   

  CPPUNIT_TEST_SUITE(TestTDCFileReader);
  CPPUNIT_TEST(testTDCHeaderFileReader);
  CPPUNIT_TEST(testTDCFileReader);
  CPPUNIT_TEST(testTDCPopFileReader);
  CPPUNIT_TEST(testTDCParamInfoFileReader);
  CPPUNIT_TEST_SUITE_END();

};

#endif

