/*****************************************************************************
 *
 *  TestDeCodeSBML.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>
#ifndef TESTDECODESBML_H

#define TESTDECODESBML_H
#include <DeCodeSBML.h>
#include <sbml/SBMLReader.h>


class TestDeCodeSBML : public CppUnit::TestFixture, public DeCodeSBML {

 public:

	 TestDeCodeSBML();
  virtual ~TestDeCodeSBML();

  void setUp(void);
  void tearDown(void);

  void testCheckParameter(void);

  void testCheckParametersNestedInKineticLaws(void);
 
	void testSetConstraint(void);
	
	void testPrintFunctionDefinition(void);
	
	void testPrintInvalidFunctionDefinitionWithNoMathPrintsNothing (void);
	void testPrintAssignmentRule(void);
	void testPrintAlgebraicRule(void);
	void testPrintRateRule(void) ;
	void testPrintReactionMathHandleError(void) ;
	void testPrintReactionMathHandleErrorIfKineticLawHasNoMath(void);
	void testPrintReactionMathUsingVderPolModel(void);
	void testPrintEventMathUsingBioMD55 (void);
	void testhandleNoEventsPrintsNothing (void);

  CPPUNIT_TEST_SUITE(TestDeCodeSBML);
	//CPPUNIT_TEST(testCheckParameter);
	CPPUNIT_TEST(testCheckParametersNestedInKineticLaws);
	CPPUNIT_TEST(testSetConstraint);
	CPPUNIT_TEST(testPrintFunctionDefinition);
	CPPUNIT_TEST(testPrintInvalidFunctionDefinitionWithNoMathPrintsNothing);
	CPPUNIT_TEST(testPrintAssignmentRule); 
        CPPUNIT_TEST(testPrintAlgebraicRule);
	CPPUNIT_TEST(testPrintRateRule);
	CPPUNIT_TEST(testPrintReactionMathHandleError);
	CPPUNIT_TEST(testPrintReactionMathHandleErrorIfKineticLawHasNoMath);
	CPPUNIT_TEST(testPrintReactionMathUsingVderPolModel);
	CPPUNIT_TEST(testPrintEventMathUsingBioMD55);
	CPPUNIT_TEST(testhandleNoEventsPrintsNothing);
  CPPUNIT_TEST_SUITE_END();
	
	private :
	cModel * getParsedModel();

};

#endif

