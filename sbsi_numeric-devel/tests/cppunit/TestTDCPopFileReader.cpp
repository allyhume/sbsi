
/*****************************************************************************
 *
 *  TestFileReadPop.cpp
 *
 *  See TestFileReadPop.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestTDCFileReader.h"

#define TOLERANCE DBL_EPSILON

void TestTDCFileReader::testTDCPopFileReader(void) {

    string filename="CppunitTestData/ABCPop.dat";
    FileReader &fr= FileReader::SetFileReader(TabDelimitedColumnPop);
    fr.Read(filename);

    CPPUNIT_ASSERT(fr.PopSets.size() == 12);
    CPPUNIT_ASSERT(fr.PopSets.at(0).params.size() == 3);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(fr.PopSets.at(11).params.front(),1.23529082285965,TOLERANCE);
    return;
}

