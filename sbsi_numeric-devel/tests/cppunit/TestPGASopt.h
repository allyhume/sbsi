
/*****************************************************************************
 *
 *  TestPGASopt.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <PGASopt.h>

#ifndef TESTPGASopt_H
#define TESTPGASopt_H

class TestPGASopt : public CppUnit::TestFixture {

 public:

  TestPGASopt();
  virtual ~TestPGASopt();

  void setUp(void);
  void tearDown(void);


  CPPUNIT_TEST_SUITE(TestPGASopt);
  CPPUNIT_TEST_SUITE_END();

};

#endif

  void testPGAPSopt();
  void testPGAPSopt();
  void testPGAPSopt_Run();
  void testRun();
  void testInitialise();
  void testendOfGen();
  void testsetPSOGenerationNumber();
  void testDestroy();
  void testDone();
  void testCreateNewGeneration();
  void testCreateBestGeneration();
