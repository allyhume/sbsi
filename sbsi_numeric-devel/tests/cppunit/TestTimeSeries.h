/*****************************************************************************
 *
 *  TestTimeSeries.h
 *
 *  Unit tests for TimeSeries class.

 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>
#ifndef TESTTIMESERIES_H

#define TESTTIMESERIES_H
#include "TimeSeries.h"


class TestTimeSeries : public CppUnit::TestFixture, public TimeSeries {

 public:

	 TestTimeSeries(){};
  virtual ~TestTimeSeries(){};

  void setUp(void);
  void tearDown(void);

  void testSetModel();
  void testSetTimeSeries();
  void testIsTimeSeriesEmpty();
  void testGetTimeSeries();
  void testWriteToSBSIDataFormat();
  void testWriteHeaderFile();
  void testCreateHeaderFileName();
  void testCreateColumnHeaderLine();

  CPPUNIT_TEST_SUITE(TestTimeSeries);
  CPPUNIT_TEST(testSetTimeSeries);
  CPPUNIT_TEST(testIsTimeSeriesEmpty);
  CPPUNIT_TEST(testGetTimeSeries);
  CPPUNIT_TEST(testSetModel);
  CPPUNIT_TEST(testCreateHeaderFileName);
  CPPUNIT_TEST(testCreateColumnHeaderLine);
  CPPUNIT_TEST(testWriteHeaderFile);
  CPPUNIT_TEST(testWriteToSBSIDataFormat);
  CPPUNIT_TEST_SUITE_END();

};

#endif

