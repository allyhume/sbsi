
/*****************************************************************************
 *
 *  TestSingleObjCost.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <SingleObjCost.h>
#include <CostFunction.h>

#ifndef TESTSingleObjCost_H
#define TESTSingleObjCost_H

class TestSingleObjCost : public CppUnit::TestFixture {

 public:

  TestSingleObjCost(){};
  virtual ~TestSingleObjCost(){};

  void setUp(void);
  void tearDown(void);
  void testGetCost();
  void testSetParameters();
  void testGetParamNames();
  void testSetParamNames();
  void testAddObjective();

  CPPUNIT_TEST_SUITE(TestSingleObjCost);
  CPPUNIT_TEST(testSetParameters);
  CPPUNIT_TEST(testSetParamNames);
  CPPUNIT_TEST(testGetParamNames);
  CPPUNIT_TEST(testAddObjective);
  CPPUNIT_TEST(testGetCost);
  CPPUNIT_TEST_SUITE_END();

  CostFunction *scf;
  CostFunction *ucf;

};

#endif

