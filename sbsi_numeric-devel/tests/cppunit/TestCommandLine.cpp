
/*****************************************************************************
 *
 *  TestCommandLine.cpp
 *
 *  See TestCommandLine.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestCommandLine.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestCommandLine)

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestCommandLine::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestCommandLine::tearDown(void) {

  return;
}

void TestCommandLine::testreset(){}
void TestCommandLine::testnum(){}
void TestCommandLine::testarg(){}
void TestCommandLine::testarg_as_int(){}
void TestCommandLine::testarg_as_hex(){}
void TestCommandLine::testarg_as_double(){}
void TestCommandLine::testarg(){}
void TestCommandLine::testarg_as_int(){}
void TestCommandLine::testarg_as_hex(){}
void TestCommandLine::testarg_as_double(){}
