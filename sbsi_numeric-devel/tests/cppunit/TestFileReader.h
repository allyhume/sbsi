
/*****************************************************************************
 *
 *  TestFileReader.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <FileReader.h>

#ifndef TESTFileReader_H
#define TESTFileReader_H

class TestFileReader: public CppUnit::TestFixture, public FileReader {

 public:

  TestFileReader(){};
  virtual ~TestFileReader(){};
  virtual void Read(string fname){};

  void setUp(void);
  void tearDown(void);
  void testClearSets();
  //void testSetModelArg();
  void testSetDataSet();
  void testSetFileReader();
  void testGetLine();
  void testIsDelimiter();
  void testIsWhitespace();
  void testIsWord();
  void testIsNum();
  void testToDouble();
  void testStripWhitespace();
  void testSetTokenDelimiters();

  CPPUNIT_TEST_SUITE(TestFileReader);
  CPPUNIT_TEST(testClearSets);
  //CPPUNIT_TEST(testSetModelArg);
  CPPUNIT_TEST(testSetDataSet);
  CPPUNIT_TEST(testSetFileReader);
  CPPUNIT_TEST(testGetLine);
  CPPUNIT_TEST(testIsDelimiter);
  CPPUNIT_TEST(testIsWhitespace);
  CPPUNIT_TEST(testIsWord);
  CPPUNIT_TEST(testIsNum);
  CPPUNIT_TEST(testToDouble);
  CPPUNIT_TEST(testStripWhitespace);
  CPPUNIT_TEST(testSetTokenDelimiters);
  CPPUNIT_TEST_SUITE_END();
};

#endif

