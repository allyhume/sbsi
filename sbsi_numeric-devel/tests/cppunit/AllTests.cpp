#include <cppunit/TestRunner.h>
#include <cppunit/ui/text/TestRunner.h>

#include "TestSobol64Bit.h"
#include "TestDecodeHelper.h"
#include "TestDeCodeSBML.h"
#include "TestParseConfigFile.h"
#include "TestcModel.h"
#include "TestParamInfo.h"
#include "TestOptArg.h"
#include "TestDataSet.h"
#include "TestFileReader.h"
#include "TestTDCFileReader.h"
#include "TestCostFunction.h"
#include "TestSingleObjCost.h"
#include "TestMultiObjCost.h"
#include "TestSolver.h"
#include "TestCVODESolver.h"
#include "TestTimeSeries.h"
#include "TestModelCostFunction.h"
#include "TestChi2Cost.h"
#include "TestFFTCost.h"
#include "TestMathsUtils.h"
#include "TestSetup.h"
#include "TestOptimiser.h"
#include "TestPGSWrapper.h"

int main(int argc, char* argv[])
{
    CppUnit::TextUi::TestRunner runner;

    runner.addTest(TestSobol64Bit::suite());

#ifdef HAVE_LIBSBML
    runner.addTest(TestDecodeHelper::suite());

    runner.addTest(TestDeCodeSBML::suite());
#endif

    runner.addTest(TestParseConfigFile::suite());

    runner.addTest(TestcModel::suite());

    runner.addTest(TestOptArg::suite());

    runner.addTest(TestParamInfo::suite());

    runner.addTest(TestDataSet::suite());

    runner.addTest(TestFileReader::suite());

    runner.addTest(TestTDCFileReader::suite());

    runner.addTest(TestTimeSeries::suite());

    runner.addTest(TestCostFunction::suite());

    runner.addTest(TestSingleObjCost::suite());

    runner.addTest(TestMultiObjCost::suite());

    runner.addTest(TestSolver::suite());

    runner.addTest(TestCVODESolver::suite());

    runner.addTest(TestModelCostFunction::suite());

    runner.addTest(TestChi2Cost::suite());

    runner.addTest(TestFFTCost::suite());

    runner.addTest(TestMathsUtils::suite());

    runner.addTest(TestSetup::suite());

    string createdir("mkdir -p CppunitTestData/UserModel/results");
    system(createdir.c_str());

    runner.addTest(TestOptimiser::suite());

   MPI_Init(&argc, &argv);
   int my_rank(0);
   MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

   runner.addTest(TestPGSWrapper::suite());

   bool wasSuccessful=runner.run();

    string deletedir("rm -Rf UserModel");
    system(deletedir.c_str());

    deletedir="rm -Rf CppunitTestData/UserModel/results";
    system(deletedir.c_str());

    int errorCode= wasSuccessful? 0:1;

    return  errorCode;
}

