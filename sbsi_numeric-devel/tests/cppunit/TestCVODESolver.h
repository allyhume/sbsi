
/*****************************************************************************
 *
 *  TestCVODESolver.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <CVODESolver.h>

#ifndef TESTCVODESolver_H
#define TESTCVODESolver_H

class TestCVODESolver : public CppUnit::TestFixture, public CVODESolver {

 public:

  TestCVODESolver(){};
  virtual ~TestCVODESolver(){};

  void setUp(void);
  void tearDown(void);

  void testSolveCreate();
  void testSolveInput();
  //void testCheckFlag();

  void testCvsSolver();
  void testSolve();

  CPPUNIT_TEST_SUITE(TestCVODESolver);
  CPPUNIT_TEST(testSolveCreate);
  CPPUNIT_TEST(testSolveInput);
  CPPUNIT_TEST(testSolve);
  CPPUNIT_TEST_SUITE_END();

};

#endif

