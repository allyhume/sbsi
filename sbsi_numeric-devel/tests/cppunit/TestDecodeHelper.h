/*****************************************************************************
 *
 *  TestDeocodeHelper.h
 *
 *  Unit tests for TestDeocodeHelper class.
 *
 *  $Id: TestDecodeHelper.h,v 1.1.2.1 2010/05/20 14:36:25 ayamaguc Exp $
 *

 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#ifndef TESTDECODEHELPER
#define TESTDECODEHELPER

#include <sbml/SBMLReader.h>


class TestDecodeHelper : public CppUnit::TestFixture {

 public:
	TestDecodeHelper() {}
	virtual ~TestDecodeHelper(){}

     void setUp(void);
     void tearDown(void);

   void testGetInequality(void);
   void testPopulateBottomLogicalNodes(void);
   void testPopulateList(void);
   void testComplexInequality(void);
   void testComplexNestedInequality(void);
   void testCheckModelForReservedWords(void);
   void testCheckCompartmentIDsAreChangedInSpecies(void);
   void testMathsInKineticLawsAreChangedInSpecies(void);
   void testMathsInKineticLawsParametersAreChanged (void);
   void testGlobalParameterChangeID (void);
   void testAssignmentRuleChangeID(void);
   void testConstraintChangeID(void);
   void testInitialAssignmentChangeID(void);
   void testFunctionDefinitionChangeID(void);
   void testEventAssignmentChangeID(void);
   void testPiecewiseInAssignmentRule(void);

  CPPUNIT_TEST_SUITE(TestDecodeHelper);
  	CPPUNIT_TEST(testGetInequality); 
  	CPPUNIT_TEST(testPopulateBottomLogicalNodes);
	CPPUNIT_TEST(testPopulateList);
	CPPUNIT_TEST(testComplexInequality);
	CPPUNIT_TEST(testComplexNestedInequality);
	CPPUNIT_TEST(testCheckModelForReservedWords);
	CPPUNIT_TEST(testCheckCompartmentIDsAreChangedInSpecies);
	CPPUNIT_TEST(testMathsInKineticLawsAreChangedInSpecies);
	CPPUNIT_TEST(testMathsInKineticLawsParametersAreChanged);
	CPPUNIT_TEST(testGlobalParameterChangeID);
	CPPUNIT_TEST(testAssignmentRuleChangeID);
	CPPUNIT_TEST(testConstraintChangeID);
	CPPUNIT_TEST(testInitialAssignmentChangeID);
	CPPUNIT_TEST(testFunctionDefinitionChangeID);
	CPPUNIT_TEST(testEventAssignmentChangeID);
	CPPUNIT_TEST(testPiecewiseInAssignmentRule);
  CPPUNIT_TEST_SUITE_END();


};

#endif

