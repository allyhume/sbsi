
/*****************************************************************************
 *
 *  TestAnneal.cpp
 *
 *  See TestAnneal.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestAnneal.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestAnneal)

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestAnneal::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestAnneal::tearDown(void) {

  return;
}

void TestAnneal::testRun(){}
void TestAnneal::testCalcEnergy(){}
void TestAnneal::testStopCondition(){}
void TestAnneal::testPrintPop(){}
