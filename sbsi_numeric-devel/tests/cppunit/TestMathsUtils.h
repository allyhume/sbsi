
/*****************************************************************************
 *
 *  TestMathsUtils.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <MathsUtils.h>

#ifndef TESTMATHSUTILS_H
#define TESTMATHSUTILS_H

class TestMathsUtils : public CppUnit::TestFixture {

 public:

  TestMathsUtils(){};
  virtual ~TestMathsUtils(){};

  void setUp(void);
  void tearDown(void);

  void testMean();
  void testMeanWithEmptyVector();

  void testSum();
  void testSumWithEmptyVector();

  void testSumProduct();
  void testSumProductWithDifferentSizedVectors();
  void testGetIndexVector();
  void testGetIndexVectorWithZeroSize();

  void testDetrend();

  CPPUNIT_TEST_SUITE(TestMathsUtils);
  CPPUNIT_TEST(testMean);
  CPPUNIT_TEST(testMeanWithEmptyVector);
  CPPUNIT_TEST(testSum);
  CPPUNIT_TEST(testSumWithEmptyVector);
  CPPUNIT_TEST(testSumProduct);
  CPPUNIT_TEST(testSumProductWithDifferentSizedVectors);
  CPPUNIT_TEST(testGetIndexVector);
  CPPUNIT_TEST(testGetIndexVectorWithZeroSize);
  CPPUNIT_TEST(testDetrend);
  CPPUNIT_TEST_SUITE_END();

};

#endif

