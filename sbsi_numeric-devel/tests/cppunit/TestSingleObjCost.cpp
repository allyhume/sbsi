
/*****************************************************************************
 *
 *  TestSingleObjCost.cpp
 *
 *  See TestSingleObjCost.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestSingleObjCost.h"
#include "CppunitTestData/UserCostFunction.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestSingleObjCost);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestSingleObjCost::setUp(void) {
  scf= new SingleObjCost();
  CPPUNIT_ASSERT(scf !=NULL);

  ucf=new DD_sum();
  CPPUNIT_ASSERT(ucf !=NULL);
  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestSingleObjCost::tearDown(void) {
  scf = NULL;
  ucf = NULL;
  return;
}

void TestSingleObjCost::testAddObjective(){
   int num=scf->AddObjective(*ucf);
   CPPUNIT_ASSERT(num==1);
}
void TestSingleObjCost::testSetParameters(){

  scf->AddObjective(*ucf);

  vector<double> param;
  for(int i=0;i<20;i++) param.push_back((double)i*0.01);

  scf->SetParameters(param);

  vector<double> tmp = scf->GetParameters();
  cout << tmp.size() << endl;

  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.front(),0,TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp.back(),0.19,TOLERANCE);
 
}
void TestSingleObjCost::testSetParamNames(){
  vector<string> names;
  names.push_back("A");
  names.push_back("B");
  names.push_back("C");
  scf->AddObjective(*ucf);
  scf->SetParamNames(names);
  vector<string> tmp = scf->GetParamNames();
  CPPUNIT_ASSERT_EQUAL(names.front(),tmp.front());
  CPPUNIT_ASSERT_EQUAL(names.back(),tmp.back());


}
void TestSingleObjCost::testGetParamNames(){
  vector<string> names;
  names.push_back("A");
  names.push_back("B");
  names.push_back("C");
  scf->AddObjective(*ucf);
  scf->SetParamNames(names);
  vector<string> tmp=scf->GetParamNames();
  CPPUNIT_ASSERT_EQUAL(names.front(),tmp.front());
  CPPUNIT_ASSERT_EQUAL(names.back(),tmp.back());

}
void TestSingleObjCost::testGetCost(){
  scf->AddObjective(*ucf);

  vector<double> param;
  for(int i=0;i<20;i++) param.push_back((double)i*0.01);

  scf->SetParameters(param);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(scf->GetCost(),1.901,TOLERANCE);

}
