
/*****************************************************************************
 *
 *  TestParseConfigFile.cpp
 *
 *  See TestParseConfigFile.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include "TestParseConfigFile.h"

using namespace std;

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestParseConfigFile);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestParseConfigFile::setUp(void) {
  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestParseConfigFile::tearDown(void) {

  return;
}

void TestParseConfigFile::testParseConfig(){
 string dir="UserModel/" ;
 ParseConfigFile parser(dir);
 string expdir="UserModel/exp_data/";
 CPPUNIT_ASSERT_EQUAL(expdir, parser.exp_dir);
 string resultdir="UserModel/results/";
 CPPUNIT_ASSERT_EQUAL(resultdir, parser.results_dir);
 string timeseriesdir="UserModel/results/";
 CPPUNIT_ASSERT_EQUAL(timeseriesdir, parser.timeseries_dir);
 string ckpointdir="UserModel/ckpoint/";
 CPPUNIT_ASSERT_EQUAL(ckpointdir, parser.ckpoint_dir);
 
}
void TestParseConfigFile::testparseSetup(){
}
void TestParseConfigFile::testparseOptimiser(){}
void TestParseConfigFile::testparseCostFunctions(){}
void TestParseConfigFile::testparseFFT(){}
void TestParseConfigFile::testparseState(){}
void TestParseConfigFile::testparseX2Cost(){}
void TestParseConfigFile::testparseSolver(){}
void TestParseConfigFile::testparseResults(){}
void TestParseConfigFile::testloadData(){}
