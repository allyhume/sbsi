
/*****************************************************************************
 *
 *  TestCommandLine.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <CommandLine.h>

#ifndef TESTCommandLine_H
#define TESTCommandLine_H

class TestCommandLine : public CppUnit::TestFixture {

 public:

  TestCommandLine();
  virtual ~TestCommandLine();

  void setUp(void);
  void tearDown(void);
  void testreset();
  void testnum();
  void testarg();
  void testarg_as_int();
  void testarg_as_hex();
  void testarg_as_double();
  void testarg();
  void testarg_as_int();
  void testarg_as_hex();
  void testarg_as_double();


  CPPUNIT_TEST_SUITE(TestCommandLine);
  CPPUNIT_TEST_SUITE_END();

};

#endif

