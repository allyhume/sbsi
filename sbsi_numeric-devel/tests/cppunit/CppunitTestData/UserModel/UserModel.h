//#include <vector>
//#include <string>

using namespace std;

#ifndef __UserModel_h__
#define __UserModel_h__

#include <cModel.h>

class  UserModel:public cModel {
public: 

  UserModel(){};

   /********** Model has a compartment *********/
   /********** the compartment 0         ********/
   /*                Name; Default 	 		*/
   /*                  Id; Default   	*/
   /*  Spatial dimensions; 3			*/
   /*                Size; 1			*/
   /*            Constant; 1	 	 	*/
   /**********************************************/

   /*********** Model has 3 states. *******/

   void inputModel(void) ;


   /*********** Model has 2 Rules. *******/
   /*********** Model has 2 Reactions. *******/

   void getRHS(double * y_in, double t, double* yout); 
   void update_assignment_variables(vector<vector<double> > * results);

  vector<double> calVarVec(int iv, vector< vector<double> >); 
  void calCoef(); 
};
static cModel* getModel(){return new UserModel();}

#endif
