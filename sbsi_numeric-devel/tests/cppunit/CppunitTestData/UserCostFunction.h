#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <exception>
using namespace std;

#include<CostFunction.h>

#ifndef __UserCostFunction_h__
#define __UserCostFunction_h__

#define SHIFT 1
#define BASE_COST 0.001
#define PI 3.14159265358979323846

class UserCostFunction: public CostFunction
{
public:

	virtual void Init(){};

};

class Rosenbrock: public UserCostFunction
{

public:

	Rosenbrock(int n=32) {D=n;};

	virtual double GetCost() {
		vector<double> x=ParamValues;
		double xid,f=BASE_COST;
		for(vector<double>::iterator d=x.begin();d!=x.end()-1;d++) {
			xid=1-*d;
			f+=xid*xid;
			xid= *d * *d-*(d+1);
			f+=100*xid*xid;
		}
		return f;

	};

protected:

	int D;

};

class Rastrigin: public UserCostFunction
{

public:

	Rastrigin() {};

	virtual double GetCost() {
		vector<double> x=ParamValues;
		double f=BASE_COST,k=10;
		for(vector<double>::iterator d=x.begin();d!=x.end();d++) {
			f+= *d * *d - k* cos(2*PI* *d);
		}
		f+=k*x.size();
		return f;
	};


};

class DD_sum: public UserCostFunction
{

public:

	DD_sum(){};

	virtual double GetCost() {
		vector<double> x=ParamValues;
		double f=BASE_COST;

		for(vector<double>::iterator d=x.begin();d!=x.end();d++) {
			f+= *d;
		}
		return f;

	};


};

class DD_product: public UserCostFunction
{

public:

	DD_product() {};

	virtual double GetCost() {
		vector<double> x=ParamValues;
		double f=1;
		for(vector<double>::iterator d=x.begin();d!=x.end();d++) {
			f = f * *d;
		}
		return f;

	};


};

/*
 * Easom
 * Bounds [-100.0,100.0]
 * Global minimum 0 position (PI,PI,...,PI)
 */
class Easom: public UserCostFunction
{

public:

	//Easom(int n=3) {D=n;};
	Easom(){};


	virtual double GetCost() {
		vector<double> x=ParamValues;
		double f=1,xid=0;
		cout<<"======================================"<<endl;
		for(vector<double>::iterator d=x.begin();d!=x.end();d++) {
			f*=cos(*d);
			xid+=(*d-PI)*(*d-PI);
		}
		return 1-f*exp(-1*xid)+BASE_COST;

	};


};

#endif
