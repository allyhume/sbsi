/*****************************************************************************
 *
 *  TestTimeSeries.cpp
 *
 *  Unit tests for TimeSeries class.
 *
 *****************************************************************************/

#include <cfloat>
#include <cModel.h>
#include <FileReader.h>
#include "TestTimeSeries.h"
#include "CppunitTestData/UserModel/UserModel.h"

#define TOLERANCE DBL_EPSILON

using namespace std;

/*****************************************************************************
 *
 *  Register the tests
 *
 *****************************************************************************/

CPPUNIT_TEST_SUITE_REGISTRATION(TestTimeSeries);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestTimeSeries::setUp(void) {
  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestTimeSeries::tearDown(void) {

  return;
}

void TestTimeSeries::testIsTimeSeriesEmpty(){
  CPPUNIT_ASSERT(IsTimeSeriesEmpty() == true);
}

void TestTimeSeries::testSetTimeSeries(){
  vector<vector<double> > res;
  res.resize(3);
  for(int i=0;i<10; i++){
      res.at(0).push_back((i+1)*0.01);
      res.at(1).push_back((i+1)*0.1);
      res.at(2).push_back((i+1)*1.0);
  }
  SetTimeSeries(&res);
  CPPUNIT_ASSERT(IsTimeSeriesEmpty() != true);

  CleanTimeSeries();
  CPPUNIT_ASSERT(IsTimeSeriesEmpty() == true);
}

void TestTimeSeries::testGetTimeSeries(){
  vector<vector<double> > res;
  res.resize(3);
  for(int i=0;i<10; i++){
      res.at(0).push_back((i+1)*0.01); // time
      res.at(1).push_back((i+1)*0.1); // state 1
      res.at(2).push_back((i+1)*1.0); // state 2
  }
  SetTimeSeries(&res);
  vector<double> tmp1=GetAStateTimeSeries(0); //return state 1

  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp1.front(),res.at(1).front(),TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(tmp1.back(),res.at(1).back(),TOLERANCE);

  vector<double> time=GetTimeSeriesTime(); //return time
  CPPUNIT_ASSERT_DOUBLES_EQUAL(time.back(),res.at(0).back(),TOLERANCE);
  vector<vector <double> > ret;
  ret=GetTimeSeries();
  CPPUNIT_ASSERT(ret.size() == res.size());
  CPPUNIT_ASSERT_DOUBLES_EQUAL(ret.at(0).back(),res.at(0).back(),TOLERANCE);
}

void TestTimeSeries::testSetModel(){
  cModel *Model= new UserModel();
  cModelArg ModelArg;

  Model->setModelArg(ModelArg);
  Model->initModelArg();
  Model->inputModel();

  SetModel(Model);
  CPPUNIT_ASSERT( Model == model);

}

void TestTimeSeries::testCreateHeaderFileName(){
 string filename="CppunitTestData/UserModel/TimeSeries.dat";
 string EXPECTED="TimeSeries.sbsiheader";
 CPPUNIT_ASSERT_EQUAL(EXPECTED,CreateHeaderFileName(filename));
}

void TestTimeSeries::testCreateColumnHeaderLine(){
  cModel *Model= new UserModel();
  cModelArg ModelArg;

  Model->setModelArg(ModelArg);
  Model->initModelArg();
  Model->inputModel();

  SetModel(Model);

  string EXPECTED="Time\tA\tB\tC\n";

  string hdr=CreateColumnHeaderLine(Model->getStateNames());
  CPPUNIT_ASSERT_EQUAL(EXPECTED,hdr);

}
void TestTimeSeries::testWriteHeaderFile(){
  cModel *Model= new UserModel();
  cModelArg ModelArg;

  Model->setModelArg(ModelArg);
  Model->initModelArg();
  Model->inputModel();

  SetModel(Model);

  cout << model->getParamByName("k1") << endl;

  vector<string> names=Model->getStateNames();
  string filename="CppunitTestData/TimeSeries.sbsiheader";
  WriteHeaderFile(filename,names);

  FileReader *hfr;
  DataSet dataset;
  hfr = new TDCHeaderFileReader();
  hfr->ClearSets();
  hfr->SetDataSet(&dataset);
  hfr->Read(filename);

  CPPUNIT_ASSERT(dataset.GetNumInitialStateValues() == 0);
  CPPUNIT_ASSERT(dataset.GetNumInitialStateNames() == 0);

  CPPUNIT_ASSERT(dataset.GetNumInitialParamValues() == 8);
  CPPUNIT_ASSERT(dataset.GetNumInitialParamNames() == 8);

  string EXPECTED="k1";
  CPPUNIT_ASSERT_EQUAL(EXPECTED,dataset.GetAInitialParamName(0));
  CPPUNIT_ASSERT_DOUBLES_EQUAL(dataset.GetAInitialParamValue(7),0.0,TOLERANCE);

}
void TestTimeSeries::testWriteToSBSIDataFormat(){

  cModel *Model= new UserModel();
  cModelArg ModelArg;

  Model->setModelArg(ModelArg);
  Model->initModelArg();
  Model->inputModel();

  SetModel(Model);

  vector<vector<double> > res;
  res.resize(4);
  for(int i=0;i<10; i++){
      res.at(0).push_back((i+1)*0.01); // time
      res.at(1).push_back((i+1)*0.1); // state 1
      res.at(2).push_back((i+1)*1.0); // state 2
      res.at(3).push_back((i+1)*10.0); // state 2
  }
  SetTimeSeries(&res);

  string filename="CppunitTestData/TimeSeries.dat";
  WriteToSBSIDataFormat(filename);

 FileReaderType expfilereadtype=TabDelimitedColumnData;
 FileReader &file_reader=FileReader::SetFileReader(expfilereadtype);

 file_reader.Read(filename);

 CPPUNIT_ASSERT(file_reader.Sets.size() == 1);

 DataSet dataset = file_reader.Sets.front();
 CPPUNIT_ASSERT(dataset.GetNumDataValues() == 4);

 CPPUNIT_ASSERT_DOUBLES_EQUAL(res.at(0).front(), dataset.GetADataTime(0),TOLERANCE);
 CPPUNIT_ASSERT_DOUBLES_EQUAL(res.at(0).back(), dataset.GetEndDataTime(), TOLERANCE);

 CPPUNIT_ASSERT_DOUBLES_EQUAL(res.back().front(), dataset.GetADataValue(3,0), TOLERANCE);

 string EXPECTED="TimeSeries.sbsiheader";
 CPPUNIT_ASSERT_EQUAL(EXPECTED,dataset.GetSetName());
}












