
/*****************************************************************************
 *
 *  TestMathsUtils.cpp
 *
 *  See TestMathsUtils.h for a description.
 *
 *
 *****************************************************************************/

#include "TestMathsUtils.h"

#define TOLERANCE DBL_EPSILON


CPPUNIT_TEST_SUITE_REGISTRATION(TestMathsUtils);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestMathsUtils::setUp(void) {

  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestMathsUtils::tearDown(void) {

  return;
}

void TestMathsUtils::testMean() {
  vector<double> data;
  data.push_back(1.5);
  data.push_back(2.5);
  data.push_back(3.5);
  data.push_back(4.5);

  double result = MathsUtils::mean(data);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(3.0, result, TOLERANCE);
}

void TestMathsUtils::testMeanWithEmptyVector() {
  vector<double> data;
  double result = MathsUtils::mean(data);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, result, TOLERANCE);
}

void TestMathsUtils::testSum() {
  vector<double> data;
  data.push_back(1.5);
  data.push_back(2.5);
  data.push_back(3.5);
  data.push_back(4.5);
  data.push_back(5.5);

  double result = MathsUtils::sum(data);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(17.5, result, TOLERANCE);
}

void TestMathsUtils::testSumWithEmptyVector() {
  vector<double> data;
  double result = MathsUtils::sum(data);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0, result, TOLERANCE);
}

void TestMathsUtils::testSumProduct() {
  vector<double> data1;
  data1.push_back(1.0);
  data1.push_back(2.0);
  data1.push_back(0.5);

  vector<double> data2;
  data2.push_back(2.2);
  data2.push_back(4.4);
  data2.push_back(6.6);

  // 1.0 * 2.2 + 2.0 * 4.4 + 0.5 * 6.6 = 14.3

  double result = MathsUtils::sumProduct(data1, data2);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(14.3, result, TOLERANCE);
}

void TestMathsUtils::testSumProductWithDifferentSizedVectors() {
  vector<double> data1;
  data1.push_back(1.0);
  data1.push_back(2.0);
  data1.push_back(0.5);
  data1.push_back(2.4);
  data1.push_back(3.5);

  vector<double> data2;
  data2.push_back(2.2);
  data2.push_back(4.4);
  data2.push_back(6.6);

  // 1.0 * 2.2 + 2.0 * 4.4 + 0.5 * 6.6 = 14.3

  double result = MathsUtils::sumProduct(data1, data2);
  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(
    "data1 larger than data2", 14.3, result, TOLERANCE);

  result = MathsUtils::sumProduct(data2, data1);
  CPPUNIT_ASSERT_DOUBLES_EQUAL_MESSAGE(
    "data2 larger than data1", 14.3, result, TOLERANCE);
}

void TestMathsUtils::testGetIndexVector() {
  vector<double> data;
  data.push_back(5.0);   // dummy data that should be removed
  MathsUtils::getIndexVector(4, data);

  CPPUNIT_ASSERT_EQUAL(4, (int)data.size());
  
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, data[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, data[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(2.0, data[2], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(3.0, data[3], TOLERANCE);
}

void TestMathsUtils::testGetIndexVectorWithZeroSize() {
  vector<double> data;
  data.push_back(5.0);   // dummy data that should be removed
  MathsUtils::getIndexVector(0, data);

  CPPUNIT_ASSERT_EQUAL(0, (int)data.size());
}

void TestMathsUtils::testDetrend() {

  // Make most data on a staight line from -5 to 5
  // But put some of the values 0.5 off the line
  // balancing each other out. The regression equation
  // for this is y = -5.0454545 + 1.00909091*x
  double a = -5.0454545;
  double b = 1.00909091;

  vector<double> data;
  data.push_back(-5.0);
  data.push_back(-4.5);  // -0.5 off the line
  data.push_back(-2.5);  // +0.5 off the line 
  data.push_back(-2.0);
  data.push_back(-1.0);
  data.push_back(0.0);
  data.push_back(1.0);
  data.push_back(2.0);
  data.push_back(2.5);   // -0.5 off the line 
  data.push_back(4.5);   // +0.5 off the line
  data.push_back(5.0);

  vector<double> result;

  MathsUtils::detrend(data, result);

  // Ensure that the input data has not been changed
  CPPUNIT_ASSERT_EQUAL(11, (int)data.size());
  CPPUNIT_ASSERT_DOUBLES_EQUAL(-5.0, data[0], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(-4.5, data[1], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(-2.5, data[2], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(-2.0, data[3], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(-1.0, data[4], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0, data[5], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(1.0, data[6], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(2.0, data[7], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(2.5, data[8], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(4.5, data[9], TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(5.0, data[10], TOLERANCE);

  // Check the result data
  CPPUNIT_ASSERT_EQUAL(11, (int)result.size());
  for (int i=0; i<11; ++i) {
    CPPUNIT_ASSERT_DOUBLES_EQUAL(data[i]-(a+b*i), result[i], 0.0001);
  }
}
