
/*****************************************************************************
 *
 *  TestModelCostFunction.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <ModelCostFunction.h>

#ifndef TESTMODELCOSTFUNCTION_H
#define TESTMODELCOSTFUNCTION_H

class TestModelCostFunction : public CppUnit::TestFixture,public ModelCostFunction {

 public:

  TestModelCostFunction(){};
  virtual ~TestModelCostFunction(){};
  double GetCost(){return 0;};
  double getcost(){return 0;};

  void setUp(void);
  void tearDown(void);

  void testInit();
  void testSetGlbCostArg();
  void testSetSolver();
  void testSetModel();
  void testSolverInit();
  void testSetModelCostFunction();
  void testSetTimeSeries();
  void testSetParameters();
  void testGetResultVec();
  void testGetCosts();
  void testSolveRHS();

  CPPUNIT_TEST_SUITE(TestModelCostFunction);
  CPPUNIT_TEST(testInit);
  CPPUNIT_TEST(testSetGlbCostArg);
  CPPUNIT_TEST(testSetSolver);
  CPPUNIT_TEST(testSetModel);
  CPPUNIT_TEST(testSolverInit);
  CPPUNIT_TEST(testSetModelCostFunction);
  CPPUNIT_TEST(testSetTimeSeries);
  CPPUNIT_TEST(testSetParameters);
  CPPUNIT_TEST(testGetResultVec);
  CPPUNIT_TEST(testGetCosts);
  CPPUNIT_TEST(testSolveRHS);
  CPPUNIT_TEST_SUITE_END();

};

#endif

