
/*****************************************************************************
 *
 *  TestFileReadPop.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <FileReadPop.h>

#ifndef TESTFILEREADPOP_H
#define TESTFILEREADPOP_H

class TestFileReadPop : public CppUnit::TestFixture {

 public:

  TestFileReadPop();
  virtual ~TestFileReadPop();

  void setUp(void);
  void tearDown(void);
  void Read();


  CPPUNIT_TEST_SUITE(TestFileReadPop);
  CPPUNIT_TEST_SUITE_END();

};

#endif

