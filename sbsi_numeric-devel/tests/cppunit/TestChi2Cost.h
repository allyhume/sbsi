
/*****************************************************************************
 *
 *  TestChi2Cost.h
 *
 *
 *****************************************************************************/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestFixture.h>

#include <ModelCostFunction.h>
#include <Chi2Cost.h>

#ifndef TESTChi2Cost_H
#define TESTChi2Cost_H

class TestChi2Cost : public CppUnit::TestFixture, public Chi2Cost{

 public:

  TestChi2Cost(){};
  virtual ~TestChi2Cost(){};

  void setUp(void);
  void tearDown(void);

  void testSetDataSets();
  void testGetNorm();
  void testSetDataSet();
  void testUpdateScale();
  void testgetcost();
  void testGetCost();


  CPPUNIT_TEST_SUITE(TestChi2Cost);
  CPPUNIT_TEST(testGetNorm);
  CPPUNIT_TEST(testSetDataSets);
  CPPUNIT_TEST(testUpdateScale);
  CPPUNIT_TEST(testSetDataSet);
  CPPUNIT_TEST(testgetcost);
  CPPUNIT_TEST_SUITE_END();

};

#endif

