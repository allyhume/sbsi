
/*****************************************************************************
 *
 *  TestCVODESolver.cpp
 *
 *  See TestCVODESolver.h for a description.
 *
 *
 *****************************************************************************/

#include <cfloat>
#include <cModel.h>
#include <TimeSeries.h>
#include "TestCVODESolver.h"
#include "CppunitTestData/UserModel/UserModel.h"

#define TOLERANCE 1.0e-7


CPPUNIT_TEST_SUITE_REGISTRATION(TestCVODESolver);

/*****************************************************************************
 *
 *  setUp
 *
 *****************************************************************************/

void TestCVODESolver::setUp(void) {
  return;
}

/*****************************************************************************
 *
 *  tearDown
 *
 *****************************************************************************/

void TestCVODESolver::tearDown(void) {
  return;
}

//void TestCVODESolver::testCheckFlag(){
//  int flag(0);
//
///* case opt ==0 memory check */
//  void *mem;
//  mem=NULL;
//  CPPUNIT_ASSERT(CheckFlag(mem,"Checkflag case 1 ",0) == 1); // error 
///* case opt ==1 flag check */
// // flag=0;
//  CPPUNIT_ASSERT(CheckFlag(&flag,"Checkflag case 2",1) == 0); // OK
//  flag=-4;
//  CPPUNIT_ASSERT(CheckFlag(&flag,"Checkflag case 3",1) == 1); // error
///* case opt ==2 flag check */
//  CPPUNIT_ASSERT(CheckFlag(mem,"Checkflag case 4",2) == 1); // error
//
//}
void TestCVODESolver::testSolveCreate(){
  /* From Solver */
  Solver *solver = new CVODESolver();
  bool flag = solver->SolveCreate();
  CPPUNIT_ASSERT(flag == true);
  /* Inside */
  /* before */
  CPPUNIT_ASSERT(solver_mem == NULL);
  SolveCreate();
  /* after */
  CPPUNIT_ASSERT(solver_mem != NULL);
}
void TestCVODESolver::testSolveInput(){
  cModel *Model= new UserModel();
  cModelArg ModelArg;
  

  Model->setModelArg(ModelArg);
  Model->initModelArg();
  Model->setAtol(1.0e-04);
  Model->setReltol(1.0e-14);
  Model->setInit(20.04);
  Model->setFinal(10.0);
  Model->setInterval(0.01);
  Model->setOutInterval(0.1);
  Model->setMaxTimes(1000);
  Model->inputModel();

  SetModel(Model);
  solver_mem=NULL;
  SolveInput();
  CPPUNIT_ASSERT(solver_mem != NULL);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(NV_DATA_S(abstol)[0],Model->getAtol(), TOLERANCE);
  vector<double> temp=Model->getStates();
  CPPUNIT_ASSERT_DOUBLES_EQUAL(NV_DATA_S(y_cvs)[0],temp.front(), TOLERANCE);
  
}
void TestCVODESolver::testSolve(){

  cModel *Model= new UserModel();
  cModelArg ModelArg;

  Model->setModelArg(ModelArg);

  Model->setAtol(1.0e-04);
  Model->setReltol(1.0e-14);
  Model->setInit(0);
  Model->setFinal(10.0);
  Model->setInterval(0.01);
  Model->setOutInterval(0.1);
  Model->setMaxTimes(1000);

  Model->inputModel();

  TimeSeries tseries;
  tseries.SetModel(Model);

  SetModel(Model);
  SetTimeSeries(&tseries);
  SolveInput();
  Solve();

  vector< vector<double> > results=tseries.GetTimeSeries();

  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.1,results.at(0).front(), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.9091079,results.at(1).front(), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0864296,results.at(2).front(), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.00446247,results.at(3).front(), TOLERANCE);

  CPPUNIT_ASSERT_DOUBLES_EQUAL(10.0,results.at(0).back(), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.0158297,results.at(1).back(), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.00978311,results.at(2).back(), TOLERANCE);
  CPPUNIT_ASSERT_DOUBLES_EQUAL(0.97438578,results.at(3).back(), TOLERANCE);
}
