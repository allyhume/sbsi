//#include <vector>
//#include <string>

using namespace std;

#ifndef __def_1_h__
#define __def_1_h__

#include <cModel.h>

class  def_1:public cModel {
public: 

  def_1(){};

   /********** Model has a compartment *********/
   /********** the compartment 0         ********/
   /*                Name; Default 	 		*/
   /*                  Id; Default   	*/
   /*  Spatial dimensions; 3			*/
   /*                Size; 1			*/
   /*            Constant; 1	 	 	*/
   /**********************************************/


   void inputModel(void) ;


   void getRHS(double * y_in, double t, double* yout); 
   void update_assignment_variables( vector< vector <double> > * results);

  vector<double> calVarVec(int iv, vector< vector<double> >); 
  void calCoef(); 
};
static cModel* getModel(){return new def_1();}

#endif
