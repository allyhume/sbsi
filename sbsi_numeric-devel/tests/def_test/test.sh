#!/bin/sh

np=2

echo Test for def Model with FFT costfuncion with GA with mpi np=$np

mkdir UserModel/ckpoint UserModel/results

mpirun -np $np ./defTest UserModel config_FFT.xml

echo 
echo Check the period for state.  Should be all arond 1.0

 t_file_list=`ls -lt UserModel/results/*Tseries*dat |awk '{print $NF}'`
 for i in $t_file_list
  do
     t_start=`head -3 $i | tail -1 | awk '{print $1}'`
     t_end=`tail -1 $i | awk '{print $1}'`
      echo $i $t_start $t_end
        ../../util/FFT $i $t_start $t_end 0.02
  done





