/********************************************************************/
/*!
  @file cModelArg.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.3 $

*/
/******************************************************************/

#ifndef __cModelArg_h__
#define __cModelArg_h__


using namespace std;

struct cModelArg {

public:

  //double TFinal;
  //double TInit;

  string KeyParam;
  double Final;
  double Init;

  double TExpInit;  /* solver result samplint time t0 */

  int MaxTimes;


  /*
   * allan: Okay so the Interval is the time step used by the solver.
   * The OutputInterval specifies the distance in time recorded by the
   * solver for the purposes of comparing the recorded timeseries with 
   * the model data, hence this should in general be larger than Interval
   * but at least as small as the smallest gap in time of the model data,
   * but generally smaller such that an appropriate time point in the
   * computed simulation timeseries can be found for each time point in
   * the data file.
   * Finally the ReportInterval is that which is output in the best
   * timeseries file, to avoid rather large timeseries files.
   */
  double Interval;
  double OutputInterval;
  double ReportInterval;

  double Atol;
  double Reltol;

  double Fnormtol;
  double Steptol;

  int Mset;
  int Str;

  vector<double> Params;

  vector<double> States;
  vector<double> Vars;

  string result_Dir;

  vector<string> ParamNames;

  vector<string> StateNames;

  vector<string> VarNames;

  vector<double> Constraints;

};

#endif
