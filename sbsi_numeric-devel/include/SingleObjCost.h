/********************************************************************/
/*!
  @file SingleObjCost.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.2 $

*/
/******************************************************************/

using namespace std;

#ifndef __SingleObjCost_h__
#define __SingleObjCost_h__

#include <CostFunction.h>

class SingleObjCost: public CostFunction
{
public:
  virtual void Init();

  ~SingleObjCost() {};

  double GetCost(void) ;

  virtual void SetParameters(vector<double> in_param) ;

  virtual vector<double>& GetParameters() ;

  virtual vector<string>& GetParamNames() ;
  virtual void   SetParamNames(vector<string> name) ;

  int AddObjective(CostFunction &acf) {
    cfun = &acf;
    return 1;
  };

  CostFunction *cfun;

};


#endif
