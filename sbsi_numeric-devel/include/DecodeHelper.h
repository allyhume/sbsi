/********************************************************************/
/*!
  @file DecodeHelper.h
  @date 2010/03/29

  @author Richard
  $Revision: 1.1.2.1 $

*/
/******************************************************************/

#include <sbml/SBMLTypes.h>
#ifndef __DecodeHelper_h__
#define __DecodeHelper_h__
using namespace std;

#define CPPKeyWordNum 71


/**
 * Class for holding various utility functions to help the DecodeSBML class.
 */
class DecodeHelper
{

public:

  DecodeHelper();

  ~DecodeHelper() {};
  /**
   * The main public method for this class. Will return a C++ compilable string of a MathML expression returning a boolean value.
   * This assumes that the Mathml is correct according to the MAthml and SBML criteria.
   * \param An ASTNode that is a LOGICAL or RELATIONAL type
   * \return A string of the type ' (( A >B && c ==2) || ( f > 4 && f <3 )) '
   */
  string getString (ASTNode * logicalOrRelationalNode);

  bool searchAndReplaceReservedWords(Model *model);


  /**
   * Converts piecewise math into a conditional assignment string
   * \param An ASTNode pointer to a piecewise element of math (i.e., a node where isPiecewise() returns true )
   * \return a C++ conditional expression string.
   */
  string getPiecewiseString (const ASTNode * pieceWiseMath, string var) ;

  void createInequality(ostream &oss, const ASTNode * ineq);
  void populateBottomLogicalNodes(ASTNode * root, vector <ASTNode *> &bottomlogEqs,  map<ASTNode *, ASTNode *> &child2ParentMap);
  void popList(vector<ASTNode *> &bottomlogEqs, map<ASTNode *, ASTNode *> &child2ParentMap);

  vector<string> CPP_KEYWORDS;

  void load_cppkeywords();

  bool isKeyword(string name);
  bool changeMathIds (const ASTNode * root, bool isChanged);

private :

  string getSymbolFor (const ASTNode * node);


};
#endif
