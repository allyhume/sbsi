/*********************************************************/
/*!
  @file BVPSolver.h
  @date 2010/03/29
  @brief For Bounday Value Problem Solver
  @author Azusa Yamaguchi
*/
/*********************************************************/

using namespace std;

#ifndef __BVPSolver_h__
#define __BVPSolver_h__

#include <cModel.h>
#include <Solver.h>

/*!
  @brief Class for a boundary value problem solver
         Not implimented yet.
*/
class BVPSolver: public Solver
{

public:

  BVPSolver() {};

  virtual bool Solve();
  virtual bool SolveInput();
  virtual bool SolveInit();
  virtual bool SolveCreate();


};

#endif

