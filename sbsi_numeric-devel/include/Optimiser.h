/********************************************************************/
/*!
  @file Optimiser.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.1 $

*/
/******************************************************************/

#include <fstream>
#include <sstream>
#include <cmath>

#ifndef __Optimiser_h__
#define __Optimiser_h__

#include <CostFunction.h>
#include <OptArg.h>
#include <ParamInfo.h>
#include <ParamSet.h>
#include <ERR.h>


using namespace std;


class Optimiser
{

public:

  virtual ~Optimiser() {};

  void Init(OptArg *oa, ParamInfo &pi, vector<ParamSet> *ps) {
    optarg = oa;
    paraminfo = pi;
    paramsets = ps;
  };

  ////////////////////////////////////////////////////
  // Utilities
  ////////////////////////////////////////////////////

  static Optimiser & SetOptimiser(OptimiserType which);

  void SetCostFunction( CostFunction *cf) {
    costfunction = cf;
  };

  double GetInitialCost();

  virtual double GetTheBestCost();

  vector<double> GetTheBest();

  virtual double GetTheCost(int aI);

  vector<ParamSet> *GetParamSets() {
    return paramsets;
  };

  virtual int Run() = 0;

  // for StopInfo FILE output
  void WriteStopInfo_MAXITER(ofstream &fp, double cost);

  void WriteStopInfo_TOOSIMILAR(ofstream &fp, double cost);

  void WriteStopInfo_NOIMPROVEMENT(ofstream &fp, double cost);

  void WriteStopInfo_REACHED(ofstream &fp, double cost);

  void WriteGeneralInfo();


  CostFunction *costfunction;

  static ParamInfo paraminfo;

  static Optimiser *optimiser;

  static vector<ParamSet> *paramsets;

  OptArg *optarg;


};

#endif

