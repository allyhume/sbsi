/********************************************************************/
/*!
  @file DeCodeSBML.h
  @date 2008/06/

  @author Azusa Yamaguchi
  $Revision: 1.1.2.2 $

*/
/******************************************************************/

#include <dirent.h>
#include <sys/stat.h>
#include <fstream>
#include <sbml/SBMLTypes.h>

#ifndef __DeCodeSBML_h__
#define __DeCodeSBML_h__

#include <cModel.h>

/*!
 * Class for translating an SBML model into a  C++ class for use in optimisation framework. This tool is broadly supportive of libSBML level 2, and
  supports rate rules, kinetic laws, reactions, species, function definitions, events and compartments.
 * For models that contain identifiers that are C++ reserved words, the tool will replace variable occurrences with the variable name appended with "_MODEL".

 * The following elements are not specifically handled yet :
 *  -# Delays in event assignments
 *  -# Initial assigments to species, apart from direct assignment of literal values.
 *  -# Constraints are not handled by this class, as they are supplied in a separate file called [modelID]_Init_Param.dat
 *  -# Piecewise elements in Math functions, unless a piecewise element is the top level math element in an assignment rule.( I.e., a conditional assignment).
 *  -# The 'power' function can be ambiguous if both arguments are integers and may cause the model compilation to fail.

 In terms of the math supported, this class will generate C++ code for any math that is correctly specified for its enclosing SBML element. However, not all
 math functions supported by SBML's specification have standard C++ functions. The following is a list of math functions that will generate C code that will not
 currently compile with the standard C++ math library:

 *   - Inverse trig functions such as \em sec, \em cosec, \em cot, \em arcsec, \em arccosec, \em arccot and inverses of hyperbolic trig functions such as \em arccosh, \em arcsinh, \em arctanh
 *   - Logarithms to bases other than \em e or 10.
 *   - Roots other than square roots.

 *  This class has a single public method:

    \verbatim
        void DeCode (cModel *model,const string infname)
    \endverbatim


 * To use this class, a basic C model needs to be initialised as follows:
 *
    \verbatim
 cModel_arg smodel_arg;
 cModel *usermodel;
 usermodel  = new SBMLModel();
 usermodel->init(smodel_arg);

  DeCodeSBML  decoder;
 decoder.DeCode(usermodel,newsbmlfilename);
     \endverbatim

 For conversion to work, the SBML document must be error free. Any errors will cause the converter to exit.

 References to time: In any function definitions, event triggers or math, time should be given the identifier 't'.
 */
class DeCodeSBML
{

public:

  DeCodeSBML() {};
  ~DeCodeSBML() {};
  /*!
   * Public method for converting an SBML model, stored in  infname, into a C++ model.
   * \param model a cModel * to an initialised c_model to hold the parsed c_model structure.
   * \param infname a String filename for the location of the model, that can be written to.
   * As a side effect, this class generates the runtime directories and writes a header and .C file representing the model.
   */
  void DeCode (cModel *model, const string infname);

  /**
   * The following methods are protected for testing in a test subclass.
   */
protected:

  /*!
   * This method reads a single constraint for a parameter, and sets  the upper and lower bounds of the constraints in the model.
   * This method only works with constraints of the type  a lt|gt  b && c lt|gt d. E.g., see VderPol.xml for an example.
   * I.e., to be parsed correctly the constraint needs an upper and lower bound joined by an 'and' expression.
   * \param constraint a Constraint * to a non-null constraint of the type described above.
   */
  void setConstraintIntoParamList(const Constraint* constraint);

  void printInitialAssignmentIntocalCoef(const Model* m, 
               const InitialAssignment* assign, ostream &stream);


  /*!
   * This method obtains the lower value of a parameter's constraint. If the parameter name does not recognised, or if the parameter value is lower
   * than the  constraint, it returns the value pvalue
   *\param name the parameter's ID
   *\param pvalue the current parameter value
   */
  double getParamLowerByName(string name, double pvalue);

  /*!
   * For a given function definition, will  print a function in the following way:
    \verbatim
    / *A FunctionDefinition 1* \/
    double fd1( double a)
    {
    return 4 + b;
   };
   \endverbatim
    where the name of the function printed is the id of the FunctionDefinition element.
    Arguments to the function are obtained from the <em> bvar</em> elements in the FunctionDefinition.
    Variables in the function body should correspond to those in the argument list, as described in the SBML specification.

    If no math is assigned to this function definition, or the top level element is not a \em lamda element, nothing is printed.

   \param n  the nth function definition index, to appear in a comment.
   \param fd A non-null, syntactically correct FunctionDefinition (i.e., with a single \em lambda element)
   \param  fout A writable output stream for writing the code.
   */
  void printFunctionDefinition (size_t n, const FunctionDefinition* fd, ostream &fout);

  /**
     Prints out a rule into the 'getRHS()' method.
     For an \em assignment rule, this method will print out in the format :
    \verbatim
      sp1 = 4 + a
    \endverbatim
     where the LHS is a valid SBML identifier (e.g., compartment, species or parameter) and the RHS can be evaluated.
    \param r A non-null, valid SBML Rule object
    \param fout The output stream to write to.
   */
  void printRuleMath ( const Rule* r, ostream &fout);

  /**
    Setter for the model to enable testing of decoding functionality that needs a C++ model.
    \param cmodel A non-null, initialised *cmodel.
  */
  void setModel( cModel* cmodel);



  /**
    Prints the reaction kinetics in the form:
    \verbatim
   {
   double Reac_1=V_flow_1_;
   // A
   yout[0]-=1*Reac_1;
   // B
   yout[1]+=1*Reac_1;
   }
    \endverbatim ( from the abc_1.xml example )
    For the reaction math to be printed, each reaction must contain a valid kinetic law, with a math definition of the law set inside.
    If this is not the case, a comment will be generated in the C source file.
   \param r A valid Reaction object.
   \param fout An ostream to write to.
   */
  void printReactionMath (const Reaction* r, ostream &fout);


  /**
   * Prints out event assigments within the getRHS() method of cModel. For this work correctly:
   *  -# A single Trigger element must be present for each Event.
   *  -# The trigger element's math must contain a relational operator as its first node. I.e., the trigger expression must be of the form: a > b, a < b, etc for
   *     the standard relational  operators  >, <, >=, <=, !=, and  ==.
   *  -# The math on each side of the relational operator should be interpretable by the SBML_Formula_toString() utility method. I.e., it should not contain further inequalities.
   *
   * Delay elements are not supported yet in the model.

    A sample of SBML that is interpretable by SBML2C is written below:

   * \verbatim
   <event>
   <trigger>
   <math xmlns="http://www.w3.org/1998/Math/MathML">
   <apply>
   <leq/>
   <ci> t </ci>
   <apply>
   <times/>
   <ci> num_cycles_e </ci>
   <ci> cyclePeriod_e </ci>
   </apply>
   </apply>
   </math>
   </trigger>
   <listOfEventAssignments>
   <eventAssignment variable="light">
   <math xmlns="http://www.w3.org/1998/Math/MathML">
   <apply>
   <ci> LightFunction </ci>
   <csymbol encoding="text" definitionURL="http://www.sbml.org/sbml/symbols/time"> t </csymbol>
   <ci> lightOffset </ci>
   <ci> lightAmplitude </ci>
   <ci> phase </ci>
   <ci> photoPeriod_e </ci>
   <ci> cyclePeriod_e </ci>
   </apply>
   </math>
   </eventAssignment>
   </listOfEventAssignments>
   </event>
   * \endverbatim
   *
   * \param A non null, valid Event *
  *  \param A writeable output stream.
   */
  void printEventMath  (const Event* e, ostream &fout);


  /*!
   * This method obtains the upper value of a parameter's constraint. If the parameter name does not recognised, or if the parameter value is higher
   * than the constraint  constraint,it returns the value pvalue
   *\param name the parameter's ID
   *\param pvalue the current parameter value
   */
  double getParamUpperByName(string name, double pvalue);

private :

  void printEventAssignmentMath ( const EventAssignment* ea, ostream &fout);



  void printMath (const Model* m, ostream &fout);
  void printStateUpdate(string str, ostream &stream);

  void printHeader (const Model* m, ostream &hout);



  string Transform_formula(const char* formula);

  void setParamUpperByName(string name, double value);
  void setParamLowerByName(string name, double value);



  cModel *cmodel;

  vector<string>ParamNames;
  vector<double>upper;
  vector<double>lower;

};// class

#endif
