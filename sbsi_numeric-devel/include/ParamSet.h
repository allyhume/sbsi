/********************************************************************/
/*!
  @file ParamSet.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.2 $

*/
/******************************************************************/

using namespace std;

#ifndef __ParamSet_h__
#define __ParamSet_h__

class ParamSet
{
public:


  double cost;

  vector<double> params;

  string filename;

  /* Utilities */

  ParamSet(vector<double> p, double c, string f): cost(c), params(p), filename(f) {}

  ParamSet(vector<double> p): params(p) {}

  ParamSet(vector<double> p, double c): cost(c), params(p) {}

  ParamSet() {}
};
#endif



