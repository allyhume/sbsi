/********************************************************************/
/*!
  @file ModelCostFunction.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.3 $

*/
/******************************************************************/

using namespace std;
#include <stdint.h>
#ifndef __ModelCostFunction_h__
#define __ModelCostFunction_h__

#include <CostFunction.h>
#include <cModel.h>
#include <Solver.h>
#include <DataSet.h>
#include <CostArg.h>
#include <TimeSeries.h>


class ModelCostFunction: public CostFunction
{
public:

  virtual ~ModelCostFunction() {
    delete solver;
  };

  virtual void Init(cModel *model,  vector<CostArg>& ca, vector<string>& pn);

  virtual void Init() {
    costargs.clear();
  };
  ;
  void SetUnOptimiseParameters(vector<string>& pn, vector<double>& pv) {
    UnOptimiseParamNames = pn;
    UnOptimiseParamValues = pv;
  };

  void SetUnOptimiseParam();

  void SetGlbCostArg(CostArg &gbca) {
    GlbCostArg = gbca;
  };

  void SetCostArgs(vector<CostArg> &ca) {
    costargs = ca;
  };

  void SetSolver(Solver * s) {
    solver = s;
  };
  void SetSolver();

  void SetModel(cModel * m) {
    Model = m;
  };
  void SetModelCostWeight(double w){
    model_cost_weight = w;
  }


  static ModelCostFunction& SetModelCostFunction(const ModelCostFunctionType m);

  void SetTimeSeries(TimeSeries *ts) {
    TSeries = ts;
  };

  void SetParameters(const vector<double> in_param);

  vector<double> GetCosts(void) {
    return Costs;
  };

  vector<double> GetResultVec(string f_dataname);
  vector<double> GetResultTimeVec();

  virtual void SetDataSets(vector<DataSet> *ds_s) {
    dataset_s = ds_s;
  };
  virtual void SetDataSet(const size_t d_i);
  DataSet* GetADataSet(const size_t d_i) {
    return &(*dataset_s)[d_i];
  };

  CostArg GetACostArg(int a) {
    return costargs.at(a);
  }
  bool IsCostArgsEmpty() {
    return costargs.empty();
  };
  double GetDataStartTime() {
    return costarg.ExpT0; // in only Chi2Cost
  }

  double GetOutputInterval() {
    return costarg.OutputInterval; // in only ModelCostFunction
  }

  virtual double GetCost() = 0;
  virtual double getcost() = 0;

  //virtual void DoMyCheck(){
  //   cout <<  "dataset_s " << dataset_s->size() << endl;
  //   cout <<  "dataset_s Initial " << GetADataSet(0)->GetNumInitialStateNames()<< endl;
  //};

  bool Solve() {
    SolverInit();
    return SolveRHS();
  };


  vector<double> Costs;
  cModel *Model;
  Solver *solver;
  TimeSeries *TSeries;
  double model_cost_weight;

protected:
  vector<DataSet> *dataset_s;
  bool SolveRHS();
  void SolverInit();
  vector<vector<double> > Weight;
  vector<double> Curve;
  vector<CostArg> costargs;
  CostArg costarg;

  DataSet *dataset;


  vector<string> UnOptimiseParamNames;
  vector<double> UnOptimiseParamValues;

};

#endif

