/*********************************************************/
/*!
  @file KINETICSolver.h
  @date 2010/03/29
  @brief For Bounday Value Problem Solver
  @author Azusa Yamaguchi
*/
/*********************************************************/

using namespace std;

#ifndef __KINETICSolver_h__
#define __KINETICSolver_h__

#include <cModel.h>
#include <Solver.h>
#include <kinsol/kinsol.h>
#include <kinsol/kinsol_dense.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_types.h>
#include <sundials/sundials_math.h>


/*!
  @brief Class for a boundary value problem solver
         Not implimented yet.
*/
class KINETICSolver: public Solver
{

public:

  KINETICSolver() {};

  virtual bool Solve();
  virtual bool SolveInput();
  virtual bool SolveInit();
  virtual bool SolveCreate();

protected:
  bool ksl_solver();
  int ksl_f(N_Vector u, N_Vector f);
  //int  CheckFlag(void *flagvalue, const char *funcname, int opt);
  // Used to bounce back to C++ proper with f_data set to "this"
  static int   ksl_f_bouncer( N_Vector u, N_Vector f, void *f_data);


  void *solver_mem;

  N_Vector u; /* initial guess */
  N_Vector c; /* constraints */
  N_Vector s; /* scale */

  vector <vector<double> > results;

  double functol;
  double steptol;

  int mset;  /* MaxSetupCall */
  int glstr; /* specify globslization strategy KIN_NONE or KIN_LINESEARCH */

};

#endif

