
#ifndef __PGASobol_h__
#define __PGASobol_h__

#include <Optimiser.h>
#include <PGSWrapper.h>

class PGASOBOL: public PGSWrapper
{

public:

  PGASOBOL() {
    fixedSeed = 0;
  }
  PGASOBOL(int seed) {
    fixedSeed = seed;
  }

  virtual void Initialise();

  virtual int Run();

  virtual void endOfGen(PGAContext *ctx);

protected:

};

#endif

