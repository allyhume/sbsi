/******************************************************/
/*!
  @ AdditiveMultiObjCost.h
  @date 2009
  @author Anatoly Sorokin
  $Revision: 1.1.2.2 $

  $LastChangedBy: Azusa Yamaguchi$
  $LastChangeedDate: 2010/03/29$
*/
/****************************************************/


#ifndef __AdditiveMultiObjCost_h__
#define __AdditiveMultiObjCost_h__

#include <CostArg.h>
#include <MultiObjCost.h>

using namespace std;

/*!
  @brief Class for additive multi objective cost function
*/
class AdditiveMultiObjCost: public MultiObjCost
{

  /*!
     @bried Get a cost value.
     @return a cost value
   */
public:
  virtual double GetCost();
};

#endif
