/********************************************************************/
/*!
  @file CostFunction.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.3 $

*/
/******************************************************************/

#include <iostream>
#include <exception>
#include <cmath>
using namespace std;

#include <CostArg.h>

#ifndef __CostFunction_h__
#define __CostFunction_h__


/*!
 @brief A top class for an operation of cost functions.
 */
class CostFunction
{

public:

  CostFunction() {};

  virtual ~CostFunction() {};

  /*!
   @brief A virtual function for initialisation of ParamNames and ParamValues vector
   */
  virtual void Init() {
    ParamNames.clear();
    ParamValues.clear();
  };

  /*!
   @brief A function to set a static cost function.
   @param a costFunctionType
   @return the pointer of the CostFunction
   */
  static CostFunction& SetCostFunction(const CostFunctionType m);

  /*!
   @brief A virtual function to add an cost function. It is implimented on MultiObjCostFunction and SingleObjCostFunction.
   @param a pointer of  the costFunctionType
   */
  virtual int AddObjective(CostFunction &acf) {
    return 0;
  };

  /*!
   @brief A virtual function for a top level function to calculate cost values. Should be implemented by all cost functions.
   @return a cost value
   */
  virtual double GetCost(void) = 0;

  /* ParamNames */
  /*!
   @brief a function for operating a vector of paramerer names.
   @return a vector of parameter names.
   */
  virtual vector<string> &GetParamNames() {
    return ParamNames;
  };

  /*!
   @brief a function for checking a vector of paramerer names is empty.
   @return a bool value.
   */
  bool IsParamNamesEmpty() {
    return ParamNames.empty();
  }

  /*!
   @brief a virtual function for set a vector of paramerer names to the ParamNames vector.
   @param a vector of string
   */
  virtual void SetParamNames(const vector<string> pn) {
    ParamNames = pn;
  };

  /*!
   @brief a function for checking a size of the ParamNames vector.
   @return size_t the size of ParamNames.
   */
  size_t GetNumParamNames() {
    return ParamNames.size();
  };


  /* ParamValues */
  /*!
   @brief a virtual function for setting a vector of parameter values
   @param a double vector of parameter values
   */
  virtual void SetParameters(const vector<double> in_param) {
    ParamValues = in_param;
  };

  size_t GetNumParameters() {
    return ParamValues.size();
  };

  void AddAParameters(double a) {
    ParamValues.push_back(a);
  }
  /*!
   @brief a virtual function for returning the vector of parameter values
   @return the double vector of parameter values
   */
  virtual vector<double>  &GetParameters() {
    return ParamValues;
  };

  /* CostArg */

  /*!
   @brief a virtual function  for settting a global cost function arguments
   @param CostArg for the global cost function arguments
   */
  virtual void SetGlbCostArg(CostArg &gbca) {
    GlbCostArg = gbca;
  };

  /*!
   @brief a function  for returnig a solver type from GlbCostArg
   @return solvertype
   */
  SolverType GetSolverType() {
    return GlbCostArg.solvertype;
  }

  /*!
   @brief a function  for returnig a MaxCost value from GlbCostArg
   @return double MAXCOST
   */
  double GetMAXCost() {
    return GlbCostArg.MAXCOST;
  }

  /*!
   @brief a function  for returnig the maximum number of random/sobol number generation
   @return int MAX_RandomNumGen
   */
  int GetMAXNumRandomGen() {
    return GlbCostArg.MAX_RandomNumGen;
  }

  virtual void DoMyCheck() {};

protected:
  /*!
   @brief the vector of parameter nanmes
   */
  vector<string> ParamNames;

  /*!
   @brief the vector of parameter values, the size should be the same as the ParamNames
   */
  vector<double> ParamValues;

  /*!
   @brief the argument of the global costfunction. It has a solver type, MaxCost and MAX_RandomNumGen.
   */
  CostArg GlbCostArg;


};


#endif
