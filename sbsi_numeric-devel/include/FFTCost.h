/********************************************************************/
/*!
  @file FFTCost.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.3 $

*/
/******************************************************************/

#include <config.h>

#ifdef ENABLE_FFTW2

#include <drfftw.h>
#include <dfftw.h>

#else

#include <fftw3.h>

#endif

using namespace std;

#ifndef __FFTCostFunction_h__
#define __FFTCostFunction_h__

#include "ModelCostFunction.h"

class FFTCost : public ModelCostFunction
{

public:

  ~FFTCost() {};

  virtual void SetDataSet(size_t d_i);
  virtual double GetCost();
  virtual double getcost();

  size_t GetNumFFTTarget() {
    return costarg.FFTTarget.size();
  };
  vector<string> GetFFTTargets() {
    return costarg.FFTTarget;
  }
  string GetAFFTTarget(int a ) {
    return costarg.FFTTarget.at(a);
  }

  size_t GetNumFFTPeriod() {
    return costarg.Period.size();
  };
  vector<double> GetFFTPeriods() {
    return costarg.Period;
  }
  double GetAFFTPeriod(int a ) {
    return costarg.Period.at(a);
  }

  int GetAFFTAmpFnc(int a) {
    return costarg.AmpFnc.at(a);
  }

  size_t GetNumFFTTstart() {
    return costarg.TStart.size();
  };
  vector<double> GetFFTTstart() {
    return costarg.TStart;
  }
  double GetAFFTTstart(int a ) {
    return costarg.TStart.at(a);
  }

  size_t GetNumFFTTend() {
    return costarg.TEnd.size();
  };
  vector<double> GetFFTTend() {
    return costarg.TEnd;
  }
  double GetAFFTTend(int a ) {
    return costarg.TEnd.at(a);
  }

  size_t GetNumMinAmplitude() {
    return costarg.MinAmplitude.size();
  };
  vector<double> GetMinAmplitude() {
    return costarg.MinAmplitude;
  }
  double GetAMinAmplitude(int a ) {
    return costarg.MinAmplitude.at(a);
  }

  size_t GetNumMaxAmplitude() {
    return costarg.MaxAmplitude.size();
  };
  vector<double> GetMaxAmplitude() {
    return costarg.MaxAmplitude;
  }
  double GetAMaxAmplitude(int a ) {
    return costarg.MaxAmplitude.at(a);
  }

  vector<double> GetResultPeriods() {
    return Period;
  }

  double GetAPeriod(int a) {
    return Period.at(a);
  }

// XXX ACH - removing as not used I think
  //double GetAPhase(int a) {
  //  return Phase.at(a);
 // }

  void GetFFT3(vector<double> vec);

  void GetFFT2(vector<double> vec);

protected:
  
  vector<double> Period;
   // XXX ACH - removing as not used i think
  //vector<double> Phase;

  vector<double> power_spect;
  //vector<double> phase;
  fftw_plan p;

};
#endif

