using namespace std;

#ifndef __ParamInfo_h__
#define __ParamInfo_h__

struct ParamInfo {

public:

  ParamInfo() {
    Clear();
  }

  ~ParamInfo() {}

  void Clear() {
    Upper.clear();
    Lower.clear();
    InitialStep.clear();
    ParamName.clear();
    UnOptimiseParamName.clear();
    UnOptimiseParamValue.clear();
  }

  bool IsUpperEmpty()const {
    return Upper.empty();
  }
  size_t GetNumUpper()const {
    return Upper.size();
  }
  void SetAUpper(int aI, double v) {
    Upper.at(aI) = v;
  }
  void AddAUpper(double v) {
    Upper.push_back(v);
  }
  void SetUpper(vector<double> v) {
    Upper = v;
  }
  double GetAUpper(int aI)const {
    return Upper.at(aI);
  }
  vector<double>& GetUpperVec() {
    return Upper;
  }
  double* GetUpper() {
    return &Upper[0];
  }

  bool IsLowerEmpty()const {
    return Lower.empty();
  }
  size_t GetNumLower()const {
    return Lower.size();
  }
  void SetALower(int aI, double v) {
    Lower.at(aI) = v;
  }
  void AddALower(double v) {
    Lower.push_back(v);
  }
  void SetLower(vector<double> v) {
    Lower = v;
  }
  double GetALower(int aI)const {
    return Lower.at(aI);
  }
  vector<double> &GetLowerVec() {
    return Lower;
  }
  double* GetLower() {
    return &Lower[0];
  }

  size_t GetNumInitialStep()const {
    return InitialStep.size();
  }
  bool IsInitialStepEmpty()const {
    return InitialStep.empty();
  }
  void SetAInitialStep(int aI, double v) {
    InitialStep.at(aI) = v;
  }
  void AddAInitialStep(double v) {
    InitialStep.push_back(v);
  }
  void SetInitialStep(vector<double> v) {
    InitialStep = v;
  }
  double GetAInitialStep(int aI)const {
    return InitialStep.at(aI);
  }
  vector<double>& GetInitialStep() {
    return InitialStep;
  }

  size_t GetNumParamName()const {
    return ParamName.size();
  }
  bool IsParamNameEmpty() {
    return ParamName.empty();
  }
  void SetAParamName(int aI, string v) {
    ParamName.at(aI) = v;
  }
  void AddAParamName(string v) {
    ParamName.push_back(v);
  }
  void SetParamName(vector<string> v) {
    ParamName = v;
  }
  string GetAParamName(int aI)const {
    return ParamName.at(aI);
  }
  vector<string>& GetParamName() {
    return ParamName;
  }

  size_t GetNumUnOptimiseParamName()const {
    return UnOptimiseParamName.size();
  }
  bool IsUnOptimiseParamNameEmpty() {
    return UnOptimiseParamName.empty();
  }
  void SetAUnOptimiseParamName(int aI, string v) {
    UnOptimiseParamName.at(aI) = v;
  }
  void AddAUnOptimiseParamName(string v) {
    UnOptimiseParamName.push_back(v);
  }
  void SetUnOptimiseParamName(vector<string> v) {
    UnOptimiseParamName = v;
  }
  string GetAUnOptimiseParamName(int aI)const {
    return UnOptimiseParamName.at(aI);
  }
  vector<string>& GetUnOptimiseParamName() {
    return UnOptimiseParamName;
  }

  size_t GetNumUnOptimiseParamValue()const {
    return UnOptimiseParamValue.size();
  }
  bool IsUnOptimiseParamValueEmpty() {
    return UnOptimiseParamValue.empty();
  }
  void SetAUnOptimiseParamValue(int aI, double v) {
    UnOptimiseParamValue.at(aI) = v;
  }
  void AddAUnOptimiseParamValue(double v) {
    UnOptimiseParamValue.push_back(v);
  }
  void SetUnOptimiseParamValue(vector<double> v) {
    UnOptimiseParamValue = v;
  }
  double GetAUnOptimiseParamValue(int aI)const {
    return UnOptimiseParamValue.at(aI);
  }
  vector<double>& GetUnOptimiseParamValue() {
    return UnOptimiseParamValue;
  }


protected:

  vector<double> Upper;
  vector<double> Lower;
  vector<double> InitialStep;
  vector<string> ParamName;
  vector<string> UnOptimiseParamName;
  vector<double> UnOptimiseParamValue;

};
#endif



