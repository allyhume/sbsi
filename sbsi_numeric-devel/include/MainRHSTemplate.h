
#include <iostream>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <ctype.h>
#include <unistd.h>
#include <sys/param.h>//MAXPATHLEN
#include <mpi.h>

#include <CVODESolver.h>
#include <FileReader.h>


string get_line(fstream &f)
{
  char buf[1048576];
  buf[0] = '\0';
  f.getline(buf, 1048576);
  return string(buf);
}

vector<string> tokenize(string line)
{
  vector<string> list;
  // line = StripWhitespace(line);
  string::iterator i = line.begin();
  while (i != line.end()) {
    string::iterator j = i;
    for (j = i; *j != '\t' && j != line.end(); j++) {
      ;
    }

    string word = string(i, j);
    list.push_back(word);
    i = j;
    if ( i != line.end() ) {
      i++;
    }
  }
  return list;
}


void override_parameters(cModel *model, const char* file)
{
  // First we quickly check if the file exists for reading
  // if not, then we must error since by this stage we have already
  // checked the possibility that there are no param-overrides to be made.
  if ( access( file, R_OK ) == -1 ) {
    printf("ERROR: parameter overrides file is not accessible\n");
    exit(8);
  }

  fstream f;
  f.open(file, ios::in);


  while (!f.eof()) {
    string line = get_line(f);
    vector<string> columns = tokenize(line);
    if (columns.size() >= 2) {
      string param_name = columns.front();
      double param_value = atof(columns.at(1).c_str());
      model->setParamByName(param_name, param_value);
    }
  }// End of the while loop
  f.close();
}


int main(int argc, char **argv)
{
  /************************************************************/
  /*                      SKELTON                             */
  /*                                                          */
  /* Part 1 ; INPUT                                           */
  /*    1) set FileReader Type for Exp. data and its filename */
  /*    2) set cModel_arg                                      */
  /*            i) set CostFunction Type;                     */
  /*           ii) set Solver Type                            */
  /*          iii) set cModelCostFunctionType                  */
  /*                                                          */
  /* Part 2; EXECUTE                                          */
  /*    1) Create cModel                                       */
  /*    2) Calculate RHS of the Model                          */
  /*    2) Write the result of Timeseris to the file          */
  /************************************************************/
  MPI_Init(&argc, &argv);
  int my_rank(0);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  /* set default*/

  /* read argv */

  int argv_i = 0;

  char *model_name = argv[++argv_i];
  string MODEL_NAME = model_name;

  /*make model_dir under current dir */

  char currentdir[MAXPATHLEN];

  getcwd(currentdir, MAXPATHLEN);

  string modeldir = currentdir;


  modeldir += "/";
  modeldir += MODEL_NAME;
  modeldir += "/";

  //chdir(modeldir.c_str());

  getcwd(currentdir, MAXPATHLEN);


  /* read set up numbers */

  int tmp_int;
  double tmp_double;

  const char *scan_int_str = "%d";
  const char *scan_double_str = "%lf";

  /* Input cModel_arg */
  cModelArg model_arg;

  if (sscanf(argv[++argv_i], scan_double_str, &tmp_double) != 1) {
    printf("ERROR: t_final scannging argv[%d]\n ", argv_i);
    exit(8);
  }
  model_arg.Final = tmp_double;

  if (my_rank == 0) {
    printf("t_final %f\n", model_arg.Final);
  }

  if (sscanf(argv[++argv_i], scan_double_str, &tmp_double) != 1) {
    printf("ERROR: t_init scannging argv[%d]\n ", argv_i);
    exit(8);
  }
  model_arg.Init = tmp_double;

  if (sscanf(argv[++argv_i], scan_int_str, &tmp_int) != 1) {
    printf("ERROR: maxTimes scannging argv[%d]\n ", argv_i);
    exit(8);
  }
  model_arg.MaxTimes = tmp_int;

  if (sscanf(argv[++argv_i], scan_double_str, &tmp_double) != 1) {
    printf("ERROR: interval scannging argv[%d]\n ", argv_i);
    exit(8);
  }
  model_arg.Interval = tmp_double;

  if (sscanf(argv[++argv_i], scan_double_str, &tmp_double) != 1) {
    printf("ERROR: ouput interval scannging argv[%d]\n ", argv_i);
    exit(8);
  }
  model_arg.OutputInterval = tmp_double;

  if (sscanf(argv[++argv_i], scan_double_str, &tmp_double) != 1) {
    printf("ERROR: atol scannging argv[%d]\n ", argv_i);
    exit(8);
  }
  model_arg.Atol = tmp_double;

  if (sscanf(argv[++argv_i], scan_double_str, &tmp_double) != 1) {
    printf("ERROR: reltol scannging argv[%d]\n ", argv_i);
    exit(8);
  }
  model_arg.Reltol = tmp_double;


  /* Set the TimeSeries file name*/
  char * t_seriesfilename = argv[++argv_i];
  ostringstream oss_tfout;
  oss_tfout << t_seriesfilename  << "_RHS.dat";

  string t_series_fname = oss_tfout.str();

  /* Set the filename for the parameter overrides if there is one
   * given on the command-line
   */
  char * param_overrides_filename = NULL;
  if (argc > argv_i) {
    param_overrides_filename = argv[++argv_i];
  }

  /*************************EXECUTE ****************************************/



  /* create the model */
  static cModel   *model = getModel();

  model->setModelArg(model_arg);
  model->inputModel();

  /* set TimeSeries */
  TimeSeries t_series;
  t_series.SetModel(model);

  /* set Solver */

  CVODESolver  cvodesolver;
  Solver *solver = &(cvodesolver);
  solver->SetModel(model);
  solver->SolveCreate();
  solver->SolveInit();
  solver->SetTimeSeries(&t_series);

  // Now we can override the parameters if a param-overrides file
  // has been specified on the command-line
  if (param_overrides_filename != NULL) {
    override_parameters(model, param_overrides_filename);
  }

  model->calCoef();
  solver->SolveInput();
  if (solver->Solve()) {
    if (my_rank == 0) {
      t_series.WriteToSBSIDataFormat(t_series_fname);
    }
  }

  int stat(0);
  MPI_Initialized(&stat);
  if (stat) {
    MPI_Finalize();
  }

  if (my_rank == 0) {
    cout << "finish jobs" << endl;
  }


}

