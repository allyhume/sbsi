/********************************************************************/
/*!
  @file ERR.h
  @date 2010/03/29
  @author Azusa Yamaguchi
  $Revision: 1.1.2.1 $

*/
/******************************************************************/

#include <string>
#include <cstdarg>
#include <vector>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <mpi.h>


using namespace std;

#ifndef INCLUDED_ERROR_h
#define INCLUDED_ERROR_h

#ifdef __GNUC__
#define NORETURN __attribute__ ((noreturn))
#else
#define NORETURN
#endif

/*!
  @class Error
  @brief Error handler class: Error messages will be in the OFW.error and stdout.
*/
class Error
{
private:

  string error_class_name;
  string error_func_name;
  string error_file_name;

  //!< Types of error.
  enum {
    pointer,
    file_r,
    file_w,
    file_a,
    not_implemented,
    general,
    n_error_types
  };

  vector<int> exit_value;
  vector<string> error_string;


public:

  Error();

  virtual ~Error();

  //!< Error message for an uninitialized pointer.
  void Pointer(const string, const string, const string) NORETURN;

  //!< Error message for failure to open a file to read.
  void FileR(const string, const string, const string) NORETURN;

  //!< Error message for failure to open a file to write.
  void FileW(const string, const string, const string) NORETURN;

  //!< Error message for failure to open a file to append.
  void FileA(const string, const string, const string) NORETURN;

  //!< Error message when something is not implemented.
  void NotImplemented(const string, const string) NORETURN;

  //!< Error message when something is not implemented.
  void NotImplemented(const string, const string, const string, ...) NORETURN;


  //!< Error message for miscellaneous failure.
  void General(const string, const string, const string, ...) NORETURN;


};


//------------------------------------------------------------------
/*! An instance of the Error class, named ERR, should be
  created at the highest scope (outside main). This external declaration
  allows  access to the error message printing mechanisms.
*/
//------------------------------------------------------------------
extern Error ERR;

#endif



