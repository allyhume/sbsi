/********************************************************************/
/*!
  @file cModel.h
  @date 2010/02/19

  @author Azusa Yamaguchi
  $Revision: 1.1.2.3 $

*/
/******************************************************************/

#include <string>
#include <vector>

using namespace std;

#ifndef __cModel_h__
#define __cModel_h__

#include <cModelArg.h>

/*!
 @brief Model handler
*/
class cModel
{
public:

  cModel() {};

  virtual ~cModel() {};

  /*!
   @brief virtual interface implemented by specific models
  */
  virtual void inputModel() = 0;

  /*!
   @brief set ModelArg to this model.
   */
  void setModelArg(cModelArg & ma) {
    ModelArg = ma;
  };

  /*!
  @brief Initialise the model arg. It should be called after setModelArg.
  */
  void initModelArg();


  /*!
   @brief virtual, it returns values of right hand side of the system to be solved
     dydt, according to CVODE
   @param a double pointer of y_in, double time, double pointer of  y_out
  */
  virtual void getRHS(double *y_in, double t, double *yout) = 0;


  /*!
   * @brief virtual, it updates a timeseries row for the assignment rules.
   * Variables which are assigned to within assignment rules cannot be
   * calculated by the solver since it only asks the user model to
   * calculate the derivative at any given time.
   */
  virtual void update_assignment_variables(
     vector<vector <double> > *results) = 0;

  /*!
   @brief virtual interface for calculating a varuable using the results of solved the right hand side  of the model.
   @param the index of the varuable.
   @return the varuable value
  */
  //virtual double calVar(int iv) = 0;

  virtual vector<double> calVarVec(int iv, vector<vector<double> > ) = 0;

  /*!
   @brief virtual interface for calculating the coefficient using the parameters.
   */
  virtual void calCoef() = 0;

  virtual void setBoundary() {};
  virtual void setInitialGuess() {};
  /*******************************************************************/
  /* Generic parameter handling that is model independent            */
  /*******************************************************************/

  /*Params */
  size_t getNumParameters(void);
  void setParameters(vector<double>& values);
  vector<double>& getParameters(void);
  vector<string>& getParamNames(void);
  void setParamByName(string name, double value);
  double getParamByName(string name);
  size_t getParamIndexByName(string name);
  string getParamNameByIndex(size_t i);
  void setParamNameByIndex(size_t i, string name);
  void clearParams();

  /* States */
  size_t getNumStates(void);
  void setStates(vector<double>& values);
  vector<double>& getStates(void);
  vector<string>& getStateNames(void);
  void setStateByName(string name, double value);
  double getStateByName(string name);
  string getStateNameByIndex(size_t i);
  size_t getStateIndexByName(string name);
  void clearStates();

  /* Vars */
  void setVars(vector<double>& values);
  vector<double>& getVars(void);
  vector<string>& getVarNames(void);
  void setVarByName(string name, double value);
  double getVarByName(string name);
  size_t getVarIndexByName(string name);
  size_t getNumVars(void);
  void clearVars();

  /* Times */

  void setExpTInit(double t0);
  double getExpTInit();
  void setOutInterval(double t0);
  double getOutInterval();
  void setInterval(double t0);
  double getInterval();
  void setReportInterval(double t0);
  double getReportInterval();
  double getInit();
  void setInit(double kpi);
  double getFinal();
  void setFinal(double kpf);
  string getKeyParamName();
  void setKeyParamName(string name);

  /* Other */

  void setAtol(double atol);
  void setReltol(double reltol);
  double getAtol();
  double getReltol();
  void setStr(int glstr);
  void setMset(int mset);
  int getStr();
  int getMset();
  void setFnormtol(double fnormtol);
  void setSteptol(double steptol);
  double getFnormtol();
  double getSteptol();
  int getMaxTimes();
  void setMaxTimes(int max_times);

  virtual void setConstraints() {};
  vector<double>& getConstraints() {
    return ModelArg.Constraints;
  };


  /* Util */
  void writeResults(const string filename);

  cModelArg ModelArg;

  string cModel_id;
};

#endif
