/********************************************************************/
/*!
  @file DataSet.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.2 $

*/
/******************************************************************/
#include <fstream>
#include <vector>
#include <iostream>
using namespace std;

#ifndef __DataSet_h__
#define __DataSet_h__

struct DataSet {
public:
  DataSet() {
    SetName.clear();
    DataName.clear();
    InitialStateNames.clear();
    InitialStateValues.clear();
    InitialParamNames.clear();
    InitialParamValues.clear();
    DataValues.clear();
  }


  bool IsSetNameEmpty()const {
    return SetName.empty();
  }
  void SetSetName(string name) {
    SetName = name;
  }
  string GetSetName()const {
    return SetName;
  }

  bool IsDataNamesEmpty()const {
    return DataName.empty();
  }
  const vector<string>& GetDataNames() {
    return DataName;
  }
  size_t GetNumDataNames()const {
    return DataName.size();
  }
  string GetADataName(int a)const {
    return DataName.at(a);
  }
  void SetADataName(int a, string name) {
    DataName.at(a) = name;
  }
  void AddADataName(string name) {
    DataName.push_back(name);
  }

  bool IsInitialStateNamesEmpty()const {
    return InitialStateNames.empty();
  }
  const vector<string>& GetInitialStateNames() {
    return InitialStateNames;
  }
  size_t GetNumInitialStateNames()const {
    return InitialStateNames.size();
  }
  vector<string>::iterator GetInitialStateNamesBegin() {
    return InitialStateNames.begin();
  }
  string GetAInitialStateName(int a)const {
    return InitialStateNames.at(a);
  }
  void SetAInitialStateName(int a, string name) {
    InitialStateNames.at(a) = name;
  }
  void AddAInitialStateName(string name) {
    InitialStateNames.push_back(name);
  }

  bool IsInitialStateValuesEmpty()const {
    return InitialStateValues.empty();
  }
  const vector<double>& GetInitialStateValues() {
    return InitialStateValues;
  }
  size_t GetNumInitialStateValues()const {
    return InitialStateValues.size();
  }
  double GetAInitialStateValue(int a)const {
    return InitialStateValues.at(a);
  }
  void SetAInitialStateValue(int a, double v) {
    InitialStateValues.at(a) = v;
  }
  void AddAInitialStateValue(double v) {
    InitialStateValues.push_back(v);
  }

  bool IsInitialParamNamesEmpty()const {
    return InitialParamNames.empty();
  }
  const vector<string>& GetInitialParamNames() {
    return InitialParamNames;
  }
  size_t GetNumInitialParamNames()const {
    return InitialParamNames.size();
  }
  string GetAInitialParamName(int a)const {
    return InitialParamNames.at(a);
  }
  void SetAInitialParamName(int a, string name) {
    InitialParamNames.at(a) = name;
  }
  void AddAInitialParamName(string name) {
    InitialParamNames.push_back(name);
  }

  bool IsInitialParamValuesEmpty()const {
    return InitialParamValues.empty();
  }
  const vector<double>& GetInitialParamValues() {
    return InitialParamValues;
  }
  size_t GetNumInitialParamValues()const {
    return InitialParamValues.size();
  }
  double GetAInitialParamValue(int a)const {
    return InitialParamValues.at(a);
  }
  void SetAInitialParamValue(int a, double v) {
    InitialParamValues.at(a) = v;
  }
  void AddAInitialParamValue(double v) {
    InitialParamValues.push_back(v);
  }

  const vector<vector<double> >& GetDataValues() {
    return DataValues;
  }

  size_t GetNumDataValues()const {
    return DataValues.size();
  }
  bool IsDataValuesEmpty()const {
    return DataValues.empty();
  }

  size_t GetNumDataTimes()const {
    return DataValues.at(0).size();
  }
  bool IsDataTimeEmpty()const {
    return DataValues.at(0).empty();
  }
  const vector<double>& GetDataTimes() {
    return DataValues.at(0);
  }
  double GetADataTime(int a)const {
    return DataValues.at(0).at(a);
  }
  double GetEndDataTime()const {
    return DataValues.at(0).back();
  }


  size_t GetNumADataValues(int a)const {
    return DataValues.at(a).size();
  }
  const vector<double>& GetADataValues(int a) {
    return DataValues.at(a);
  }
  double GetADataValue(int a, int b)const {
    return DataValues.at(a).at(b);
  }
  double GetAEndDataValue(int a)const {
    return DataValues.at(a).back();
  }

  void AddADataValues(vector<double> vs) {
    DataValues.push_back(vs);
  }
  void SetADataValue(int a, int b, double v) {
    DataValues.at(a).at(b) = v;
  }
  void AddADataValue(int a, double v) {
    DataValues.at(a).push_back(v);
  }
  void SetSizeDataValues(size_t a) {
    DataValues.resize(a);
  }

  //friend ostream &operator << (ostream &stream,DataSet *ds);
protected:
  string SetName; // Column 1
  vector<string> DataName;   // column names ; 1st one is time(t, T) and last ones are data name
  vector<string> InitialStateNames;  // from hdr file
  vector<double> InitialStateValues; // from hdr file
  vector<string> InitialParamNames;  // from hdr file
  vector<double> InitialParamValues; // from hdr file

  vector<vector<double> > DataValues; // 1st one is time(t or T) and last ones are data


};
#endif



