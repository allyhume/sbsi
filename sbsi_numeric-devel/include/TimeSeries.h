/********************************************************************/
/*!
  @file TimeSeries.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.3 $

*/
/******************************************************************/

#include <sstream>
#include <iostream>
#include <iomanip>

#ifndef __TimeSeries_h__
#define __TimeSeries_h__

#include <cModel.h>

using namespace std;

/*!
 * Encapsulates a time series data set.
 */
class TimeSeries
{

public:

  void SetModel(cModel *m) {
    model = m;
  } ;  // To write files


  void  CleanTimeSeries() {
    results.clear();
  };

  vector<double>&  GetAStateTimeSeries(int aI);

  vector<double>&  GetTimeSeriesTime() {
    return results.at(0);
  };

  vector<vector<double> >& GetTimeSeries() {
    return results;
  };

  vector<double > GetValuesAtATime(int aI);

  void SetTimeSeries(vector<vector<double> > *s_results);

  bool IsTimeSeriesEmpty() {
    return results.empty();
  }

  size_t GetNumTimeSeriesTime() {
    return results.front().size();
  }
  /* File OutPut Functions */

  /* the Head function to write a file with the file name is fileName */
  void WriteToSBSIDataFormat(string fileName);

  //Internal
  string CreateHeaderFileName(string dataFileName);
  string CreateColumnHeaderLine(vector<string> names);
  void WriteHeaderFile(string headerFileName, vector<string> names);
protected:

  cModel *model;
  vector<vector<double> > results;

};

#endif
