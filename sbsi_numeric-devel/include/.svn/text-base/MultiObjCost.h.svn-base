/********************************************************************/
/*!
  @file MultiObjCost.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.2 $

*/
/******************************************************************/

using namespace std;

#ifndef __MultiObjCost_h__
#define __MultiObjCost_h__

#include <CostFunction.h>


class MultiObjCost: public CostFunction
{

public:
  void Init() {
    multiobjcost_val.clear();
  };


public:
  virtual double GetCost();


  ~MultiObjCost() {};

  /*!
   * return all cost functions. It is possible that there are more than one (multiobjective optimisation)
   */
public:
  vector<double> GetCosts() {
    return multiobjcost_val;
  };

public:
  void SetParameters(const vector<double> in_param, int aI);

  /*!
   * return specific cost value from set of objectives to evaluate
   */

public:
  double GetCost(int aI) {
    return multiobjcost_val[aI];
  };

  void SetParameters(const vector<double> in_param);

  vector<string>& GetParamNames() ;

  vector<string>& GetParamNames(int aI) ;

  void  SetParamNames(const  vector<string> names) ;

  void  SetParamNames(const vector<string> names, int aI) ;

  /*!
   * return number of objectives for the cost function
   */
  virtual size_t GetNumObjectives() {
    return MCFun.size();
  };
  /*!
   * Set all objectives at once
   */
  virtual size_t SetObjectives(vector<CostFunction *> cfuns) {
    MCFun = cfuns;
    return MCFun.size();
  };


  /*!
   * add specific CostFunction to be responsible for calculation of i-th objective
   */
  int AddObjective(CostFunction &aMCFun) {
    MCFun.push_back(&aMCFun);
    return (int)MCFun.size() - 1;
  };

  //virtual void DoMyCheck(){};


  vector<double> multiobjcost_val;

  vector<CostFunction *> MCFun;

};


#endif
