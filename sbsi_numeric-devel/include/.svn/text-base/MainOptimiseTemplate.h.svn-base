#include <sys/time.h>
#include <mpi.h>          //MAXPATHLEN

#include <Setup.h>
#include <Optimiser.h>
#include <MultiObjCost.h>
#include <CommandLine.h>
#include <ParseConfigFile.h>
#include <ERR.h>

using namespace std;

double getnow()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + 1e-6 * tv.tv_usec;
}

void writeBestCostResults( const string fname,
                           const double cost,
                           cModel *model);

void SetMCF(cModel *m,
            vector<CostArg> &cas,
            vector<DataSet> &ds,
            ParamInfo &pi,
            CostArg &gblca,
            TimeSeries &ts,
            ModelCostFunction *mcf,
            double weight);

vector<DataSet> GetUCF_DataSets(UserCostFunctionSet &ucf, string exp_dir);

// A small clean up function, this is mostly so that we can call this
// from the INITCOST code which ends quite a bit before the optimiser code
// hence we don't have a really long preprocessor ifdef just for this
// bit of cleaning up.
void clean_up(int my_rank, double tstart){
  int stat(0);
  MPI_Initialized(&stat);
  if (stat) {
    MPI_Finalize();
  }

  if (my_rank == 0) {
    double elapsed = getnow() - tstart;
    cout << "finish jobs; elapsed " << elapsed << "sec" << endl;

  }

  return;
}


int main(int argc, char **argv)
{
  /* usage Model_name logfilename loglevel xmlfile */
  if (argc < 3) {
    cout << "Not right usage. Usage: " << argv[0]
         << " [Model ID] [config.xml file]" << endl;
    exit(8);
  }

  MPI_Init(&argc, &argv);
  int my_rank(0);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  double tstart = getnow();

  CommandLine::is(argc, argv, my_rank);


  /* usage Model_name  xmlfile */
  const string MODEL_NAME = CommandLine::arg();
  const string xmlfilename = CommandLine::arg();

  // cout << __LINE__ << endl;
  string modeldir = MODEL_NAME + "/";
  string exp_dir = modeldir + "exp_data/";
  string results_dir = modeldir + "results/";
  string ckpoint_dir = modeldir + "ckpoint/";

  /* prepare args */
  cModelArg ModelArg;
  OptArg opt_arg;

  /* set default values */
  opt_arg.setCkpointStartNum(0);
  opt_arg.setPrintBestPop(true);
  opt_arg.MyMutation = NULL;
  opt_arg.MyCrossOver = NULL;
  opt_arg.MyEndOfGen = NULL;

  opt_arg.setResultDir(results_dir);
  opt_arg.setCkpointDir(ckpoint_dir);

  ParseConfigFile parser(modeldir);

  CostArgSet setup_cost_args;
  CostArgSet optimiser_cost_args;
  CostArg GlbCostArg;

  parser.parseConfig(xmlfilename, opt_arg, GlbCostArg, setup_cost_args,
                     optimiser_cost_args, ModelArg);
  parser.loadData(setup_cost_args);
  parser.loadData(optimiser_cost_args);

#ifndef SBSI_NUMERICS_GETINITCOST
  /* EXEC */
  if (my_rank == 0) {
    cout << modeldir + parser.Init_Param_File << endl;
  }
#endif


  /*
   * Read param info file  and set paraminfo
   * Currently even just getting the initial cost requires a
   * proper paraminfo structure, which in turn requires a param
   * info file reader. Hence we have to make this up whether we are
   * simply getting the initial cost or running an entire optimisation
   * process. However note that in the case of getting the initial
   * cost we don't actually got to the trouble of reading the parameter
   * file. In that case we will then clear the sets of parameters and
   * use that when we initialise the optimiser which we need to
   * do in order to call Optimiser::GetInitialCost.
   */
  FileReader &paraminfo_freader =
    FileReader::SetFileReader(TabDelimitedColumnParamInfo);
  paraminfo_freader.ClearSets();
 /*
   * Currently even just getting the initial cost requires a
   * proper paraminfo structure, which in turn requires a param
   * info file reader. Hence 
   * This is simply a dummy which we won't read
   * any file with. We will then clear the sets of parameters and
   * use that when we initialise the optimiser which we need to
   * do in order to call Optimiser::GetInitialCost.
   */
#ifndef SBSI_NUMERICS_GETINITCOST
  paraminfo_freader.Read(modeldir + parser.Init_Param_File);
  // Not sure if this actually has to come after the initialisation
  // of paraminfo below, TEST THIS!
  /* create setup for the first parameter sets */
  Setup &setup_param = Setup::SetupParam(opt_arg.getSetupType());
#endif
  ParamInfo paraminfo = paraminfo_freader.paraminfo;

  ModelCostFunction &mcf_x2c = ModelCostFunction::SetModelCostFunction(X2Cost);
  ModelCostFunction &mcf_fft = ModelCostFunction::SetModelCostFunction(FFT);

  /* set CostFunction  */
  /* if there are no costfunctions set to the setup,
   * use the same costfunctions with opt_arg */

#ifndef NO_MODEL
  /* create the model */
  static cModel *model = getModel();
  model->setModelArg(ModelArg);
  model->inputModel();
  model->setFinal(std::max(model->getFinal(), setup_cost_args.maxtime()));
  TimeSeries t_series;
  t_series.SetModel(model);
#endif

  MultiObjCost *cf = new MultiObjCost();

  if (!setup_cost_args.X2C_cost_arg.empty() ||
      !setup_cost_args.FFT_cost_arg.empty() ||
      !setup_cost_args.UserCostFunctions.empty() ) {
    cf->Init();
  }

#ifndef NO_MODEL

  if (!setup_cost_args.X2C_cost_arg.empty()) {
    // Clearly incorrect to just take the back answer, however
    // I think we are essentially assuming that there is only one
    // chi-squared cost function, possibly with multiple data sets.
    double cost_weight = setup_cost_args.X2C_weights.back();
    SetMCF(model,
           setup_cost_args.X2C_cost_arg,
           setup_cost_args.X2C_dataset,
           paraminfo,
           GlbCostArg,
           t_series,
           &mcf_x2c,
           cost_weight);
    cf->AddObjective(mcf_x2c);
  }
  if (!setup_cost_args.FFT_cost_arg.empty()) {
    // As above it's clearly not the correct thing to do to just use
    // the back answer but we are assuming that there is only one
    // fft cost function (again possibly with multiple states/datasets)
    double cost_weight = setup_cost_args.FFT_weights.back();
     SetMCF(model,
           setup_cost_args.FFT_cost_arg,
           setup_cost_args.FFT_dataset,
           paraminfo,
           GlbCostArg,
           t_series,
           &mcf_fft,
           cost_weight);
   cf->AddObjective(mcf_fft);
  }
#endif

#ifndef NO_UCF
  vector<UserCostFunction*> ucf_p;
  vector<DataSet> UCF_DataSets;

  if (!setup_cost_args.UserCostFunctions.empty()) {
    for (size_t i = 0; i < setup_cost_args.UserCostFunctions.size(); i++) {
      UserCostFunction &ucf =
        UserCostFunction::setUserCostFunction(setup_cost_args.UserCostFunctions.at(i).type);
#ifdef SBSI_TEST
      vector<string> allpname = paraminfo.GetParamName();
      int anum(20);
      vector<string> pname;
      for (int j = 0; j < anum; j++) {
        pname.push_back(allpname.at(i * anum + j));
      }
      ucf.SetParamNames(pname);
#else
      ucf.SetModel(model);
      ucf.SetGlbCostArg(GlbCostArg);
      ucf.SetCostArgs(setup_cost_args.X2C_cost_arg);

      /* set dataset from file name */
      UCF_DataSets = GetUCF_DataSets(setup_cost_args.UserCostFunctions.at(i), parser.exp_dir);
      if (!UCF_DataSets.empty()) {
        ucf.SetDataSets(&UCF_DataSets);
      }
      ucf.SetTimeSeries(&t_series);
      ucf.SetSolver();
#endif
      ucf_p.push_back(&ucf);
      cf->AddObjective(*ucf_p.back());
    }
  }
#endif


  if (!paraminfo.IsParamNameEmpty()) {
    cf->SetParamNames(paraminfo.GetParamName());
  }


  /* Global CostArg to CostFunction */
  cf->SetGlbCostArg(GlbCostArg);

#ifndef SBSI_NUMERICS_GETINITCOST
  setup_param.Init();

  setup_param.SetParamInfo(&paraminfo);
  setup_param.SetOptArg(&opt_arg);

  if (opt_arg.getSetupType() == ReadinFile) {
    ostringstream paramreadinfile;
    if (opt_arg.getCkpointStartNum() == 0) {
      paramreadinfile << opt_arg.getCkpointDir()
                      << "/" << parser.paramreadinfile;
    } else {
      paramreadinfile << opt_arg.getCkpointDir() << "/BestPop."
                      << opt_arg.getCkpointStartNum() << ".ga";
    }
    setup_param.SetFileName(paramreadinfile.str());
  }

  setup_param.SetCostFunction(cf);
  setup_param.run();
#endif

  /*optimiser */
#ifndef NO_MODEL
  model->setFinal(std::max(model->getFinal(),
                  optimiser_cost_args.maxtime()));
#endif

/*
 * allan: paramset doesn't actually seem to be used anywhere, it
 * is given to Optimiser.Init but that just stores it in a field
 * which doesn't ever seem to be accessed.
 */
#ifndef SBSI_NUMERICS_GETINITCOST
  vector<ParamSet> paramset = setup_param.paramset;
#else
  vector<ParamSet> paramset;
#endif

  Optimiser &myoptimiser = 
            Optimiser::SetOptimiser(opt_arg.getOptimiserType());

  myoptimiser.Init(&opt_arg, paraminfo, &paramset);

  delete cf;
  cf = new MultiObjCost();
  cf->Init();

  cf->SetGlbCostArg(GlbCostArg);

  /*
   * Note:
   * If there is no setting costfunctions for the optimiser,
   * use the same costfunction and the same dataset as the setup.
   * If there is any setting, use its optimiser setting costfunctions
   * and datasets
   *
   * Allan: I'm doing a somewhat major re-factor of this logic.
   * I'm pretty sure we had the logic incorrect, in particular we
   * used the setup cost args if ANY of the optimiser cost args were
   * empty. Since the usual case is that the user cost args are empty
   * this meant we were generally using the setup cost args. 
   *
   * TODO: However I do need to revisit this to work out how to
   * correctly appy the weights, I feel that they should actually be
   * a cost_arg such that they can be applied individually to each
   * data set, rather than to just all of the chi-squared or all of
   * an FFT cost function.
   */
#ifndef NO_MODEL
  if (optimiser_cost_args.X2C_cost_arg.empty()){
    if (!setup_cost_args.X2C_cost_arg.empty()) {
      // Clearly incorrect to just take the back answer, however
      // I think we are essentially assuming that there is only one
      // chi-squared cost function, possibly with multiple data sets.
      double cost_weight = setup_cost_args.X2C_weights.back();
      
      SetMCF(model,
             setup_cost_args.X2C_cost_arg,
             setup_cost_args.X2C_dataset,
             paraminfo, GlbCostArg, t_series, &mcf_x2c, cost_weight);
      cf->AddObjective(mcf_x2c);
    }
  } else {
      // See above, taking the last weight cannot be correct,
      // but we're assuming there is only one such cost function.
      double cost_weight = optimiser_cost_args.X2C_weights.back();
      SetMCF(model, 
             optimiser_cost_args.X2C_cost_arg,
             optimiser_cost_args.X2C_dataset,
             paraminfo, GlbCostArg, t_series, &mcf_x2c, cost_weight);
      cf->AddObjective(mcf_x2c);
  }


  // So we do the exact same thing for FFT that we just did for X2
  if (optimiser_cost_args.FFT_cost_arg.empty()){
    if (!setup_cost_args.FFT_cost_arg.empty()) {
      // And yet again, taking the last answer cannot be correct
      // but we're assuming there is only one such fft cost function
      double cost_weight = setup_cost_args.FFT_weights.back();
      SetMCF(model,
             setup_cost_args.FFT_cost_arg,
             setup_cost_args.FFT_dataset,
             paraminfo, GlbCostArg, t_series, &mcf_fft, cost_weight);
      cf->AddObjective(mcf_fft);
    }
  } else {
      // AND again, taking the last answer cannot be correct but
      // we're assuming there is only one FFT cost function defined
      // in the optmiser section.
      double cost_weight = optimiser_cost_args.FFT_weights.back();
      SetMCF(model,
             optimiser_cost_args.FFT_cost_arg,
             optimiser_cost_args.FFT_dataset,
             paraminfo, GlbCostArg, t_series, &mcf_fft, cost_weight);
      cf->AddObjective(mcf_fft);
  }
#endif

/////////////////////////////////////////////////////////

/*
 * Allan: I've not looked into the use of user cost functions as of
 * yet, and hence I may have left some of this in a bit of mess as a
 * result of the above refactoring. I hope not though.
 */
#ifndef NO_UCF
    ucf_p.clear();
    UCF_DataSets.clear();
    if (!setup_cost_args.UserCostFunctions.empty()) {
      vector<string> allpname = paraminfo.GetParamName();
      for (size_t i = 0; i < setup_cost_args.UserCostFunctions.size(); i++) {
        UserCostFunction &ucf = UserCostFunction::setUserCostFunction(setup_cost_args.UserCostFunctions.at(i).type);
#ifdef SBSI_TEST
        int anum(20);
        vector<string> pname;
        for (int j = 0; j < anum; j++) {
          pname.push_back(allpname.at(i * anum + j));
        }
        ucf.SetParamNames(pname);
#else
        ucf.SetModel(model);
        ucf.SetParamNames(allpname);
        ucf.SetGlbCostArg(GlbCostArg);
        ucf.SetCostArgs(setup_cost_args.X2C_cost_arg);
        /* set dataset from file name */
        UCF_DataSets = GetUCF_DataSets(setup_cost_args.UserCostFunctions.at(i), parser.exp_dir);
        if (!UCF_DataSets.empty()) {
          ucf.SetDataSets(&UCF_DataSets);
        }
        ucf.SetTimeSeries(&t_series);
        ucf.SetSolver();
#endif
        ucf_p.push_back(&ucf);
        cf->AddObjective(*ucf_p.back());
      }
    }
    ucf_p.clear();
    UCF_DataSets.clear();
    if (!optimiser_cost_args.UserCostFunctions.empty()) {
      vector<string> allpname = paraminfo.GetParamName();
      for (size_t i = 0; i < optimiser_cost_args.UserCostFunctions.size(); i++) {
        UserCostFunction &ucf = UserCostFunction::setUserCostFunction(optimiser_cost_args.UserCostFunctions.at(i).type);
#ifdef SBSI_TEST
        int anum(20);
        vector<string> pname;
        for (int j = 0; j < anum; j++) {
          pname.push_back(allpname.at(i * anum + j));
        }
        ucf.SetParamNames(pname);
#else
        ucf.SetModel(model);
        ucf.SetParamNames(allpname);
        ucf.SetGlbCostArg(GlbCostArg);
        ucf.SetCostArgs(optimiser_cost_args.X2C_cost_arg);
        /* set dataset from file name */
        UCF_DataSets = GetUCF_DataSets(optimiser_cost_args.UserCostFunctions.at(i), parser.exp_dir);
        if (!UCF_DataSets.empty()) {
          ucf.SetDataSets(&UCF_DataSets);
        }
        ucf.SetTimeSeries(&t_series);
        ucf.SetSolver();
#endif
        ucf_p.push_back(&ucf);
        cf->AddObjective(*ucf_p.back());
      }
    }
#endif

  if (!paraminfo.IsParamNameEmpty()) {
    cf->SetParamNames(paraminfo.GetParamName());
  }

  myoptimiser.SetCostFunction(cf);

  /*
   * I've taken out the reporting of the initial cost (left in
   * for the INITCOST build) for now because this is not really the
   * initial cost at all, it is the initial cost given that the setup
   * of the parameters has already taken place, and since the initial
   * setup of the parameters depends upon the optimisable parameter
   * specification they may not be very close to the initial parameters
   * found in the actual model.
   */
#ifdef SBSI_NUMERICS_GETINITCOST
  double initial_cost = myoptimiser.GetInitialCost();
  if (my_rank == 0){
    printf("The initial cost is calculated to be: %f\n", initial_cost);
    ofstream init_cost_file;
    init_cost_file.open ("InitialCost.txt");
    init_cost_file << initial_cost << endl;
    init_cost_file.close();
  }

  // Clean up and then that concludes the INITCOST work so we
  // can (and should) return.
  clean_up(my_rank, tstart);
  return 0;
#endif

  double opt_tstart = getnow();

  /* Optimiser Run */
  int iter = myoptimiser.Run();

  /* Finish Run */

  if (my_rank == 0) {

    double opt_elapsed = getnow() - opt_tstart;

    cout << "elapsed time for Optimiser " << opt_elapsed << "sec" << endl;

    int cNumGen = myoptimiser.optarg->getCkpointStartNum() + iter;

    string result_dir = myoptimiser.optarg->getResultDir();

    vector<double> b_param = myoptimiser.GetTheBest();

    CostArgSet cost_args;

    if (optimiser_cost_args.X2C_cost_arg.empty() && 
        optimiser_cost_args.FFT_cost_arg.empty()) {
      cost_args = setup_cost_args;
    } else {
      cost_args = optimiser_cost_args;
    }

    // Now we must set the data sets of those which are non-empty
    // The previous logic for this was completely wrong, in that it
    // set the FFT data set if and only if the X^2 one was empty, whether
    // or not the FFT data set was empty.
    if (!cost_args.X2C_dataset.empty()){
      mcf_x2c.SetDataSets(&cost_args.X2C_dataset);
    }
    if (!cost_args.FFT_dataset.empty()){
      mcf_fft.SetDataSets(&cost_args.FFT_dataset);
    }

    // And similarly for the parameters.
    if (!cost_args.X2C_cost_arg.empty()){
      mcf_x2c.SetParameters(b_param);
    }
    if (!cost_args.FFT_cost_arg.empty()){
      mcf_fft.SetParameters(b_param);
    }

    for (size_t i = 0; i < cost_args.X2C_dataset.size(); i++) {

      mcf_x2c.SetDataSet(i);

      string dataSetname = cost_args.X2C_dataset[i].GetSetName();

      /*
       * Different data sets may set parameters differently and hence
       * give rise to different time series. Of course the time series
       * for the best costing parameters have all been computed before
       * so it's a bit of a shame that that information isn't stored
       * anywhere. Here we resolve the models which allows us to give a
       * correct score for each data set as well as output the correct
       * (best scoring) time series.
       */
      mcf_x2c.Solve();
      double x2_cost_value = mcf_x2c.getcost();

      dataSetname.resize(dataSetname.rfind("."));
      cout.precision(8);
      cout << "========================= Result for the dataset: "
           << dataSetname << endl;
      cout << "\tCostValue is " << x2_cost_value << endl;
#ifndef SBSI_TEST
      ostringstream b_cost_fname;
      b_cost_fname << result_dir << "BestCost_"
                   << dataSetname << ".txt." << cNumGen;
      writeBestCostResults( b_cost_fname.str(), x2_cost_value, model);

      ostringstream t_series_fname;
      t_series_fname << parser.timeseries_dir << "ChiSq_"
                     << parser.timeseries_fname << "_"
                     << dataSetname << "_" <<  cNumGen << ".dat";
      t_series.WriteToSBSIDataFormat(t_series_fname.str());
#endif
    }


    for (size_t i = 0; i < cost_args.FFT_dataset.size(); i++) {
      mcf_fft.SetDataSet(i);

      string dataSetname = cost_args.FFT_dataset[i].GetSetName();
      /*
       * Different data sets may set parameters differently and hence
       * give rise to different time series. Of course the time series
       * for the best costing parameters have all been computed before
       * so it's a bit of a shame that that information isn't stored
       * anywhere. Here we resolve the models which allows us to give a
       * correct score for each data set as well as output the correct
       * (best scoring) time series.
       */
      mcf_fft.Solve();
      double fft_cost_value = mcf_fft.getcost();
 
      dataSetname.resize(dataSetname.rfind("."));
      cout << "========================= Result for the dataset: "
           << dataSetname << endl;
      cout << "\tCostValue is " << fft_cost_value << endl;
#ifndef SBSI_TEST
      ostringstream b_cost_fname;
      b_cost_fname << result_dir << "BestCost_FFT_"
                   << dataSetname << ".txt." << cNumGen;

      writeBestCostResults( b_cost_fname.str(), fft_cost_value, model);

      ostringstream t_series_fname;
      t_series_fname << parser.timeseries_dir <<  "FFT_"
                     << parser.timeseries_fname << "_"
                     << dataSetname << "_" <<  cNumGen << ".dat";
      t_series.WriteToSBSIDataFormat(t_series_fname.str());
#endif
    }
  }
  
  clean_up(my_rank, tstart);
  return 0;
}

void writeBestCostResults(  const string fname , const double cost,  cModel *model)
{

  ofstream bfp(fname.c_str());

  if (!bfp) {
    ERR.FileW("Result Out", __func__, fname.c_str());
  }

  bfp.precision(8);

  bfp << "CostValue is " << cost << endl;

  vector<string> paramNames = model->getParamNames();

  for (unsigned int p_i = 0; p_i < paramNames.size(); p_i++) {
    bfp << paramNames[p_i] << ":\t" << model->getParamByName(paramNames[p_i]) << ";\n" ;
  }


}

void SetMCF(cModel *m, vector<CostArg> &cas, vector<DataSet> &ds, ParamInfo &pi, CostArg &gblca, TimeSeries &ts, ModelCostFunction *mcf, double weight)
{
  mcf->Init(m, cas, pi.GetParamName());
  if (!pi.IsUnOptimiseParamNameEmpty()) {
    mcf->SetUnOptimiseParameters(pi.GetUnOptimiseParamName(),
                                 pi.GetUnOptimiseParamValue());
  }

  mcf->SetDataSets(&ds);
  mcf->SetGlbCostArg(gblca);
  mcf->SetTimeSeries(&ts);
  mcf->SetSolver();
  mcf->SetModelCostWeight(weight);
}

vector<DataSet> GetUCF_DataSets(UserCostFunctionSet &ucfs, string exp_dir)
{

  vector<DataSet> ds;

  if (!ucfs.datafilename.empty() || !ucfs.hdrfilename.empty()) {
    FileReaderType expfilereadtype = TabDelimitedColumnData;
    FileReader &exp_file_reader = FileReader::SetFileReader(expfilereadtype);
    TDCHeaderFileReader hdr;
    for (vector<string>::iterator f_i = ucfs.datafilename.begin(); f_i != ucfs.datafilename.end(); f_i++) {
      exp_file_reader.Sets.clear();
      exp_file_reader.Read(exp_dir + *f_i);
      ds.push_back(exp_file_reader.Sets.back());
    }
    size_t n = ds.size();
    ds.resize(n + ucfs.hdrfilename.size());
    for (size_t f_i = 0; f_i < ucfs.hdrfilename.size(); f_i++) {
      ds.at(n + f_i).SetSetName(ucfs.hdrfilename.at(f_i));
      hdr.SetDataSet(&(ds.at(n + f_i)));
      hdr.Read(exp_dir + ucfs.hdrfilename.at(f_i));
    }
  }
  return ds;

}
