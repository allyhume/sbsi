/********************************************************************/
/*!
  @file ParseConfig.h
  @date 2010/03/

  @author Carl
  $Revision: 1.1.2.3 $

*/
/******************************************************************/
#include <libxml/xmlmemory.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <sys/param.h>

#ifndef __ParseConfigFile_h__
#define __ParseConfigFile_h__

#include <FileReader.h>
#include <CostArg.h>
#include <CostFunction.h>
#include <ModelCostFunction.h>
#include <OptArg.h>
#include <DataSet.h>
#include <ERR.h>


struct UserCostFunctionSet {

  UserCostFunctionSet() {};

  string type;
  vector<string> datafilename;
  vector<string> hdrfilename;

};

// Configuration for the optimiser (for "Setup" or "Optimiser" phase)
struct CostArgSet {
  // CT: These should be the same size, right? Or one cost_arg more than datasets?
  // AY: These should be the same size
  vector<CostArg> FFT_cost_arg;
  vector<string> FFT_hdrfilename;
  vector<DataSet> FFT_dataset;
  vector<double> FFT_weights;

  // AY: These should be the same size
  vector<CostArg> X2C_cost_arg;
  vector<string> X2C_filename;
  vector<DataSet> X2C_dataset;
  vector<double> X2C_weights;

  // UserCostFunction
  vector<UserCostFunctionSet> UserCostFunctions;

  inline double maxtime() const {
    double mt = 0;
    for (vector<DataSet>::const_iterator i = X2C_dataset.begin(); i != X2C_dataset.end(); ++i) {
      if (i->IsDataValuesEmpty() || i->IsDataTimeEmpty()) {
        ERR.General("Cost", __func__, "Dataset empty!");
      }
      mt = std::max(i->GetEndDataTime(), mt);
    }
    return mt;
  }
};


class ParseConfigFile
{
public:

  ParseConfigFile(const string &modeldir):
    exp_dir(modeldir + "exp_data/"),
    results_dir(modeldir + "results/"),
    timeseries_dir(modeldir + "results/"),
    ckpoint_dir(modeldir + "ckpoint/") {
  }
  ~ParseConfigFile() {}

  void parseConfig(const string &xmlfilename, OptArg &optarg, CostArg &GblCostArg,
                   CostArgSet &setup_cost_args, CostArgSet &optimiser_cost_args, cModelArg &ModelArg);
  void parseSetup(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, OptArg &optarg,
                  CostArg &glb_cost_arg, CostArgSet &cost_args);
  void parseOptimiser(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, OptArg &optarg,
                      CostArgSet &cost_args);
  void parseCostFunctions(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArgSet &cost_args);
  void parseUserCostFunctions(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArgSet &cost_args);
  void parseUserCostFunctionFile(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArgSet &cost_args);
  void parseFFT(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArgSet &cost_args);
  void parseState(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArg &FFT_cost_arg);
  void parseX2Cost(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArgSet &cost_args);
  void parseSolver(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, cModelArg &ModelArg, CostArg &glb_cost_arg);
  void parseRanking(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArg &glb_cost_arg);
  void parseResults(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur);
  void loadData(CostArgSet &args);

  string exp_dir;
  string results_dir;
  string timeseries_dir;
  string ckpoint_dir;

  // For Setup only
  string Init_Param_File;
  string paramreadinfile;

  // for Results
  string timeseries_fname;

};

#endif
