/********************************************************************/
/*!
  @file Setup.h
  @date 2008/06/14

  @author Azusa Yamaguchi
  $Revision: 1.1.2.3 $

*/
/******************************************************************/

#include<cmath>
#include<cfloat>

#ifndef __Setup_h__
#define __Setup_h__

#include<FileReader.h>
#include<ParamSet.h>
#include<CostFunction.h>
#include<OptArg.h>

using namespace std;

vector<ParamSet> sortParamSet(const vector<ParamSet> set);

class Setup
{

public:

  Setup() {
    optarg = NULL;
    costfunction = NULL;
    numPoint = 0;
    InFileName.clear();
    MaxCostValue = DBL_MAX;
  }
  virtual ~Setup() {};

  void Init(ParamInfo *pi, OptArg *oa) {
    paraminfo = pi;
    optarg = oa;
    paramset.resize(0);
  }

  void Init() {
    paramset.resize(0);
  }

  void SetParamInfo(ParamInfo *pi) {
    paraminfo = pi;
  }
  void SetOptArg(OptArg *oa) {
    optarg = oa;
  }


  void setMaxCost(double dbl) {
    MaxCostValue = dbl;
  }
  double getMaxCost() {
    return MaxCostValue;
  }

  size_t GetParamSetNum() {
    return paramset.size();
  }

  virtual void run() = 0;

  static Setup & SetupParam(SetupType which);

  void  SetFileName(string fname) {
    InFileName = fname;
  }

  void SetCostFunction(CostFunction *cf) {
    costfunction = cf;
  }

  void WriteStart();

  OptArg *optarg;
  ParamInfo *paraminfo;

  CostFunction *costfunction;

  vector<ParamSet> paramset;

  int numPoint;

  string InFileName;

  char cfilename[100];  // for MPI

  double MaxCostValue;

};

class Sobol: public Setup
{
public :
  virtual void run();
};

class Read_File: public Setup
{
public :
  virtual void run();
};

#endif
