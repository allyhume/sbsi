/********************************************************************/
/*!
  @file Cost_arg.h
  @date 2010/03/

  @author Azusa Yamaguchi
  $Revision: 1.1.2.5 $

  $LastChangedBy: Carl$

*/
/******************************************************************/

#include <string>
#include <vector>
#include <map>

#ifndef __CostArg_h__
#define __CostArg_h__

using std::vector;
using std::string;
using std::map;

typedef enum {
  InvalidRanking,
  Stochastic,
  Standard
} RankingType;

typedef enum {
  InvalidSolver,
  CVODE,
  KINETIC,
  BVP
} SolverType;

typedef enum {
  InvalidCostFunction,
  Single,
  Multi,
} CostFunctionType;

typedef enum {
  InvalidModelCostFunction,
  X2Cost,
  FFT
} ModelCostFunctionType;

typedef enum {
  InvalidScale,
  AbsAverage,
  Average,
  MaxMin,
  Fix,
  None
} ScaleType;

typedef enum {
  InvalidCostValue,
  Normed,
  Direct
} CostValueType;

typedef enum {
  AmpFncNone,
  AmpFncFunctional,
  AmpFncMax,
  AmpFncFunctionalNone,
  AmpFncDampFunctional,
  AmpFncDampNone,
  AmpFncDampMax,
  AmpFncDampFunctionalNone,  
} AmpFncType ;

/*!
 @brief A struct for cost value calculation argument
*/
struct CostArg {
  /*!
   @brief a constructor
   Initialize to invalid and require all to be explicitly set
   */

  CostArg():
    solvertype(InvalidSolver), costfunctiontype(InvalidCostFunction),
    modelcostfunctiontype(InvalidModelCostFunction),
    scaletype(InvalidScale), costvaluetype(InvalidCostValue) {}

  /*!
   @brief ODE Sovler type
   */
  SolverType solvertype;

  RankingType rankingtype;

  /*!
   @brief the weight for the cost function to be multiplied by
   */
  // I haven't been able to set this correctly to work, we're not sure
  // exactly to what we should apply weights, currently they are applied
  // at model cost function level, but could be done at data set level.
  // vector<double> weight;

  /*!
   @brief a type of Cost function
   */
  CostFunctionType costfunctiontype;

  /*!
   @brief A type of Model Cost Function
   */
  ModelCostFunctionType modelcostfunctiontype;

  /*!
   @brief A type of scaling data values.
   */
  ScaleType scaletype;

  /*!
   @brief A type of estimation of a cost value
   */
  CostValueType costvaluetype;

  /*!
   @brief State names which have a frequency, for only FFT model cost function
   */
  vector<string> FFTTarget;

  /*!
   @brief a vector of the target period, with the size of the size of the fft_arget, for only FFT model cost function
   */
  vector<double> Period;

  /*!
   @brief a vector of the times of start sampling for fourier transform,for only FFT model cost function
   */
  vector<double> TStart;

  /*!
   @brief a vector of the times of end sampling for fourier transform,for only FFT model cost function
   */
  vector<double> TEnd;

  /*!
   @brief a vector of the minimum amplitude ( cut-off ) for fourier transform,for only FFT model cost function
   */
  vector<double> MinAmplitude;

  /*!
   @brief a vector of the maximum amplitude ( cut-off ) for fourier transform,for only FFT model cost function
   */
  vector<double> MaxAmplitude;

  /*!
   @brief a vector of the amplitude function ( 0 or 1 ) for fourier transform,for only FFT model cost function . 0;default not use function amplitude ; use the function of amplitude
   */
  vector<AmpFncType> AmpFnc;

  /*!
   @brief a value for adjust data starting time, only for Chi2Cost model cost function
  */
  double ExpT0;

  /*!
   @brief a vector of fixed values for scaling data, only for Chi2Cost model cost function
   */
  vector<double> FixScale;

  /*!
   @brief a value of the interval of output from solver, for both FFT and Chi2Cost model cost function
     If Chi2Cost, it shouldn't be bigger than the minimum interval of fitting data.
   */
  double OutputInterval;

  /*!
   @brief a value for Maximum cost value. It is used for filtering parameter set with SobolSelection setup type. It is used as a penalty value when solver failed to solve ODE.
   */
  double MAXCOST;

  /*!
   @brief the maximum number of generating sobol numbers for a setup parameter set
   */
  int MAX_RandomNumGen;

};

#endif
