/********************************************************************/
/*!
  @file Chi2Cost.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.3 $

*/
/******************************************************************/

using namespace std;

#ifndef __Chi2CostFunction_h__
#define __Chi2CostFunction_h__

#include <CostFunction.h>
#include <cModel.h>
#include <Solver.h>
#include <DataSet.h>
#include <CostArg.h>
#include <TimeSeries.h>


/*!
 @brief Class for estimation sum-squared difference between the model solution and experimental data
*/
class Chi2Cost : public ModelCostFunction
{

public:

  ~Chi2Cost() {};

  /*!
    @brief Set experimental data sets
    @param the pointer of vector<DataSet>
  */
  virtual void SetDataSets(vector<DataSet> *ds_s);

  /*!
    @brief Set a experimental data set
    @param d_i  index of the data set
   */
  virtual void SetDataSet(const size_t d_i);

  /*!
   @brief the high level routine for Chi2Cost class, for calculation the cost value, which is sum-squared difference between the model solution and experimental data.
   @return the sum-squared difference between the model solution and all experimental data sets.
   */
  double GetCost();

  /*!
   @brief the high level routine for Chi2Cost class, for calculation the cost value, which is sum-squared difference between the model solution and a experimental data.
   @return the sum-squared difference between the model solution and one of experimental data sets.
   */
  double getcost();

  double getcost_nokey();
  /*!
    @brief Return the number of a vector for scaling the data set with a fixed number. It is valid when Fixed Scale type is chosen.
    @return size_t the size of the vector FixScale, which should be the same as the number of data column of the data set.
   */
  size_t GetNumFixScale() {
    return costarg.FixScale.size();
  }

  /*!
    @brief Return a vector for scaling the data set with a fixed number. It is valid when Fixed Scale type is chosen.
    @return vector<double> FixScale vector
   */
  vector<double> GetFixScale() {
    return costarg.FixScale;
  }

  /*!
    @brief Return a value of the vector for scaling the data column with a fixed number. It is valid when Fixed Scale type is chosen.
    @param int  the index of the data column.
    @return double a value of FixScaled vector
   */
  double GetAFixScale(int a ) {
    return costarg.FixScale.at(a - 1);
  }

  /*!
    @brief Return a cost value type for a data set.
    @param int  the index of the data set.
    @return a cost value type
   */
  CostValueType GetACostValueType(int a) {
    return costargs.at(a).costvaluetype;
  }

  /*!
    @brief Return a scale type for a data set.
    @param int  the index of the data set.
    @return a cost value type
   */
  ScaleType GetScaleType() {
    return costarg.scaletype;
  }

protected:

  /*!
   @brief To calculate the norm of the data set. It is executed when the cost value type Normed is chosen.
   */
  void GetNorm();


  /*!
   @brief scale the computed time course according to the scaling
          factors computed by 'UpdateScale'
   @param The result vector to scale
   @param The column index from which to retrive the scaling factor
          and base numbers.
   */
  void scale_result_vector(vector<double> *, unsigned);

  /*!
   @brief calclulate the scaling value for the data set.
   @param a scaletype
   */
  void UpdateScale(const ScaleType sc); //to transform resuts of simulations


  /*!
   @brief a vector for the scaling number.
  */
  vector<double> Scale;

  /*!
   @brief a vector for the base number.
  */
  vector<double> Base;

  /*!
   @brief a 2 dimensional vector for the norm number for all data sets.
  */
  vector< vector<double> > Norms;

  /*!
   @brief a vector for the norm number for a data set.
  */
  vector<double> Norm;

};

#endif

