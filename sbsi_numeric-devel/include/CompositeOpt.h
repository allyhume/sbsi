/********************************************************************/
/*!
  @file CompositeOpt.h
  @date 2010/03/29

  @author Azusa Yamaguchi
  $Revision: 1.1.2.1 $

*/
/******************************************************************/


#ifndef __CompositeOpt_h__
#define __CompositeOpt_h__

#include <Optimiser.h>


using namespace std;

class CompositeOpt : public Optimiser
{
public:
  virtual int Run();
};

#endif
