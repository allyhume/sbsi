#include <string>
#include <iostream>

using namespace std;

#ifndef COMMANDLINE_H
#define COMMANDLINE_H

/*!
  simple class to provide access to the command-line arguments
  with type conversion (and checking).
*/
class CommandLine
{
private:

  const char* cname;
  int    argc;
  char** argv;
  int   count;
  int my_rank;

private:

  static CommandLine inst;

  CommandLine():
    cname("CommandLine"),
    argc (0),
    argv (0x0),
    count(0),
    my_rank(0) {
    ;
  }

public:

  /*! register the command-line arguments with the class (always do this)*/
  static void is( int _argc, char** _argv, int rank) {
    inst.argc = _argc;
    inst.argv = _argv;
    inst.my_rank = rank;
  }


  static void reset()      {
    inst.count = 0;
  }
  static int    num() {
    return inst.count;
  }

  static string arg         ( int num );
  static int   arg_as_int  ( int num );
  static int   arg_as_hex  ( int num );
  static double arg_as_double( int num );


  static string arg         () ;
  static int   arg_as_int  ();
  static int   arg_as_hex  ();
  static double arg_as_double();
};

#endif
