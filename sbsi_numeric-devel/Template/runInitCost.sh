#!/bin/sh

## This script takes two mandatory arguments :
## 1) Model ID (The SBML model ID)
## 2) Configuration xml file
## It simply runs the SBSINumerics executable 
if [ $# -eq 2 ] ; then
 model_name=$1
 config_file=$2
else
 echo "You must give two arguments, the model id and the configuration file"
 exit
fi


mydir=$PWD

sub_dir=UserModel
model_dir=$mydir/$sub_dir

NUM_CORES=1
platform=`uname`
if [ $platform == 'Darwin' ] ; then
    NUM_CORES=`sysctl -a hw | grep ncpu: | awk '{print $2}'`
else
    NUM_CORES=$(cat /proc/cpuinfo | grep processor | wc -l)
fi

BIN=${model_name}_INITCOST.exe

if [ -f OFW.error ] ; then 
 rm OFW.error;
fi

mpirun -np $NUM_CORES  $mydir/${BIN} $sub_dir $config_file

