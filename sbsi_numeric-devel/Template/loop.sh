#!/bin/bash

# This script runs the SBSINumerics executable model multiple times and is designed for use on regular Unix based machines rather than HPC.
# to take periodic checkpoints and results.
##  Takes  2  mandatory arguments :
## 1) Model ID (The SBML model ID)
## 2) Configuration xml file
# 
# This script will create all subdirectories needed for results and checkpoints - either 
# by reading the configuration file or using default locations if they are not already 
# created.

if [ $# -eq 2 ] ; then
 model_name=$1
 config_file=$2

else
  echo Usage: loop.sh modeID configxml
  echo ERROR Wrong arguments -> should be loop.sh modeID configxml >> $logfile
  exit
fi

mydir=$PWD

sub_dir=UserModel
model_dir=$mydir/$sub_dir

##### calculate number of cores
## ASSUMPTION - we have OSX (Darwin) and non OSX, and all non 'Darwin' support /proc/cpuinfo 
## caveat - dont know what this will return with new intel (i5, i7 cpus) that have hyperthreading

NUM_CORES=1
platform=`uname`
if [ $platform == 'Darwin' ]
then
    NUM_CORES=`sysctl -a hw | grep ncpu: | awk '{print $2}'`
else
    NUM_CORES=$(cat /proc/cpuinfo | grep processor | wc -l)
fi
NP=$NUM_CORES

logfile=OFW.status
StatusFile=$logfile
ConfigTMP=config_tmp



BIN=$model_name.exe

echo Execute file is  $BIN
if [ ! -f $BIN ] ; then
 echo no executable file
  echo ERROR No Executable file  found  >> $logfile
 exit
fi

exp_data_dir=$model_dir/exp_data

if [ ! -d $exp_data_dir ] ; then
   mkdir $exp_data_dir;
fi

ckpoint_dir=$model_dir/ckpoint

if [ ! -d $ckpoint_dir ] ; then
   mkdir $ckpoint_dir;
fi

result_dir=$model_dir/results
if [ ! -d $result_dir ] ; then  
  mkdir $result_dir;
fi


LIST=`grep FileName $config_file  | sed 's%<FileName>%%' | awk -F "<" '{print $1}'`
for i in $LIST
  do
   if [ ! -f ${exp_data_dir}/$i ]; then 
      cp $i $exp_data_dir;
   fi
done

if [ `ls -l *sbsiheader | wc -l ` -gt 0 ] ; then
    LIST=`ls -l *sbsiheader | awk '{print $NF}' `
    for i in $LIST
    do
     if [ ! -f ${exp_data_dir}/$i ] ; then 
       cp $i $exp_data_dir;
     fi
    done
fi
  
## Copy header files into exp_data
if [ `ls -l *hdr | wc -l ` -gt 0 ] ; then
   LIST=`ls -l *hdr | awk '{print $NF}' `
   for i in  $LIST
   do
    if [ ! -f ${exp_data_dir}/$i ] ; then 
      cp $i $exp_data_dir;
    fi
   done
fi


scr=loop
interrupt=END

###############################
# up dir
###############################

if [ -f OFW.error ] ; then 
 rm OFW.error;
fi

if [ -f $logfile ] ; then 
 rm $logfile
fi
if [ -f $ConfigTMP ]; then
 echo " Removing old tmp file "
 rm $ConfigTMP 
fi

ckpoint_num=0

## Exit if conflicting checkpoint files found
if [`grep CkpointNum $config_file | wc -l ` -gt 1 ] ; then
  echo "There is  more than one statement for ckpoint Num "
  echo "There is  more than one statement for ckpoint Num " >> $logfile
  exit
fi

# Make sure ckpoint file is OK
if [`grep CkpointNum $config_file | wc -l ` -gt 0 ] ; then
  echo `grep CkpointNum $config_file | sed 's%<CkpointNum>%%' | awk -F "<"  '{print $1}'`
  ckpoint_num=`grep CkpointNum $config_file | sed 's%<CkpointNum>%%' |  sed 's/[[:space:]]//g'| awk -F "<"  '{print $1}'`
  echo "ckpoint num in config files is $ckpoint_num"
  
  ## IF ckpoint num > 0, then fail if the checkpoint file does not exist.
  if [ ${ckpoint_num} -gt 0 ] ; then
  echo "ckpoint num in config files is [$ckpoint_num]"
   if [ ! -f ${ckpoint_dir}/BestPop.${ckpoint_num}.ga ]  ;then
    echo "ERROR; There is no checkpoint file : $ckpoint_dir/BestPop.${ckpoint_num}.ga "
    echo "ERROR; There is no checkpoint file  : $ckpoint_dir/BestPop.${ckpoint_num}.ga"   >> $logfile
    exit
   fi
  fi
fi

## Now handle case where a ckpoint file name is set.
if [ `grep CkpointFileName $config_file | wc -l ` -eq 1 ]; then
    ckpointfilename=`grep CkpointFileName $config_file | sed 's%<CkpointFileName>%%' | sed 's/[[:space:]]//g' |awk -F "<"  '{print $1}'`
   echo " Checkpoint file name is: $ckpointfilename"yy
   if [ ! -f $ckpoint_dir/$ckpointfilename ]  ;then
      echo there is no file for ckpoint ; $ckpoint_dir/$ckpointfilename
    #  echo there is no file for ckpoint ; $ckpoint_dir/$ckpointfilename >> $logfile
      exit
   fi
elif [ `grep CkpointFileName $config_file | wc -l ` -gt 1 ]; then
   echo "ERROR: there is multiple setting for Ckpoint File Name "
   echo "ERROR: there is multiple setting for Ckpoint File Name " >> $logfile
fi


##################################################
# EXE
##################################################

 total_numGen=${ckpoint_num}
 MaxNumGen=`grep MaxNumGen $config_file  | sed 's%<MaxNumGen>%%' | awk -F "<" '{print $1}'`
 NumGen=`grep NumGen $config_file  | sed 's%<NumGen>%%' |  sed 's/[[:space:]]//g' | awk -F "<" '{print $1}'`

 echo $total_numGen
 echo $MaxNumGen

 echo $BIN executes on $NP procs
## This is the main loop. It will repeatedly invoke the executable until MaxNumGen is reached or another Stop Condition is reached.
echo "STARTED  Launching framework..." >> $StatusFile
STOPPED=0 #boolean flag as to whether is stopped or not. 0= false, any other value  = true
while [ $total_numGen -lt $MaxNumGen ];  do

echo mpirun -np $NP  $mydir/${BIN} $sub_dir $config_file

mpirun -np $NP  $mydir/${BIN} $sub_dir $config_file

 # check OFW.error
 if [ -f OFW.error ] ; then
 prog_status = `grep Error OFW.error `
 echo $scr : $PWD $model_name : ERROR $prog_status
 echo ERROR  $prog_status >> $logfile
 exit -3
 fi

 echo "RESULT  directory is " ./${result_dir}

 if [ `find ${result_dir} -name "*.Stopinfo.*" -print | wc -l ` -gt 0 ] ;then
  recent_Stop=`ls -t $result_dir/*.Stopinfo.* | awk '{print $NF}' | head -1`
  echo " Looking at most recent stop file to get generation number so far...:" $recent_Stop
  current_gen=`ls -t $result_dir/*.Stopinfo.* | awk '{print $NF}' | head -1| awk -F. '{print $NF}' `
  echo " Curr gen =  $current_gen "

  condition=` head -1 $recent_Stop |  awk -F : '{print $1}' `
  current_cost=` head -1 $recent_Stop |  awk -F : '{print $NF}' `
  sobol_seed=` tail -1  $recent_Stop |  awk -F : '{print $NF}' `


  echo "CONDITION " $condition
  echo "COST " $current_cost
  echo "SOBOL " $sobol_seed
  echo RUNNING numGen:$current_gen  cost:$current_cost >> $logfile

  total_numGen=$current_gen

  if [ "$condition" == "Normal Exit" ] ;then
   echo Finished loop after $NumGen generations, current cost is $current_cost at $current_gen generation
   ckpointNum=$current_gen
    #get the most recent ckfile
    recent_ckpointfile=`ls -t $ckpoint_dir/*.ga | awk -F/ '{print $NF}' | head -1`
    ckpoint_file=$recent_ckpointfile

    ## If we did not start this loop with a read in file, then change set up condition to read me for the next loop.
    if [ `grep ReadinFile $config_file | wc -l ` -eq 0 ] ; then
       if [ `grep Sobol $config_file | wc -l ` -ge  1 ] ; then

        echo "Changing Setup Condition; Setup type using Sobol to ReadinFile"
        sed  's/<SetupType>Sobol[^/]*<\/SetupType>/<SetupType>ReadinFile<\/SetupType>/' $config_file  > $ConfigTMP
	 cp $ConfigTMP $config_file


       fi
    fi

     
    
    if [ `grep CkpointNum $config_file | wc -l ` -eq 0 ] ; then

      echo "Adding the check point in config file"

      awk '{print}/ReadinFile/{X=NR}NR==X{print "\t\t<CkpointNum>'$current_gen'</CkpointNum>"}' $config_file > $ConfigTMP

      cp $ConfigTMP $config_file

    elif [ `grep CkpointNum $config_file | wc -l ` -eq 1 ] ; then

      echo "Changing the check point in config file"

        old_ckpoint=`grep "<CkpointNum>" $config_file |  sed 's%<CkpointNum>%%' |  sed 's/[[:space:]]//g' | awk -F "<" '{print $1}'`
         echo old checkpoint is $old_ckpoint

         sed 's/<CkpointNum>'$old_ckpoint'<\/CkpointNum>/<CkpointNum>'$current_gen'<\/CkpointNum>/ ' $config_file > $ConfigTMP

        cp $ConfigTMP $config_file
        newckpoint=`grep "<CkpointNum>" $config_file |  sed 's%<CkpointNum>%%' |  sed 's/[[:space:]]//g' | awk -F "<" '{print $1}'`
         echo new checkpoint is $newckpoint


    elif [ `grep CkpointNum $config_file | wc -l ` -gt 1 ] ; then

         echo There is multiple CkpointNum setting in the file: $config_file   
         echo "ERROR There is multiple CkpointNum setting in the file: $config_file"   >> $logfile
          exit

   fi
  fi

 if [ "$condition" == "STOP NO_IMPROVEMENT" ] ;then
    echo Stop for non improvement of cost -  $current_cost at $current_gen
    echo FINISHED  Cost is $current_cost at $current_gen generations, stopped as is no longer decreasing. >> $logfile
    STOPPED=1
    break
  fi

  if [ "$condition" == "STOP TOO_SIMILAR" ] ;then
    echo Stop for too_similar parameter sets, cost  $current_cost at $current_gen generation
    echo FINISHED Too similar: cost  is $current_cost at $current_gen generations >> $logfile
    STOPPED=1
    break
  fi

  if [ "$condition" == "REACHED" ] ;then
    echo FINISHED Stop, cost  = $current_cost, reached target cost, at $current_gen generation >> $logfile
    STOPPED=1
    break
  fi

 else
  echo No Stop Condition file
  echo FINISHED No Stop Condition file >> $logfile
  exit -3
 fi
## end of while loop
done
# If we exit loop and have not stopped for any other reason, we have reached MaxNumGen
if [ $STOPPED -ne 1 ]; then
     recent_Stop=`ls -t $result_dir/*.Stopinfo.* | awk '{print $NF}' | head -1`

  current_cost=` head -1 $recent_Stop |  awk -F : '{print $NF}' `

   echo "FINISHED $MaxNumGen generations, cost=$current_cost, reached iteration cut-off." >>$StatusFile

fi










