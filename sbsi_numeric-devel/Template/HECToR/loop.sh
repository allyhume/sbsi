#!/bin/sh 

mailto=ayamaguc@staffmail.ed.ac.uk
scr=loop
interrupt=END
runfile=run.sh
work_dir=$PWD

if [$# -neq 2 ]; then
        echo "Usage: ./loop.sh subdir config.xml";
        echo "ERROR  Usage: ./loop.sh subdir config.xml"; " >> $StatusFile
        exit 2;
fi

subdir=$1
configfile=$2

StatusFile=OFW.status
if [ -f $StatusFile ]; then  
  rm $StatusFile
fi
touch $StatusFile
echo "STARTED  Launched script" >> $StatusFile
:
num_node=`grep mppwidth $runfile | head -1 | awk -F= '{print $2}'`

hours=`grep walltime $runfile | head -1 | awk -F= '{print $2}' | awk -F: '{print $1}'`

jobname=`grep "PBS -N"  $runfile |  awk  '{print $NF}'`

if [ `grep "<ResultDir>"  $config_file | wc -l ` -lt 1 ]; then
  result_dir=$work_dir/$subdir/results
else
  result_dir=`grep "<ResultDir>" $config_file |  sed 's%<ResultDir>%%' |  sed 's/[[:space:]]//g' | awk -F "<" '{print $1}'`
fi


if [ `grep "<CkpointDir>"  $config_file | wc -l ` -lt 1 ]; then
  ckpoint_dir=$work_dir/$subdir/ckpoint
else
  ckpoint_dir=`grep "<CkpointDir>" $config_file |  sed 's%<CkpointDir>%%' |  sed 's/[[:space:]]//g' | awk -F "<" '{print $1}'`
fi

if [`grep CkpointNum $config_file | wc -l ` -eq 1 ] ; then

  echo `grep CkpointNum $config_file | sed 's%<CkpointNum>%%' | awk -F "<"  '{print $1}'`
  ckpoint_num=`grep CkpointNum $config_file | sed 's%<CkpointNum>%%' | awk -F "<"  '{print $1}'`

  if [ ${ckpoint_num} -gt 0 ] ; then
   if [ ! -f ${ckpoint_dir}/BestPop.${ckpoint_num}.ga ]  ;then
    echo "ERROR; There is no ckpoin file : $ckpoint_dir/BestPop.${ckpoint_num}.ga "
    echo "ERROR; There is no ckpoin file  : $ckpoint_dir/BestPop.${ckpoint_num}.ga"   >> $logfile
    exit
   fi
  fi

else
  if [`grep CkpointNum $config_file | wc -l ` -gt 1 ] ; then
    echo "ERROR; There is more than 1 line for setting Ckpoint Num "
    echo "ERROR; There is more than 1 line for setting Ckpoint Num " >> $logfile
  else
   ckpoint_num=0
  fi
fi


MaxNumGen=`grep MaxNumGen $config_file  | sed 's%<MaxNumGen>%%' | awk -F "<" '{print $1}'`
NumGen=`grep NumGen $config_file  | sed 's%<NumGen>%%' | awk -F "<" '{print $1}'`


targeted_costfunctionvalue=`grep targeted_costfunctionvalue $runfile | head -1| awk -F= '{print $2}'`

echo the starting ckpoint is $ckpoint_Num
echo the targeted costfunction value is $targeted_costfunctionvalue
echo the max generation number is $MAX_NUMGEN

jobfile = $jobname.log
logfile = $jobname.out

if [-f $logfile) rm $logfile
if [ -f OFW.error) rm OFW.error

touch $logfile

##################################################
# EXE 
##################################################

iter=0
lastrun=""

while [ $total_numGen -lt $MAX_NUMGEN ]; do

  if [`grep CkpointNum $config_file | wc -l ` -eq 1 ] ; then

    echo `grep CkpointNum $config_file | sed 's%<CkpointNum>%%' | awk -F "<"  '{print $1}'`
    ckpoint_num=`grep CkpointNum $config_file | sed 's%<CkpointNum>%%' | awk -F "<"  '{print $1}'`

    if [ ${ckpoint_num} -gt 0 ] ; then
     if [ ! -f ${ckpoint_dir}/BestPop.${ckpoint_num}.ga ]  ;then
      echo "ERROR; There is no ckpoin file : $ckpoint_dir/BestPop.${ckpoint_num}.ga "
      echo "ERROR; There is no ckpoin file  : $ckpoint_dir/BestPop.${ckpoint_num}.ga"   >> $logfile
      exit
     fi
    fi

  else
    if [`grep CkpointNum $config_file | wc -l ` -gt 1 ] ; then
      echo "ERROR; There is more than 1 line for setting Ckpoint Num "
      echo "ERROR; There is more than 1 line for setting Ckpoint Num " >> $logfile
    else
     ckpoint_num=0
    fi
  fi

  echo $ckpoint_Num
  total_numGen = $ckpoint_Num

  @ iter+=1


  qsub $runfile >&  $jobfile

  if [`tail -1 $jobfile | grep "not been submitted" | wc -l `>0 ]; then
   if [?${mailto}) mail -s  "$scr: JOB SUBMIT ERROR "  mail $mailto < $jobfile
  else
    tail -1 $jobfile 
    jobid = `tail -1 $jobfile | awk -F. '{print $1}' `
    echo $jobid
    echo $scr : JobId id  $jobid  >> $logfile    
    echo $scr : Submit Time : `date`  >> $logfile    
  fi

  llid=Q
  sleeptime=1
  tmp=0
  
  @ tmp+= $num_node / 128 
  @ sleeptime*= $tmp
  echo sleeptime is  $sleeptime

  while [ "$llid" != "R" ] ; do 
    llid = `qstat  | grep $jobid | awk '{print $5}'` 
    echo jobstatus is $llid : `date` >> $logfile
    echo "WAITING Job Iteration: " $iter " " $lastrun >> $StatusFile   
      if ("$llid" == "Q" ) sleep $sleeptime
    echo slept $llid
    qdel $jobid.sdb
  done

  checktime=$hours
  @ checktime *=1
  echo Job will be checked every $checktime sec

  while [ "$llid" == "R" ]; do
    echo job $jobid is running
    echo $jobid jobstatus is $llid : `date` >> $logfile
    if [ $iter -eq 1 ];  then
       echo "RUNNING Job iteration:" $iter >> $StatusFile
    else
       echo "RUNNING Job iteration:"$iter "(Last Run:" $lastrun ")" >> $StatusFile    
    fi
    sleep $checktime
    if [ `qstat | grep $jobid | wc -l` -gt 0  ] ;then
      llid = `qstat  | grep $jobid | awk '{print $5}'` 
    else
      echo no in the list
      goto finish
    fi
  done


  finish:
  echo finish the job
  echo $jobid jobstatus is finish : `date` >> $logfile

  if [-f OFW.error ]; then
    prog_status = `grep Error OFW.error `
    echo "ERROR  `more OFW.error`" >> $StatusFile
    echo $scr : $PWD $jobname : ERROR $prog_status
    exit -3
  else 
    echo no OFW error
  fi

 if [`find $result_dir/ -name "PGA.Stopinfo.*" -print | wc -l ` >0) then
  recent_PGAStop = `ls -t $result_dir/PGA.Stopinfo.* | awk '{print $NF}' | head -1`
  echo $recent_PGAStop
  current_gen = `ls -t $result_dir/PGA.Stopinfo.* | awk '{print $NF}' | head -1| awk -F. '{print $NF}' `

  echo the current genetation is $current_gen
  condition=` head $recent_PGAStop |  awk -F : '{print $1}' `
  current_cost=` head $recent_PGAStop |  awk -F : '{print $NF}' `
  lastrun="NumGen:$current_gen  cost:$current_cost"
  echo "RUNNING "$lastrun  >> $StatusFile

  echo $jobid finishing condition is $condition
  echo $jobid current cost is $current_cost

   @ total_numGen = $current_gen

  if ( "$condition" == "Normal Exit" ) then
   echo Stop for iter = $NumGen, current cost is $current_cost at $current_gen generation
   sed -e "s/ckpointNum=$ckpoint_Num/ckpointNum=$current_gen/g" $runfile > run.tmp
   echo $ckpoint_Num $current_gen
   ckpoint_Num=$current_gen
  cp run.tmp $runfile
#    if [$?mailto) mail -s "$PWD : Application Normal Exit| $condition " $mailto < $recent_PGAStop
  fi

  if ( "$condition" == "STOP NO_IMPROVEMENT" ) then
    echo Stop for non improvement with constfunction valued is $current_cost at $current_gen
 echo "FINISHED Stopped for non-improvement of  costfunction: value is $current_cost after $current_gen generations." >>$StatusFile
    goto stop
  fi

  if ( "$condition" == "STOP TOO_SIMILAR" ) then
    echo "FINISHED  Stopped for too_similar costfunction value - value  is $current_cost after $current_gen generations" >>$StatusFile
    echo Stop for too_similar, constfunction valued is $current_cost at $current_gen generation
    goto stop
  fi

  if ( "$condition" == "REACHED" ) then
    echo "FINISHED   Stopped: reached target costfunction value. Cost  is $current_cost at $current_gen generation." >>$StatusFile
    echo Stop reached target costfunction value, Costvalued is $current_cost at $current_gen generation
    goto done
  fi

 else
  echo No Stop Condition file
  exit -3
 fi

end
echo "FINISHED Normal finish, stopped after maximum Generation limit $MAX_NUMGEN generations" >>$StatusFile 
done:
    if [ $?mailto) ]; then 
      mail -s "$PWD : Application Normal Exit| $condition " $mailto < $recent_PGAStop
    fi

stop:
  if [ $?mailto) ]; then
      mail -s "$PWD : Application Exit | Stopping by $condition" $mailto < $recent_PGAStop
 echo $scr : Stop Time : `date` >> $logfile
  fi

