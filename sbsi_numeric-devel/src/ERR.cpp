
using namespace std;
#include <ERR.h>

#define MAX_LEN 500

char message[MAX_LEN];


//------------------------------------------------------------------
// Constructor
/*!
   The exit codes and standard messages are initialised here.
*/
//------------------------------------------------------------------
Error ERR;

static inline void Exit(int status) NORETURN;
static inline void Exit(int status)
{
  MPI_Abort(MPI_COMM_WORLD, status);
  exit(status);
}

Error::Error() :  exit_value(n_error_types), error_string(n_error_types)
{

  error_class_name = "Error";

  // Error message format strings

  error_string[pointer] = "Error in %s::%s :\n\tpointer %s is not initialized.\n";
  error_string[file_r] = "Error in %s::%s :\n\tcannot open file %s to read.\n";
  error_string[file_w] = "Error in %s::%s :\n\tcannot open file %s to write.\n";
  error_string[file_a] = "Error in %s::%s :\n\tcannot open file %s to append.\n";
  error_string[not_implemented] = "Error in %s::%s :\n\tnot implemented.\n\t";
  error_string[general] = "Error in %s::%s : ";


  // Error exit values
  /*
  exit_value[pointer] = -1;
  exit_value[file_r] = -2;
  exit_value[file_r] = 2;
  exit_value[file_w] = -3;
  exit_value[file_a] = -4;
  exit_value[not_implemented] = -5;
  exit_value[general] = -7;
     */

  // File to which error messages are written

  error_file_name = "OFW.error";

}



//------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------
Error::~Error() {}


//------------------------------------------------------------------
/*!
  Prints the message
  \n<tt>Error in <i>class name</i>::<i>function name</i> : </tt>\n
  <tt>pointer <i>pointer name</i> is not initialized.</tt>\n
  to \c stdout and to the file \c phys.error.
  Exits with -1.

  \param class_name The name of a class whose method is being entered.
  \param func_name The name of a function or method.
  \param ptr_name The variable name of the pointer.
*/
//------------------------------------------------------------------
void Error::Pointer(const string class_name, const string func_name,
                    const string ptr_name)
{


  error_func_name = "Pointer";

  printf(error_string[pointer].c_str(), class_name.c_str(), func_name.c_str(), ptr_name.c_str());
  ofstream fp;
  fp.open(error_file_name.c_str()) ;
  if (!fp.is_open()) {

    printf(error_string[file_w].c_str(), error_class_name.c_str(), error_func_name.c_str(), error_file_name.c_str());
    Exit(exit_value[file_w]);
  }

  snprintf(message, MAX_LEN, error_string[pointer].c_str(), class_name.c_str(), func_name.c_str(), ptr_name.c_str());
  message[MAX_LEN - 1] = 0;
  fp << message << endl;
  //fp.close();

  Exit(exit_value[pointer]);

}


//------------------------------------------------------------------
/*!
  Prints the message
  \n<tt>Error in <i>class name</i>::<i>function name</i> : </tt>\n
  <tt>can not open file <i>file name</i> to read.</tt>\n
  to \c stdout and to the file \c phys.error.
  Exits with -2.

  \param class_name The name of a class.
  \param func_name The name of a function or method.
  \param file_name The name of the file.
*/
//------------------------------------------------------------------
void Error::FileR(const string class_name, const string func_name,
                  const string file_name)   // file name
{
  ofstream fp;
  error_func_name = "FileR";

  printf(error_string[file_r].c_str(), class_name.c_str(), func_name.c_str(), file_name.c_str());

  fp.open(error_file_name.c_str()) ;
  if (!fp.is_open()) {

    printf(error_string[file_w].c_str(), error_class_name.c_str(), error_func_name.c_str(), error_file_name.c_str());
    Exit(exit_value[file_w]);
  }

  snprintf(message, MAX_LEN, error_string[file_r].c_str(), class_name.c_str(), func_name.c_str(), file_name.c_str());
  message[MAX_LEN - 1] = 0;
  fp << message << endl;

  cout << exit_value[file_r] << endl;
  Exit(exit_value[file_r]);
}


//------------------------------------------------------------------
/*!
  Prints the message
  \n<tt>Error in <i>class name</i>::<i>function name</i> : </tt>\n
  <tt>can not open file <i>file name</i> to write.</tt>\n
  to \c stdout and to the file \c phys.error.
  Exits with -3.

  \param class_name The name of a class.
  \param func_name The name of a function or method.
  \param file_name The name of the file.
*/
//------------------------------------------------------------------
void Error::FileW(const string class_name, const string func_name,
                  const string file_name)   // file name
{
  ofstream fp;
  error_func_name = "FileW";

  printf(error_string[file_w].c_str(), class_name.c_str(), func_name.c_str(), file_name.c_str());

  fp.open(error_file_name.c_str()) ;
  if (!fp.is_open()) {

    printf(error_string[file_w].c_str(), error_class_name.c_str(), error_func_name.c_str(), error_file_name.c_str());
    Exit(exit_value[file_w]);
  }
  snprintf(message, MAX_LEN, error_string[file_w].c_str(), class_name.c_str(), func_name.c_str(), file_name.c_str());
  message[MAX_LEN - 1] = 0;
  fp << message << endl;

  fp.close();

  Exit(exit_value[file_w]);

}


//------------------------------------------------------------------
/*!
  Prints the message
  \n<tt>Error in <i>class name</i>::<i>function name</i> :</tt>\n
  <tt>can not open file <i>file name</i> to append.</tt>\n
  to \c stdout and to the file \c phys.error.
  Exits with -4.

  \param class_name The name of a class.
  \param func_name The name of a function or method.
  \param file_name The name of the file.
*/
//------------------------------------------------------------------
void Error::FileA(const string class_name, const string func_name,
                  const string file_name)   // file name
{
  ofstream fp;
  error_func_name = "FileA";

  printf(error_string[file_a].c_str(), class_name.c_str(), func_name.c_str(), file_name.c_str());

  fp.open(error_file_name.c_str()) ;
  if (!fp.is_open()) {
    printf(error_string[file_w].c_str(), error_class_name.c_str(), error_func_name.c_str(), error_file_name.c_str());
    Exit(exit_value[file_w]);
  }
  snprintf(message, MAX_LEN, error_string[file_a].c_str(), class_name.c_str(), func_name.c_str(), file_name.c_str());
  message[MAX_LEN - 1] = 0;
  fp << message << endl;
  fp.close();

  Exit(exit_value[file_a]);
}


//------------------------------------------------------------------
/*!
  Prints the message
  \n<tt>Error in <i>class name</i>::<i>function name</i> :</tt>\n
  <tt>not implemented.</tt>\n
  to \c stdout and to the file \c phys.error.
  Exits with -5.

  \param class_name The name of a class.
  \param func_name The name of a function or method.
*/
//------------------------------------------------------------------
void Error::NotImplemented(const string class_name, const string func_name)
{
  ofstream fp;
  error_func_name = "NotImplemented";

  printf(error_string[not_implemented].c_str(), class_name.c_str(), func_name.c_str());

  fp.open(error_file_name.c_str()) ;
  if (!fp.is_open()) {
    printf(error_string[file_w].c_str(), error_class_name.c_str(), error_func_name.c_str(), error_file_name.c_str());
    Exit(exit_value[file_w]);
  }
  snprintf(message, MAX_LEN, error_string[not_implemented].c_str(), class_name.c_str(), func_name.c_str());
  message[MAX_LEN - 1] = 0;
  fp << message << endl;
  fp.close();

  Exit(exit_value[not_implemented]);
}


//------------------------------------------------------------------
/*!
  Prints the message
  \n<tt>Error in <i>class name</i>::<i>function name</i> :</tt>\n
  <tt>not implemented.</tt>\n
  <tt><i>message</i></tt>\n
  to \c stdout and to the file \c phys.error.
  Exits with -5.

  \param class_name The name of a class.
  \param func_name The name of a function or method.
  \param format A format string for the message (<em>&aacute; la</em> \c printf)
  \param ... Optional arguments to the format string.

*/
//------------------------------------------------------------------
void Error::NotImplemented(const string class_name, const string func_name,
                           const string format,  // format of message
                           ...)                 // argument list of message
{
  ofstream fp;
  error_func_name = "NotImplemented";
  va_list args;
  va_start(args, format);

  printf(error_string[not_implemented].c_str(), class_name.c_str(), func_name.c_str());
  vprintf(format.c_str(), args);

  fp.open(error_file_name.c_str()) ;
  if (!fp.is_open()) {
    printf(error_string[file_w].c_str(), error_class_name.c_str(), error_func_name.c_str(), error_file_name.c_str());
    Exit(exit_value[file_w]);
  }
  snprintf(message, MAX_LEN, error_string[not_implemented].c_str(), class_name.c_str(), func_name.c_str());
  va_end(args);
  va_start(args, format);
  vsnprintf(message, MAX_LEN, format.c_str(), args);
  message[MAX_LEN - 1] = 0;
  fp << message << endl;
  fp.close();

  va_end(args);
  Exit(exit_value[not_implemented]);
}




//------------------------------------------------------------------
/*!
  Prints the message
  \n<tt>Error in <i>class name</i>::<i>function name</i> :</tt>\n
  <tt><i>message</i></tt>\n
  to \c stdout and to the file \c phys.error.
  Exits with -7.

  \param class_name The name of a class.
  \param func_name The name of a function or method.
  \param format A format string for the message (<em>&aacute; la</em> \c printf)
  \param ... Optional arguments to the format string.
*/
//------------------------------------------------------------------
void Error::General(const string class_name, const string func_name,
                    const string format,  // format of message
                    ...)                 // argument list of message
{
  ofstream fp;
  error_func_name = "General";
  va_list args;
  va_start(args, format);

  printf(error_string[general].c_str(), class_name.c_str(), func_name.c_str());
  vprintf(format.c_str(), args);
  printf("\n");

  fp.open(error_file_name.c_str()) ;
  if (!fp.is_open()) {
    printf(error_string[file_w].c_str(), error_class_name.c_str(), error_func_name.c_str(), error_file_name.c_str());
    Exit(exit_value[file_w]);
  }

  snprintf(message, MAX_LEN, error_string[general].c_str(), class_name.c_str(), func_name.c_str());
  va_end(args);
  va_start(args, format);
  vsnprintf(message, MAX_LEN, format.c_str(), args);
  message[MAX_LEN - 1] = 0;
  fp << message << endl;
  fp.close();

  va_end(args);
  Exit(exit_value[general]);
}




