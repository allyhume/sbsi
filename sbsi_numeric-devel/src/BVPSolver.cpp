using namespace std;

#include <BVPSolver.h>
#include <Solver.h>
#include <ERR.h>


bool BVPSolver::SolveInput()
{
  ERR.NotImplemented("BVPSolver", __func__);
  return false;
}
bool BVPSolver::SolveInit()
{
  ERR.NotImplemented("BVPSolver", __func__);
  return false;
}
bool BVPSolver::Solve()
{
  ERR.NotImplemented("BVPSolver", __func__);
  return false;
}

bool BVPSolver::SolveCreate()
{
  ERR.NotImplemented("BVPSolver", __func__);
  return false;
}



