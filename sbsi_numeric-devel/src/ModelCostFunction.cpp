#include <algorithm>

using namespace std;

#include <ModelCostFunction.h>
#include <Chi2Cost.h>
#include <FFTCost.h>
#include <CVODESolver.h>
#include <BVPSolver.h>
#include <ERR.h>

void ModelCostFunction::Init(cModel *m, vector<CostArg> &ca, vector<string>& pn)
{

  Model = m;
  costargs = ca;
  ParamNames = pn;

}
void ModelCostFunction::SetUnOptimiseParam()
{
  for (size_t i = 0; i < UnOptimiseParamNames.size(); i++) {
    vector<string>::iterator index = find(Model->ModelArg.ParamNames.begin(),
                                          Model->ModelArg.ParamNames.end(), UnOptimiseParamNames.at(i));
    if (index != Model->ModelArg.ParamNames.end()) {
      int ptr = index - Model->ModelArg.ParamNames.begin();
      Model->ModelArg.Params.at(ptr) = UnOptimiseParamValues.at(i);
    } else {
      Model->ModelArg.ParamNames.push_back(UnOptimiseParamNames.at(i));
      Model->ModelArg.Params.push_back(UnOptimiseParamValues.at(i));
    }
  }
}

void ModelCostFunction::SetDataSet(size_t d_i)
{

  /* set cost_arg */
  if (!IsCostArgsEmpty()) {
    costarg = GetACostArg(d_i);
  }

  /* set a data set  */
  dataset = &(*dataset_s)[d_i];

  /* set default state initial values and param values  */
  Model->inputModel();

  /* set unoptimised parameters and values into model_arg */
  SetUnOptimiseParam();

  /* set parameters or initial values  from the optimiser */
  SetParameters(ParamValues);

  /* overwrite state initial values or parameter values if necessarily */

  for (size_t i = 0; i < dataset->GetNumInitialStateNames(); i++) {
    Model->setStateByName(dataset->GetAInitialStateName(i),
                          dataset->GetAInitialStateValue(i));
  }


  for (size_t i = 0; i < dataset->GetNumInitialParamNames(); i++) {
    Model->setParamByName(dataset->GetAInitialParamName(i),
                          dataset->GetAInitialParamValue(i));
  }

  /* set the window starting time as the earlist time in dataset t0*/

  Model->setOutInterval(GetOutputInterval());
}
void ModelCostFunction::SetSolver()
{

  solver = &(Solver::SetSolver(GetSolverType()));
  solver->SetModel(Model);
  solver->SolveInit();
  solver->SetTimeSeries(TSeries);
}

void ModelCostFunction:: SetParameters(const vector<double> in_param)
{

  size_t ptr(0);

  if (in_param.size() != ParamNames.size()) {
    ERR.General("ModelCostFunction", __func__, "The size of setin parameter shoule be the same as the setin parameterName size; in_param %d != paramname %d\n", in_param.size(), ParamNames.size() );
  }

  CostFunction::SetParameters(in_param);

  Model->inputModel();

  // Overwrite parameters, intial values to in_param using paramnames.
  for (size_t i = 0; i < ParamNames.size(); i++) {

    ptr = Model->getParamIndexByName(ParamNames.at(i));

    if (ptr < Model->getNumParameters()) {
      Model->setParamByName(ParamNames.at(i), in_param.at(i));
    } else {
      ptr = Model->getStateIndexByName(ParamNames.at(i));
      if (ptr < Model->getNumStates()) {
        Model->setStateByName(ParamNames.at(i), in_param.at(i));
      } else {
        ERR.General("ModelCostFuction", __func__, "%s; No such name in the Model\n", ParamNames.at(i).c_str());
      }
    }
  }

  /**********************           NOTE ***************************
   * After this, Chi2Cost and FFT overwrite parameter and initial values
   * using a dataset parameters and initial values; DataSet
   *****************************************************************/

}

void ModelCostFunction::SolverInit()
{

  solver->SetModel(Model);
  solver->SolveInput();
}


ModelCostFunction& ModelCostFunction::SetModelCostFunction(const ModelCostFunctionType m)
{
  ModelCostFunction *mcf;
  switch (m) {
  case X2Cost:
    mcf = new Chi2Cost();
    return *mcf;
  case FFT:
    mcf = new FFTCost();
    return *mcf;
  default:
    ERR.General("ModelCostFunction", __func__,
                "A bad Modelcostfunction type\n");
    return *mcf;
  }
}

bool ModelCostFunction::SolveRHS()
{
  return  solver->Solve();
}
vector<double> ModelCostFunction::GetResultTimeVec()
{
  return TSeries->GetTimeSeriesTime();
}

vector<double> ModelCostFunction::GetResultVec(string f_dataname)
{

  vector<double> result;

  size_t ptr = Model->getStateIndexByName(f_dataname);

  if ( ptr < Model->getNumStates() ) { // ptr range; 0: stateNames.size-1

    if (Model->getKeyParamName() != "NONE") {
      result = TSeries->GetAStateTimeSeries(ptr);
    } else {
      result.push_back(Model->getStateByName(f_dataname));
    }

  } else {

    ptr = Model->getVarIndexByName(f_dataname);

    vector<vector<double> > states;

    if (ptr < Model->getNumVars()) {
      if (Model->getKeyParamName() != "NONE") {
        states = TSeries->GetTimeSeries();
        result = Model->calVarVec(ptr, states);
      } else {
        states.resize(1);
        states.at(0) = Model->getStates();
        result = Model->calVarVec(ptr, states);
      }

    } else {
      ERR.General("ModelCostFunction", __func__, 
                  "%s can not be found, Unable to fit the data",
                  f_dataname.c_str());
    }
  }
  return result;

}






