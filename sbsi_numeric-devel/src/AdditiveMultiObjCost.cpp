using namespace std;

#include <Optimiser.h>
#include <AdditiveMultiObjCost.h>


double AdditiveMultiObjCost::GetCost()
{

  double c;
  double Cost = 0;
  multiobjcost_val.resize(0);
  CostFunction *acostfunc;

  for (size_t i = 0; i < MCFun.size(); i++) {

    acostfunc = MCFun[i];
    c = acostfunc->GetCost();
    multiobjcost_val.push_back(c);
    Cost += c;

  }

  return Cost;
}
