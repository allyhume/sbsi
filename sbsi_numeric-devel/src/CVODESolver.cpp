
#include <cfloat>

using namespace std;

#include <CVODESolver.h>
#include <ERR.h>

#include <cvode/cvode.h>        /* prototypes for CVODES fcts. and consts. */
#include <cvode/cvode_dense.h>     /* prototype for CVDense */
#include <nvector/nvector_serial.h>  /* serial N_Vector types, fcts., and macros */
#include <sundials/sundials_dense.h> /* definitions DenseMat and DENSE_ELEM */
#include <sundials/sundials_types.h> /* definition of type realtype */

/* for cvodes */
#define Ith(v,i)    NV_Ith_S(v,i-1)       /* Ith numbers components 1..EQNO */
#define IJth(A,i,j) DENSE_ELEM(A,i-1,j-1) /* IJth numbers rows,cols 1..EQNO */



/* solve routine for cvodes */

bool CVODESolver::Solve()
{
  return cvs_solver();
}

/*
   Call CVodeCreate to create the solver memory:

   CV_BDF     specifies the Backward Differentiation Formula
   CV_NEWTON  specifies a Newton iteration

   A pointer to the integrator problem memory is returned and stored in solver_mem.
*/

bool CVODESolver::SolveCreate()
{


  solver_mem = NULL;
  solver_mem = CVodeCreate(CV_BDF, CV_NEWTON);

  if (CheckFlag(solver_mem, "CVodeCreate", 0)) {
    ERR.Pointer("CVODESolver", __func__, "solver_mem");
  }

  return true;
}

bool CVODESolver::SolveInit()
{


  int flag(0);
  /* this should be calles solver->setModel() */
  size_t EQNO     = model->getNumStates();
  y_cvs = N_VNew_Serial(EQNO);
  abstol = N_VNew_Serial(EQNO);

  /* set initial time */
  double T0 = model->getInit();

  /* Set the scalar relative tolerance */
  double reltol = model->getReltol();

  flag = CVodeInit(solver_mem, cvs_f_bouncer, T0, y_cvs);
  if (CheckFlag(&flag, "CVodeMalloc", 1)) {
    ERR.Pointer("CVODESolver", __func__, "CVodeMalloc");
  }

  flag = CVodeSetUserData(solver_mem, (void *)this);
  if (CheckFlag(&flag, "CVodeSetFdata", 1)) {
    ERR.General("CVODESolver", __func__, "Falied to set Fdata to solver_mem\n");
  }

  return true;
}

bool CVODESolver::SolveInput()
{


  size_t EQNO     = model->getNumStates();

  /* if no solver_mem, CVODE create */
  if (solver_mem == NULL) {
    SolveCreate();
    SolveInit();
  }

  model->calCoef();

  /************************************************************
   *  Note: y_cvs and abstol
   * It is possible to use vector<double> for y_cvs and abstol
   * with NV_DATA_S ( convert a double pointer to N_Vector.
   * In N_VECTOR routine, y_cvs in N_Vector has to be memory allocation
   * in the data region (N_VNewEmpty_Serial).
   * In this reason, no gain for memory usage from using std vector
   * for these two N_Vector.
   **************************************************************/

  /* Initialize y_cvs*/
  vector<double> states = model->getStates();
  for (size_t i = 0; i < EQNO; i++) {
    Ith(y_cvs, i + 1) = states.at(i);
  }



  /* Set the vector absolute tolerance */
  double atol = model->getAtol();
  for (size_t i = 0; i < EQNO; i++) {
    Ith(abstol, i + 1) = atol;
  }

  /* Set the scalar relative tolerance */

  int flag(0);


  /* set initial time */
  double T0 = model->getInit();

  double reltol = model->getReltol();
  flag = CVodeReInit(solver_mem,  T0, y_cvs);

  /* Call CVodeSVtolerances to specify the scalar relative tolerance
   * and vector absolute tolerances */
  flag = CVodeSVtolerances(solver_mem, reltol, abstol);

  /* Call CVDense to specify the CVDENSE dense linear solver */
  flag = CVDense(solver_mem, EQNO);

  return true;
}


bool CVODESolver::cvs_solver()
{
  /*Initialize  */
  int iout(0);
  int flag(0);

  /* set interval for solver*/
  const double interval = model->getInterval();
  realtype targetTime = interval;

  /* initial time for solver*/
  realtype t = model->getInit();

  size_t EQNO = model->getNumStates();

  /* the final time for solver*/
  double t_final = model->getFinal();

  /* the maximal number of output time */
  int maxTimes = model->getMaxTimes();

  /* Initialize results */
  results.resize(0);
  results.resize(EQNO + 1);

  /*The first column of results is time */

  // ACH - 2013-07-30 These next two lines were commented out. Uncommented them to get
  //                  the initial value in the time series as these are needed by the
  //                  SBML test suite.
  results.at(0).push_back(t);
  for(int i=0;i<EQNO;i++) results.at(i+1).push_back(Ith(y_cvs,i+1));
  // End of line that were commented out

  iout++;

  // the output interval counter used to selectively reduce the
  // number of recorded time points, useful when Interval must be small
  // because the model has some fast rates.
  // The + 0.5 is because otherwise the cast will simply 'floor' the
  // o_out_d number, which is wrong in most cases, eg 3.9998 comes out
  // at 3, rather than 4. Adding the 0.5 turns the cast into a 'round'
  // operation rather than a floor operation.
  double o_out_d = model->getOutInterval() / interval;
  int o_out = static_cast<int>(o_out_d + 0.5);


  /* In loop, call CVode, save results, and test for error. */
  /*
   * Break out of loop when maxtime preset output times have
   * been reached or time reached the t_final.
   */


  while (1) {
    flag = CVode(solver_mem, targetTime, y_cvs, &t, CV_NORMAL);

    if (CheckFlag(&flag, "CVode", 1)) {
      break;
    }


    if (flag == CV_SUCCESS) {
      if ((t + interval > model->getExpTInit()) && (iout % o_out == 0)) {
        for (size_t i = 0; i < EQNO; i++) {
          if (isnan(Ith(y_cvs, i + 1))) {
            flag = 22;
            break;
          }
          results.at(i + 1).push_back(Ith(y_cvs, i + 1));
        }
        // First update the time component of the timeseries
        results.at(0).push_back(t);
        // And then call the model's own update assignment variables
        // which should modify in place the timeseries (or this
        // time's value) of any species which are calculated through
        // and assignment rule.
        model->update_assignment_variables(&results);
        
      }

      iout++;
      // Actually here we should be able to check if there is an
      // event that may fire sooner than 'targetTime'.
      targetTime += interval;
    }

    if ((targetTime > t_final + interval) || (iout > maxTimes)) {
      break;
    }
  }

  /* initialize timeseries results vector */
  t_series->CleanTimeSeries();

  /* If ODE is solved successfuly, set results to timeseries results, */
  /* otherwise return false */

  if (flag == CV_SUCCESS) {
    t_series->SetTimeSeries(&results);
    return true;
  } else {
    CVodeFree(&solver_mem);
    N_VDestroy_Serial(abstol);
    N_VDestroy_Serial(y_cvs);
    return false;
  }
}

/* ODE Function called by the solver */
int CVODESolver::cvs_f_bouncer( realtype t, N_Vector y, N_Vector ydot, void *f_data)
{
  CVODESolver *me = (CVODESolver *)f_data;
  return me->cvs_f(t, y, ydot);
}

int CVODESolver::cvs_f( realtype t, N_Vector y, N_Vector ydot)
{

  double *y_p = NV_DATA_S(y);
  double *yd_p = NV_DATA_S(ydot);

  model->getRHS(y_p, t, yd_p);

  return(0);
}
/* Check function return value...
*   opt == 0 means SUNDIALS function allocates memory so check if
*            returned NULL pointer
*   opt == 1 means SUNDIALS function returns a flag so check if
*            flag >= 0
*   opt == 2 means function allocates memory so check if returned
*            NULL pointer
*/

//int CVODESolver::CheckFlag(void *flagvalue, const char *funcname, int opt)
//{
//  int *errflag;
//
////  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
//  if (opt == 0 && flagvalue == NULL)  return(1);
//
//  /* Check if flag < 0 */
//  else if (opt == 1) {
//    errflag = (int *) flagvalue;
//    if (*errflag < 0)  {
//      fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
//              funcname, *errflag);
//     return(1);
//    }
//   }
//
//  /* Check if function returned NULL pointer - no memory allocated */
//  else if (opt == 2 && flagvalue == NULL) {
//    return(1); }
//
//  return(0);
//}

/* these are for the case with unequal solver time intervals  */
bool TimeVectorCVODESolver::cvs_solver()
{
  int iout(0);
  int flag(0);
  realtype t = 0;
  const double interval = model->getInterval();
  realtype targetTime = interval;


  size_t EQNO = model->getNumStates();
  int maxTimes = model->getMaxTimes();
  vector< vector<double> > results;
  results.resize(EQNO + 1);

  /* push in the initial time and initial state value */
  //results.at(0).push_back(t);
  //for(int i=0;i<EQNO;i++) results.at(i+1).push_back(Ith(y_cvs,i+1));

  for (size_t t_iter = 0; t_iter < time_points.size(); t_iter++) {

    targetTime = time_points.at(t_iter);
    flag = CVode(solver_mem, targetTime, y_cvs, &t, CV_NORMAL);

    results.at(0).push_back(t);
    for (size_t i = 0; i < EQNO; i++) {
      results.at(i + 1).push_back(Ith(y_cvs, i + 1));
    }

    if (CheckFlag(&flag, "CVode", 1) && (flag == CV_SUCCESS)) {
      iout++;
    }

    if ((iout == maxTimes)) {
      break;
    }

  }

  t_series->CleanTimeSeries();

  if (flag == CV_SUCCESS) {
    t_series->SetTimeSeries(&results);
    N_VDestroy_Serial(abstol);
    N_VDestroy_Serial(y_cvs);
    return true;
  } else {
    CVodeFree(&solver_mem);
    return false;
  }
}
bool TimeVectorCVODESolver::Solve()
{
  return cvs_solver();

}

void TimeVectorCVODESolver::create_time_points(double t0, double interval, vector<double> *measure_points)
{
  double t;
  int i;
  for (t = t0, i = 0; t < measure_points->back(); t += interval) {
    double tn = t + interval;
    while ((size_t)i < measure_points->size() && measure_points->at(i) <= tn) {
      time_points.push_back(measure_points->at(i));
      i++;
    }
    if (time_points.size() == 0 || time_points.back() - tn > DBL_EPSILON ) {
      time_points.push_back(tn);
    }
  }

}

