using namespace std;

#include <cmath>
#include <cfloat>
#include <algorithm>
#include <functional>
#include <FFTCost.h>
#include <ERR.h>
#include <MathsUtils.h>

/**
 * Accuracy with which we wish to measure the period.  0.01 means accuate to within
 * 1% of the desired period.
 */
static const double PERIOD_ACCURACY_FACTOR = 0.01;

/*
 * These comments don't make much sense to me, surely 3.2.x is newer
 * than 2.1.5? Essentially I think we can just forget about fftw 2.1.5
 * and insist that users install the 3.2.2 version.
 */

/* This FFTCost source code is for FFTW 3.2.x  and 2.1.5 */
/* FFTW 3.2.x's API is incompatible with the newer version 2.1.5.  */
/* FFTW 3,2,x does not support MPI distributed-memory transformd. */
/* See the detail your high performance computer enviroment */
/* FFTW 3.2.x is portable on any platform with C compiler and is default in this framework.  */
/* FFTW 2.1.5 is avairable with flag --with-fftw2=yes and --with-fftw3=no at configure*/


// For debugging change this undef into a define.
#undef DEBUG_FFT_COST


/* set a data set  */
void FFTCost:: SetDataSet(size_t d_i)
{

  ModelCostFunction::SetDataSet(d_i);

  /* set the window starting time as the earlist time in dataset t0*/
  vector<double> tstart = GetFFTTstart();
  Model->setExpTInit(*(min_element(tstart.begin(), tstart.end())));

  /* Check the all costarg arrays are filled correctly */
  if (GetNumFFTTarget() != GetNumFFTPeriod()) {
    ERR.General("FFTCost", __func__,
                " The number of FFT targets (%d)  != The number of target periods (%d).\n",
                GetNumFFTTarget(), GetNumFFTPeriod());
  }
  if (GetNumFFTTarget() != GetNumFFTTstart()) {
    ERR.General("FFTCost", __func__,
                " The number of FFT targets (%d)  != The number of sampling starting times (%d).\n",
                GetNumFFTTarget(), GetNumFFTTstart());
  }
  if (GetNumFFTTend() != GetNumFFTTstart()) {
    ERR.General("FFTCost", __func__,
                " The number of FFT start sampling time (%d)  != The number of sampling endig  times (%d).\n",
                GetNumFFTTstart(), GetNumFFTTend());
  }

}

/* get total sum cost for all data sets */
double FFTCost::GetCost()
{
  /* the case FFTCost has only one data set */
  if ((dataset_s == NULL) || (dataset_s->size() == 0)) {
    return getcost();
  }

  /* the case there are multi dataset */
  /* Initialize cost and costs.vector */
  double cost(0);
  Costs.clear();

  /* sum over a cost for each dataset */
  for (size_t i = 0; i < dataset_s->size(); i++) {
    SetDataSet(i);

    /*
     SolverInit();
     SolveRHS();
     */
    Solve();

    double c = getcost();
    c *= model_cost_weight;

    Costs.push_back(c);

    cost += c;

    // Just some debugging code
    //cout << "    FFT " << i << "th cost is: " << c << " weight is: " << model_cost_weight 
    //     << ", orig cost: " << (c/model_cost_weight) << endl;
  }

  /* 
   * We used to return the average cost over all the data sets, but
   * we instead now simply sum them. Since this is simpler for the user
   * (they can see the relation between the individual data sets and their
   * total cost more easily). It is also what we do for X^2 costs so this
   * is more consistent. 
   */
  //double result =  cost / (double)(dataset_s->size());
  double result = cost;
  return result;

}

/*
 * The cost value to return if we detect no rhythm in the time series
 * under evaluation. It is also used when we just want to return a 
 * punishingly high cost, for example if the solver failed and we have
 * no timeseries.
 */
#define FFT_ARYTHMIC_HIGH_COST 10000000


/**
 * Computes the required duration for a time series in order for FFT to be able
 * to detect the target period with sufficient accuracy.
 * 
 * @param targetPeriod   period that FFT will be used to detect
 * @param accuracy       desired accuracy
 *
 * @return a duration for the time series such that the difference in period value
 *         corresponding to adjacent points in the power spectrum around the desired
 *         period are less than the given accuracy value.
 */
double compute_required_timeseries_duration(double targetPeriod, double accuracy) {

  // Choose a starting point and avoid divide by zero errors in the code below
  double time_series_duration = 4.0*targetPeriod;

  while( true ) {

    int index = time_series_duration/targetPeriod;

    double p1 = time_series_duration/(double)index;
    double p0 = time_series_duration/((double)index-1);
    
    if (p0-p1 < accuracy) {
      return time_series_duration;
    }
    time_series_duration = 1.5*time_series_duration;
  }
}


/*
 * Compute the coefficient of deviation (that is the ratio between the
 * standard deviation and the mean) for a set of results. Note that we
 * expect the given results to be only those included within the sampled
 * time. So we do not need to worry about excluding values that are
 * outwith the sampled time.
 */
double compute_coeff_deviation(vector<double> results){
  // Now compute the mean of the result values
  double mean = MathsUtils::mean(results);
  double size_of_results = results.size();

  // Now calculate the actual deviations from the mean, and square them
  // while we're at it.
  double sum_of_squared_deviations = 0.0;
  for (int index = 0; index < size_of_results; index++){
    double this_value = results.at(index);
    // The difference between this value and the mean
    double difference = this_value - mean;
    // Square of that difference
    double squared_diff = difference * difference;
    sum_of_squared_deviations += squared_diff;
  }
  // Divide the sum of the squared differences by one less than the
  // number of values:
  double std_dev_squared = sum_of_squared_deviations / 
                           (size_of_results - 1);

  // We could avoid the square root since the square of the standard
  // deviation is still a valid measure of the amplitude.
  double standard_deviation = sqrt(std_dev_squared);

  // Rather than return the standard deviation we return the
  // coefficient of variation, which is the ratio between the
  // standard deviation and the mean of the values. In effect this
  // normalises the measure of deviation such that say different states
  // with wildly different populations/concentrations can have the same
  // effect on the cost. In a similar way to the normalisation that we do
  // for the chi-squared cost function. To put this another way a standard
  // deviation of 0.5 is great if the average of the values is 1, but if
  // the average of the values is 1000 then 0.5 barely represents a signal.
  double coeff_variation = standard_deviation / mean;
  return coeff_variation;
}

/*
 * We could do a similar thing to the above and only take in the vector
 * of results that are within the sampled time. Although we then wouldn't
 * compute the actual mid-point-time we could just use the length of the
 * results array and take the first half and the second half of the values,
 * assuming that they are uniformly distributed.
 */
double compute_damp_coeff(vector<double> times, vector<double> results,
                          double start_time, double stop_time){
    double mid_point_time = start_time + ((stop_time - start_time) / 2.0);
 
    double max_before_half_time = 0.0;
    double max_after_half_time = 0.0;
    double min_before_half_time = DBL_MAX;
    double min_after_half_time = DBL_MAX;

#ifdef DEBUG_FFT_COST
double time_of_max = 0.0;
double time_of_min = 0.0;
#endif


    // Two while loops which compute the maximum and minimum
    // values in the first and second half of the results. This allows
    // us to decide whether or not the signal is dampening. 
    int index = 0;
    while (index < results.size() && index < times.size()){
      double this_time = times.at(index);
      // We continue in this loop if we have not yet reached the
      // start_time.
      if (this_time < start_time){
        continue;
      }
      // If we have passed the mid-point of the sampled time, then it's
      // time to move onto the second of the two loops and record the
      // maximum and minimum values in the second half.
      if (this_time > mid_point_time){
        break;
      }

      // So then assuming we're after the start time but before the
      // mid-point, basically we update the maximum and minimum values
      // in the first half if the current value is higher than the maximum
      // or lower than the minimum.
      double this_value = results.at(index);
      if (this_value > max_before_half_time){
        max_before_half_time = this_value;
#ifdef DEBUG_FFT_COST
        time_of_max = this_time;
#endif
      }
      if (this_value < min_before_half_time){
        min_before_half_time = this_value;
#ifdef DEBUG_FFT_COST
        time_of_min = this_time;
#endif
      }
      index++;
    }
    // The second of the two loops records the maximum and minimum values
    // in the second half of the sampled time period.
    while (index < results.size() && index < times.size()){
      double this_time = times.at(index);
      // We know we must be after the mid point, but we do not wish to
      // record any maximum or minimum points outwith the sampled
      // region, so we break if we are after the stop time.
      if (this_time > stop_time){
        break;
      }

      // Since we know we're in the second half of the sampled time region
      // simply update the maximum and minimum values of the post-mid-point
      // appropriately, that is if the current value is higher than the
      // maximum or lower than the minimum.
      double this_value = results.at(index);
      if (this_value > max_after_half_time){
        max_after_half_time = this_value;
      }
      if (this_value < min_after_half_time){
        min_after_half_time = this_value;
      }
      index++;
    }

#ifdef DEBUG_FFT_COST
    cout << "The initial time is: " << start_time << endl;
    cout << "The end time is: " << stop_time << endl;
    cout << "The midway point is: " << mid_point_time << endl;
    cout << "The largest value before mid-way is :"
         << max_before_half_time << endl;
    cout << "Occurring at time: " << time_of_max << endl;
    cout << "The smallest value before mid-way is : "
         << min_before_half_time << endl;
    cout << "Occurring at time: " << time_of_min << endl;
    cout << "Giving period of: " << 
            (abs(time_of_max - time_of_min) * 2) << endl;
    cout << "The largest value after mid-way is :"
         << max_after_half_time << endl;
    cout << "The smallest value after mid-way is : "
         << min_after_half_time << endl;
#endif
    double pre_amplitude = max_before_half_time - min_before_half_time;
    double post_amplitude = max_after_half_time - min_after_half_time;
    double damp_coeff = pre_amplitude / post_amplitude;

    // If the dampening coefficient is less than one then that means the
    // amplitude is actually rising, since we do not necessarily wish to
    // reward that we set 1 as the minimum dampening coefficient.
    if (damp_coeff < 1){
      damp_coeff = 1;
    }
#ifdef DEBUG_FFT_COST
    cout << "The dampening coefficient is: " << damp_coeff << endl; 
#endif 

   return damp_coeff;
}

/* get cost for a data set */
/* called from getCost() */
double FFTCost::getcost()
{

  /* Initialize cost */
  double cost(0);
  string t_name;

  /* if timeseries results size is zero, ODE solver failed. */
  /* return the max cost times O(12)*/

  if (TSeries->IsTimeSeriesEmpty()) {
#ifdef DEBUG_FFT_COST
    cout << "Time series is empty, return max cost!!!" << endl;
#endif 
    return FFT_ARYTHMIC_HIGH_COST;
  }


  /* FFTW: initialize plan */
  p = NULL;
  /* create for power spectrum calculation */
  power_spect.clear();
  
  /* loop over all state or variable name in fft_target */

  Period.clear();
  Period.resize(GetNumFFTTarget());

  vector<double>  time = GetResultTimeVec();

  for (size_t i = 0; i < GetNumFFTTarget(); i++) {

    t_name = GetAFFTTarget(i);
    vector<double>  tresult = GetResultVec(t_name);
    /* set a start time and an end time*/
    double ti = GetAFFTTstart(i);
    double te = GetAFFTTend(i);

    if (tresult.size() != time.size()) {
#ifdef DEBUG_FFT_COST
      cout << "tresult.size != time.size, return max cost!!!" << endl;
#endif
      return FFT_ARYTHMIC_HIGH_COST;
    }

    if (te < 0) {
      te = Model->getFinal();
    }
    vector<double>::iterator iter_t =
        find_if(time.begin(), time.end(), 
                bind2nd(greater_equal<double>(), ti));
    if (iter_t == time.end()) {
      cerr << "Time point not found in getcost of FFTCost " << endl;
    }

    vector<double> result;

    int jj = (int)(iter_t - time.begin());
    // Note that *iter_t will result in a segmentation fault if
    // iter_t == time.end(), hence we must have that check first.
    while ( iter_t != time.end() && *iter_t <= te && jj < tresult.size() ) {
      result.push_back(tresult.at(jj));
      iter_t++;
      jj++;
    }

    double target_period = GetAFFTPeriod(i);


    // We need to pad the time series so that the FFT gives us good enough
    // accuracy around the desired period. Before padding with zeros we should
    // detrend the time series to effectively centre it around zero
    vector<double> resultForFFT;
    MathsUtils::detrend(result, resultForFFT);  
    
    // Calculate the required duration of the FFT
    int time_series_duration = compute_required_timeseries_duration(
        target_period, PERIOD_ACCURACY_FACTOR*target_period);

    // Pad the time series data to this duration
    size_t N = time_series_duration / Model->getOutInterval();
    while (resultForFFT.size() < N) {
      resultForFFT.push_back(0.0);
    }
    
	N = resultForFFT.size();

    /*FFTW:  allocate output data array */
    /*FFTW:  create plan */
    /* FFTW: one dimensional transform */
    /*get the power spectrum  part */
#ifdef ENABLE_FFTW2
    GetFFT2(resultForFFT);
#else
    GetFFT3(resultForFFT);
#endif


    /* get the max spectrum */

    vector<double>::iterator max =
          max_element(power_spect.begin() + 1, power_spect.end());
    int ptr = (int)(max - power_spect.begin());

    /* if there is no period, return maxcost * 10^(12) */
    if (ptr == 1) {
#ifdef DEBUG_FFT_COST
      cout << "The target period is: " << GetAFFTPeriod(i) << endl;
      cout << "There is no period to the data, return max cost!!!"
           << endl;
#endif
      return FFT_ARYTHMIC_HIGH_COST;
    }

    /* get the period from ptr */
    double t = (double)ptr / ((double)N * (Model->getOutInterval()));
    double myfrequency = t;
    double period = 1.0 / myfrequency;
    Period.at(i) = period;
    
#ifdef DEBUG_FFT_COST
    cout << "Model->getOutInterval: " << Model->getOutInterval() << endl;
    cout << "N: " << N << endl;
    cout << "ptr: " << ptr << endl;
    cout << "The target period is: " << target_period << endl;
    cout << "The frequency is: " << myfrequency << endl;
    cout << "The period is: " << period << endl;
    cout << "The energy is: " << *max << endl;
#endif

    // if t is larger than twice of target period,
    // return maxcost *10^(12)
    if (period > 2.0 * target_period) {
#ifdef DEBUG_FFT_COST
      cout << "Period is more than twice target, return max cost!!!"
           << endl;
#endif
      return FFT_ARYTHMIC_HIGH_COST;
    }

    // if the amplitude is bigger than the default 10^(8) or the MaxAmp,
    // return the maxcost

    if (*max > GetAMaxAmplitude(i)) {
#ifdef DEBUG_FFT_COST
      cout << "Amplitude is larger than maxamp, return max cost!!!"
           << endl;
      cout << "The amplitude is: " << *max << endl;
      cout << "The maxamp is: " << GetAMaxAmplitude(i) << endl;
#endif
      // return FFT_ARYTHMIC_HIGH_COST;
    }


    // if the amplitude is smaller than the default 10^(-8) or the MinAmp,
    // return the maxcost
    if (*max < GetAMinAmplitude(i)) {
#ifdef DEBUG_FFT_COST
      cout << "Amplitude is smaller than minamp, return max cost!!!"
           << endl;
#endif
      return FFT_ARYTHMIC_HIGH_COST;
    }

#ifdef DEBUG_FFT_COST
    cout << "The value of '*max' is: " << *max << endl;
#endif

    /* factorize amplitude to remove too small amplitude */
    // This is set in the configuration file with "FncAmplitude"
    double fct(1.0);
    double amp_coeff(1.0);
    // The amplitude of the signal could be gotten from the results
    // of the fft or from computing the coefficient of deviation.
    double amplitude_proxy = compute_coeff_deviation(result);
    // double amplitude_proxy = *max;
#ifdef DEBUG_FFT_COST
    cout << "The amplitude proxy is: " << amplitude_proxy << endl;
    cout << "Fnc option is: " << GetAFFTAmpFnc(i) << endl;
#endif

    // Set the dampening coefficient to be one so that it won't
    // affect the cost, for those amplitude functions with dampening
    // we set the damp_coeff to be something other than 1.
    double damp_coeff = 1.0; 
    switch (GetAFFTAmpFnc(i)){
      case AmpFncNone:
        fct = 1.0;
      break;
      case AmpFncFunctional:
        fct = 1.0 / sqrt(amplitude_proxy);
      break;
      case AmpFncMax:
        amp_coeff = log(amplitude_proxy + 1);
        amp_coeff += 1.0 / amp_coeff;
        fct = 1.0 / sqrt(amp_coeff);
      break;
      case AmpFncFunctionalNone:
        fct = 1.0 / sqrt(amplitude_proxy);
        fct += 1;
      break;

      // So we basically do the same again, but for all the damp
      // functional costs we have to compute the dampening coefficient.
      case AmpFncDampNone:
        fct = 1.0;
        damp_coeff = compute_damp_coeff(time, tresult, ti, te);
      break;
      case AmpFncDampFunctional:
        fct = 1.0 / sqrt(amplitude_proxy);
        damp_coeff = compute_damp_coeff(time, tresult, ti, te);
      break;
      case AmpFncDampMax:
        amp_coeff = log(amplitude_proxy + 1);
        amp_coeff += 1.0 / amp_coeff;
        fct = 1.0 / sqrt(amp_coeff);
        damp_coeff = compute_damp_coeff(time, tresult, ti, te);
      break;
      case AmpFncDampFunctionalNone:
        fct = 1.0 / sqrt(amplitude_proxy);
        fct += 1;
        damp_coeff = compute_damp_coeff(time, tresult, ti, te);
       break;
      default:
       ERR.General("FFTCost", __func__,
                   "Unimplemented amplitude function");
    }

    /* sum cost over all target fft */
    double period_differences = period - target_period;
    double this_cost = (period_differences * period_differences) * fct;
    // Finally apply the dampening coefficient
    this_cost *= damp_coeff;

#ifdef DEBUG_FFT_COST
    cout << "fct: " << fct << endl;
    cout << "Damp coeff: " << damp_coeff << endl;
    cout << "This state's cost is: " << this_cost << endl;
#endif
    cost += this_cost;

    /* clean power spect vector and ouput vector */
    power_spect.clear();
    result.clear();
    tresult.clear();
  }

  /* FFTW: destroy the plan */
#ifdef ENABLE_FFTW2
  rfftw_destroy_plan(p);
#else
  fftw_destroy_plan(p);
#endif

  /* return the average cost over fft_target */
  cost /= (double)(GetNumFFTTarget());
  return cost;

}

void FFTCost:: GetFFT3(vector<double> result )
{

#ifndef ENABLE_FFTW2
  size_t N = result.size();

  fftw_complex  *out;
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

  p = fftw_plan_dft_r2c_1d(N, &result[0], out, FFTW_ESTIMATE);

  fftw_execute(p);

  power_spect.push_back(out[0][0]*out[0][0]);
  for (size_t j = 1; j < (N + 1) / 2; j++) {
    power_spect.push_back(out[j][0]*out[j][0] + out[j][1]*out[j][1]);
  }
  if (N % 2 == 0) {
    power_spect.push_back(out[N/2][0]*out[N/2][0]);
  }
  
  fftw_free(out);
#else
  ERR.General("FFTCost", __func__, " Called FFTW v3 . \n");

#endif
}

void FFTCost:: GetFFT2(vector<double> result)
{

#ifdef ENABLE_FFTW2
  size_t N = result.size();

  vector<double> out;
  out.resize(N);

  p = rfftw_create_plan(N, FFTW_REAL_TO_COMPLEX, FFTW_ESTIMATE);

  rfftw_one(p, &result[0], &out[0]);

  power_spect.push_back(out[0]*out[0]);
  for (size_t j = 1; j < (N + 1) / 2; j++) {
    power_spect.push_back(out[j]*out[j] + out[N-j]*out[N-j]);
  }
  if (N % 2 == 0) {
    power_spect.push_back(out[N/2]*out[N/2]);
  }

  phase.push_back(0);
  for (size_t j = 1; j < (N + 1) / 2; j++) {
    phase.push_back(atan2(out[N-j], out[j]));
  }

  out.clear();
#else
  ERR.General("FFTCost", __func__, " Called FFTW v2. \n");

#endif
}

