using namespace std;

#include <FileReader.h>
#include <ERR.h>


string FileReader::GetLine(fstream &f)
{
  char buf[1048576];
  buf[0] = '\0';
  f.getline(buf, 1048576);
  return string(buf);
}

vector<string> FileReader::Tokenize(string line)
{
  vector<string> list;
  line = StripWhitespace(line);
  string::iterator i = line.begin();
  while (i != line.end()) {
    string::iterator j = i;
    for (j = i; !IsDelimiter(*j) && j != line.end(); j++) ;

    string word = string(i, j);
    list.push_back(word);
    i = j;
    if ( i != line.end() ) {
      i++;
    }
  }
  return list;
}

void FileReader::SetTokenDelimiters(string chars)
{
  delimiters = chars;
}
void FileReader::SetWhitespaces(string chars)
{
  whitespaces = chars;
}

bool FileReader::IsDelimiter(char c)
{
  string::iterator d;
  for (d = delimiters.begin(); d != delimiters.end(); d++) {
    if ( *d == c ) {
      return true;
    }
  }
  return false;
}
/*!
 * This implementation strips ALL whitespace from a string, even internal whitespace
 */
string FileReader::StripWhitespace(string in)
{
  string out;
  string::iterator i;
  for (i = in.begin(); i != in.end(); i++) {
    if ( !IsWhitespace(*i) ) {
      out = out + *i;
    }
  }
  return out;
}
bool FileReader::IsWhitespace(char c)
{
  string::iterator d;
  for (d = whitespaces.begin(); d != whitespaces.end(); d++) {
    if ( *d == c ) {
      return true;
    }
  }
  return false;
}
/*!
 * Implementation of IsWord. If the first character is alphanumeric, returns <code>true</code.
 * \param  A string to test
 * \return  <code>true</code> if first character is alphanumeric, false otherwise or if string is empty.
 */
bool FileReader::IsWord(string s)
{
  if (  s == "" ) {
    return false;
  }
  return isalpha(s[0]);
}

/*!
 * Implementation of IsNumber(). If the first character is a digit, returns <code>true</code, or if
 * first character = '-' and the second character, if any, is a number;
 * \param  A string to test
 * \return <code>true</code> if first character is a digit, false otherwise or if string is empty.
 */
bool FileReader::IsNum(string s)
{
  if (  s == "" ) {
    return false;
  }
  return isdigit(s[0]) || (s[0] == '-' && s.size() > 1 && isdigit(s[1]));
}
double FileReader::ToDouble(string s)
{
  if (  s == "" ) {
    return 0;
  }
  istringstream is(s);
  double d;

  if (!(is >> d)) {
    ERR.General("FileReader", __func__, "can not convert to double; %s\n", s.c_str());
  }

  return d;
}

FileReader &FileReader::SetFileReader(FileReaderType which)
{
  FileReader *fr;
  if ( which == TabDelimitedColumnData ) {
    fr = new TDCFileReader();
    return *fr;
  }
  if ( which == TabDelimitedColumnPop ) {
    fr = new TDCPopFileReader();
    return *fr;
  }
  if ( which == TabDelimitedColumnParamInfo ) {
    fr = new TDCParamInfoFileReader();
    return *fr;
  }
  if ( which == TabDelimitedColumnHeader ) {
    fr = new TDCHeaderFileReader();
    return *fr;
  }
  ERR.General("FileReader", __func__, "A bad filereader type\n");
  return *fr; // no reachable
}

