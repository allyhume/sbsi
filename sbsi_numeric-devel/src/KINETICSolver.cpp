using namespace std;

#include <KINETICSolver.h>
#include <Solver.h>
#include <ERR.h>


bool KINETICSolver::SolveCreate()
{
  solver_mem = NULL;
  solver_mem = KINCreate();
  if (CheckFlag((void *)solver_mem, "KINCreate", 0)) {
    return false;
  }
  return true;

}

bool KINETICSolver::SolveInit()
{
  int flag(0);
  /* this should be calles solver->setModel() */
  size_t VARNO     = model->getNumStates();
  u = N_VNew_Serial(VARNO);
  s = N_VNew_Serial(VARNO);
  c = N_VNew_Serial(VARNO);

  N_VConst_Serial(1.0, s); /* no scaling */

  /* No constraints for solutions*/

  N_VConst_Serial(0.0, c);


  /***********************/
  /* c_i=0.0 no constraints */
  /* c_i=1.0 s_i .ge. 0 */
  /* c_i=-1.0 s_i .le. 0 */
  /* c_i=2.0 s_i .gt. 0 */
  /* c_i=-2.0 s_i .lt. 0 */
  /***********************/


  flag = KINInit(solver_mem, ksl_f_bouncer, u);
  if (CheckFlag(&flag, "KINInit", 1)) {
    return false;
  }

  flag = KINSetUserData(solver_mem, (void *)this);
  if (CheckFlag(&flag, "KINSetUserData", 1)) {
    return false;
  }

}

bool KINETICSolver::SolveInput()
{
  size_t VARNO     = model->getNumStates();

  /* if no solver_mem, CVODE create */
  if (solver_mem == NULL) {
    SolveCreate();
    SolveInit();
  }

  N_VConst_Serial(0.0, u); /* Initialize u */

  /* set the boundary condition*/
  model->setBoundary();

  model->calCoef();

  /* set the constraints*/
  model->setConstraints();

  /* Initialize the first guess*/
  model->setInitialGuess();

  vector<double> states = model->getStates();
  for (size_t i = 0; i < VARNO; i++) {
    NV_Ith_S(u, i) = states.at(i);
  }

  /* Set Constraints from Model_arg*/
  NV_DATA_S(c) = &(model->getConstraints())[0];

  /* set functon tolerance and step tolerance*/
  glstr = model->getStr();
  mset = model->getMset();
  functol = model->getFnormtol();
  steptol = model->getSteptol();
  int flag = KINSetConstraints(solver_mem, c);
  if (CheckFlag(&flag, "KINSetConstraints", 1)) {
    return false;
  }

  flag = KINSetFuncNormTol(solver_mem, functol);
  if (CheckFlag(&flag, "KINSetFuncNormTol", 1)) {
    return false;
  }

  flag = KINSetScaledStepTol(solver_mem, steptol);

  /* Call KINDense to specify the linear solver */

  flag = KINDense(solver_mem, VARNO);
  if (CheckFlag(&flag, "KINDense", 1)) {
    return false;
  }

  return true;

}

bool KINETICSolver::Solve()
{
  /* Initialize*/
  int flag(0);

  flag = KINSetMaxSetupCalls(solver_mem, mset);
  if (CheckFlag(&flag, "KINSetMaxSetupCalls", 1)) {
    return false;
  }

  vector<double>tmp;
  results.clear();


  flag = KINSol(solver_mem, u, glstr, s, s);
  if (CheckFlag(&flag, "KINSol", 1)) {
    return false;
  }

  size_t VARNO     = model->getNumStates();

  tmp.clear();
  for (size_t i = 0; i < VARNO; i++) {
    tmp.push_back(NV_Ith_S(u, i));
  }
  results.push_back(tmp);

  /* if no_key */
  model->setStates(tmp);

  return true;

}

int KINETICSolver::ksl_f_bouncer( N_Vector u, N_Vector f, void *user_data)
{
  KINETICSolver *me = (KINETICSolver *)user_data;
  return me->ksl_f(u, f);
}

int KINETICSolver::ksl_f(N_Vector u, N_Vector f)
{
  double *u_p = NV_DATA_S(u);
  double *f_p = NV_DATA_S(f);
  double t(0);
  model->getRHS(u_p, t, f_p);
  return(0);
}










