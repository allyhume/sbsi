using namespace std;

#include <CommandLine.h>
#include <ERR.h>

CommandLine CommandLine::inst;

int CommandLine::arg_as_int()
{
  const int ret(arg_as_int(inst.count));
  inst.count++;
  return ret;
}

double CommandLine::arg_as_double()
{
  const double ret(arg_as_double(inst.count));
  inst.count++;
  return ret;
}

string CommandLine::arg()
{
  string ret(arg(inst.count));
  inst.count++;
  return ret;
}

string CommandLine::arg( int num )
{
  if ( num >= (inst.argc - 1) || num < 0 ) {
    if (inst.my_rank == 0) {
      ERR.General(inst.cname, "arg()", "out of range\n");
    }
  }
  string ret = inst.argv[num+1];

  return ret;
}


double CommandLine::arg_as_double( int num )
{
  if ( num >= (inst.argc - 1) || num < 0 ) {
    if (inst.my_rank == 0) {
      ERR.General(inst.cname, __func__, "out of range\n");
    }
  }
  double tmp_dbl;
  if ( sscanf(inst.argv[num+1], "%lg", &tmp_dbl) != 1 ) {
    if (inst.my_rank == 0) {
      ERR.General(inst.cname, __func__, "bad conversion; command-line arg %i\n", num) ;
    }
  }
  return tmp_dbl;
}


int CommandLine::arg_as_int( int num )
{
  if ( num >= inst.argc - 1 || num < 0 ) {
    if (inst.my_rank == 0) {
      ERR.General(inst.cname, __func__, "out of range\n");
    }
  }
  int tmp_int;
  if ( sscanf(inst.argv[num+1], "%i", &tmp_int) != 1 ) {
    if (inst.my_rank == 0) {
      ERR.General(inst.cname, __func__, "bad conversion; command-line arg %i\n", num) ;
    }
  }
  return tmp_int;
}


