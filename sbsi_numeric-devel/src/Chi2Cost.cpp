#include <cfloat>
using namespace std;

#include <algorithm>
#include <functional>
#include <ModelCostFunction.h>
#include <Chi2Cost.h>
#include <ERR.h>


// adc: wow this seems to be set pretty high, I wonder if
// the author actually intended 0.00009?
#define TOLERANCE 0.9999

// For debugging change this undef into a define.
#undef DEBUG_CHI_COST


void Chi2Cost::SetDataSets( vector<DataSet> *ds_s)
{
  ModelCostFunction::SetDataSets( ds_s );
  GetNorm();
}

void Chi2Cost:: GetNorm()
{

  Norms.resize(0);
  Norms.resize(dataset_s->size());

  size_t start(1);
  if (Model->getKeyParamName() == "NONE") {
    start = 0;
  }

  for (size_t i = 0; i < dataset_s->size(); i++) {

    DataSet *dataset = &(*dataset_s).at(i);

    for (size_t j = start; j < dataset->GetNumDataValues(); j++) {
      if (GetACostValueType(i) == Normed) {
        double avg(0);
        size_t jj(0);
        int count = 0;
        while ((jj < dataset->GetNumDataTimes()) && 
               (dataset->GetADataTime(jj) <= Model->getFinal())) {
          // if (see below) we are ignoring the time point 0.0 in the
          // data set, then really we should also ignore it for the purposes
          // normalising the data, hence: This does somewhat feel like a
          // kludge.
          if (dataset->GetADataTime(jj) != 0.0){
            // Note if there are negative data points then we want the
            // average of their magnitude, not their true average, since
            // that might be very close to zero, even though the numbers
            // involved are quite large. -- allan.
            avg += abs(dataset->GetADataValue(j, jj));
            count++;
          }
          jj++;
        }
        // count is introduced because it may not now be the same as jj
        // since we might be ignoring the zero time point.
        avg /= (double) count;// (double)(jj);
        avg *= avg;
        /* If all the data points are zero
         * then just return the normalisation as 1, this is probably
         * not quite correct, since if you had even one data point as
         * 0.01, you would end up with a very different normalisation
         * than if you had all zeros, which seems wrong to me -- allan.
         */
        if (avg == 0.0){
          Norms.at(i).push_back(1);
        } else {
          Norms.at(i).push_back(1.0 / avg);
        }
      } else {
        Norms.at(i).push_back(1);
      }



    }

  }

}

void Chi2Cost:: SetDataSet(size_t d_i)
{

  ModelCostFunction::SetDataSet(d_i);

  Scale.resize(0);
  Curve.resize(0);
  Base.resize(0);
  Weight.resize(0);

  Weight.resize(dataset->GetNumDataNames());
  for (size_t i = 0; i < dataset->GetNumDataNames(); i++) {
    Scale.push_back(1);
    Base.push_back(0);
    Curve.push_back(1);
    for (size_t j = 0; j < dataset->GetNumADataValues(i); j++) {
      Weight.at(i).push_back(1);
    }
  }

  /* set the intial time of  exp_data */
  if (Model->getKeyParamName() != "NONE") {
    Model->setExpTInit(dataset->GetADataTime(0) + GetDataStartTime());
  }

  /* set norm */
  Norm.resize(0);
  Norm.resize(Norms.at(d_i).size());
  Norm = Norms.at(d_i);

  return;
}

double Chi2Cost::GetCost()
{
  double cost(0);
  double c(0);

  Costs.clear();

  for (size_t i = 0; i < dataset_s->size(); i++) {

    SetDataSet(i);

    /* Note;
     * In the case that each dataset uses different solver, (BVP, CVODE or LSODE),
     * ( i.e. each DataSet's CosArg has different solvertype),
     * the solver has to be set every time time at the call of SetDataSet.
     * Also the timeseries is set to the solver, time series pointer also to be
     * set each time to each solver.
     * SetSolver(CostArg.solvertype)  : currently done in ModelCostFunction::SetSolver
     * solver->setTimeSeries(TSeries) : currently done in ModelCostFunction::SetSolver
     */
    if ( Solve()) {
      if (Model->getKeyParamName() != "NONE") {
        c = getcost();
      } else {
        c = getcost_nokey();
      }
    } else {
      return DBL_MAX;
    }

    c *= model_cost_weight;

    Costs.push_back(c);

    // Just debugging code
    // cout << "    X2 " << i << "th cost = " << c << endl ;
    cost += c;

  }
  return cost;

}

/*
 * Scales the time course result returned from numerical analysis
 * in the way described by the configuration. Where it is not None
 * then the timeseries results must be 'scaled' before they can be
 * compared against the data (which are assumed to have been adjusted
 * or measured in that way to begin with). -- allan
 */
void Chi2Cost::scale_result_vector(vector<double> *result,
                                   unsigned column_index)
{
  double scale_denominator = Scale.at(column_index);
  double base = Base.at(column_index);

  if (scale_denominator == 0){
    /*
     * If the scale_denominator is equal to zero then we certainly
     * cannot divide through all the results by zero. However for this
     * to be the case, it must be that all the results values sum to
     * zero. So either they are all zero, or some are negative and the
     * answer just happens to sum to zero. This is a highly unlikely
     * event, however we may as well check for it. -- allan
     */
    for (size_t jj = 0; jj < result->size(); jj++){
      if (result->at(jj) != 0){
        ERR.General("Chi2Cost", __func__,
          "The scaling of results for a column in the\n"
          "simulated time course data has produced a scale factor\n"
          "of zero, but all of the data points are not zero.\n"
          "Meaning that the sum of the data points is (by chance) zero.\n"
          "We cannot recover from this, either it was a very unfortunate\n"
          "selection of parameter values, in which case please try again\n"
          "Or, we were calculating the initial cost, in which case\n"
          "please change some parameter values in your model file\n"
          "and try again.\n");
      }
    }
    // We still need to subtract the base from the given value,
    // but in order to avoid NaNs we'll set the scale to one.
    // This feels, not quite correct, because the Base should somehow
    // be divided by something, but for now this seems to be the most
    // reasonable. -- allan
    scale_denominator = 1.0;
  }

  /*
   * So then assuming the scale denominator is not zero, we simply
   * perform the re-scaling of the computed timecourse. --allan
   */
  for (size_t jj = 0; jj < result->size(); jj++) {
    result->at(jj) = (result->at(jj) - base) / scale_denominator;
  }
  return ;
}

double Chi2Cost::getcost_nokey()
{


  UpdateScale(GetScaleType());

  double total_cost(0);
  double l_cost(0);
  int count(0);
  for (unsigned i = 0; i < dataset->GetNumDataNames(); i++) {

    vector<double> result = GetResultVec(dataset->GetADataName(i));

    /* scale the result vector prior to comparing it to the experimental
     * data, since the experimental data is assumed to be pre-scaled
     */
    scale_result_vector(&result, i);

    /* take the Weight */
    vector<double> w = Weight.at(i);

    double cal_y = result.front();
    double  data_y = dataset->GetADataValue(i, 0);

    l_cost = (cal_y - data_y) * (cal_y - data_y) / w.at(0);
    l_cost *= Norm.at(i) * Curve.at(i);

    total_cost += l_cost;

    if (isnan(l_cost)) {
      ERR.General("Chi2Cost", __func__, "Get NaN for cost value\n");
    }

  }

  total_cost /= (double)(dataset->GetNumDataNames());

  return total_cost;

}

double Chi2Cost::getcost()
{

  UpdateScale(GetScaleType());

  double total_cost(0);
  double l_cost(0);
  int count(0);

  if (TSeries->IsTimeSeriesEmpty()) {
    return DBL_MAX;
  }

  vector<double> time = GetResultTimeVec();

#ifdef DEBUG_CHI_COST
  cout << "T    Result  Data    Diff    Diff^2" << endl;
#endif

  // dataset->Name[0] is always Time
  for (unsigned i = 1; i < dataset->GetNumDataNames(); i++) {

    vector<double> result = GetResultVec(dataset->GetADataName(i));

    l_cost = 0.0;
    count = 0;
    /* scale the result vector prior to comparing it to the experimental
     * data, since the experimental data is assumed to be pre-scaled
     * Of course this will do nothing if the scale type is None.
     */
    scale_result_vector(&result, i);

    /* take the Weight */
    vector<double> w = Weight.at(i);

    size_t it(0);

    for (unsigned j = 0; j < dataset->GetNumADataValues(i); j++) {

      double  data_time = dataset->GetADataTime(j) + GetDataStartTime();

      /* A simple fix for the zero time data point, here we just
       * ignore it. This is probably not the correct thing to do
       * and in particular it won't help where one of the parameters
       * we are trying to fit is actually the initial population of
       * something. Although if you know the actual value of that why
       * are you trying to fit it? Anyway basically if the data time is
       * zero we just (for now) ignore this when computing the cost function.
       * We should definitely revisit this at some point. In particular the
       * time series computed by the solvers should start at 0.0 (if that
       * is the desired start time). 
       *
       * Note also this is not just checking if the data's time point is
       * equal to zero but if it is less than the first interval in such
       * a way that it wouldn't be recognised above as being at the first
       * time point eg if the data is
       * T E S
       * 0.05 10 10
       * and the time series interval is 0.1 then 0.05 won't be recognised
       * as being close enough to 0.1
       *
       * NOTE: also note that if you change this here, you should also 
       * change the computation of the normalisation in GetNorm, it is
       * commented there as well so this should be easy to spot. Basically
       * because we are ignoring the zero time point here, we will ignore
       * it in the normalisation, but obviously if we don't ignore it here
       * then we shouldn't when calculating the normalisation.
       */
      // if (data_time < (time.at(0) - Model->getOutInterval()*TOLERANCE)){
      if (data_time == 0.0){
        // Don't do anything, we don't wish to update the cost function.
        continue;
      }

      // This just shortcuts the loop below and says that if we are beyond
      // the last point of the time series then there will be no time
      // series reference point for here. It is kind of useless though
      // since in that case we error anyway so speed is not really of
      // the essence and in fact doing this check everytime is a pointless
      // waste of time in the common case.
      // Hence I'm taking this test out.
      //
      // if (data_time > Model->getOutInterval() +  time.back() ) {
      //  it = time.size();
      // }

      // This while loop should find the time point in the results
      // time series which most closely matches the data point time
      // in fact it doesn't quite do that but finds the first one which
      // is close enough.
      while (it < time.size()) {
        // how close the current data point time needs to be to the
        // results' reference time to be considered a match.
        double proximity_limit = Model->getOutInterval()*TOLERANCE ;
        if ( abs(data_time - time.at(it) )  < proximity_limit) {
          break;
        }
        it++;
      }
      

      if (it < time.size()) {

        double cal_y = result.at(it);
        double data_y = dataset->GetADataValue(i, j);
        double difference = cal_y - data_y;

        double this_cost = (difference * difference) / w.at(j);

#ifdef DEBUG_CHI_COST
        // cout << "T    Result  Data    Diff    Diff^2" << endl;
        cout << data_time << "\t"
             << cal_y << "\t"
             << data_y << "\t"
             << difference << "\t"
             << difference * difference << endl;
#endif
        l_cost += this_cost;

        count++;

        if (isnan(l_cost)) {
          ERR.General("Chi2Cost", __func__, "Get NaN for cost value\n");
        }
      } else {
        if (time.size() > 0){
          double last_time = time.at(time.size() - 1);
          ERR.General("Chi2Cost", __func__, 
                      "The reference time %f does not exist in "
                      "the result time series.\n"
                      "Last time point is %d\n"
                      , data_time, last_time);
        } else {
          ERR.General("Chi2Cost", __func__, 
                      "The reference time %f does not exist in "
                      "the result time series.\n"
                      "Result time series appears empty!"
                      , data_time);
        }
      }

    } // The end of the 'j' for-loop

    if (count == 0) {
      ERR.General("Chi2Cost", __func__, "Matched time data set is 0\n");
    }

#ifdef DEBUG_CHI_COST
    cout << "Norm.at(i - 1) = " << Norm.at(i - 1) << endl;
    cout << "Curve.at(i) = " << Curve.at(i) << endl;
    cout << "count = " << count << endl;
#endif

    /*
     * Allan:
     * This kind of line drives developers nuts. First of all why is it
     * Norm.at(i-1) rather than Norm.at(i) ? Presumably because Norm is
     * a zero indexed vector and we start at i = 1. Okay but then why are
     * all the others, and in particular Curve.at(i) not the same?
     * For that matter what the hell is 'Curve'? It seems that all of the
     * Curve values are 1, so I think that isn't affecting the result.
     */
    total_cost += l_cost * Norm.at(i - 1) * Curve.at(i) / (double)count;

  } // end of the 'i' for-loop

  return total_cost;
}


void Chi2Cost::UpdateScale(const ScaleType sc)
{

  if ( TSeries->IsTimeSeriesEmpty()) {
    return;
  }
  if ( sc == None ) {
    return;
  }


  for (size_t i = 1; i < dataset->GetNumDataNames(); i++) { // dataset->Name[0] is always Time

    vector<double>  result = GetResultVec(dataset->GetADataName(i));


    double dtime_0 = dataset->GetADataTime(0) + GetDataStartTime();
    double dtime_f = dataset->GetEndDataTime() + GetDataStartTime();

    vector<double>::iterator iter_re = result.begin();

    vector<double> time = TSeries->GetTimeSeriesTime();
    vector<double>::iterator iter_t = find_if(time.begin(), time.end(), bind2nd(greater_equal<double>(), dtime_0));

    if (iter_t != time.end()) {

      int jj = (int)(iter_t - time.begin());

      iter_re += jj;


      if (sc == MaxMin) {

        double max = *max_element(iter_re, result.end());
        double min = *min_element(iter_re, result.end());
        Base.at(i) = min;
        Scale.at(i) = ( (max - min) < DBL_EPSILON ) ? 1 : (max - min);

      } else {

        if (sc == AbsAverage || sc == Average) {

          int c_data(0);
          double sum_data(0);
          while (iter_re != result.end() && iter_t != time.end() && *(iter_t) < dtime_f) {
            if (sc == AbsAverage) {
              double tmp = *(iter_re);
              sum_data += abs(tmp);
            } else {
              if (sc == Average) {
                sum_data += *(iter_re);
              }
            }
            c_data++;
            iter_re ++;
            iter_t ++;
          }
          Scale.at(i) = sum_data / (double)c_data;

        } else {
          if (sc == Fix && GetNumFixScale() != 0) {
            Scale.at(i) = GetAFixScale(i);
          } else {
            ERR.General("ModelCostFunction", __func__, "Bad Scaletype.\n");
          }
        }
      }

    }


  }//for i



}

