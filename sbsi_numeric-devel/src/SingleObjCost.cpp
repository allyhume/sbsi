using namespace std;

#include <SingleObjCost.h>
#include <ModelCostFunction.h>

void SingleObjCost::Init()
{
  cfun->Init();
}

void SingleObjCost::SetParameters(vector<double> in_param)
{
  cfun->SetParameters(in_param);
}

vector<double>& SingleObjCost::GetParameters()
{
  return cfun->GetParameters();
}
vector<string>& SingleObjCost::GetParamNames()
{
  return cfun->GetParamNames();
}

void SingleObjCost::SetParamNames( vector<string> name)
{
  cfun->SetParamNames(name);
}

double SingleObjCost::GetCost()
{
  return cfun->GetCost();
}
