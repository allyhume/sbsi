using namespace std;

#include <DeCodeSBML.h>
#include <DecodeHelper.h>
#include <ERR.h>

// utility function
void printModelCPPFileIncludes (ofstream &fout, string model_name) ;

void DeCodeSBML::setConstraintIntoParamList(const Constraint* constraint)
{
  const ASTNode* math;
  const ASTNode* math_tmp;


  if ( constraint->isSetMath() ) {
    math = constraint->getMath();

    for (size_t n = 0; n < math->getNumChildren() ; ++n) {
      math_tmp    = math->getChild(n);

      char *formula = SBML_formulaToString(math_tmp->getChild(math_tmp->getNumChildren() - 1));

      if ( string((char*)math_tmp->getName() ) == "geq") {
        setParamLowerByName( math_tmp->getLeftChild()->getName(), atof(formula));
      }

      if ( string((char*)math_tmp->getName() ) == "leq") {
        setParamUpperByName( math_tmp->getLeftChild()->getName(), atof(formula) );
      }
      free(formula);
    }
  }
}

double DeCodeSBML::getParamUpperByName(string name, double pvalue)
{

  for (size_t i = 0; i < ParamNames.size(); i++) {
    if (ParamNames.at(i) == name) {
      return  pvalue < upper.at(i) ? upper.at(i) : pvalue;
    }
  }
  return pvalue;
}

double DeCodeSBML::getParamLowerByName(string name, double pvalue)
{
  for (size_t i = 0; i < ParamNames.size(); i++) {
    if (ParamNames.at(i) == name) {
      return pvalue  < lower.at(i) ? pvalue : lower.at(i);
    }
  }
  return pvalue;
}

void DeCodeSBML::setParamUpperByName(string name, double value)
{

  for (size_t i = 0; i < ParamNames.size(); i++) {
    if (ParamNames.at(i) == name) {
      upper.at(i) = value;
      return;
    }
  }
  ParamNames.push_back(name);
  upper.push_back(value);
  lower.resize(upper.size());
}

void DeCodeSBML::setParamLowerByName(string name, double value)
{
  for (size_t i = 0; i < ParamNames.size(); i++) {
    if (ParamNames.at(i) == name) {
      lower.at(i) = value;
      return;
    }
  }
  ParamNames.push_back(name);
  lower.push_back(value);
  upper.resize(lower.size());
}


void DeCodeSBML::printFunctionDefinition (size_t n, const FunctionDefinition* fd, ostream &stream)
{
  const ASTNode* math;
  char* formula;

  if ( fd->isSetMath() ) {

    stream << "\t \t /*A FunctionDefinition " << n << "*/" << endl;

    stream << "\t double " << fd->getId();

    math = fd->getMath();

    /* Print function arguments. */
    if (math->getNumChildren() > 1) {
      stream << "( double " <<  math->getLeftChild()->getName();

      for (size_t n = 1; n < math->getNumChildren() - 1; ++n) {
        stream << ", double " << ( math->getChild(n) )->getName();
      }
    }

    stream << ") " << endl;
    stream << "\t{ " << endl;;

    /* Print function body. */
    if (math->getNumChildren() != 0) {
      math    = math->getChild(math->getNumChildren() - 1);
      formula = SBML_formulaToString(math);
      stream << "\t return " << formula << "; \n \t}; " << endl;
      free(formula);
    }
  }
}


void
DeCodeSBML::printRuleMath (const Rule* r, ostream &stream)
{
  char* formula;

  if ( r->isSetMath() ) {

    formula = SBML_formulaToString( r->getMath() );
    string statename = r->getVariable();
    size_t ptr = cmodel->getStateIndexByName(statename);

    if (statename.length() > 0) {
      if (r->isRate()) {
        stream  << "\t // " << statename << endl;

        if (ptr < cmodel->getNumStates()) {
          stream << "\tyout[" << ptr << "]=" ;

          /* change all statename to y_init[n] */
          string s_formula = Transform_formula(formula);

          stream << s_formula << ";" << endl;

          stream << endl;

        } else {

          stream << "\t/* No state name  in the input species ; " << statename <<  "*/" << endl;
          string s_formula = Transform_formula(formula);
          stream << "\t " << statename <<  "=" << s_formula << ";" << endl;

        }
      } else if (r->isAssignment()) {
        /*
         * Allan: This didn't quite work, we would produce something like:
         * light = <math which evaluates RHS of assignment rule>
         * Which means that if any of the reaction rates involve 'light'
         * then this works fine. However if you wish to plot 'light'
         * it won't work since you're not updating the 'yout' array
         * member corresponding to 'light'.
         * Hence what we wish to output is something more like:
         * light = <...>;
         * yout[X] = <rate of change of light>
         * Where X is the index of the 'light' species.
         * How this plays when we have an assignment rule AND a
         * reaction which both update the same species, I'm not sure.
         */

        if (!r->getMath()->isPiecewise()) {
          stream << "\t" << statename << "=" << formula << ";" << endl;
        } else {
          DecodeHelper helper;
          stream << "\t" <<
                 helper.getPiecewiseString(r->getMath(), statename) << endl;
        }
        /* We are now plotting via a different mechanism, that of
         * actually modifying the timeseries after the actual calculation
         * has been done, so we don't do any of this now.
         *
        stream << "\t// Update the y_in which will give us *some*" << endl;
        stream << "\t// ability to plot the species with some " << endl;
        stream << "\t// amount of inaccuracy." << endl;
        stream << "\ty_in[" << ptr << "] = " << statename << " ;" << endl;
         */ 
      }
    } else if (r->isAlgebraic()) {
      stream << "\t" << formula << " = 0 ;" << endl;
    }

    free(formula);
  }
}


void DeCodeSBML::printReactionMath ( const Reaction* r, ostream &stream)
{
  char* formula;
  const KineticLaw* kl;


  const SpeciesReference *sp;

  if (!r->isSetKineticLaw())  {
    stream << "// No kinetic law defined for reaction :" << r->getId() << endl;
    return;
  }

  kl = r->getKineticLaw();
  if (! kl->isSetMath() ) {
    stream << "// No math defined for kinetic law in reaction :" << r->getId() << endl;
    return;
  }
  formula = SBML_formulaToString( kl->getMath() );

  /* change all statename to y_init[n] */
  string s_formula = Transform_formula(formula);

  stream << "\t{" << endl;
  for (size_t kn = 0; kn < kl->getNumParameters(); kn++) {
    const Parameter *p = kl->getParameter(kn);
    string pname = p->getId();
    pname += "_";
    pname += r->getId();
    stream << "\t double " <<  p->getId() << "=" << pname << ";" << endl;
  }


  stream << "\tdouble " << r->getId() << "=" << s_formula << ";" << endl;


  for (size_t i = 0; i < r->getNumReactants(); i++) {
    sp = r->getReactant(i);
    printStateUpdate(sp->getSpecies(), stream);
    /* Technically here we should warn if the stoichiometry isn't set.
     * More specifically we should warn only in the case that the model
     * is a level 3 model, level 2 models have a default stiochiometry
     * of one, but in level 3 a missing stoichiometry value means that
     * the stoichiometry is undefined
     */
    /*
     * This is a simple attempt to work for both, version 5.0.0 of
     * of libSBML and earlier versions.
     * The 'later 5.0.0 code is in comments below.
     */
    double stoich = sp->getStoichiometry();
    if (isnan(stoich)){
      stoich = 1;
    }
    /*
    double stoich = 1;
    if (sp->isSetStoichiometry()){
      stoich = sp->getStoichiometry();
    }
    */
    stream << "-=" << stoich << "*" << r->getId() << ";" << endl;
    }

  for (size_t i = 0; i < r->getNumProducts(); i++) {
    sp = r->getProduct(i);
    printStateUpdate(sp->getSpecies(), stream);
    // See the above comment, but basically the 'correct' code is below
    // but this version works also with versions of libSBML which are
    // earlier than 5.0.0
    double stoich = sp->getStoichiometry();
    if (isnan(stoich)){
      stoich = 1;
    }

    /*
    double stoich = 1;
    if (sp->isSetStoichiometry()){
      stoich = sp->getStoichiometry();
    }
    */
    stream << "+=" << stoich << "*" << r->getId() << ";" << endl;
  }

  stream << "\t}" << endl;

  stream << endl;

  free(formula);

}
void DeCodeSBML::printStateUpdate(string str, ostream &stream)
{
  stream << "\t // " << str << endl;
  stream << "\tyout["  ;
  stream <<  cmodel->getStateIndexByName(str);
  stream << "]" ;
}

void DeCodeSBML::printEventAssignmentMath ( const EventAssignment* ea, ostream &stream)
{
  std::string variable;
  char* formula;

  if ( ea->isSetMath() ) {
    variable = ea->getVariable();
    formula  = SBML_formulaToString( ea->getMath() );

    stream  <<   "\t\t  " << variable << " = " << formula << ";" << endl;

    cmodel->setVarByName(variable, 0);

    free(formula);
  }
}


/*
 * Our own version of SBML_formulaToString which actually attempts
 * to turn the formula into a true C expression. This doesn't currently
 * work very well since it only recognises relational and logical
 * operators at the top level, not those nested within, however
 * it at least operates better than before.
 */
string sbml_FormulaToC(const ASTNode_t *tree){
  switch(tree->getType()){
    case AST_LOGICAL_AND:
      return sbml_FormulaToC(tree->getLeftChild()) +
             " && " +
             sbml_FormulaToC(tree->getRightChild());
    case AST_RELATIONAL_EQ :
      return sbml_FormulaToC(tree->getLeftChild()) +
             " == " +
             sbml_FormulaToC(tree->getRightChild());
    case AST_RELATIONAL_GEQ :
      return sbml_FormulaToC(tree->getLeftChild()) +
             " >= " +
             sbml_FormulaToC(tree->getRightChild());
    case AST_RELATIONAL_GT :
      return sbml_FormulaToC(tree->getLeftChild()) +
             " > " +
             sbml_FormulaToC(tree->getRightChild());
 
    case AST_RELATIONAL_LEQ :
      return sbml_FormulaToC(tree->getLeftChild()) +
             " <= " +
             sbml_FormulaToC(tree->getRightChild());
 
    case AST_RELATIONAL_LT :
      return sbml_FormulaToC(tree->getLeftChild()) +
             " < " +
             sbml_FormulaToC(tree->getRightChild());
 
    case AST_RELATIONAL_NEQ :
      return sbml_FormulaToC(tree->getLeftChild()) +
             " != " +
             sbml_FormulaToC(tree->getRightChild());
 
    default:
      return SBML_formulaToString(tree);
  }
} 	


void DeCodeSBML::printEventMath ( const Event* e, ostream &stream)
{
  char* formula;
  size_t i;

  if ( e->isSetDelay() ) {
    formula = SBML_formulaToString( e->getDelay()->getMath() );
    free(formula);
  }

  if ( e->isSetTrigger() ) {
    stream << "\t  if ( " <<
           sbml_FormulaToC(e->getTrigger()->getMath())
           << " ) { " << endl;
  }
  for (i = 0; i < e->getNumEventAssignments(); ++i) {
    printEventAssignmentMath( e->getEventAssignment(i), stream);
  }
  // close conditional brace
  if (e->isSetTrigger()) {
    stream << " \t}" << endl;
  }

}

void DeCodeSBML::printMath (const Model* m, ostream &stream)
{
  size_t  n;

  string model_name = cmodel->cModel_id;

  /* set state by name */
  stream << "/*********** Model has " << m->getNumSpecies() << " states. *******/" << endl;
  stream << endl;
  stream << " void " << model_name << "::inputModel(){ " << endl;
  stream << endl;
  for (n = 0; n < m->getNumSpecies(); ++n) {
    stream << "\t/*  " <<  n << "th " ;
    const Species *sp = m->getSpecies(n);
    if (sp->isSetId()) {
      stream << "Species with Id " << sp->getId();
    }
    if (sp->isSetName()) {
      stream << "Species with Name " << sp->getName();
    }
    if (sp->isSetSpeciesType()) {
      stream << "  Species type " << sp->getSpeciesType() ;
    }
    if (sp->isSetCompartment()) {
      stream << "  in " << sp->getCompartment() ;
    }
    if (sp->isSetInitialAmount()) {
      stream << "  Initial value " << sp->getInitialAmount() ;
    }
    if (sp->isSetInitialConcentration()) {
      stream << "  Initial concentration " << sp->getInitialConcentration() ;
    }
    if (sp->getConstant()) {
      stream << "is constant";
    }
    stream << " */   " << endl;
    if (sp->isSetInitialAmount()) {
      stream << "\tsetStateByName(\"" << sp->getId() << "\"," << sp->getInitialAmount() << ");" << endl;
    } else if (sp->isSetInitialConcentration()) {
      stream << "\tsetStateByName(\"" << sp->getId() << "\"," << sp->getInitialConcentration() << ");" << endl;
    } else {
      // Okay a temporary fix here to see if this works, but basically
      // if we are otherwise not able to set the state by the initial
      // amount then it must be an initial assignment (can be check that?)
      stream << "\t// Setting the state to zero as it should be " << endl;
      stream << "\t// updated by an initial assinment" << endl;
      stream << "\tsetStateByName(\"" << sp->getId() << "\",0);" << endl;
    }
    stream << endl;

    /* Direct Input */
    /* cModel is virtual. Can set up state values in virtual model without model factory? */

    string statename = sp->getId();
    double statevalue = (double) sp->getInitialConcentration();
    cmodel->setStateByName(statename, statevalue);


  }
  stream << endl;
  for (n = 0; n < m->getNumRules(); ++n) {
    const Rule* r = m->getRule(n);
    if (r->isAssignment()) {
      stream << "\tsetVarByName(\"" << r->getVariable() << "\",0);" << endl;
      cmodel->setVarByName(r->getVariable(), 0);
    }
  }
  stream  << endl;

  for (n = 0; n < m->getNumParameters(); ++n) {

    const Parameter *p = m->getParameter(n);
    string pname = p->getId();
    double pvalue = p->getValue();
    /* Direct Input */
    cmodel->setParamByName(pname, pvalue);

  }

  // Parameters from Reaction if those are not in m->getParameters
  for (n = 0; n < m->getNumReactions(); n++) {

    const KineticLaw* kl;


    if (m->getReaction(n)->isSetKineticLaw()) {
      kl = m->getReaction(n)->getKineticLaw();
      for (size_t kn = 0; kn < kl->getNumParameters(); kn++) {
        const Parameter *p = kl->getParameter(kn);
        string pname = p->getId();
        pname += "_" ;
        pname += m->getReaction(n)->getId();
        double pvalue = p->getValue();
        cmodel->setParamByName(pname, pvalue);
      }
    }
  }

  for (unsigned c_i = 0; c_i < m->getNumCompartments(); c_i++) {
    const Compartment * comp = m->getCompartment(c_i);
    string pname = comp->getId();
    double pvalue = comp->getSize();
    cmodel->setParamByName(pname, pvalue);
  }

  for (size_t pi = 0; pi < cmodel->getNumParameters(); pi++) {
    if (cmodel->getParamNameByIndex(pi) == "default") {
      cmodel->setParamNameByIndex(pi, "defaultparameter");
    }
    stream << "\tsetParamByName(\"" << cmodel->getParamNameByIndex(pi) << "\"," << cmodel->getParamByName(cmodel->getParamNameByIndex(pi)) << ");" << endl;
  }


  stream << "	};" << endl;

  stream << endl;
  /*    Set parameters */


  for (n = 0; n < m->getNumConstraints() ; n++) {
    setConstraintIntoParamList( m->getConstraint(n));
  }

  ofstream pfile;
  ostringstream oss ;
  /*
  if(m->isSetId()) {
                   oss<< m->getId() <<  "/" << m->getId() ;
                   }else{
                  if(m->isSetName()) {
                  oss<< m->getName() <<  "/" << m->getName() ;
                                     }else oss<< model_name<< "/temp";
                  }
  */

  oss << "UserModel/InitParam.dat" ;

  ifstream ipfile(oss.str().c_str());
  if ((!ipfile.is_open()) && (m->getNumConstraints() != 0)) {

    pfile.open(oss.str().c_str());

    for (n = 0; n < m->getNumParameters(); ++n) {

      const Parameter *p = m->getParameter(n);
      string pname = p->getId();
      double pvalue = p->getValue();

      double uvalue(pvalue);
      double lvalue(pvalue);

      uvalue = getParamUpperByName(pname, pvalue);
      lvalue = getParamLowerByName(pname, pvalue);

      pfile  << p->getId() << "\t" << lvalue << "\t" << uvalue  << endl;
    }

    pfile.close();
  }


  /* function definition */

  stream << endl;
  if (m->getNumFunctionDefinitions() != 0) {
    stream << "/*********** Model has " << m->getNumFunctionDefinitions() << " functions. *******/" << endl;
  }

  for (n = 0; n < m->getNumFunctionDefinitions(); ++n) {
    printFunctionDefinition( n + 1, m->getFunctionDefinition(n), stream);
  }

  if ((m->getNumRules() != 0) || (m->getNumReactions() != 0)) {
    stream << endl;
    if (m->getNumRules() != 0) {
      stream << "/*********** Model has " << m->getNumRules() << " Rules. *******/" << endl;
    }
    if (m->getNumReactions() != 0) {
      stream << "/*********** Model has " << m->getNumReactions() << " Reactions. *******/" << endl;
    }
    stream << endl;

    stream << "// Set 'time' to 't' for COPASI models" << endl;
    stream << "// This is, to say the least, fragile" << endl;
    stream << "#define time t" << endl;

    stream << "void " << model_name << "::getRHS(double * y_in, double t, double* yout){ " << endl;
    stream << endl;

  }

  stream << endl;

  vector<string> statenames = cmodel->getStateNames();
  for (n = 0; n < statenames.size(); ++n) {
    stream << "\t//" << statenames.at(n) << endl;
    stream << "\t yout[" << n << "]=0.0;" << endl;
    // stream << "\t double " << statenames.at(n) << " = y_in[" << n << "];" <<endl;
    stream << "\t " << statenames.at(n) << " = y_in[" << n << "];" << endl;
  }
  stream << endl;

  for (n = 0; n < m->getNumEvents(); ++n) {
    printEventMath(m->getEvent(n), stream);
  }

  for (n = 0; n < m->getNumRules(); ++n) {
    printRuleMath( m->getRule(n), stream);
  }

  for (n = 0; n < m->getNumReactions(); ++n) {
    printReactionMath(m->getReaction(n), stream);
  }

  // stream << "\tcout << \"yout[2] = \" << yout[2] << \" : \" << t << endl;" << endl;
  stream << " }; " << endl;

 
 /* Print update_assignment_variables */
 stream << endl;
 stream << "/* The method update_assignment_variables is used by" << endl;
 stream << " * the solver to calculate the timeseries of" << endl;
 stream << " * variables/species assigned to by an assignment" << endl;
 stream << " * such species cannot be updated during solving" << endl;
 stream << " * since in that case we are updating an array of" << endl;
 stream << " * derivatives and the derivative of such variables" << endl;
 stream << " * may not be immediately apparent" << endl;
 stream << " */" << endl;

 stream << "void " << model_name
        << "::update_assignment_variables" 
        << "(vector<vector<double> > *results){ " 
        << endl;
 stream << endl;


 /* We first output other species which are likely used in the
  * assignment rule maths. We just go ahead and output all such
  * species since we hope that the C compiler will compile out any
  * needless assignments
  * Hmm, since they are not local variables maybe the C compiler
  * cannot do this, something to check up on?
  */
 stream << "  double t = results->at(0).back();" << endl;
 for (n = 0; n < statenames.size(); ++n) {
   stream << "  //" << statenames.at(n) << endl;
   stream << "  " << statenames.at(n) 
          << " = results->at(" << n + 1 << ").back();" 
          << endl;
 }
 stream << endl;

 /*
  * Having done that we now output, for each assignment rule
  * code to update the timeseries for the assigned to species.
  */
 for (n = 0; n < m->getNumRules(); ++n) {
   const Rule* r = m->getRule(n);
   string statename = r->getVariable();
   if (r->isAssignment()){
     size_t ptr = cmodel->getStateIndexByName(statename);
     /*
      * This is very fragile, however getStateIndexByName returns
      * the size of the state names if there is no such state.
      * In this case we can expect that it is a variable and not
      * a species name hence we do not wish to output anything.
      */
     if (ptr == statenames.size()){
       continue;
     }
     char* formula = SBML_formulaToString( r->getMath() );
     stream << "  "
            << statename << " = "
            << formula        
            << " ;" << endl;
           
     stream << "  results->at(" << ptr + 1 
            << ").back() = " << statename 
            << " ;" << endl;
   }
 }
 stream << "}" << endl; // The end of update_assignment_variables



  /* Print calVarVec */

  stream << "vector<double> " << model_name << "::calVarVec(int iv, vector<vector<double> > states){" << endl;
  stream << "\t  vector<double> f_out ;" << endl;
  stream << "\t" << endl;

  if (cmodel->getNumVars() != 0) {
    vector<string> varnames = cmodel->getVarNames();
    for (n = 0; n < varnames.size(); n++) {
      stream << "// " << varnames.at(n) << endl;
      stream << "\t if(iv==" << n << "){" << endl;
      stream << "\t }" << endl;
    }
  }

  stream << "\t  return f_out ;" << endl;


  stream << "};" << endl;

  /* Print calCoef */
  /* Assign parameter */
  stream << "/*" << endl;
  stream << " * calCoef is essentially similar to inputModel" << endl;
  stream << " * in that it initialises parameters for the model" << endl;
  stream << " * prior to solving the model, however the main" << endl;
  stream << " * difference is that some parameters (and hence values" << endl;
  stream << " * which depend on those parameters) may have been" << endl;
  stream << " * set as part of the parameter optimisation, that is" << endl;
  stream << " * they may not be the same value as they were set to in" << endl;
  stream << " * inputModel" << endl;
  stream << " */" << endl;

  stream << "void " << model_name << "::calCoef(){" << endl;
  if (cmodel->getNumParameters() != 0) {
    stream << "\t /* \t assign parameters from model_arg \t */" << endl;
    vector<string> paramnames = cmodel->getParamNames();
    vector<string>::iterator p_name = paramnames.begin();
    for (p_name = paramnames.begin(); p_name != paramnames.end(); ++p_name) {
      stream << "\t" << *(p_name) << " = "
             << "getParamByName(\"" << *(p_name) << "\");" <<  endl;
    }
  }
  /* Assign the initial values */
  stream << endl;
  stream << "\t/* Assigning from initial assignment expressions */" << endl;
  for (n = 0; n < m->getNumInitialAssignments(); ++n) {
    printInitialAssignmentIntocalCoef(m, m->getInitialAssignment(n), stream);
  }
  stream << "};" << endl;

  /* Print calc_Mu */
  /*
  stream << "double " << model_name << "::calc_Mu(int itr){" << endl;
  stream << "\t };" << endl;
  */
}

void DeCodeSBML::printInitialAssignmentIntocalCoef(const Model* m,
	const InitialAssignment* assign, ostream &stream)
{
  const ASTNode* math;
  const ASTNode* math_tmp;
  string name = assign->getSymbol();
  if (name.length() == 0){
    cout << "Initial assigment to name of zero length" << endl;
  }
  if ( assign->isSetMath() ) {
    // Note that getMath() returns the child of the <math> element
    // not the <math> element itself, which makes sense.
    math = assign->getMath();
    char* formula = SBML_formulaToString(math);
    stream  << "\t" << name << " = " << formula << ";" << endl;

    // Now that we have made the assignment we must use this to set
    // model state or parameter. Otherwise we would only have used that
    // which would have been set in inputModel which may well be simply 0.
    //
    // We need to know whether to use 'setStateByName' or 'setParamByName'
    // this is a rather kludgy way to figure this out, but it's non-obvious
    // how else to do this. I suppose when using setStateByName in the
    // printing of the 'inputModel' we could record those states which require
    // initialAssignments, that would mean we would also be able to warn/error
    // when there is no corresponding initial assignment. I think that should
    // go into the Java port of this.
    bool isStateName = false;
    for (int n = 0; n < m->getNumSpecies(); ++n) {
      const Species *sp = m->getSpecies(n);
      if (sp->isSetId() && sp->getId() == name){
        isStateName = true;
        break;
      }
      if (sp->isSetName() && sp->getName() == name) {
        isStateName = true;
        break;
      }
    }
    // Note: I slightly fear that it could also be a variable name which
    // should be set with setVarByName.
    if (isStateName){
      stream << "\tsetStateByName(\"" << name << "\", " << name << ");" << endl;
    } else {
      stream << "\tsetParamByName(\"" << name << "\", " << name << ");" << endl;
    }

    free(formula);
  } else {
    cout << name + " assign is not set to math" << endl;
  }
}


string getDirectoryName (const string& str)
{
  size_t found = 0;
  // cout << "Splitting: " << str << endl;
  found = str.find_last_of("/\\");
  // cout << " folder: " << str.substr(0,found) << endl;
  // cout << " file: " << str.substr(found+1) << endl;
  // cout << "found : = " << found << endl;
  if (found != str.npos){
    return str.substr(0, found + 1);
  }
  // If directory splitter wasn't found, return the empty string.
  return "";
}

void DeCodeSBML::DeCode (cModel *cm, const string fname)
{


  cmodel = cm;

  const char* filename   = fname.c_str();
  SBMLDocument* document = readSBML(filename);

  if (document->getNumErrors() > 0) {
    document->printErrors(cerr);
    ERR.General("DeCodeSBML", __func__, "Encountered the SBML errors: Stop.\n" );
  }

  Model* model = document->getModel();

  if (model == 0) {
    ERR.General("DeCodeSBML", __func__, "No model present.\n");
  }

  string model_name;

  if (model->isSetId()) {
    model_name = model->getId() ;
  } else {
    if (model->isSetName()) {
      model_name = model->getName();
    } else {
      model_name = "temp_Model" ;
    }
  }

  ofstream hout;
  ofstream fout;

  DIR *dir = NULL;

  if (model_name.size() != 0) {

    /*make the project directory */
    string model_dir = getDirectoryName(fname);
    string user_model_dir = model_dir + "UserModel";
    const char* user_model_dir_str = user_model_dir.c_str();
    dir = opendir(user_model_dir_str);

    if ( dir == NULL) {
      mkdir(user_model_dir_str,
            S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP | S_IROTH);
    }

    // Work out the filename path of the UserModel.C file
    ostringstream oss_fout ;
    oss_fout << user_model_dir << "/UserModel.C" ;
    // Do the same to work out the filename path of the UserModel.h file
    ostringstream oss_hout ;
    oss_hout << user_model_dir << "/UserModel.h" ;


    cmodel->cModel_id = model_name;

    fout.open(oss_fout.str().c_str());

    if (!fout.is_open()) {
      ERR.FileW("DeCodeSBML", __func__, oss_fout.str().c_str());
    }

    hout.open(oss_hout.str().c_str());

    if (!hout.is_open()) {
      ERR.FileW("DeCodeSBML", __func__, oss_hout.str().c_str());
    }



  } else {
    ERR.General("DeCodeSBML", __func__, " Model has no name.\n");
  }

  /* print header of model cpp file */
  printModelCPPFileIncludes(fout, model_name);


  printMath(model, fout);

  printHeader(model, hout);
  //closedir(dir);
  delete document;


}


void DeCodeSBML::setModel( cModel* cmodel)
{
  this->cmodel = cmodel;
}
void printModelCPPFileIncludes (ofstream &fout, string model_name)
{


  fout << "#include <cmath>" << endl;

  fout << "using namespace std;" << endl;
  fout <<  endl ;

  fout << "#include <cModel.h>" << endl;
  //fout << "#include \"" << model_name  << ".h\"" << endl;
  fout << "#include \"UserModel.h\"" << endl;
  // Uncomment the line below if you require the translated C program
  // to be able to print out, usually for debugging purposes
  // fout << "#include <iostream>" << endl;

  fout << "#define pi M_PI " <<  endl;
  fout <<  endl;

}

void
DeCodeSBML::printHeader(const Model* m, ostream &hstream)
{

  hstream << "using namespace std;" << endl;
  hstream <<  endl;

  string model_name = cmodel->cModel_id;

  hstream << "#ifndef __" << model_name << "_h__" << endl;
  hstream << "#define __" << model_name << "_h__" << endl;
  hstream <<  endl;

  hstream << "#include <cModel.h>" << endl;
  hstream <<  endl;

  /*
  hstream << "// A couple of helper definitions for dealing with phases"
          << endl;
  hstream << "#define timegap(t1, t2, r) ";
  hstream << "( ( (t > t1 && t < t2) ?  r : 0) )" << endl;
  hstream << "#define timegaplower(t1, r) ";
  hstream << "( ( (t < t1) ?  r : 0) )" << endl;
  hstream << "#define timegapupper(t1, r) ";
  hstream << "( ( (t > t1) ?  r : 0) )" << endl;
  */
  hstream << "// piecewise is a function defined in SBML" << endl;
  hstream << "#define piecewise(x, b, y) ( (b ? x : y))" << endl;
  hstream << "#define gt(x,y) ( (x > y))" << endl;
  hstream << "#define lt(x,y) ( (x < y))" << endl;
  hstream << "#define geq(x,y) ( (x >= y))" << endl;
  hstream << "#define leq(x,y) ( (x <= y))" << endl;




  hstream << "class  " << model_name << ":public cModel {" << endl;
  hstream << "public: " << endl;
  hstream <<  endl;
  hstream << "  " << model_name << "(){};" << endl;
  hstream <<  endl;



  if (m->getNumCompartments() != 0) {
    if (m->getNumCompartments() == 1) {
      hstream << "   /********** Model has a compartment *********/"  << endl;
    } else {
      hstream << "   /********** Model has " << m->getNumCompartments() << " compartments *********/"  << endl;
    }

    for (unsigned i = 0; i < m->getNumCompartments(); i++) {
      const Compartment * comp = m->getCompartment(i);
      hstream << "   /********** the compartment " << i <<  "         ********/" << endl;
      if ( comp->isSetName()) {
        hstream << "   /*                Name; " << comp->getName()  << " 	 		*/" << endl;
      }
      if ( comp->isSetId() ) {
        hstream << "   /*                  Id; " << comp->getId()    << "   	*/" << endl;
      }
      hstream << "   /*  Spatial dimensions; " << comp->getSpatialDimensions() <<  "			*/" << endl;
      hstream << "   /*                Size; " << comp->getSize()    << "			*/" << endl;
      if (comp->isSetUnits()) {
        hstream << "   /*           Size unit; " << comp->getUnits()   << "			*/" << endl;
      }
      hstream << "   /*            Constant; " << comp->getConstant() << "	 	 	*/" << endl;
    }
  }

  hstream << "   /**********************************************/" << endl;
  hstream << endl;

  hstream << "   /*********** Model has " << m->getNumSpecies() << " states. *******/" << endl;
  hstream << endl;
  hstream << "   void inputModel(void) ;" << endl;
  hstream << endl;

  if (m->getNumFunctionDefinitions() != 0) {
    hstream << "   /*********** Model has " << m->getNumFunctionDefinitions() << " functions. *******/" << endl;
  }

  hstream << endl;

  if (m->getNumRules() != 0) {
    hstream << "   /*********** Model has " << m->getNumRules() << " Rules. *******/" << endl;
  }
  if (m->getNumReactions() != 0) {
    hstream << "   /*********** Model has " << m->getNumReactions() << " Reactions. *******/" << endl;
  }

  hstream << endl;
  hstream << "   void getRHS(double * y_in, double t, double* yout); "
          << endl;
  hstream << "   void update_assignment_variables"
          << "(vector<vector<double> > * results); " 
          << endl;

  hstream << "  vector<double> calVarVec("
          << "int iv, vector<vector<double> > states); " 
          << endl;

  hstream << "  void calCoef(); " << endl;

  hstream << " /************ Species list  *****************/ " << endl;
  vector<string> statenames = cmodel->getStateNames();
  for (int n = 0; n < statenames.size(); ++n) {
    hstream << "//\t" << statenames.at(n) << endl;
    hstream << "\t double " << statenames.at(n) << ";" << endl;
  }
  hstream << endl;



  hstream << " /************ Parameter list ****************/  " << endl;
  if (cmodel->getNumParameters() != 0) {
    vector<string> paramnames = cmodel->getParamNames();
    vector<string>::iterator p_name = paramnames.begin();
    for (p_name = paramnames.begin(); p_name != paramnames.end(); ++p_name) {
      hstream << "\tdouble " << *(p_name)  << ";" << endl;
    }
  }


  hstream << "};" << endl;
  hstream << "static cModel* getModel(){return new " << model_name << "();}" << endl << endl;
  hstream << "#endif" << endl;

}



string DeCodeSBML::Transform_formula(const char* formula)
{

  string s_formula = string((char*)formula);
  return s_formula;
}



