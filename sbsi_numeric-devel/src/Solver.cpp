using namespace std;

#include <Solver.h>
#include <CVODESolver.h>
#include <KINETICSolver.h>
#include <BVPSolver.h>
#include <ERR.h>

Solver& Solver::SetSolver(const SolverType s)
{

  Solver *sol;
  switch (s) {
  case CVODE:
    sol = new CVODESolver();
    break;
  case KINETIC:
    sol = new KINETICSolver();
    break;
  case BVP:
    sol = new BVPSolver();
    break;
  default:
    ERR.General("Solver", __func__, "A bad solvertype\n");
  }

  sol->SolveCreate();
  return *sol;
}

int Solver::CheckFlag(void *flagvalue, const char *funcname, int opt)
{

  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if (opt == 0 && flagvalue == NULL) {
    fprintf(stderr,
            "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
            funcname);
    return(1);
  }

  /* Check if flag < 0 */
  else if (opt == 1) {
    errflag = (int *) flagvalue;
    if (*errflag < 0) {
#ifndef NO_FPRINTF_OUTPUT
      fprintf(stderr,
              "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
              funcname, *errflag);
#endif // NO_FPRINTF_OUTPUT
      return(1);
    }
  }

  /* Check if function returned NULL pointer - no memory allocated */
  else if (opt == 2 && flagvalue == NULL) {
    fprintf(stderr,
            "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
            funcname);
    return(1);
  }

  return(0);

}



