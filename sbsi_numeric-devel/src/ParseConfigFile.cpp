
#include <CommandLine.h>
#include <ERR.h>
#include <ParseConfigFile.h>
#include <CostArg.h>


static AmpFncType toAmplitudeFnc(const string &type)
{
  if (type == "None") {
    return AmpFncNone;
  }
  if (type == "Functional" || "ReciprocalSqrtAmp") {
    return AmpFncFunctional;
  }
  if (type == "Max" || "ReciprocalSqrtLogAmp") {
    return AmpFncMax;
  }
  if (type == "FunctionalNone"){
    return AmpFncFunctionalNone;
  }
  if (type == "DampNone"){
    return AmpFncDampNone;
  }
  if (type == "DampFunctional"){
    return AmpFncDampFunctional;
  }
  if (type == "DampFunctionalNone"){
    return AmpFncDampFunctionalNone;
  }
  if (type == "DampMax"){
    return AmpFncDampMax;
  }
  ERR.General("parserDoc", __func__,
              "Invalid Amplitude function choice: %s", type.c_str());
}
static int toStrategyType(const string &type)
{
  if (type == "None") {
    return 0;
  }
  if (type == "LinearSearch") {
    return 1;
  }
  ERR.General("parserDoc", __func__,
              "Invalid Strategy choice: %s", type.c_str());
}

static SolverType toSolverType(const string &solvertype)
{
  if (solvertype == "CVODESolver") {
    return CVODE;
  }
  if (solvertype == "BVPSolver") {
    return BVP;
  }
  if (solvertype == "KNTCSolver") {
    return KINETIC;
  }
  ERR.General("parserDoc", __func__, "Invalid solvertype choice: %s", solvertype.c_str());
  return InvalidSolver;
}

static ScaleType toScaleType(const string &scaletype)
{
  if (scaletype == "AbsAverage") {
    return AbsAverage;
  }
  if (scaletype == "Average") {
    return Average;
  }
  if (scaletype == "MaxMin") {
    return MaxMin;
  }
  if (scaletype == "Fix") {
    return Fix;
  }
  if (scaletype == "None") {
    return None;
  }
  ERR.General("parseDoc", __func__, "Invalid scaletype choice: %s", scaletype.c_str());
  return InvalidScale;
}

static CostValueType toCostValueType(const string &costvaluetype)
{
  if (costvaluetype == "Normed") {
    return Normed;
  }
  if (costvaluetype == "Direct") {
    return Direct;
  }
  ERR.General("parseDoc", __func__, "Invalid costvaluetype choice: %s", costvaluetype.c_str());
  return InvalidCostValue;
}

static OptimiserType toOptimiserType(const string &optimisertype)
{
  if (optimisertype == "PGA") {
    return PGA;
  }
  if (optimisertype == "PGAPSO") {
    return PGAPSO;
  }
  if (optimisertype == "Composite") {
    return Composite;
  }
  if (optimisertype == "DirectSearch") {
    return DirectSearch;
  }
  ERR.General("parseDoc", __func__,
              "Invalid optimiser type: %s", optimisertype.c_str());
  return InvalidOptimiser;
}

static SetupType toSetupType(const string &setuptype)
{
  if (setuptype == "SobolSelect") {
    return SobolSelect;
  }
  if (setuptype == "SobolUnselect") {
    return SobolUnselect;
  }
  if (setuptype == "ReadinFile") {
    return ReadinFile;
  }
  ERR.General("parseDoc", __func__, "Invalid setup type: %s", setuptype.c_str());
  return InvalidSetup;
}


static string childString(xmlDocPtr doc, xmlNodePtr node)
{
  xmlChar *s = xmlNodeListGetString(doc, node->xmlChildrenNode, 1);
  string ret = (char *)s;
  xmlFree(s);
  return ret;
}

static int childInteger(xmlDocPtr doc, xmlNodePtr node, const char *name)
{
  xmlChar *s = xmlNodeListGetString(doc, node->xmlChildrenNode, 1);
  istringstream ss((char *)s);
  xmlFree(s);
  int ret;
  ss >> ret;
  if (!ss || !ss.eof()) {
    ERR.General("XML", __func__, "%s must be an integer", name);
  }
  return ret;
}

static double childDouble(xmlDocPtr doc, xmlNodePtr node, const char *name)
{
  xmlChar *s = xmlNodeListGetString(doc, node->xmlChildrenNode, 1);
  istringstream ss((char *)s);
  xmlFree(s);
  double ret;
  ss >> ret;
  if (!ss || !ss.eof()) {
    ERR.General("XML", __func__, "%s must be a number", name);
  }
  return ret;
}

// A little wrapper so we don't have to worry about calling xmlFreeDoc
struct myXml {
  myXml(const string &fn):
    doc(xmlParseFile(fn.c_str())) {
    if (!doc) {
      ERR.FileR("XML", __func__, fn);
    }
  }
  ~myXml() {
    xmlFreeDoc(doc);
  }
  xmlDocPtr doc;
};

void ParseConfigFile::parseConfig(const string &xmlfilename,
                                  OptArg &optarg,
                                  CostArg &GlbCostArg,
                                  CostArgSet &setup_cost_args,
                                  CostArgSet &optimiser_cost_args,
                                  cModelArg &ModelArg)
{
  myXml xml(xmlfilename);
  xmlNodePtr root = xmlDocGetRootElement(xml.doc);
  if (!root) {
    ERR.General("XML", __func__, "XML document is empty");
  } else if (xmlStrcmp(root->name, (const xmlChar *) "OFWConfig")) {
    ERR.General("XML", __func__, "The document of the wrong type, root node != OFWConfig");
  }
  xmlNsPtr ns = root->ns;

  /* set default values */
  optarg.setCkpointStartNum(0);
  optarg.setPrintBestPop(true);
  optarg.MyMutation = NULL;
  optarg.MyCrossOver = NULL;
  optarg.MyEndOfGen = NULL;
  optarg.setVerbose(-1);

  optarg.setResultDir(results_dir);
  optarg.setCkpointDir(ckpoint_dir);


  //start traversing the tree
  for (xmlNodePtr node = root->xmlChildrenNode; node; node = node->next) {
    if (!(xmlStrcmp(node->name, (const xmlChar *) "Setup"))) {
      parseSetup(xml.doc, ns, node, optarg, GlbCostArg, setup_cost_args);
    } else if (!(xmlStrcmp(node->name, (const xmlChar *) "Optimiser"))) {
      parseOptimiser(xml.doc, ns, node, optarg, optimiser_cost_args);
    } else if (!(xmlStrcmp(node->name, (const xmlChar *) "Solver"))) {
      parseSolver(xml.doc, ns, node, ModelArg, GlbCostArg);
    } else if (!(xmlStrcmp(node->name, (const xmlChar *) "Results"))) {
      parseResults(xml.doc, ns, node);
    } else if (node->type == XML_ELEMENT_NODE) {
      ERR.General("XML", __func__, "Unknown node parsing top level elements:  [%s]", node->name);
    }
  }

}


void ParseConfigFile::parseSetup(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur,
                                 OptArg &optarg, CostArg &glb_cost_arg, CostArgSet &cost_args)
{
  for (xmlNodePtr node = cur->xmlChildrenNode; node; node = node->next) {
    if (!xmlStrcmp(node->name, (const xmlChar *) "SetupType")) {
      optarg.setSetupType(toSetupType(childString(doc, node)));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "CkpointFileName")) {
      paramreadinfile = childString(doc, node);
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "CkpointNum")) {
      optarg.setCkpointStartNum(childInteger(doc, node, "CkpointNum"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "MaxRandomNumberGenerator")) {
      glb_cost_arg.MAX_RandomNumGen = childInteger(doc, node, "MaxRandomNumberGenerator");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "MaxCost")) {
      glb_cost_arg.MAXCOST = childDouble(doc, node, "MaxCost");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "ParameterInitialFile")) {
      Init_Param_File = childString(doc, node);
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "ModelCostFunction")) {
      parseCostFunctions(doc, ns, node, cost_args);
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "UserCostFunction")) {
      parseUserCostFunctions(doc, ns, node, cost_args);
    } else if (node->type == XML_ELEMENT_NODE) {
      ERR.General("XML", __func__, "Unknown node parsing setup: [%s]", node->name);
    }
  }
}

void ParseConfigFile::parseOptimiser(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur,
                                     OptArg &optarg, CostArgSet &cost_args)
{
  for (xmlNodePtr node = cur->xmlChildrenNode; node; node = node->next) {
    if (!xmlStrcmp(node->name, (const xmlChar *) "OptimiserType")) {
      optarg.setOptimiserType ( toOptimiserType(childString(doc, node)));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "PopSize")) {
      optarg.setPopSize ( childInteger(doc, node, "PopSize"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "NumGen")) {
      optarg.setNumGen ( childInteger(doc, node, "NumGen"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "PSONumGen")) {
      int psoNumGen = childInteger(doc, node, "PSONumGen");
      optarg.setPSONumGen (psoNumGen);
      // Note that the genetic algorithm can sometimes get confused
      // if NumGen < PSONumGen, in particular it outputs incorrectly named
      // Stopinfo and BestPop files. This prevents this from happening
      // however this depends on PSONumGen following NumGen in the
      // configuration file, we could consider moving this below the
      // for-loop.
      if (optarg.getNumGen() < psoNumGen){
        optarg.setNumGen(psoNumGen);
      }
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "NumBest")) {
      optarg.setNumBest ( childInteger(doc, node, "NumBest"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "MaxNoChange")) {
      optarg.setMaxNoChange ( childInteger(doc, node, "MaxNoChange"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "MaxSimilarity")) {
      optarg.setMaxSimilarity ( childInteger(doc, node, "MaxSimilarity"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "RestartFreq")) {
      optarg.setRestartFreq ( childInteger(doc, node, "RestartFreq"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Seed")) {
      optarg.setSeed ( childInteger(doc, node, "Seed"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "SobolSeed")) {
      optarg.setSobolSeed ( childInteger(doc, node, "SobolSeed"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "PrintFreq")) {
      optarg.setPrintFreq ( childInteger(doc, node, "PrintFreq"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Verbose")) {
      optarg.setVerbose ( childInteger(doc, node, "Verbose"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Social")) {
      optarg.setSocial ( childDouble(doc, node, "Social"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Inertia")) {
      optarg.setInertia ( childDouble(doc, node, "Inertia"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Congnitive")) {
      optarg.setCongnitive ( childDouble(doc, node, "Congnitive"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "TargetCostFunctionValue")) {
      optarg.setMinCost ( childDouble(doc, node, "TargetCostFunctionValue"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "ResultDir")) {
      optarg.setResultDir ( childString(doc, node) + "/" );
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "CkpointDir")) {
      optarg.setCkpointDir ( childString(doc, node) + "/" );
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "CkpointFreq")) {
      optarg.setCkpointFreq ( childInteger(doc, node, "CkpointFreq"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "MutantProbability")) {
      optarg.setMutProb ( childDouble(doc, node, "MutantProbability"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Mu")) {
      optarg.setMu ( childDouble(doc, node, "Mu"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "ModelCostFunction")) {
      parseCostFunctions(doc, ns, node, cost_args);
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "UserCostFunction")) {
      parseUserCostFunctions(doc, ns, node, cost_args);
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "MaxNumGen")) {
      // this is not used in main, only in loop.sh
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "ParameterInitialFile")) {
      // already parsed in setup
    } else if (node->type == XML_ELEMENT_NODE) {
      ERR.General("XML", __func__, "Unknown node parsing optimiser: [%s]", node->name);
    }
  }
}

void ParseConfigFile::parseUserCostFunctions(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArgSet &cost_args)
{
  for (xmlNodePtr node = cur->xmlChildrenNode; node; node = node->next) {
    if ((!xmlStrcmp(node->name, (const xmlChar *) "Type"))) {
      string key = childString(doc, node);
      cost_args.UserCostFunctions.push_back(UserCostFunctionSet());
      cost_args.UserCostFunctions.back().type = key;
      cout << key << endl;
      parseUserCostFunctionFile(doc, ns, node, cost_args);
      cout << node->name << endl;
    }
  }
}

void ParseConfigFile::parseUserCostFunctionFile(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArgSet &cost_args)
{
  for (xmlNodePtr node = cur; node; node = node->next) {
    if (!(xmlStrcmp(node->name, (const xmlChar *) "DataSetConfig"))) {
      for (xmlNodePtr d_node = node->xmlChildrenNode; d_node; d_node = d_node->next) {
        if (!xmlStrcmp(d_node->name, (const xmlChar *) "FileName")) {
          cost_args.UserCostFunctions.back().datafilename.push_back(childString(doc, d_node));
        } else if (!xmlStrcmp(d_node->name, (const xmlChar *) "HeaderFileName")) {
          cost_args.UserCostFunctions.back().hdrfilename.push_back(childString(doc, d_node));
        } else if (d_node->type == XML_ELEMENT_NODE) {
          ERR.General("XML", __func__, "Unknown node parsing UserCostFunction [%s]", d_node->name);
        }
      }
    } else if (node->type == XML_ELEMENT_NODE && (xmlStrcmp(node->name, (const xmlChar *) "Type"))) {
      ERR.General("XML", __func__, "Unknown node %s", node->name);
    }
  }
}


void ParseConfigFile::parseCostFunctions(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArgSet &cost_args)
{
  for (xmlNodePtr node = cur->xmlChildrenNode; node; node = node->next) {
    if (!xmlStrcmp(node->name, (const xmlChar *) "Type")) {
      string key = childString(doc, node);
      if (key == "FFT") {
        parseFFT(doc, ns, node, cost_args);
      } else if (key == "X2Cost") {
        parseX2Cost(doc, ns, node, cost_args);
      } else if (node->type == XML_ELEMENT_NODE) {
        ERR.General("XML", __func__, "Unknown CostFunction Type %s", key.c_str());
      }
      // case FFT
      //      read dataset file name hdr
      //      set dataset fft.target
      //      set dataset T and t0
      // case X2Cost
      //      read dataset file name hdr
      //      set dataset starttime
      //      set dataset scaletype and valuetype
    }
  }
}

void ParseConfigFile::parseFFT(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArgSet &cost_args)
{
  // CT: How can the input be validated? Pushing a bunch of stuff to vectors
  // without requiring them to be the same size doesn't seem quite right.

  // Allan: I completely agree, this code doesn't do too much validating.

  // The weight is optional but cost_args.X2C_weights must be the
  // correct length at the end so we must push the default weight
  // and then, if one is actually set we override it.
 
  cost_args.FFT_weights.push_back(1.0);
  for (xmlNodePtr node = cur; node; node = node->next) {
    if (!xmlStrcmp(node->name, (const xmlChar *) "DataSetConfig")) {
      cost_args.FFT_cost_arg.push_back(CostArg());
      for (xmlNodePtr d_node = node->xmlChildrenNode; d_node; d_node = d_node->next) {
        if (!xmlStrcmp(d_node->name, (const xmlChar *) "FileName")) {
          cost_args.FFT_hdrfilename.push_back(childString(doc, d_node));
        } else if (!xmlStrcmp(d_node->name, (const xmlChar *) "Interval")) {
          cost_args.FFT_cost_arg.back().OutputInterval = childDouble(doc, d_node, "Interval");
        } else if (!xmlStrcmp(d_node->name, (const xmlChar *) "State")) {
          parseState(doc, ns, d_node, cost_args.FFT_cost_arg.back());
        } else if (d_node->type == XML_ELEMENT_NODE) {
          ERR.General("XML", __func__, "Unknown node parsing FFT [%s]", d_node->name);
        }
      }
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Weight")){
      cost_args.FFT_weights.back() = childDouble(doc, node, "Weight");
    } else if (node->type == XML_ELEMENT_NODE && (xmlStrcmp(node->name, (const xmlChar *) "Type"))) {
      ERR.General("XML", __func__, "Unknown node %s", node->name);
    }
  } // End of the for loop
}

void ParseConfigFile::parseState(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArg &FFT_cost_arg)
{

  for (xmlNodePtr node = cur->xmlChildrenNode; node; node = node->next) {
    if (!xmlStrcmp(node->name, (const xmlChar *) "TargetName")) {
      /*set its name to fft_target */
      FFT_cost_arg.FFTTarget.push_back(childString(doc, node));
      /* set default for MinAmp and MaxAmp*/
      FFT_cost_arg.MinAmplitude.push_back(1.0e-08);
      FFT_cost_arg.MaxAmplitude.push_back(1.0e+08);
      FFT_cost_arg.AmpFnc.push_back(AmpFncNone);
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "TargetPeriod")) {
      FFT_cost_arg.Period.push_back(childDouble(doc, node, "TargetPeriod"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "StartSampling")) {
      FFT_cost_arg.TStart.push_back(childDouble(doc, node, "StartSampling"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "EndSampling")) {
      FFT_cost_arg.TEnd.push_back(childDouble(doc, node, "EndSampling"));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "MinAmplitude")) {
      FFT_cost_arg.MinAmplitude.back() = childDouble(doc, node, "MinAmplitude");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "MaxAmplitude")) {
      FFT_cost_arg.MaxAmplitude.back() = childDouble(doc, node, "MaxAmplitude");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "FncAmplitude")) {
      FFT_cost_arg.AmpFnc.back() = toAmplitudeFnc(childString(doc, node));
    } else if (node->type == XML_ELEMENT_NODE) {
      ERR.General("XML", __func__, "Unknown node parsing FFT State [%s]", node->name);
    }
  }
  // Should check that all 3 tags were seen exactly once
}

void ParseConfigFile::parseX2Cost(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur, CostArgSet &cost_args)
{
  // The weight is optional but cost_args.X2C_weights must be the
  // correct length at the end so we must push the default weight
  // and then, if one is actually set we override it.
  // This then works, but silently chooses the last weight set, if
  // there are multiple weights set.
 
  cost_args.X2C_weights.push_back(1.0);
  for (xmlNodePtr node = cur; node; node = node->next) {
    if ((!xmlStrcmp(node->name, (const xmlChar *) "DataSetConfig")) && (node->ns == ns)) {

      cost_args.X2C_cost_arg.push_back(CostArg());
      for (xmlNodePtr d_node = node->xmlChildrenNode; d_node; d_node = d_node->next) {
        /* I'm slightly bemused, but essentially this would cause us
         * to fail if there was ever a text node which was a child
         * of the DataSetConfig. The d_node->ns->href would seg fault.
         * Seems strange that this bug was never exercised before -- allan
         */
        if (d_node->ns != ns && d_node->type == XML_ELEMENT_NODE) {
          ERR.General("XML", __func__, 
                      "Namespace mismatch %s", d_node->ns->href);
        } else if (d_node->type == XML_TEXT_NODE) {
        } else if (!xmlStrcmp(d_node->name, (const xmlChar *) "FileName")) {
          cost_args.X2C_filename.push_back(childString(doc, d_node));
        } else if (!xmlStrcmp(d_node->name, (const xmlChar *) "DataFileStartTime")) {
          cost_args.X2C_cost_arg.back().ExpT0 = childDouble(doc, d_node, "DataFileStartTime");
        } else if (!xmlStrcmp(d_node->name, (const xmlChar *) "Interval")) {
          cost_args.X2C_cost_arg.back().OutputInterval = childDouble(doc, d_node, "Interval");
        } else if (!xmlStrcmp(d_node->name, (const xmlChar *) "ScaleType")) {
          cost_args.X2C_cost_arg.back().scaletype = toScaleType(childString(doc, d_node));
        } else if (!xmlStrcmp(d_node->name, (const xmlChar *) "ValueType")) {
          cost_args.X2C_cost_arg.back().costvaluetype = toCostValueType(childString(doc, d_node));
        } else if (d_node->type == XML_ELEMENT_NODE) {
          ERR.General("XML", __func__, "Unknown node parsing X2Cost [%s]", d_node->name);
        }
      }
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Weight")){
      cost_args.X2C_weights.back() = childDouble(doc, node, "Weight");
    } else if (node->type == XML_ELEMENT_NODE && (xmlStrcmp(node->name, (const xmlChar *) "Type"))) {
      ERR.General("XML", __func__, "Unknown node %s", node->name);
    }
  } // End of for (xmlNodePtr node ..
}


void ParseConfigFile::parseSolver(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur,
                                  cModelArg &ModelArg, CostArg &glb_cost_arg)
{
  for (xmlNodePtr node = cur->xmlChildrenNode; node; node = node->next) {
    if (!xmlStrcmp(node->name, (const xmlChar *) "SolverType")) {
      glb_cost_arg.solvertype = toSolverType(childString(doc, node));
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "TFinal")) {
      ModelArg.Final = childDouble(doc, node, "TFinal");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "TInit")) {
      ModelArg.Init = childDouble(doc, node, "TInit");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "KeyParamName")) {
      ModelArg.KeyParam = childString(doc, node);
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Final")) {
      ModelArg.Final = childDouble(doc, node, "KeyParamFinal");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Init")) {
      ModelArg.Init = childDouble(doc, node, "KeyParamInit");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Interval")) {
      ModelArg.Interval = childDouble(doc, node, "Interval");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "ReportInterval")){
      ModelArg.ReportInterval = childDouble(doc, node, "ReportInterval");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "MaxTimes")) {
      ModelArg.MaxTimes = childInteger(doc, node, "MaxTimes");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Atol")) {
      ModelArg.Atol = childDouble(doc, node, "Atol");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Reltol")) {
      ModelArg.Reltol = childDouble(doc, node, "Reltol");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Functol")) {
      ModelArg.Fnormtol = childDouble(doc, node, "Functol");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Steptol")) {
      ModelArg.Steptol = childDouble(doc, node, "Functol");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Maxset")) {
      ModelArg.Mset = childInteger(doc, node, "Maxset");
    } else if (!xmlStrcmp(node->name, (const xmlChar *) "Strategy")) {
      ModelArg.Str = toStrategyType(childString(doc, node));
    } else if (node->type == XML_ELEMENT_NODE) {
      ERR.General("XML", __func__, "Unknown node %s", node->name);
    }
  }
}

void ParseConfigFile::parseResults(xmlDocPtr doc, xmlNsPtr ns, xmlNodePtr cur)
{
  for (xmlNodePtr node = cur->xmlChildrenNode; node; node = node->next) {
    if (!xmlStrcmp(node->name, (const xmlChar *) "TimeSeries")) {
      for (xmlNodePtr t_node = node->xmlChildrenNode; t_node; t_node = t_node->next) {
        if (!xmlStrcmp(t_node->name, (const xmlChar *) "TimeSeriesResultDir")) {
          timeseries_dir = childString(doc, t_node) + "/" ;
        } else if (!xmlStrcmp(t_node->name, (const xmlChar *) "TimeSeriesFileName")) {
          timeseries_fname = childString(doc, t_node);
        } else  if (t_node->type == XML_ELEMENT_NODE) {
          ERR.General("XML", __func__, "Unknown node %s", t_node->name);
        }
      }
    } else if (node->type == XML_ELEMENT_NODE) {
      ERR.General("XML", __func__, "Unknown node %s", node->name);
    }
  }
}

void ParseConfigFile::loadData(CostArgSet &args)
{
  FileReaderType expfilereadtype = TabDelimitedColumnData;
  FileReader &exp_file_reader = FileReader::SetFileReader(expfilereadtype);
  TDCHeaderFileReader hdr;

 for (int index = 0; index < args.X2C_filename.size(); index++){
    string filename = args.X2C_filename[index];
    exp_file_reader.ClearSets();
    exp_file_reader.Read(exp_dir + filename);
    args.X2C_dataset.push_back(exp_file_reader.Sets.front());
  }

  size_t n = args.FFT_hdrfilename.size();
  args.FFT_dataset.resize(n);
  for (size_t i = 0; i < n; ++i) {
    string setname = args.FFT_hdrfilename.at(i);
    args.FFT_dataset[i].SetSetName(setname);
    // allan: Hmm, I'm not sure I understand, hdr appears to be used
    // uninitialised?
    hdr.SetDataSet(&(args.FFT_dataset.at(i)));
    hdr.Read(exp_dir + args.FFT_hdrfilename.at(i));
  }


}

