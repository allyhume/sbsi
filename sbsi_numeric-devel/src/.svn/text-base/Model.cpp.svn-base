/*****************************************************************************
 *
 *  cModel.cpp
 *
 *   $Id: Model.cpp,v 1.1.2.3 2011/02/28 11:36:47 ayamaguc Exp $
 *
 *****************************************************************************/

using namespace std;

#include <cModel.h>
#include <cModelArg.h>
#include <ERR.h>


void cModel::initModelArg()
{

  setFinal(0.0);
  setInit(0.0);
  setKeyParamName("T"); //default time"
  setMaxTimes(0);
  setInterval(0.0);
  setOutInterval(0.0);
  setReportInterval(0.0);
  setExpTInit(0.0);


  setAtol(1.0e-14);
  setReltol(1.0e-04);

  setMset(1);
  setFnormtol(1.0e-06);
  setSteptol(1.0e-08);
  setStr(0);

  clearParams();
  clearStates();
  clearVars();

};

// Param manipulation

void cModel::clearParams()
{
  ModelArg.ParamNames.clear();
  ModelArg.Params.clear();
}

size_t cModel::getNumParameters(void)
{
  return ModelArg.ParamNames.size();
}

vector<string>& cModel::getParamNames(void)
{
  return ModelArg.ParamNames;
};

void cModel::setParameters(vector<double>& values)
{
  ModelArg.Params = values;
};

vector<double>& cModel::getParameters(void)
{
  return ModelArg.Params;
}

void cModel::setParamByName(string name, double value)
{
  for (size_t i = 0; i < ModelArg.ParamNames.size(); i++) {
    if ( ModelArg.ParamNames.at(i) == name ) {
      ModelArg.Params.at(i) = value;
      return;
    }
  }
  ModelArg.ParamNames.push_back(name);
  ModelArg.Params.push_back(value);
}

double cModel::getParamByName(string name)
{
  for (size_t i = 0; i < ModelArg.ParamNames.size(); i++) {
    if ( ModelArg.ParamNames.at(i) == name ) {
      return ModelArg.Params.at(i);
    }
  }
  ERR.General("Model", __func__, " the Parameter name %s;  Not found.\n ", name.c_str());
  return 0;
}

size_t cModel::getParamIndexByName(string name)
{
  for (size_t i = 0; i < ModelArg.ParamNames.size(); i++) {
    if ( ModelArg.ParamNames.at(i) == name ) {
      return i;
    }
  }
  return ModelArg.ParamNames.size();
}

string cModel::getParamNameByIndex(size_t i)
{
  if (i < ModelArg.ParamNames.size()) {
    return ModelArg.ParamNames.at(i);
  }
  ERR.General("Model", __func__, " The index %d is out of Parameter size, %d.", i, ModelArg.ParamNames.size());
  return "missing";
}
void cModel::setParamNameByIndex(size_t i, string name)
{
  if (i < ModelArg.ParamNames.size()) {
    ModelArg.ParamNames.at(i) = name;
  } else {
    ERR.General("Model", __func__, " The index is out of size of Parameters");
  }
}
// State manipulation

size_t cModel::getNumStates(void)
{
  return ModelArg.StateNames.size();
}

void cModel::clearStates()
{
  ModelArg.StateNames.clear();
  ModelArg.States.clear();
}

vector<string>& cModel::getStateNames(void)
{
  return ModelArg.StateNames;
};

void cModel::setStates(vector<double>& values)
{
  ModelArg.States = values;
};

vector<double>& cModel::getStates(void)
{
  return ModelArg.States;
}

void cModel::setStateByName(string name, double value)
{
  for (size_t i = 0; i < ModelArg.StateNames.size(); i++) {
    if ( ModelArg.StateNames.at(i) == name ) {
      ModelArg.States.at(i) = value;
      return;
    }
  }
  ModelArg.StateNames.push_back(name);
  ModelArg.States.push_back(value);
}

double cModel::getStateByName(string name)
{

  for (size_t i = 0; i < ModelArg.StateNames.size(); i++) {
    if ( ModelArg.StateNames.at(i) == name ) {
      return ModelArg.States.at(i);
    }
  }
  ERR.General("Model", __func__, " the state name %s;  Not found.\n ", name.c_str());
  return 0;

}


size_t cModel::getStateIndexByName(string name)
{

  for (size_t i = 0; i < ModelArg.StateNames.size(); i++) {
    if ( ModelArg.StateNames.at(i) == name ) {
      return i;
    }
  }
  //ERR.General("Model",__func__," the state name %s;  Not found.\n ",name.c_str());
  return ModelArg.StateNames.size();

}

string cModel::getStateNameByIndex(size_t i)
{
  if (i < ModelArg.States.size()) {
    return ModelArg.StateNames.at(i);
  }
  ERR.General("Model", __func__, " The index %d is out of state size, %d.", i, ModelArg.StateNames.size());
  return "missing";
}
// Var manipulation

vector<string>& cModel::getVarNames(void)
{
  return ModelArg.VarNames;
};

size_t cModel::getNumVars(void)
{
  return ModelArg.VarNames.size();
}

void cModel::clearVars()
{
  ModelArg.VarNames.clear();
  ModelArg.Vars.clear();
}
//void cModel::setVars(double *values) {
void cModel::setVars(vector<double>& values)
{
  ModelArg.Vars = values;
};

vector<double>& cModel::getVars(void)
{
  return ModelArg.Vars;
}

void cModel::setVarByName(string name, double value)
{
  for (size_t i = 0; i < ModelArg.VarNames.size(); i++) {
    if ( ModelArg.VarNames.at(i) == name ) {
      ModelArg.Vars.at(i) = value;
      return;
    }
  }
  ModelArg.VarNames.push_back(name);
  ModelArg.Vars.push_back(value);
  return;
}

double cModel::getVarByName(string name)
{
  for (size_t i = 0; i < ModelArg.VarNames.size(); i++) {
    if ( ModelArg.VarNames.at(i) == name ) {
      return ModelArg.Vars.at(i);
    }
  }
  ERR.General("Model", __func__, " the variable name %s;  Not found.\n ", name.c_str());

}

size_t cModel::getVarIndexByName(string name)
{
  for (size_t i = 0; i < ModelArg.VarNames.size(); i++) {
    if ( ModelArg.VarNames.at(i) == name ) {
      return i;
    }
  }
  return ModelArg.VarNames.size();

}

/* TIME */

void cModel::setExpTInit(double t0)
{
  ModelArg.TExpInit = t0;
}

void cModel::setReportInterval(double t0)
{
  ModelArg.ReportInterval = t0;
}

void cModel::setOutInterval(double t0)
{
  ModelArg.OutputInterval = t0;
}

void cModel::setInterval(double t0)
{
  ModelArg.Interval = t0;
}

void cModel::setFinal(double tf)
{
  ModelArg.Final = tf;
}
void cModel::setInit(double ti)
{
  ModelArg.Init = ti;
}
void cModel::setKeyParamName(string name)
{
  ModelArg.KeyParam = name;
}
string cModel::getKeyParamName()
{
  return ModelArg.KeyParam;
}

double cModel::getReportInterval()
{
  return ModelArg.ReportInterval;
}

double cModel::getOutInterval()
{
  return ModelArg.OutputInterval;
}

double cModel::getInterval()
{
  return ModelArg.Interval;
}

double cModel::getFinal()
{
  return ModelArg.Final;
}
double cModel::getInit()
{
  return ModelArg.Init;
}

double cModel::getExpTInit()
{
  return ModelArg.TExpInit;
}

/*Other */

void cModel::setAtol(double Atol)
{
  ModelArg.Atol = Atol;
}

void cModel::setStr(int str)
{
  ModelArg.Str = str;
}

void cModel::setMset(int mset)
{
  ModelArg.Mset = mset;
}

void cModel::setFnormtol(double fnormtol)
{
  ModelArg.Fnormtol = fnormtol;
}

void cModel::setSteptol(double steptol)
{
  ModelArg.Steptol = steptol;
}

void cModel::setReltol(double Reltol)
{
  ModelArg.Reltol = Reltol;
}

double cModel::getAtol()
{
  return ModelArg.Atol;
}

double  cModel::getReltol()
{
  return ModelArg.Reltol;
}

void cModel::setMaxTimes(int max_times)
{
  ModelArg.MaxTimes = max_times;
}

int cModel::getMaxTimes()
{
  return ModelArg.MaxTimes;
}

double cModel::getFnormtol()
{
  return ModelArg.Fnormtol;
}

double cModel::getSteptol()
{
  return ModelArg.Steptol;
}

int cModel::getStr()
{
  return ModelArg.Str;
}

int cModel::getMset()
{
  return ModelArg.Mset;
}



