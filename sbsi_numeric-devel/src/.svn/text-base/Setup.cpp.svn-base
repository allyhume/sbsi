/*
 * Setup.cpp
 *
 *  Created on: Jul 14, 2008
 *      Author: ayamaguc
 */


#include <cfloat>
#include <algorithm>
#include <mpi.h>

#include <Sobol64Bit.h>
#include <Setup.h>
#include <ERR.h>

using namespace std;

#define max_size 1024
class Send_Buf
{
public:
  double cost;
  double params[max_size];
  // char cfilename[max_size];
};

vector<Send_Buf> Allgather(Send_Buf *l_paramset, int local_popSize)
{

  /* setting for MPI */
  int my_rank(0);
  int p_num(0);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p_num);

  /* set the sender buffer: MPI needs static buffer */
  Send_Buf local_paramset[local_popSize];
  for (int i = 0; i < local_popSize; i++) {
    local_paramset[i] = l_paramset[i];
  }

  /* build MPI datatype structure */
  int local_paramset_counts[2];
  vector<Send_Buf> paramset_buf(local_popSize * p_num);

  MPI_Datatype local_paramset_type, local_paramset_types[2];
  MPI_Aint start_address;
  MPI_Aint local_paramset_displacements[2];
  MPI_Aint address;

  local_paramset_counts[0] = 1;
  local_paramset_counts[1] = max_size;
  //local_paramset_counts[2]=max_size;
  local_paramset_types[0] = MPI_DOUBLE;
  local_paramset_types[1] = MPI_DOUBLE;
  //local_paramset_types[2]=MPI_CHAR;

  local_paramset_displacements[0] = 0;
  MPI_Address(&local_paramset, &start_address);
  MPI_Address( &(local_paramset[0].params[0]), &address);
  local_paramset_displacements[1] = address - start_address;
  //MPI_Address( &(local_paramset[0].cfilename),&address);
  //local_paramset_displacements[2]=address-start_address;

  MPI_Type_struct(2, local_paramset_counts, local_paramset_displacements, local_paramset_types, &local_paramset_type);
  MPI_Type_commit(&local_paramset_type);

  /* correct local parameter and then send all pes */
  MPI_Allgather( &(local_paramset), local_popSize, local_paramset_type, (void *)&(*paramset_buf.begin()), local_popSize, local_paramset_type, MPI_COMM_WORLD);

  return paramset_buf;
}

bool operator<(const ParamSet&x, const ParamSet&y)
{
  return x.cost < y.cost;
}

vector<ParamSet> sortParamSet(const vector<ParamSet> set)
{

  vector<ParamSet> sortset;
  sortset = set;
  sort(sortset.begin(), sortset.end());
  return sortset;

}

Setup &Setup::SetupParam(SetupType which)
{

  Setup* sp;
  switch (which) {

  case SobolUnselect:
    sp = new Sobol();
    break;

  case SobolSelect:
    sp = new Sobol();
    break;

  case ReadinFile:
    sp = new Read_File();
    break;

  case InvalidSetup:
    ERR.General("Setup", __func__, "Invaid setup type\n");

  default:
    sp = new Sobol();
    break;
  }
  return *sp;
}


/* Setup function with the readin file */
/* This setup function is for the case which has already the start parameter set */
void Read_File::run()
{


  /* MPI setting */
  int my_rank(0);
  int p_num(0);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p_num);

  /* Set file reader and read the file*/

  FileReader &fr = FileReader::SetFileReader(TabDelimitedColumnPop);
  fr.Read(InFileName);

  /* Check the size of the population from the file*/


  if (fr.PopSets.size() == 0) {
    ERR.General("Setup", "Read from File", "No readin parameters in this file %s\n", InFileName.c_str());
  }

  if ((int)fr.PopSets.size() != optarg->getPopSize()) {
    ERR.General("Setup", "Read from File", "The number of pop in the file( %s)  %d != The input popSize %d\n", InFileName.c_str(), fr.PopSets.size(), optarg->getPopSize());
  }

  if (my_rank == 0) {
    WriteStart();
  }

  /* Copy to paramset */

  string filename;


  for (unsigned int i = 0; i < fr.PopSets.size(); i++) {

    /* calculate the cost value */

    costfunction->SetParameters(fr.PopSets.at(i).params);
    double Cost = costfunction->GetCost();

    //ostringstream fname;
    //fname << optarg->getResultDir() << "/popserup" << i<< ".txt" ;
    //filename=fname.str();
    paramset.push_back(ParamSet(fr.PopSets.at(i).params, Cost));
  }

  /* sort the parameter set */
  sort(paramset.begin(), paramset.end());


  delete &fr;
  if (my_rank == 0) {
    cout << "======================= Setup Finish  ===================" << endl;
  }
}

/* Setup function using Sobol */
/* It create the first population to be passed to the optimiser, GA or SA */
/* This DOES count the cost value.  */
/* The parameter set with the cost value larger than costfunction->MaxCost is to be disposed.*/
/* The trial parameter set is created until the sobol number sequence called over  the
cost_arg->MAX_RandomNumGen times. */
void Sobol::run()
{
  SetupType s_type = optarg->getSetupType();

  if (s_type == SobolSelect) {
    setMaxCost(costfunction->GetMAXCost());
  }


  /* MPI setting */
  int my_rank(0);
  int p_num(0);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p_num);


  /* set a population size for a local pe*/
  /* At least one pop is created */

  int local_popSize = 1;
  if (optarg->getPopSize() > p_num) {
    local_popSize = int(optarg->getPopSize() / p_num) + 1;
  }


  /* check the number of constraints */

  if (paraminfo->GetNumUpper() != paraminfo->GetNumLower()) {
    ERR.General("Setup", "Sobol selection", "The parameters Upper limits' number ;%d has to be equal to Lower limits' number: %d\n", paraminfo->GetNumUpper(), paraminfo->GetNumLower());
  }


  /* the number of constraints is equal to the number of parameters to be created*/
  int paramNum = paraminfo->GetNumUpper();


  /* the number of parameters to be created has to be more than one. */
  /* NOTE: In GA case, it has to be more than 3 because of crossover. */
  /*       However, in the case of SA, two is still enough. */

  if (paramNum <= 1) {
    ERR.General("Setup", "Sobol_selection", "the number of parameters to be optimised has to be larger than 1\n");
  }


  /* Check the number of sobol number sequence funcion call*/

  if (costfunction->GetMAXNumRandomGen() == 0) {
    ERR.General("Setup", "SobolSelect", "No set Max numbber of Sobol number sequence");
  }


  /* set the local number of sobol number sequece function call */

  int local_rNum = (int)ceil((double)costfunction->GetMAXNumRandomGen() / (double)p_num);


  /* set the sobol funcion */
  /* set the seed and the number of parameters */

  int sobolseed = optarg->getSobolSeed();
  long long int seed(0);
  if (sobolseed < 0) {
    seed = (my_rank + 1) * local_popSize;
  } else {
    seed = sobolseed;
  }

  Sobol64Bit * Sobol64 = new Sobol64Bit(paramNum);
  Sobol64->setSeed(seed);


  /* set the vector for sobol number sequence */

  vector<double> param_in(paramNum);

  /* set static array for parameter set for local pe */

  Send_Buf local_paramset[local_popSize];


  /* set the counter */

  /* this is for sobol numbers, range [0:local_rNum] */

  int j(0);

  /* this is parameter set, range [0:local_popSize] */
  int jj(0);


  if (my_rank == 0) {
    WriteStart();
  }
  /* create paramsets until
   * A. the sobol number sequence is generated over local_rNum.: Fail
   * or
   * B. the number of local_popSize parameter set with cost under
   * cost_arg->MaxCost are create.: SUCCEED
   */

  while ( (j < local_rNum) && (jj < local_popSize) ) {

    /* call sobol numbers, and set into the param_in vector */

    Sobol64->reap(param_in);

    /* rewrite param_in with the constraints */

    for (int i = 0; i < paramNum; i++) {
      param_in.at(i) = paraminfo->GetALower(i) +
                       param_in.at(i) *
                       (paraminfo->GetAUpper(i) -
                        paraminfo->GetALower(i));

    }

    /* calculate costfunction*/

    costfunction->SetParameters(param_in);
    double Cost = costfunction->GetCost();


    /* if the cost is smaller than MAXCOST, set it into the local paramsets */

    if (Cost < getMaxCost() ) {

      //sprintf(local_paramset[jj].cfilename,"%s/popsetup%d.txt",optarg->getResultDir().c_str(),my_rank*local_popSize+jj);
      local_paramset[jj].cost = Cost;
      for (int i = 0; i < paramNum; i++) {
        local_paramset[jj].params[i] = param_in.at(i);
      }

      /* increment jj counter */
      jj++;

      /* output for standard output */
      if (my_rank == 0) {
        cout << "\tSetup; pe# " << my_rank << " : " << j << "th cost of " <<
             local_popSize << " has " << Cost << " total num is " << jj << endl;
      }

    }

    /* increment j counter */

    j++;

  } //while

  /* if the size of the paramset is smaller thatn popSize, */
  /* give the error message and also give the advice to increase MAXCOST */

  if ( jj < local_popSize) {
    ERR.General("Setup", "Sobol_Select", "Setup FAILED: local paramset size %d .lt. popSize %d.\n ", jj , local_popSize );
  }


  /* save sobol seed*/

  optarg->setSobolSeed((int)Sobol64->getSeed());


  /* create the paramset array to receive from locals pe */

  vector<Send_Buf>paramset_buf;


  /* MPI_Barrier to blocks the caller until all group members have called it */
  /* the call returns at any process only after all group members have entered the call */

  MPI_Barrier(MPI_COMM_WORLD);


  /* Gathers data from all processes and distributes it to all processes */

  paramset_buf = Allgather(local_paramset, local_popSize);


  /* MPI_Barrier to blocks the caller until all group members have called it */
  /* the call returns at any process only after all group members have entered the call */

  MPI_Barrier(MPI_COMM_WORLD);

  /* COPY paramset_buf to paramset */

  string filename;
  for (unsigned int i = 0; i < paramset_buf.size(); i++) {

    for (unsigned int j = 0; j < param_in.size(); j++) {
      param_in.at(j) = paramset_buf[i].params[j];
    }
    //filename=paramset_buf[i].cfilename;
    paramset.push_back(ParamSet(param_in, paramset_buf[i].cost));


    /* Sort the paramset with the cost values */

    sort(paramset.begin(), paramset.end());

    /* In case that the parameter set number exceeds PopSize, bin the worst cost value parameters */
    if ((int)paramset.size() > optarg->getPopSize()) {
      paramset.pop_back();
    }
  }

  delete Sobol64;

  if (my_rank == 0) {
    cout << "======================= Setup: Finish  ===================" << endl;
  }
}

void Setup::WriteStart()
{

  SetupType s_type = optarg->getSetupType();

  string str;

  switch (s_type) {

  case SobolUnselect:
    str = "Sobol unselection";
    break;

  case SobolSelect:
    str = "Sobol selection";
    break;

  case ReadinFile:
    str = "Read in File";
    break;

  case InvalidSetup:
    ERR.General("Setup", __func__, "Invaid setup type\n");

  default:
    str = "Sobol unselection";
    break;

  }

  cout << "======================= Setup: " << str << " ===================" << endl;

}


