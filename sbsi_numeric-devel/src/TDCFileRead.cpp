
using namespace std;

#include <FileReader.h>
#include <ERR.h>

/*! Reader for tab delimited files. By default, whitespaces are set as " " or "\r"
 * and token delimiters are set to "\t".
 */
void TDCFileReader::Read(string file)
{

  fstream f;
  f.open(file.c_str(), ios::in);

  SetTokenDelimiters("\t");
  SetWhitespaces(" \r");

  try {

    if ( !f ) {
      ERR.FileR("FileReader", __func__, file.c_str());
    }


    int set = 0;
    int nset = 0;
    while ( !f.eof() ) {
      string line = GetLine(f);

      vector<string> columns = Tokenize(line);


      if ( !columns.empty() ) {
        if ((columns.at(0).find("@!") != string::npos) && ( columns.size() == 1 ) ) {

          nset++;
          Sets.resize(nset);
          set = nset - 1;
          string setname = columns.front();
          setname.erase(setname.begin(), setname.begin() + 2);
          Sets.at(set).SetSetName(setname);
          /* read header file */

          FileReader *hfr;
          hfr = new TDCHeaderFileReader();
          hfr->ClearSets();
          hfr->SetDataSet(&(Sets.at(set)));
          file.erase(file.rfind("/") + 1, file.size() - file.rfind("/") - 1);
          file.append(setname);
          hfr->Read(file);

          delete hfr;

        }
      }
      if ( columns.size() >= 2 ) {

        Sets.at(set).SetSizeDataValues(columns.size());

        for (size_t i = 0; i < columns.size(); i++) {


          if ( IsWord(columns.at(i)) ) {

            Sets.at(set).AddADataName(columns.at(i));

          } else {

            //Sets.at(set).Values.at(i).push_back(atof(columns.at(i).c_str()));
            Sets.at(set).AddADataValue(i, atof(columns.at(i).c_str()));
          }


        }
      }

    }

  }


  catch (const string str) {
    ERR.FileR("TDCFileReader", __func__, str.c_str());
  }
  f.close();
}

