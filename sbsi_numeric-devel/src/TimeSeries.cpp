#include <iomanip>
using namespace std;

#include <CostFunction.h>
#include <ModelCostFunction.h>
#include <TimeSeries.h>
#include <ERR.h>

vector<double>& TimeSeries::GetAStateTimeSeries(int aI)
{
  if ((aI + 1) > (int)results.size()) {
    ERR.General("TimeSeris", __func__, " Out range Index: %d > %d\n", aI, results.size());
  }
  return  results.at(aI + 1) ;
}

vector<double> TimeSeries::GetValuesAtATime(int aI)
{
  if ( aI > (int)results.front().size()) {
    ERR.General("TimeSeries", __func__, " Out range Index: %d > %d\n", aI, results.front().size());
  }
  vector<double> sout;
  for (size_t i = 1; i < results.size(); i++) {
    sout.push_back(results.at(i).at(aI));
  }
  return sout;
}
void  TimeSeries::SetTimeSeries(vector<vector<double> > *s_results)
{

  results.resize(s_results->size());
  for (size_t i = 0; i < results.size(); i++) {
    results.at(i) = s_results->at(i);
  }

}

void TimeSeries::WriteToSBSIDataFormat (string fileName)
{

  // Part 0 a. Check results vector
  int col = results.size();

  if (col < 2) {
    cerr << "no result to be out " << __func__ << endl;
    cout << fileName << endl;
    return ;
  }

  // Part 0 b. Get result directory path

  string dir_path = fileName;
  dir_path.resize(dir_path.rfind("/") + 1);

  // Part Ia. print State

  ofstream rfp( fileName.c_str());
  if (!rfp.is_open()) {
    ERR.FileW("Time Series", __func__, fileName.c_str());
  }

  cout << "=========================== Time Series File Info. =====================" << endl;

  cout << " \t Time Series  of States will be saved  to " << fileName << "." << endl;
  string hdrfile = CreateHeaderFileName(fileName);
  rfp << "@!" << hdrfile << endl;

  vector<string> names = model->getStateNames();
  string colstr = CreateColumnHeaderLine(names);
  rfp << colstr ;

  rfp.precision(12);

  /*
   * The reporting interval may be larger than the recording interval
   * (which in other places is annoyingly named the 'OutInterval'.
   * So we may require a record interval of let's say 0.1, because our
   * data that we are matching against has time points at say:
   * 2.1, 3.3, 4.2, 4.8, 5.1, so we need a rather small record interval
   * to be sure that each data time point can be matched closely. But
   * when recording the timeseries to a file we may wish for much coarser
   * granularity, let's say 1.0. Hence there are some recorded time points
   * that we do not wish to output here. In this example we would want to
   * output one every 10th recorded time point. We get this number from
   * dividing the ReportInterval by the OutInterval (or RecordInterval as
   * I call it).
   */
  // Note it would be better if the TimeSeries itself stored its own
  // interval, rather than relying on it being the same as the model's
  // out/record interval. 
  int granularity = 1;
  double report_interval = model->getReportInterval();
  double record_interval = model->getOutInterval();
  if (report_interval > record_interval){
    double gran_d = report_interval / record_interval;
    // The + 0.5 is because otherwise the cast will simply 'floor' the
    // granularity which is wrong in most cases, eg 3.9998 comes out
    // at 3, rather than 4. Adding the 0.5 turns the cast into a 'round'
    // operation rather than a floor operation.
    granularity = static_cast<int>(gran_d + 0.5);
  }

  for (size_t i = 0; i < results.front().size(); i++) {
    if ((i % granularity) == 0){
      // Output the first column, which should be the time
      rfp << results.at(0).at(i) ;
      // Then for each subsequent column output a separator
      // followed by the value at the column
      for (size_t j = 1; j < results.size(); j++) {
        rfp << "\t" << results.at(j).at(i) ;
      }
      rfp << endl;
    }
  }

  rfp.close();

  // Part Ib. print HeaderFile

  WriteHeaderFile(dir_path + hdrfile, names);


  // Part II. Variables

  if (model->getNumVars() != 0) {

    vector<string> varnames = model->getVarNames();
    string colstr = CreateColumnHeaderLine(varnames);

    // Set Variable filename
    size_t found = fileName.find_last_of(".");
    if (found != string::npos) {
      fileName.insert(found, "_Var");
    } else {
      fileName.append("_Var");
    }

    // Calculate Variables
    vector<vector<double> > var_results;
    vector<string> statename = model->getStateNames();
    vector< vector<double> > states = GetTimeSeries();

    for (size_t j = 0; j < varnames.size(); j++) {

      vector<double> result;
      result = model->calVarVec(j, states);

      if (!result.empty()) {
        var_results.push_back(result);
      }
    }
    if (!var_results.empty()) {

      // IIa. Print Variable to File
      ofstream v_rfp( fileName.c_str());
      if (!v_rfp.is_open()) {
        ERR.FileW("Time Series", __func__, fileName.c_str());
      }
      cout << " \t Time Series of Variables will be saved  to " << fileName << "." << endl;

      vector<double> time = GetTimeSeriesTime();

      hdrfile = CreateHeaderFileName(fileName);
      v_rfp << "@!" << hdrfile << endl;
      v_rfp << colstr ;
      v_rfp.precision(12);

      for (size_t i = 0; i < var_results.at(0).size(); i++) {
        v_rfp << time.at(i) ;
        for (size_t j = 0; j < var_results.size(); j++) {
          v_rfp << "\t" << var_results.at(j).at(i) ;
        }
        v_rfp <<  endl;
      }
      v_rfp.close();

      //II.b Print HeaderFile
      WriteHeaderFile(dir_path + hdrfile, varnames);
    }

  }

  return;
}

void TimeSeries::WriteHeaderFile(string headerFileName, vector<string> colname)
{

  ofstream rfp( headerFileName.c_str());

  if (!rfp.is_open()) {
    ERR.FileW("Time Series", __func__, headerFileName.c_str());
  }

  rfp << "*Annotation*" << endl;
  rfp << "Generated at : " << __TIME__ << endl;
  rfp << "*Parameters*" << endl;
  vector <string> paramname = model->getParamNames();
  vector <double> paramvalue = model->getParameters();
  for (size_t i = 0; i < paramname.size();  i++) {
    rfp <<  paramname.at(i) << "\tn/a\t" << paramvalue.at(i) << endl;
  }

  rfp << "*Columns*" << endl;
  rfp << "Time\tn/a" << endl;
  //        vector <string> statename = model->getStateNames();
  for (vector<string>:: iterator j = colname.begin();
       j != colname.end();
       j++) {
    rfp <<  *(j) << "\tn/a" << endl;
  }
  rfp << endl;

  rfp.close();

  return;

}

string TimeSeries::CreateHeaderFileName (string dataFileName)
{
  dataFileName.erase(dataFileName.begin(), dataFileName.begin() + dataFileName.rfind("/") + 1);
  dataFileName.resize(dataFileName.rfind("."));
  return dataFileName + ".sbsiheader";

}

string TimeSeries::CreateColumnHeaderLine (vector<string> names)
{

  ostringstream oss;
  oss << "Time";
  for (vector<string>::iterator j = names.begin(); j != names.end(); j++) {
    oss << "\t" << *(j);
  }
  oss << endl;
  return oss.str();
}

