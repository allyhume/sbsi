#include <algorithm>

using namespace std;

#include <MultiObjCost.h>
#include <ERR.h>

void MultiObjCost::SetParamNames(const vector<string> names)
{

  CostFunction::SetParamNames(names);

  for (size_t i = 0; i < MCFun.size(); i++) {
    if (MCFun.at(i)->IsParamNamesEmpty()) {
      SetParamNames(names, (int)i);
    }
  }
}

void MultiObjCost::SetParamNames(const vector<string> names, int aI)
{

  CostFunction *aObjCost;

  aObjCost = MCFun.at(aI);
  aObjCost->SetParamNames(names);

}

void MultiObjCost::SetParameters(const vector<double> in_param, int aI)
{

  CostFunction *aObjCost;
  aObjCost = MCFun.at(aI);

  vector<double> pvalue;

  if (!aObjCost->IsParamNamesEmpty()) {

    vector<string>::iterator l_p = aObjCost->GetParamNames().begin();

    int ptr(0);
    while (l_p != aObjCost->GetParamNames().end()) {

      vector<string>::iterator m_pb = CostFunction::GetParamNames().begin();
      vector<string>::iterator m_pe = CostFunction::GetParamNames().end();
      vector<string>::iterator index = find(m_pb, m_pe, *l_p);
      if (index != m_pe) {
        ptr = index - m_pb;
        pvalue.push_back(in_param.at(ptr));
        //MCFun.at(1)->DoMyCheck() ;
      } else {
        ERR.General("MultiObjCost", __func__, "Invalid parameter name %s.\n", (*l_p).c_str());
      }
      l_p++;
      //MCFun.at(1)->DoMyCheck() ;

    }
    aObjCost->SetParameters(pvalue);
  } else {
    aObjCost->SetParameters(in_param);
  }



}

double MultiObjCost::GetCost()
{

  double Cost (0);
  double c(0);
  multiobjcost_val.resize(0);
  CostFunction *acostfunc;
  // debugging code useful for finding out the makeup of the overall cost
  // cout << "========================================" << endl;
  for (size_t i = 0; i < MCFun.size(); i++) {
    acostfunc = MCFun.at(i);
    if (!acostfunc) {
    } else {
    }
    c = acostfunc->GetCost();
    multiobjcost_val.push_back(c);
    // Debugging again
    // cout << "Calculating the " << i << "th cost at: " << c << endl;
    Cost += c;
  }
  // And again more debugging.
  // cout << "The entire cost is: " << Cost << endl;
  return Cost;
}

void MultiObjCost::SetParameters(const vector<double> in_param)
{

  CostFunction::SetParameters(in_param);

  for (size_t i = 0; i < MCFun.size(); i++) {
    SetParameters(in_param, (int)i);
  }


}
vector<string>& MultiObjCost::GetParamNames()
{
  /*
   * CostFunction *acostfunc;
   * acostfunc = MCFun.front();
   * return acostfunc->GetParamNames();
   */
  return CostFunction::GetParamNames();
}

vector<string>& MultiObjCost::GetParamNames(int aI)
{
  CostFunction *acostfunc;
  acostfunc = MCFun.at(aI);
  return acostfunc->GetParamNames();
}

