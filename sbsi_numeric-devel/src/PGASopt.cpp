/*
 * $Id: PGASopt.cpp,v 1.1.2.2 2011/01/11 09:31:38 ayamaguc Exp $
 * author Anatoly Sorokin
 * Date: 14 Jul 2007
 *
 * Modified and Impliment to framework v9 by Azusa
 * particle swarm PSOArray
 */
#include <PGASopt.h>
#include <ERR.h>


//SET ====
//Ioan Cristian Trelea.
//The particle swarm optimization algorithm: convergence analysis and parameter selection.
//Inf. Process. Lett., 85(6):317-325, 2003.
#define V_MAX_RATIO 1.0
//=====SET

using namespace std;

FILE *logPSO = NULL;

/*!****************************************************************************
  PGAPSopt::Run - High-level routine to execute the Particle swarm algorithm.
  It is called after PGACreate and PGASetup have been called.
  If a NULL communicator is given, a sequential execution method is used,
  otherwise, work is divided among the processors in the communicator.

  Category: Generation

  Inputs:
    ctx      - context variable
    evaluate - a pointer to the user's evaluation function, which must
    have the calling sequence shown in the example.
    comm     - an MPI communicator

  Outputs:
    none

  Example:
    PGAContext *ctx;
    double f(PGAContext *ctx, int p, int pop);
    :
    PGARunGM(ctx, f, MPI_COMM_WORLD);

****************************************************************************U*/
int PGAPSopt::Run()
{
  Initialise();
  int iter = PGAPSopt_Run();
  
  /*
   * We unconditionally print out the best population to the
   * BestPop.<num>.ga file at the end of the optimisation run.
   * This is in order to work correctly with the loop.sh mechanism
   * for splitting an optimisation into several runs to avoid a long
   * run being bumped from a cluster. - allan
   */ 
  PGSWrapper::printPop();
  Destroy();
  return iter;
}
int PGAPSopt::PGAPSopt_Run()
{

  int  rank, Restarted;
  int  iter(0);

  rank = PGAGetRank(ctx, MPI_COMM_WORLD);

  if (rank == 0) {
    printf("PGAPSopt::Run()\n");
    fflush(stdout);
  }

  // set a rank to each parameter set
  PGAEvaluate(ctx, PGA_OLDPOP, evaluate, MPI_COMM_WORLD);
  if (rank == 0) {
    PGAFitness(ctx, PGA_OLDPOP);
  }

  while (!Done(ctx, MPI_COMM_WORLD) ) {
    if (rank == 0) {
      Restarted = PGA_FALSE;
      PGASelect(ctx, PGA_OLDPOP);
      CreateNewGeneration(ctx, PGA_OLDPOP, PGA_NEWPOP);
    }


    MPI_Bcast(&Restarted, 1, MPI_INT, 0, MPI_COMM_WORLD);


    PGAEvaluate(ctx, PGA_NEWPOP, evaluate, MPI_COMM_WORLD);


    if (rank == 0) {
      PGAFitness(ctx, PGA_NEWPOP);
    }

    /*  If the GA wasn't restarted, update the generation and print
           *  stuff.  We do this because a restart is NOT counted as a
           *  complete generation.
     */
    PGAUpdateGeneration(ctx, MPI_COMM_WORLD);
    if (rank == 0) {
      PGAPrintReport(ctx, stdout, PGA_OLDPOP);
    }
  }//end of while(!Done..)
  iter = PGAGetGAIterValue(ctx);

  if (rank == 0) {
    printf("Finishing ::Particle Swarm Optimisation::Run()\n");
  }
  fflush(stdout);

  /*
   * allan: We should have run the above while loop until iteration is
   * larger than the number of generations for particle swarm
   * optimisation. However there may still be some more generations
   * of normal PGA to run if NumGen is greater than PSONumGen.
   * In this case we have to run the normal PGA algorithm for the
   * remaining number of generations.
   */
  if (iter >= psoGenerationNumber && iter < optarg->getNumGen() ) {
    if (rank == 0) {
      CreateBestGeneration(ctx, PGA_NEWPOP);
    }
    MPI_Bcast(&Restarted, 1, MPI_INT, 0, MPI_COMM_WORLD);
    PGAEvaluate(ctx, PGA_NEWPOP, evaluate, MPI_COMM_WORLD);
    if (rank == 0) {
      PGAFitness(ctx, PGA_NEWPOP);
    }
    PGAUpdateGeneration(ctx, MPI_COMM_WORLD);
    if (rank == 0) {
      printf("Starting  ::Genetic Algorithm::Run()\n");
    }
    // allan: So now we call the vanilla PGA algorithm to run for
    // NumGen - PSONumGen generations.
    iter = PGSWrapper::PGSWrapper_Run();
  } else {
    /*
     * allan: However if we don't have any more generations to run, so if
     * PSONumber >= NumGen then we're done.
     */
    if (rank == 0) {
      int best_p = PGAGetBestIndex(ctx, PGA_OLDPOP);
      printf("%s The Best Evaluation: %e.\n", __func__,
             PGAGetEvaluation(ctx, best_p, PGA_OLDPOP));
      printf("The Best String:\n");
      PGAPrintString(ctx, stdout, best_p, PGA_OLDPOP);

      // allan: Hmm, I'm not sure why I need to make this call because
      // in the PGASopt::Initialise we call PGSWrapper::Initialise and
      // that should set the user stop function to PGSWrapper::StopCond
      // so I was under the impression that it should get called 
      // automatically but if I comment out these two lines no Stopinfo
      // file is printed, so this seems to be necessary.
      bool output(true);
      PGSWrapper::StopCond(ctx, output);

      fflush(stdout);
    }
  }
  return iter;
}

int PGAPSopt::Done(PGAContext *ctx, MPI_Comm comm)
{

  int done(0);
  if ((PGAGetGAIterValue(ctx) >= psoGenerationNumber)) {
    done = PGA_TRUE;
    return done;
  } else {
    return PGADone(ctx, comm);
  }
}

void PGAPSopt::CreateBestGeneration(PGAContext *ctx, int newpop)
{
  int popsize, num;
  int rank = PGAGetRank(ctx, MPI_COMM_WORLD);

  if (rank == 0) {
    popsize = PGAGetPopSize(ctx);
    num = PGAGetStringLength(ctx);

    /* set the global best to the 0th pop */
    for (int j = 0; j < num; j++) {
      PGASetRealAllele(ctx, 0, newpop, j, gBest.at(j));
    }

    PGASetEvaluationUpToDateFlag(ctx, 0, newpop, PGA_FALSE);
    for (int i = 1; i < popsize; i++) {
      for (int j = 0; j < num; j++) {
        PGASetRealAllele(ctx, i, newpop, j, pBest.at(i).at(j));

      }
      PGASetEvaluationUpToDateFlag(ctx, i, newpop, PGA_FALSE);
    }
  }
}

/* This is the master node's job */
void PGAPSopt::CreateNewGeneration(PGAContext *ctx, int oldpop, int newpop)
{

  int popsize = PGAGetPopSize(ctx);
  int len = PGAGetStringLength(ctx);

  /* get the current best */

  int cbest = PGAGetBestIndex(ctx, oldpop);
  double cBest = PGAGetEvaluation(ctx, cbest, oldpop);

  if (cBest <= gVBest) {
    /* update group best */
    gVBest = cBest;
    /* update position of the global best  */
    for (int j = 0; j < len; j++) {
      gBest.at(j) = PGAGetRealAllele(ctx, cbest, oldpop, j);
    }
  } else {
    cbest = -2; // the older is better
  }

  for (int i = 0; i < popsize; i++) {

    /* get ith cost value */
    double fit = PGAGetFitness(ctx, i, oldpop);

    /* get  ith best position */
    vector<double> pbest = pBest.at(i);
    /* and its value */
    double pbestv = vBest.at(i);

    /* get  ith velocity */
    vector<double> vi = velocities.at(i);

    for (int j = 0; j < len; j++) {

      /* get the current jth position */
      double cur = PGAGetRealAllele(ctx, i, oldpop, j);

      /*set a boundary for jth param value */
      double max = paraminfo.GetAUpper(j);
      double min = paraminfo.GetALower(j);

      /* if the current fit value is better than previous ones,
            set current position at pBest */
      if (fit <= pbestv) {
        pbest.at(j) = cur;
      }

      /* compute velocity */
      double vij = (inertia.at(i) * vi.at(j) + Congnitive * (pbest.at(j) - cur) * PGARandom01(ctx, 0) + Social * (gBest.at(j) - cur) * PGARandom01(ctx, 0));

      // control of VMAX
      if (vij > vMax.at(j)) {
        vi.at(j) = vMax.at(j);
        vij = vMax.at(j);
      } else if (vij < -1 * vMax.at(j)) {
        vi.at(j) = -1 * vMax.at(j);
        vij = -1 * vMax.at(j);
      } else {
        vi.at(j) = vij;
      }

      //Control parameter boundaries
      if ((vij + cur) > max) {
        cur = reflect(cur, vij, max);
      } else if ((vij + cur) < min) {
        cur = reflect(cur, vij, min);
      } else {
        cur += vij;
      }

      PGASetRealAllele(ctx, i, newpop, j, cur);
    }
    if (fit <= pbestv) {
      vBest[i] = fit;
    }
    pBest.at(i) = pbest;
    velocities.at(i) = vi;
    PGASetEvaluationUpToDateFlag(ctx, i, newpop, PGA_FALSE);
  }
}


void PGAPSopt::endOfGen(PGAContext *ctx)
{
  PGSWrapper::endOfGen(ctx);
  fflush(stdout);
}

void PGAPSopt::Initialise()
{
  int rank(0);
  MPI_Comm_rank( MPI_COMM_WORLD, &rank);

  PGSWrapper::Initialise(); //create ctx

  psoGenerationNumber = optarg->getPSONumGen();

  if (psoGenerationNumber < 0) {
    //PGAGetRestartFrequencyValue(ctx));
    setPSOGenerationNumber(restartFreq);  
  }

  if (rank == 0) {
    size_t len = paraminfo.GetNumLower();

    for (size_t j = 0; j < len; j++) {
      vMax.push_back(V_MAX_RATIO*(paraminfo.GetAUpper(j) - paraminfo.GetALower(j)));
      gBest.push_back((paraminfo.GetAUpper(j) + paraminfo.GetALower(j)) / 2.0);
    }

    costfunction->SetParameters(gBest);
    gVBest = costfunction->GetCost();

    int popSize = optarg->getPopSize();

    velocities.resize(popSize);
    pBest.resize(popSize);

    Inertia = optarg->getInertia();
    Congnitive = optarg->getCongnitive();
    Social = optarg->getSocial();

    for (int i = 0; i < popSize; i++) {
      for (size_t j = 0; j < len; j++) {
        double temp = PGARandom01(ctx, 0);
        velocities.at(i).push_back(vMax.at(j)*(1 - 2.0 * temp));
        pBest.at(i).push_back(PGAGetRealAllele(ctx, i, PGA_OLDPOP, j));
      }
      inertia.push_back(Inertia); // standard PSO
      costfunction->SetParameters(pBest.at(i));
      vBest.push_back(costfunction->GetCost());
    }

    if (verbose != 0) {
      string logfilename;
      logfilename = "PSOLog-" + runName + ".txt";
      if ((logPSO = fopen(logfilename.c_str(), "wt")) == NULL) {
        ERR.FileW("PGAPSopt", __func__, logfilename.c_str());
      }
    }
  } // rank 0
}


void  PGAPSopt::Destroy()
{
  int rank = PGAGetRank(ctx, MPI_COMM_WORLD);
  if (rank == 0) {
    printf("PGAPSopt::Destroy\n");
    if (logPSO != NULL) {
      fclose(logPSO);
    }
    printf("finishing PGAPSopt::Destroy\n");
  }
  PGSWrapper::Destroy();
}
/*
 * $Log: PGASopt.cpp,v $
 * Revision 1.1.2.2  2011/01/11 09:31:38  ayamaguc
 * using sundial 2.3.0
 *
 * Revision 1.1.2.1  2010/05/20 14:36:26  ayamaguc
 * remove 0.0 to devel
 * add devel dir
 *
 * Revision 1.1.2.11  2010/05/10 03:50:48  ayamaguc
 * fixed the iter bug
 *
 * Revision 1.1.2.10  2010/05/07 15:55:05  ayamaguc
 * *** empty log message ***
 *
 * Revision 1.1.2.9  2010/05/07 15:43:27  ayamaguc
 * fix the bug occured at multiple thread
 *
 * Revision 1.1.2.8  2010/05/06 15:54:47  ayamaguc
 * *** empty log message ***
 *
 * Revision 1.1.2.7  2010/05/06 12:18:09  ayamaguc
 * fixed bugs in PSO
 *
 * Revision 1.1.2.6  2010/05/06 01:26:34  ayamaguc
 * add PSO numGen
 *
 * Revision 1.1.2.4  2010/04/29 12:59:38  ayamaguc
 * *** empty log message ***
 *
 * Revision 1.4  2010/01/20 15:52:35  ayamaguc
 * Match with xml file input.
 * Note: commandline argument is NO LONGER valid.
 *
 * No Log: all error message will be passed to ERR.
 *
 * Xml file example is avaiarable in Template.
 *
 * Revision 1.3  2009/08/05 09:53:22  nhanlon
 * mostly signedness and unused variable warning fixes
 *
 * Revision 1.2  2009/06/08 01:02:15  radams
 * docs + user configurable logging level
 *
 * Revision 1.1  2009/05/21 15:34:40  kevin
 *
 *
 * Renamed from .C
 *
 * Revision 1.1.1.1  2009/04/29 01:04:19  ecb-09
 *
 *
 * Revision 1.2  2009/04/29 00:15:03  ecb-09
 * *** empty log message ***
 *
 * Revision 1.10  2007/07/22 21:37:36  asorokin
 * Define verbosity of output. Value of parameter define frequence of output. If less than or equal to 0 output every generation, otherwise every v generations. Default value is -1
 *
 * Best population is printed out in ascending order, so population of smaller size could be extracted.
 *
 * Revision 1.9  2007/07/22 20:29:18  asorokin
 * on finish PSO send to GA location of best points for each particle rather than last generation.
 *
 * Revision 1.8  2007/07/22 16:07:14  asorokin
 * organise afterInit invocation properly. PGSWrapper::AfterInit is always invoked now
 *
 * Revision 1.7  2007/07/19 22:46:34  asorokin
 * definition of constraints is added
 * number of cost function evaluation is calculated and printed
 *
 * Revision 1.6  2007/07/19 16:32:30  asorokin
 * BUG with ability of gene value to come out of specified range is fixed.
 * Some minor bag with improper asignment of initial walues in Sobol procedure was fixed
 *
 * Revision 1.5  2007/07/19 13:10:37  asorokin
 * Calculations could be caied out from previous results.
 * -bp key specify name of the file to be read
 *
 * Revision 1.4  2007/07/18 18:32:21  asorokin
 * Cumulateive cost function is working on LAM/MPI and on BlueGene
 *
 * Revision 1.3  2007/07/14 20:45:25  asorokin
 * Valgrind returns no memory errors
 *
 * Revision 1.2  2007/07/14 13:13:17  asorokin
 * Comments and notes
 *
 */

