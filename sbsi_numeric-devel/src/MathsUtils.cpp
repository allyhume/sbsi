using namespace std;

#include <MathsUtils.h>
#include <iostream>

/**
 *  Computes the mean of a vector of double values.
 *
 * @param data  data for which to compute the mean
 *
 * @return the mean of the data vales, or zero if the vector is empty.
 */
double MathsUtils::mean(const vector<double>& data) {
  double total_value = 0.0;
  double size_of_data = data.size();

  for (int index = 0; index < size_of_data; index++){
    total_value += data.at(index);
  }
  if (size_of_data == 0) {
    return 0; 
  }
  else {
    return total_value/size_of_data;
  }
}
  
  
/**
 * Computes the sum of the vector of double values.
 * 
 * @param data  data for which to compute the sum
 *
 * @return the sum of the data values.
 */
double MathsUtils::sum(const vector<double>& data) {
  double result = 0;
  
  for(int i=0; i<data.size(); ++i) result += data[i];

  return result;
}

/**
 * Computes the sum of the product of two vectors. The result is
 * data1[0]*data2[0] + data1[1]*data2[1] + ...
 *
 * The sum is only computed for those indexes for which there is
 * data in both vectors. Thus if one vector is larger than the other
 * the additional entries are ignored.
 * 
 * @param data1   vector of data values
 * @param data2   vector of data values
 *
 * @return the sum of the product of corresponding pairs of data values.
 */
double MathsUtils::sumProduct(const vector<double>& data1, const vector<double>& data2) {

  double result = 0;
  int size = data1.size();
  if (data2.size() < size) size = data2.size();
  
  for(int i=0; i<size; ++i) result += data1[i] * data2[i];

  return result;
}


/**
 * Creates a vector of indexes starting from 0 increasing by 1 until
 * the specied size (exclusive).  For example, is size is 4 then the 
 * result vector will be [0, 1, 2, 3]
 *
 * @param size     size of the result vector
 * @param result   result vector. Any exising contents will be overwritten.
 */
void MathsUtils::getIndexVector(int size, vector<double>& result) {

  result.clear();
  result.reserve(size);
  
  for (int i=0; i<size; ++i) {
    result.push_back(i);
  }
}


/**
 * Performs basic linear detreanding. Uses linear regression to fit a line
 * through the data then subtracts the line from the data. The method 
 * assumes a constant time interval in the given time series.
 *
 * @param y        data values of a time series
 * @param result   detrended time series
 */
void MathsUtils::detrend(const vector<double>& y, vector<double>& result) {

  vector<double> x;
  getIndexVector(y.size(), x);

  double sumx = sum(x);
  double sumx2 = sumProduct(x, x);
  double sumxy = sumProduct(x, y);
  double sumy = sum(y);
  int suminvar = x.size();
  double delta = suminvar * sumx2 - sumx * sumx;

  double a, b;
  if (delta == 0) {
    a = 0.0;
    b = 0.0;
  } else {
    a = ((sumx2 * sumy) - (sumx * sumxy)) / delta;
    b = ((suminvar * sumxy) - (sumx * sumy)) / delta;
  }

  // Subtract trend from data.
  // y = y - (a+b*x)
  result.clear();
  result.reserve(y.size());
  for (int i = 0; i < y.size(); ++i) {
    result.push_back(y[i] - (a + b * x[i]));
  }
}
