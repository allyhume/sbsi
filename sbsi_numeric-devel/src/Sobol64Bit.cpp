/*****************************************************************************
 *
 *  Sobol64Bit.cpp
 *
 *  See Sobol64Bit.h for a description.
 *
 *  $Id: Sobol64Bit.cpp,v 1.1.2.1 2010/05/20 14:36:26 ayamaguc Exp $
 *
 *  Millar Laboratory, Centre for Systems Biology at Edinburgh
 *  (c) The University of Edinburgh (2009)
 *  Kevin Stratford (kevin@epcc.ed.ac.uk)
 *
 *****************************************************************************/

#include <math.h>

#include <Sobol64Bit.h>
#include <ERR.h>

/*****************************************************************************
 *
 *  Constructor
 *
 *  Takes the number of dimensions.
 *
 *****************************************************************************/

Sobol64Bit::Sobol64Bit(const long long int dimension)
{

  if (dimension <= 1) {
    ERR.General("Sobol64Bit", "constructor", "dimension <= 1\n");
  }
  if (dimension > SOBOL_DIM_MAX) {
    ERR.General("Sobol64Bit", "constructor", "dimension > SOBOL_DIM_MAX\n");
  }

  dimension_ = dimension;
  rnormalise_ = 1.0 / pow(2.0, SOBOL_LOG_MAX);
  maxseed_ = (int) pow(2.0, SOBOL_LOG_MAX) - 1;
  initialisePolynomials();
  initialiseDirectionNumbers();
}

/*****************************************************************************
 *
 *  Destructor
 *
 *****************************************************************************/

Sobol64Bit::~Sobol64Bit()
{
}

/*****************************************************************************
 *
 *  setSeed
 *
 *  Set the private seed for the current sequence and reset the
 *  state.
 *
 *****************************************************************************/
long long int Sobol64Bit::getSeed()
{
  return seed_;
}

void Sobol64Bit::setSeed(const long long int new_seed)
{

  long long int i, j, lowbit;

  if (new_seed < 0) {
    ERR.General("Sobol64Bit", "setSeed" , "new_seed < 0\n");
  }

  if (new_seed > maxseed_) {
    ERR.General("Sobol64Bit", "setSeed", "new_seed > %d\n", maxseed_);
  }

  seed_ = new_seed;

  for (i = 0; i < dimension_; i++) {
    lastq_[i] = 0;
  }

  for (j = 0; j <= seed_ - 1; j++) {
    lowbit = lowZeroBit(j);
    for (i = 0; i < dimension_; i++) {
      lastq_[i] = lastq_[i] ^ v_[lowbit-1][i];
    }
  }

  return;
}

/*****************************************************************************
 *
 *  reap
 *
 *  Return the next set of values in the sequence, and increment
 *  the private state.
 *
 *  The argument is the vector of doubles to hold the numbers
 *  s[0], ..., s[dimension_ - 1].
 *
 *****************************************************************************/

void Sobol64Bit::reap(double * sequence)
{

  long long int lowbit;
  long long int i;

  lowbit = lowZeroBit(seed_);

  for (i = 0; i < dimension_; i++) {
    sequence[i] = rnormalise_ * lastq_[i];
    lastq_[i] = lastq_[i] ^ v_[lowbit-1][i];
  }

  seed_++;

  /* Could check here that we have not exceeded largest seed value,
   * but avoid the cost of the conditional */

  return;
}

/*****************************************************************************
 *
 *  reap
 *
 *  The vector<double> interface calls the above.
 *
 *****************************************************************************/

void Sobol64Bit::reap(std::vector<double> &sequence)
{

  this->reap(&sequence[0]);

  return;
}


/*****************************************************************************
 *
 *  lowZeroBit
 *
 *  Returns the position of the right-most (lowest) zero bit in the
 *  binary representation of n.
 *
 *****************************************************************************/

long long int Sobol64Bit::lowZeroBit(const long long int n) const
{

  long long int bit = 0;
  long long int n1 = n;
  long long int n2;

  while (true) {
    bit++;
    n2 = n1 / 2;
    if (n1 == 2 * n2) {
      break;
    }
    n1 = n2;
  }

  return bit;
}

/*****************************************************************************
 *
 *  initialisePolynomials
 *
 *  Initialise the table of encoded primitive polynomials.
 *  This table is appropriate for DIM_MAX = 1111 following
 *  Joe and Kuo (2003).
 *
 *  This table is so unweildy, it comes from a separate file.
 *
 *****************************************************************************/

void Sobol64Bit::initialisePolynomials(void)
{

#include <SobolPolynomials.h>

  return;
}

/*****************************************************************************
 *
 *  initialiseDirectionNumbers
 *
 *  These are the initial (free) choices for direction numbers v_,
 *  again computed by Joe and Kuo (2003).
 *
 *  The remaining direction numbers are computed from the recurrence
 *  relation.
 *
 *  Again, the actual tables come from a separate file.
 *
 *****************************************************************************/

void Sobol64Bit::initialiseDirectionNumbers(void)
{

  int i, j;

  for (j = 0; j < SOBOL_LOG_MAX; j++) {
    for (i = 0; i < SOBOL_DIM_MAX; i++) {
      v_[j][i] = 0;
    }
  }

#include <SobolDirectionNumbers.h>

  recurrenceRelation();

  return;
}

/*****************************************************************************
 *
 *  recurrenceRelation
 *
 *  Compute the rest of the direction numbers, follow John Burkardt.
 *
 *****************************************************************************/

void Sobol64Bit::recurrenceRelation(void)
{

  int i, k, m;
  long long int j, j2, newv, lowbit;
  bool include[SOBOL_LOG_MAX];

  i = 0;

  for (j = 0; j < SOBOL_LOG_MAX; j++) {
    v_[j][i] = 1;
  }

  for (i = 1; i < dimension_; i++) {

    j = poly_[i];
    m = 0;

    while ( true ) {
      j = j / 2;
      if (j <= 0) {
        break;
      }
      m++;
    }

    j = poly_[i];
    for ( k = m - 1; k >= 0; k-- ) {
      j2 = j / 2;
      include[k] = (j != ( 2 * j2 ));
      j = j2;
    }

    for ( j = m; j < SOBOL_LOG_MAX; j++ ) {
      newv = v_[j-m][i];
      lowbit = 1;

      for ( k = 0; k < m; k++ ) {
        lowbit = 2 * lowbit;
        if (include[k]) {
          newv = newv ^ (lowbit * v_[j-k-1][i]);
        }
      }
      v_[j][i] = newv;
    }
  }

  lowbit = 1;
  for (j = SOBOL_LOG_MAX - 2; j >= 0; j--) {
    lowbit = 2 * lowbit;
    for (i = 0; i < dimension_; i++) {
      v_[j][i] = v_[j][i] * lowbit;
    }
  }

  return;
}
