
#include <Anneal.h>
#include <Sobol64Bit.h>
#include <cfloat>
#include <ERR.h>
#include <Setup.h>
#include <mpi.h>

#define MAC


int Anneal::Run()
{


  int my_rank(0);
  int p_num(0);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &p_num);

  size_t r_i(0);

  vector<double> randomNums;

  int iter(0);

  /* 
   * Run SA for each population, however there is normally only one.
   */
  for (int p = 0; p < optarg->getPopSize(); p++) { 
    ParamSet p_i = ParamSet((*paramsets).at(p).params, (*paramsets).at(p).cost);


    // Set seed and Sobol
    long long int seed = (p + 1) * (my_rank + 1);
    if (optarg->getSobolSeed() > 0) {
      seed = optarg->getSobolSeed();
    }

    Sobol64Bit * Sobol64 = new Sobol64Bit(2 * (int)p_i.params.size());
    Sobol64->setSeed(seed);

    int count(0);
    iter = 0;
    double mu = optarg->getMu();

    // Print out the information
    WriteGeneralInfo();
    cout << "iter #" << "\tField\t" << "Value"  << endl;

    while ((iter < optarg->getNumGen()) && 
           (count <= optarg->getMaxNoChange()) && 
           (p_i.cost > optarg->getMinCost())) {

      randomNums.clear();

      randomNums.resize(2 * (int)p_i.params.size());

      Sobol64->reap(randomNums);

      ParamSet p_f(p_i.params, p_i.cost);

      double pre_cost = p_i.cost;

      for (int i = 0; i < (int)p_f.params.size(); i++) {

        // Create new paramset
        r_i = 2 * i;

        double next_param = p_f.params.at(i);

        if (paraminfo.IsInitialStepEmpty()) {
          next_param = paraminfo.GetALower(i) + randomNums.at(r_i) *
                       (paraminfo.GetAUpper(i) - paraminfo.GetALower(i));
        } else {
          if (i < paraminfo.GetNumInitialStep()) {
            next_param = paraminfo.GetALower(i) + randomNums.at(r_i) *
                         (paraminfo.GetAUpper(i) - paraminfo.GetALower(i));
          } else {
            next_param = p_f.params.at(i) + 2.0 * (randomNums.at(r_i) - 0.5) * paraminfo.GetAInitialStep(i);
          }
        }


        if (next_param < paraminfo.GetALower(i)) {
          next_param = paraminfo.GetALower(i);
        }
        if (next_param > paraminfo.GetAUpper(i)) {
          next_param = paraminfo.GetAUpper(i);
        }

        p_f.params.at(i) = next_param;

        // Get cost of the new paramset
        double cost_f = CalcEnergy(p_f);

        p_f.cost = cost_f;

        // Calculate tranform propability
        double pos = exp(-mu * (cost_f - p_i.cost));

        // Put into the new paramset
        if (randomNums.at(r_i + 1) < pos) {
          p_i.params.at(i) = p_f.params.at(i);
          p_i.cost = p_f.cost;
        }


      } // create the new paramser

      // Print the current cost value
      if (iter % optarg->getPrintFreq() == 0) {
        cout << iter << "\tBest\t" <<  p_i.cost << endl;
      }

      // Counting for NoImprovement
      //if(pre_cost != p_i.cost){
      if (pre_cost - p_i.cost > DBL_EPSILON) {
        count = 0;
      } else {
        count ++;
      }

      // Counting for generation
      iter++;

    }//while

    // Save seed

    optarg->setSobolSeed(Sobol64->getSeed());


    // Print Stop Info to the file
    ostringstream anneal_stopinfo_fname;
    anneal_stopinfo_fname << optarg->getResultDir() << "/" << "Anneal.Stopinfo."
                          << optarg->getCkpointStartNum() + iter ;

    ofstream anneal_stopinfo;

    anneal_stopinfo.open(anneal_stopinfo_fname.str().c_str());
    if (!anneal_stopinfo.is_open()) {
      ERR.FileW("Anneal", __func__, anneal_stopinfo_fname.str());
    }

    StopCondition(iter, count, p_i.cost, anneal_stopinfo);

    // Save the last paramset
    (*paramsets).at(p) = ParamSet(p_i.params, p_i.cost);

  }  //p


  if (optarg->getPrintBestPop()) {
    PrintPop(iter);
  }

  return iter; 

}
void Anneal::PrintPop (int iter)
{

  ostringstream anneal_fname;
  ofstream anneal_fout;

  anneal_fname << optarg->getCkpointDir() <<  "BestPop."
               << optarg->getCkpointStartNum() + iter << ".ga" ;
  cout << "The Best Pop will be saved in " << anneal_fname.str() << endl;

  anneal_fout.precision(16);
  anneal_fout.open(anneal_fname.str().c_str());

  if (!anneal_fout.is_open()) {
    ERR.FileW("Anneal", __func__, anneal_fname.str());
  }

  vector<ParamSet> set = sortParamSet(*paramsets);
  ParamSet p_i = set[0];

  vector<string> paramname;
  for (int p = 0; p < optarg->getPopSize(); p++) { // normaly only one
    anneal_fout << p << endl;
    paramname.resize(p_i.params.size());
    paramname = costfunction->GetParamNames();
    for (size_t i = 0; i < p_i.params.size(); i++) {
      anneal_fout << paramname.at(i) << ":\t" <<  p_i.params.at(i) << endl;
    }
    anneal_fout << "F=" << p_i.cost << endl;
  }

  anneal_fout.close();

  /* for stdout */
  cout << "The Best Pop cost is " << p_i.cost <<  endl;
  for (size_t i = 0; i < p_i.params.size(); i++) {
    cout << paramname.at(i) << ":\t" <<  p_i.params.at(i) << endl;
  }

}


void Anneal::StopCondition(int iter, int count, double cost, ofstream &f_p)
{

  if (iter >= optarg->getNumGen()) {
    WriteStopInfo_MAXITER(f_p, cost);
  } else {
    if (count >= optarg->getMaxNoChange()) {
      WriteStopInfo_NOIMPROVEMENT(f_p, cost);
    } else {
      if (cost <= optarg->getMinCost()) {
        WriteStopInfo_REACHED(f_p, cost);
      } else {
        ERR.General("Anneal", __func__, "No mached stop condition");
      }
    }
  }
}

double Anneal::CalcEnergy(ParamSet p)
{

  costfunction->SetParameters(p.params);
  return costfunction->GetCost();

}


