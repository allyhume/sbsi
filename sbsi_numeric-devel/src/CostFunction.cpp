using namespace std;

#include <SingleObjCost.h>
#include <MultiObjCost.h>
#include <ModelCostFunction.h>
#include <ERR.h>

/* CostFunction setting */
/* Its return value is  Single or Multi */
/* MultiObjCost fucntion return the cost as the sum of each costfunction */
/* SingleObjCost and the MultiObjcost with one costfunction is the same */
/* SingleObj and MultiObj can have any objective costfuncion, */
/* ModelCostFunction or User Defined Costfuncion ( with and without Model )*/

CostFunction& CostFunction::SetCostFunction(const CostFunctionType m)
{
  CostFunction *cf;
  switch (m) {
  case Single:
    cf = new SingleObjCost();
    return *cf;
  case Multi:
    cf = new MultiObjCost();
    return *cf;
  default:
    ERR.General("CostFunction", __func__, "Bad CostFunction Type");
    return *cf; // not reachable
  }
}




