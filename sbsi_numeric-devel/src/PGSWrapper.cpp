
#include <cModel.h>
#include <Sobol64Bit.h>
#include <PGSWrapper.h>
#include <PGASobol.h>
#include <PGASopt.h>
#include <ERR.h>

#define MAC

PGSWrapper *wrapper;
FILE *logPGA = NULL;

vector<string> paramname;

double evaluate(PGAContext *ctx, int p, int pop)
{
  return wrapper ->GetScore(ctx, p, pop);
}


extern "C" {
  void MyPrintString(PGAContext *ctx, FILE *file,  int p, int pop)
  {

    int len = PGAGetStringLength(ctx);
    if (!wrapper->costfunction->IsParamNamesEmpty()) {
      vector<string> paramname(len);
      paramname = wrapper->costfunction->GetParamNames();
      for (int i = 0; i < len; i++) {
        fprintf(file, "%s:\t%18.14lf\n", paramname.at(i).c_str(), PGAGetRealAllele(ctx, p, pop, i));
      }
    } else
      for (int i = 0; i < len; i++) {
        fprintf(file, "%d\t%18.14lf\n", i , PGAGetRealAllele(ctx, p, pop, i));
      }

    fprintf(file, "F=%12.8le\n", PGAGetFitness(ctx, p, pop) );
  }

  void EndOfGen(PGAContext *ctx)
  {
    return wrapper->endOfGen(ctx);
  }


}

int N_StopCond(PGAContext *ctx)
{

  bool output(true);

  return wrapper->StopCond(ctx, output);

}




/*  Function to randomly initialize a PGA_DATATYPE_CHARACTER string using
 *  all printable ASCII characters for the range.
 */

void PGSWrapper::N_InitString(PGAContext *ctx, int p, int pop)
{
  int  i;
  int len;
  len = PGAGetStringLength(ctx);
  vector<double> paramVals(len);
  long long int seed = wrapper->optarg->getSobolSeed();

  Sobol64Bit *Sobol64 = new Sobol64Bit(len);
  Sobol64->setSeed(seed);
  Sobol64->reap(paramVals);

  for (i = PGAGetStringLength(ctx) - 1; i >= 0; i--) {
    PGASetRealAllele(ctx, p, pop, i, paramVals.at(i));
  }

  wrapper->optarg->setSobolSeed(Sobol64->getSeed());
}


void PGAInit(PGAContext *ctx, int p, int pop)
{
  int len = PGAGetStringLength(ctx);

  ParamSet paramset;
  if (!Optimiser::paramsets->empty()) {
    paramset = (*Optimiser::paramsets).at(p);
    for (int j = 0; j < len; j++) {
      PGASetRealAllele(ctx, p, pop, j, paramset.params.at(j));
    }
  } else {

    long long int seed = wrapper->optarg->getSobolSeed();

    vector<double>param_in(len);

    Sobol64Bit *Sobol64 = new Sobol64Bit(len);
    Sobol64->setSeed(seed);
    Sobol64->reap(param_in);
    wrapper->optarg->setSobolSeed(Sobol64->getSeed());
    
    for (int j = 0; j < len; j++) {
      double param = Optimiser::paraminfo.GetALower(j) +
                     param_in.at(j) * 
                    (Optimiser::paraminfo.GetAUpper(j) -
                     Optimiser::paraminfo.GetALower(j));
      PGASetRealAllele(ctx, p, pop, j, param);
    }

  }
}

void PGSWrapper::SetParamSet(PGAContext *ctx)
{

  int pop = PGA_OLDPOP;
  PGASortPop(ctx, PGA_OLDPOP);
  vector<double> params;
  int numPop = PGAGetPopSize(ctx);
  int len = PGAGetStringLength(ctx);
  params.resize(len);

  int iter = PGAGetGAIterValue(ctx);

  for (int p = 0; p < numPop; p++) {

    for (int i = 0; i < len; i++) {
      params.at(i) = PGAGetRealAllele(ctx, p, pop, i);
    }

    double Cost = PGAGetFitness(ctx, p, pop);

    ostringstream oss ;
    oss << optarg->getResultDir() << "/timeseries_" << p 
        << "_" << optarg->getCkpointStartNum() + iter << ".dat" ;
    string filename = oss.str();
    (*paramsets).at(p) = ParamSet(params, Cost, filename);
  }

}

int PGSWrapper::StopCond(PGAContext *ctx, bool output)
{
  int  done(PGA_FALSE);
  int  best;
  int stop(0);


  /* check the stopping condition
     i) iteration limit exceeded (iter > numGen)
     ii) no improvement of costfunction ( the same best for 100 (default) generations)
     iii) populations are too similar each other( 95 percent (default) of the population can have the same costfunction value)
    */

  if (ctx->ga.iter >= ctx->ga.MaxIter) {
    done |= PGA_TRUE;
    stop = PGA_STOP_MAXITER;
  }
  if (ctx->ga.ItersOfSame >= ctx->ga.MaxNoChange) {
    done |= PGA_TRUE;
    stop = PGA_STOP_NOCHANGE;
  }
  if (ctx->ga.PercentSame >= ctx->ga.MaxSimilarity) {
    done |= PGA_TRUE;
    stop = PGA_STOP_TOOSIMILAR;
  }

  best = PGAGetBestIndex(ctx, PGA_OLDPOP);
  double cost = PGAGetEvaluation(ctx, best, PGA_OLDPOP) ;

  if (cost <= optarg->getMinCost() ) {
    done |= PGA_TRUE;
    stop = 8;
  }

  if (done == PGA_TRUE && output) {

    int iter = PGAGetGAIterValue(ctx);

    ostringstream fname;
    fname << optarg->getResultDir() << "/PGA.Stopinfo." <<  optarg->getCkpointStartNum() + iter;

    ofstream fp;
    fp.open(fname.str().c_str());
    // We shouldn't just fail here without at least attempting to create
    // the directory as that is probably the only problem here.
    if (!fp.is_open()) {
      ERR.FileW("PGSWrapper", __func__, fname.str().c_str());
    }

    if (stop & PGA_STOP_MAXITER) {
      WriteStopInfo_MAXITER(fp, cost);
    }

    if (stop & PGA_STOP_NOCHANGE) {
      WriteStopInfo_NOIMPROVEMENT(fp, cost);
    }

    if (stop & PGA_STOP_TOOSIMILAR) {
      WriteStopInfo_TOOSIMILAR(fp, cost);
    }

    if (stop & 8) {
      WriteStopInfo_REACHED(fp, cost);
    }

    fp << "           : Sobol seed : " << optarg->getSobolSeed() << endl;
  }

  return done ;
}
void PGSWrapper::endOfGen(PGAContext *ctx)
{

  int it = PGAGetGAIterValue(ctx);

  //TODO think about MPI version of statistics
  if (verbose != 0) {
    if (verbose < 0) {
      PGAPrintPopulation(ctx, logPGA, PGA_NEWPOP);
      fflush(logPGA);
    } else if (it % verbose == 0) {
      PGAPrintPopulation(ctx, logPGA, PGA_NEWPOP);
      fflush(logPGA);
    }
  }
  if ( (optarg->getCkpointFreq() > 0) && 
       (it > 1) && 
       ((it - 1) % optarg->getCkpointFreq()) == 0) {
    printPop();
  }

}


PGSWrapper::PGSWrapper()
{
  numEval = 0;
  timeEval = 0.0;
  popPerplaceType = PGA_POPREPL_BEST;
  setNoDuplicates = PGA_TRUE;
  fixedSeed = -1;
  restartFreq = -1;
  bPrintBestPop = false;
  bReadPop = false;
  verbose = -1;
  /* set run Name */
  runName = "run" ;
}

PGSWrapper::~PGSWrapper()
{
  if (rank == 0 && logPGA != NULL) {
    fclose(logPGA);
  }
}


void PGSWrapper::Initialise()
{

  int argc(1);
  char *argv[argc];
  *argv = (char *)"pgapack";

  if ( paraminfo.GetNumLower() != paraminfo.GetNumUpper()) {
    ERR.General("PGSWrapper", __func__,
                "Upper vector size does not equal to the Lower vector size: Upper size %d, Lower size %d \n", paraminfo.GetNumUpper(), paraminfo.GetNumLower());
  }


  int len = paraminfo.GetNumLower();

  fixedSeed = optarg->getSeed();

  verbose = optarg->getVerbose();
  SetPrintBestPop();
  int popSize = optarg->getPopSize();
  int numBest = optarg->getNumBest();
  int numGen = optarg->getNumGen();

  wrapper = this;

  Lower = paraminfo.GetLower();
  Upper = paraminfo.GetUpper();


  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  ctx = PGACreate ( &argc, argv, PGA_DATATYPE_REAL, len, PGA_MINIMIZE);

  PGASetPopSize(ctx, popSize);

  PGASetNumReplaceValue(ctx, (popSize - numBest));

  // the best genomes always copied to new generation
  PGASetPopReplaceType(ctx, popPerplaceType);

  // do NOT allow dublicates in population
  PGASetNoDuplicatesFlag(ctx, setNoDuplicates);

  double mutProb = optarg->getMutProb();
  if (mutProb <= 0) {
    mutProb = 1.0 / len;
  }
  PGASetMutationProb(ctx, mutProb);

  PGASetMaxSimilarityValue(ctx, optarg->getMaxSimilarity());

  PGASetMaxNoChangeValue(ctx, optarg->getMaxNoChange());

  PGASetStoppingRuleType(ctx, PGA_STOP_MAXITER);

  PGASetMaxGAIterValue(ctx, numGen);
  PGASetRealInitRange(ctx, Lower, Upper);

  PGASetFitnessMinType(ctx, PGA_FITNESSMIN_RECIPROCAL);
  PGASetMutationBoundedFlag(ctx, PGA_TRUE);
  PGASetMutationAndCrossoverFlag(ctx, PGA_TRUE);

  // set report frequency
 
  if (optarg->getPrintFreq() > 0) {
    PGASetPrintFrequencyValue(ctx, optarg->getPrintFreq());
  }

  restartFreq = optarg->getRestartFreq();

  if (restartFreq <= 0) {
    restartFreq = numGen;
  }
  PGASetRestartFrequencyValue(ctx, restartFreq);
  PGASetRestartFlag(ctx, PGA_TRUE);

  if (rank == 0) {
    WriteGeneralInfo();
  }

  if (fixedSeed > 0) {
    PGASetRandomSeed(ctx, fixedSeed);
  }

  if (rank == 0) {
    printf("---------------- Mutation Function -------------------\n");
  }
  if (optarg->MyMutation != NULL) {
    PGASetUserFunction(ctx, PGA_USERFUNCTION_MUTATION,    optarg->MyMutation);
    if (rank == 0) {
      printf("  *\tSet the userdefined Mutation.\n");
    }
    fflush(stdout);
  } else {
    if (rank == 0) {
      printf("  *\tSet a default Mutation.\n");
    }
    fflush(stdout);
  }

  if (rank == 0) {
    printf("--------------- CrossOver Function -------------------\n");
  }
  if (optarg->MyCrossOver != NULL) {
    PGASetUserFunction(ctx, PGA_USERFUNCTION_CROSSOVER,   optarg->MyCrossOver);
    if (rank == 0) {
      printf("  *\tSet the user defined CrossOver.\n");
    }
  } else {
    if (rank == 0) {
      printf("  *\tSet a default CrossOver\n");
    }
    if (len < 3) {
      ERR.General("PGSWrapper", __func__,
                  "Can not provide proper crossover because the optimised parameters size is too small; %d\n", len);
    }
  }

  if (rank == 0) {
    printf("-------------- End Of Generation ---------------------\n");
  }
  if (optarg->MyEndOfGen != NULL) {
    PGASetUserFunction(ctx, PGA_USERFUNCTION_ENDOFGEN,    optarg->MyEndOfGen);
    if (rank == 0) {

      printf("  *\tSet the user defined EndOfGen procedure.\n");
    }
  } else {
    if (rank == 0) {
      printf("  *\tSet the default EndOfGen.\n");
    }
    void *myEndOfGen = (void *)EndOfGen;
    PGASetUserFunction(ctx, PGA_USERFUNCTION_ENDOFGEN,    myEndOfGen);
  }

  void *myStopCond = (void *)N_StopCond;
  PGASetUserFunction(ctx, PGA_USERFUNCTION_STOPCOND,   myStopCond);

  PGASetUserFunction(ctx, PGA_USERFUNCTION_INITSTRING, (void*)PGAInit);

  void* myPrintString = (void *)MyPrintString;
  PGASetUserFunction(ctx, PGA_USERFUNCTION_PRINTSTRING, myPrintString);

  PGASetUp(ctx);

  if (rank == 0 && verbose != 0 ) {
    SetLogfile();
  }

  /* if param_set's parameter size is not zero, 
   * put those into the first generation
   */
  if (!paramsets->empty()) {
    ParamSet paramset;
    for (int i = 0; i < ctx->ga.PopSize; i++) {
      paramset = (*paramsets).at(i);
      for (int j = 0; j < len; j++) {
        PGASetRealAllele(ctx, i, PGA_OLDPOP, j, paramset.params.at(j));
      }

    }
  }

};

void PGSWrapper::SetLogfile()
{

  ostringstream logfilename;
  logfilename << optarg->getResultDir() << "/PGALog-" << runName << (unsigned)time(NULL) << ".txt" ;

  if ((logPGA = fopen(logfilename.str().c_str(), "wt")) == NULL) {
    ERR.FileW("PGSWrapper", __func__, logfilename.str().c_str());
  }


};


int PGSWrapper::Run()
{
  int iter(0);
  Initialise();
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    printf("\n+++++++++++++++++++++++ GA Run ++++++++++++++++++\n");
  }
  iter = PGSWrapper_Run();
  Destroy();
  return iter;
}

int PGSWrapper::PGSWrapper_Run()
{

  PGARun(ctx, evaluate);
  /*
   * We unconditionally print out the best population to the
   * BestPop.<num>.ga file at the end of the optimisation run.
   * This is in order to work correctly with the loop.sh mechanism
   * for splitting an optimisation into several runs to avoid a long
   * run being bumped from a cluster.
   * Previously this was conditional on the printBestPop configuration,
   * which until now was never implemented in any case, however
   * commenting out the condition makes this clear that this is not
   * optional. - allan
   */ 
  // if (IsPrintBestPop()) {
  printPop();
  // }
  return  PGAGetGAIterValue(ctx);
}


void PGSWrapper::printPop()
{
  if (rank == 0) {
    FILE *resFile = NULL;

    ostringstream fname;

    /* this file goes to checkpoint directly */
    int iter = PGAGetGAIterValue(ctx);
    fname << optarg->getCkpointDir() << "/BestPop." 
          << optarg->getCkpointStartNum() + iter << ".ga";
    // Here we should actually check if the ckpoint directory exists
    // and if not create it, rather than just fail.
    if ((resFile = fopen(fname.str().c_str(), "wt")) == NULL) {
      ERR.FileW("PGSWrapper", __func__, fname.str().c_str());
    }
    //int n;
    //n=PGAGetPopSize(ctx);
    PGAPrintPopulation(ctx, resFile, PGA_OLDPOP);

    fclose(resFile);
  }
}

void PGSWrapper::Destroy()
{
  if (rank == 0) {

    ostringstream costFname;
    int pop = PGA_OLDPOP; //PGAGetGAIterValue(ctx);
    int p = PGAGetBestIndex(ctx, pop);
    double c = PGAGetFitness(ctx, p, pop);
    int iter = PGAGetGAIterValue(ctx);
    costFname << optarg->getResultDir() << "/BestCost.txt." 
              << optarg->getCkpointStartNum() + iter;
    printf("-------------------------- FILE INFO ----------------------------\n");
    printf("  *\tThe Best Cost parametersets with Cost %12.8le saved to file %s\n", c, costFname.str().c_str());
    FILE *BestCostfile = NULL;
    if ((BestCostfile = fopen(costFname.str().c_str(), "w")) == NULL) {
      ERR.FileW("PGSWrapper", __func__, costFname.str().c_str());
    } else {
      fprintf(BestCostfile, "# Pop size %d; NumGen %d; NumBest %d\n", optarg->getPopSize(), optarg->getNumGen(), optarg->getNumBest());
      PGAPrintIndividual ( ctx, BestCostfile,  p, pop );
    }
    fclose(BestCostfile);
    SetParamSet(ctx);

  }
  PGADestroy(ctx);
  if (rank == 0) {
    printf("++++++++++++++++++++++ Finished  GA +++++++++++++++++++++++\n");
  }
}
double PGSWrapper::reflect(double initPoint, double step, double boundary)
{
  double res, finalPoint;
  finalPoint = initPoint + step;
  res = 2.0 * boundary - finalPoint;
  return res;
}
void PGSWrapper::summarise()
{
  PGAPrintContextVariable(ctx, stdout);
}

vector<double> PGSWrapper::GetTheBest()
{
  vector<double> val;
  int pop = PGA_NEWPOP; //PGAGetGAIterValue(ctx); // AZ SHOULD BE OLD ..? Mar 19
  int p = PGAGetBestIndex(ctx, pop);
  int len = PGAGetStringLength(ctx);
  for (int row = 0; row < len; row++) {
    val.push_back(PGAGetRealAllele(ctx, p, pop, row));
  }
  return val;
}




double PGSWrapper::GetScore(PGAContext *ctx, int p, int pop)
{


  time_t start, end;

  size_t len = PGAGetStringLength(ctx);
  if (len > paraminfo.GetNumUpper()) {
    ERR.General("PGSWrapper", __func__, "length of string %d > than length of param array %d\n", len, paraminfo.GetNumUpper());
  }

  vector<double>params;
  for (size_t i = 0; i < len; i++) {
    params.push_back(PGAGetRealAllele(ctx, p, pop, i));
  }

  time (&start);

  costfunction->SetParameters(params);
  double cost = costfunction->GetCost();

  time (&end);
  numEval++;
  timeEval += difftime (end, start);
  return cost;
}

void PGASOBOL::Initialise()
{

  PGSWrapper::Initialise();

}


int PGASOBOL::Run()
{

  return PGSWrapper::Run();
}

void PGASOBOL::endOfGen(PGAContext *ctx)
{
  PGSWrapper::endOfGen(ctx);
}
