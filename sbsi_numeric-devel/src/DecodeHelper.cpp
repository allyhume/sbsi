#include <DecodeHelper.h>

string convertString (const string toChange);


/**
 * Recursively navigates ASTNodes and replaces C++keywords.
 */

DecodeHelper::DecodeHelper()
{
  CPP_KEYWORDS.resize(CPPKeyWordNum);
  load_cppkeywords();
};

void DecodeHelper::load_cppkeywords()
{
#include <CPPKeywords.h>
};

/*
 * Iterates through identifiable object in model - if any match a C++ reserved word, replaces ID with ID + '_MODEL'
 * suffix.
 * Replaces species IDs, compartment IDs, species Refs in reactants, products and modifiers; parameter IDs (global and in kinetic laws)
 *, events, rate rules, initial assignments, function definitions and constraints.
 *\return \em true if > 0  identifiers have been modified, \em false if NO identifiers have been modified.
 */


bool DecodeHelper::searchAndReplaceReservedWords (Model * model)
{

  bool rc = false; // return value init

  // species
  for (size_t i = 0;  i < model->getNumSpecies(); i++) {
    Species * s = model->getSpecies(i);
    if (s->isSetId() ) {
      string name = s->getId();
      if (isKeyword(name)) {
        s->setId(convertString(name));
        rc = true;
      }
    }
    if (isKeyword (s->getCompartment())) {
      s->setCompartment(convertString(s->getCompartment()));
    }
  }

  //compartments
  for (size_t i = 0; i < model->getNumCompartments(); i++) {
    Compartment * c = model->getCompartment(i);
    if (c->isSetId() ) {
      string name = c->getId();
      if (isKeyword(name)) {
        c->setId(convertString(name));
        rc = true;
      }
    }
  }
  // convert parameter ids
  for (size_t i = 0; i < model->getNumParameters(); i++) {
    Parameter * p = model->getParameter(i);
    if (isKeyword(p->getId())) {
      p->setId(convertString(p->getId()));
      rc = true;
    }
  }

  // convert maths in rules
  for (size_t i = 0; i < model->getNumRules(); i++) {

    //convert variable name
    Rule * r = model->getRule(i);
    if (r->isAssignment() || r->isRate()) {
      string var = r->getVariable();
      if ( isKeyword(var)) {
        r->setVariable(convertString(var));
        rc = true;
      }
    }
    //convert ASTmath in rule
    if (r->isSetMath()) {
      const ASTNode *math = r->getMath();
      bool changed = changeMathIds(math, false);
      if (rc == false) {
        rc = changed;
      }

    }
  }

  for (size_t i = 0; i < model->getNumConstraints(); i++) {
    Constraint * c = model->getConstraint(i);
    if (c->isSetMath()) {
      bool mathsWasChanged  = changeMathIds(c->getMath(), false);
      if (rc == false) {
        rc = mathsWasChanged;
      }
    }
  }

  for (size_t i = 0; i < model->getNumInitialAssignments(); i++) {
    InitialAssignment * c = model->getInitialAssignment(i);

    if (isKeyword(c->getSymbol())) {
      c->setSymbol(convertString(c->getSymbol()));
      rc = true;
    }
    if (c->isSetMath()) {
      bool mathsWasChanged  = changeMathIds(c->getMath(), false);
      if (rc == false) {
        rc = mathsWasChanged;
      }
    }
  }

  for (size_t i = 0; i < model->getNumEvents(); i++) {
    Event * c = model->getEvent(i);
    if (isKeyword(c->getId())) {
      c->setId(convertString(c->getId()));
      rc = true;
    }
    if (c->isSetTrigger() && c->getTrigger()->isSetMath()) {
      //      const ASTNode *math = c->getTrigger()->getMath();
      bool mathsWasChanged  = changeMathIds(c->getTrigger()->getMath(), false);
      if (rc == false) {
        rc = mathsWasChanged;
      }
    }
    if (c->isSetDelay() && c->getDelay()->isSetMath()) {
      //      const ASTNode *math = c->getDelay()->getMath();
      bool mathsWasChanged  = changeMathIds(c->getDelay()->getMath(), false);
      if (rc == false) {
        rc = mathsWasChanged;
      }
    }
    for (size_t j = 0; j < c->getNumEventAssignments(); j++) {
      EventAssignment *ea = c->getEventAssignment(j);
      if (isKeyword(ea->getVariable())) {
        ea->setVariable(convertString(ea->getVariable()));
      }
      if (ea->isSetMath()) {
        bool mathsWasChanged  = changeMathIds(ea->getMath(), false);
        if (rc == false) {
          rc = mathsWasChanged;
        }
      }

    }

  }

  for (size_t i = 0; i < model->getNumFunctionDefinitions(); i++) {
    FunctionDefinition * c = model->getFunctionDefinition(i);
    if (isKeyword(c->getId())) {
      c->setId(convertString(c->getId()));
      rc = true;
    }
    if (c->isSetMath()) {
      bool mathsWasChanged  = changeMathIds(c->getMath(), false);
      if (rc == false) {
        rc = mathsWasChanged;
      }
    }
  }



  // handle species /parameter ids in reactions & kinetic laws
  for (size_t i = 0;  i < model->getNumReactions(); i++) {
    Reaction* r = model->getReaction(i);
    if (r->isSetKineticLaw()) {
      KineticLaw * kl = r->getKineticLaw();
      if (kl->isSetMath()) {
        const ASTNode * math = kl->getMath();
        bool mathsWasChanged  = changeMathIds(math, false);
        if (rc == false) {
          rc = mathsWasChanged;
        }
      }
      for (size_t i = 0; i < kl->getNumParameters(); i++ ) {
        string paramId = kl->getParameter(i)->getId();
        if (isKeyword(paramId)) {
          kl->getParameter(i)->setId(convertString(paramId));
        }

      }
    }
    for (size_t i = 0; i < r->getNumReactants(); i++ ) {
      string specRef = r->getReactant(i)->getSpecies();
      if (isKeyword(specRef)) {
        r->getReactant(i)->setSpecies(convertString(specRef));
      }
    }

    for (size_t i = 0; i < r->getNumProducts(); i++ ) {
      string specRef = r->getProduct(i)->getSpecies();
      if (isKeyword(specRef)) {
        r->getProduct(i)->setSpecies(convertString(specRef));
      }
    }
    for (size_t i = 0; i < r->getNumModifiers(); i++ ) {
      string specRef = r->getModifier(i)->getSpecies();
      if (isKeyword(specRef)) {
        r->getModifier(i)->setSpecies(convertString(specRef));
      }
    }
  }

  return rc;
}

/*
 * Recursively alters ASTNodes
 */
bool DecodeHelper::changeMathIds (const ASTNode * root, bool isChanged)
{
  for (size_t i = 0; i < root->getNumChildren(); i++ ) {
    ASTNode *child = root->getChild(i);
    if (child->getName() != NULL && isKeyword( string(child->getName()))) {
      child->setName((convertString(child->getName())).c_str());
      isChanged = true;
    }
    changeMathIds(child, isChanged); // recurse through children

  }
  return isChanged;
}


string convertString(const string toChange)
{
  return toChange + "_MODEL";
}

/*
 * Boolean test for if string is a C++ keyword
 */
bool DecodeHelper::isKeyword (const string toTest)
{
  if (toTest.empty()) {
    return false;
  }
  vector<string>::iterator it_ky = CPP_KEYWORDS.begin();
  for (it_ky = CPP_KEYWORDS.begin(); it_ky != CPP_KEYWORDS.end(); ++it_ky) {
    if ( toTest == *(it_ky) ) {
      return true;
    }
  }
  return false;
}

// returns empty string if node is not actually piecewise
string DecodeHelper::getPiecewiseString (const ASTNode * pieceWiseMath, string var )
{
  if (!pieceWiseMath->isPiecewise()) {
    return "";
  }
  string rc ("if (");
  //iterate over pairs of assignment/condition nodes
  for (size_t i = 0 , j = 1; j < pieceWiseMath->getNumChildren(); i += 2, j += 2) {
    if ( i >= 2) {
      rc.append("else if (");
    }
    const ASTNode *assignment = pieceWiseMath->getChild(i);
    const ASTNode *conditionalNode = pieceWiseMath->getChild(j);
    string conditionalString = getString(const_cast<ASTNode *> (conditionalNode));
    rc.append(conditionalString);
    rc.append(") {\n\t\t");
    rc.append(var).append(" = ").append(SBML_formulaToString(assignment)).append("; \n\t\t}");

  }
  // 'otherwise' will be an  'else' staetment if there is an odd number of children
  if ((pieceWiseMath->getNumChildren() % 2) > 0 ) {
    ASTNode *otherwise = pieceWiseMath->getRightChild();
    rc.append ("else {\n\t\t").append(var).append(" = ").append(SBML_formulaToString(otherwise)).append("; \n\t\t}");

  }
  return rc;
}


string  DecodeHelper::getString (ASTNode * logicalOrRelationalNode)
{
  if (logicalOrRelationalNode == NULL ||
      !logicalOrRelationalNode->isLogical() && !logicalOrRelationalNode->isRelational()) {
    return "";
  }
  //handle emptyNode
  if (logicalOrRelationalNode->getNumChildren() == 0) {
    return "";
  }
  vector<ASTNode *> bottomlogEqs;
  map<ASTNode *, ASTNode *> child2ParentMap;
  populateBottomLogicalNodes(logicalOrRelationalNode, bottomlogEqs, child2ParentMap);

  popList(bottomlogEqs, child2ParentMap);


  if (logicalOrRelationalNode->getUserData() != NULL) {
    return string ( ((char  *)logicalOrRelationalNode->getUserData()));
  } else {
    return string ("empty");


  }
}



//TODO check this with map, inequality etc
void DecodeHelper::populateBottomLogicalNodes(ASTNode * root, vector <ASTNode *> &bottomlogEqs, map<ASTNode *, ASTNode *> &child2ParentMap)
{
  bool hasAnyLogicalChildren(false);

  child2ParentMap.insert(pair<ASTNode *, ASTNode *>(root, NULL));

  for (size_t i = 0; i < root->getNumChildren(); i++ ) {
    ASTNode * node =  root->getChild(i);
    child2ParentMap.insert(pair<ASTNode *, ASTNode *>(node, root));

    if (node->isLogical()) {
      hasAnyLogicalChildren = true;
      populateBottomLogicalNodes(node, bottomlogEqs, child2ParentMap);
    }
  }
  if (!hasAnyLogicalChildren) {
    bottomlogEqs.push_back(root);
  }



}

void DecodeHelper::popList(vector<ASTNode *> &bottomlogEqs,  map<ASTNode *, ASTNode *> &child2ParentMap)
{
  vector <ASTNode *> parents;
  for (size_t i = 0; i < bottomlogEqs.size(); i++) {
    ostringstream oss;
    ASTNode * bottom = bottomlogEqs[i];
    if (bottom->isRelational()) {
      createInequality(oss, bottom);
      bottom->setUserData((void *)oss.str().c_str());
    } else {
      oss << "(" ;
      for ( size_t j = 0; j < bottom->getNumChildren(); j++) {
        ASTNode * ineq = bottom->getChild(j);
        if (ineq->getUserData() != NULL) {
          string str = string((char*)ineq->getUserData());
          oss <<  str;
          if (ineq != bottom->getRightChild()) {
            oss << " " << getSymbolFor(bottom) << " "; // ineq operator
          }
        } else {
          createInequality(oss, ineq);
          if (ineq != bottom->getRightChild()) {
            if (child2ParentMap[ineq] != NULL) {
              oss << " " + getSymbolFor(bottom) << " "; // logical operator
            }
          }
        }
      }
      oss << ")";
    }
    bottom->setUserData((void *)oss.str().c_str());

    if (child2ParentMap[bottom] != NULL) {

      // iterator to vector element:
      vector<ASTNode *>::iterator it =
        find (parents.begin(), parents.end(), child2ParentMap[bottom]);
      if (it == parents.end()) {
        parents.push_back(child2ParentMap[bottom]);
      }
    }
  }
  //guard for top of tree, to prevent infinite recursion.
  if (!parents.size() == 0) {
    popList(parents, child2ParentMap);
  }


}

void DecodeHelper::createInequality(ostream &sb, const ASTNode * ineq)
{
  // this section is for populating inequliity
  for (size_t i = 0; i < ineq-> getNumChildren(); i++) { // 2 nodes  f
    ASTNode * math = ineq->getChild(i);
    sb << SBML_formulaToString(math);
    if (math != ineq->getRightChild()) {
      sb << " " << getSymbolFor(ineq) << " "; // ineq operator
    }
  }
}

string DecodeHelper::getSymbolFor (const ASTNode * node)
{
  ASTNodeType_t type = node->getType();
  switch (type) {

  case  AST_RELATIONAL_GT:
    return " > " ;
  case  AST_RELATIONAL_LT:
    return " < " ;
  case  AST_RELATIONAL_LEQ:
    return " <= " ;
  case  AST_RELATIONAL_GEQ:
    return " >= " ;
  case  AST_RELATIONAL_EQ:
    return " == ";
  case  AST_RELATIONAL_NEQ:
    return " != ";
  case  AST_LOGICAL_AND:
    return "&&";
  case  AST_LOGICAL_OR :
    return " || ";
  case  AST_LOGICAL_NOT :
    return " != ";
  default:
    return " unknown " ;
  }
}
