#include <cfloat>

using namespace std;
#include <algorithm>
#include <ERR.h>
#include <FileReader.h>


void TDCParamInfoFileReader::Read(string file)
{
  fstream f;
  f.open(file.c_str(), ios::in);

  SetTokenDelimiters("\t");
  SetWhitespaces(" \r");

  if (!f) {
    ERR.FileR("FileReaderParamInfo", __func__, file.c_str());
  }

  int count = 0;
  string paramname;
  int index;

  /* the paraminfo file Format should be */

  /* (index) param name Upper Lower (initial step)  */

  /* index must start from 0 */

  while (!f.eof()) {

    string line = GetLine(f);

    vector<string> columns = Tokenize(line);

    if (columns.size() > 2) {

      count++; // line counter count stars from 1

      if (columns.size() == 3) {

        /* param name Lower Upper   */
        if (IsWord(columns.front())) {
          /* this case is  param_name Lower Upper   */
          paramname = columns.front();
          double lo = atof(columns.at(1).c_str());
          double up = atof(columns.at(2).c_str());
          if (lo < up) {
            paraminfo.AddAParamName(columns.front());
            paraminfo.AddALower(atof(columns.at(1).c_str()));
            paraminfo.AddAUpper(atof(columns.at(2).c_str()));
          } else {
            if (lo - up < DBL_EPSILON) {
              paraminfo.AddAUnOptimiseParamName(columns.front());
              paraminfo.AddAUnOptimiseParamValue(lo);
            } else {
              ERR.General("TDCFileReaderParamInfo", __func__, "Upper limit :%f is smaller than Lover limit:%f.\n", up, lo);
            }
          }

        } else {
          /* index lower upper */
          if (IsNum(columns.front())) {
            //int index = atoi(columns.front().c_str());
            if ((!IsNum(columns.at(1))) || (!IsNum(columns.at(2))) ) {
              ERR.General("TDCFileReaderParamInfo", __func__, "Columns 1 & 2 of %s should be numeric\n", line.c_str());
            }
            double lo = atof(columns.at(1).c_str());
            double up = atof(columns.at(2).c_str());
            if (lo < up) {
              paraminfo.AddAParamName(columns.front());
              paraminfo.AddALower(lo);
              paraminfo.AddAUpper(up);
            } else {
              if (lo - up < DBL_EPSILON) {
                paraminfo.AddAUnOptimiseParamName(columns.front());
                paraminfo.AddAUnOptimiseParamValue(lo);
              } else {
                ERR.General("TDCFileReaderParamInfo", __func__, "Upper limit :%f is smaller than Lover limit:%f.\n", up, lo);
              }
            }
          }
        }

      } else {
        if (columns.size() == 4) {

          double lo(0), up(0), iv(0);

          /* paramname  Lower Upper initialStep */
          /* or */
          /*index paramname Lower Upper  */
          if (IsWord(columns.front())) {

            if ( (!IsNum(columns.at(1))) || (!IsNum(columns.at(2))) || (!IsNum(columns.at(3))) ) {
              ERR.General("TDCFileReaderParamInfo", __func__, "Columns 2,3, & 4 of %s should be numeric.\n", line.c_str());
            }

            paramname = columns.front();
            lo = atof(columns.at(1).c_str());
            up = atof(columns.at(2).c_str());
            iv = atof(columns.at(3).c_str());
          } else {
            if (IsNum(columns.front())) {
              index = atoi(columns.front().c_str());
              if (IsWord(columns.at(1))) {
                paramname = columns.at(1);
              } else {
                ERR.General("TDCFileReaderParamInfo", __func__, "No parameter ID for param index %d.\n", index);
              }
              if ((!IsNum(columns.at(2))) || (!IsNum(columns.at(3))) ) {
                ERR.General("TDCFileReaderParamInfo", __func__, "Columns 2 & 3 of %s should be numeric\n", line.c_str());
              }

              lo = atof(columns.at(2).c_str());
              up = atof(columns.at(3).c_str());
            }
          }

          if (lo < up) {
            paraminfo.AddAParamName(paramname);
            paraminfo.AddALower(lo);
            paraminfo.AddAUpper(up);
            if (IsWord(columns.front())) {
              paraminfo.AddAInitialStep(iv);
            }
          } else {
            if (lo - up < DBL_EPSILON) {
              paraminfo.AddAUnOptimiseParamName(columns.front());
              paraminfo.AddAUnOptimiseParamValue(lo);
            } else {
              ERR.General("TDCFileReaderParamInfo", __func__, "Upper limit :%f is smaller than Lover limit:%f.\n", up, lo);
            }
          }

        } else {
          if (columns.size() == 5) {

            /* index paramname Lower Upper initial_step  */
            /*             OR                    */
            /* paramname index Lower Upper initial_step */

            for (int i = 0; i < 2; i++) {
              if (IsWord(columns.at(i))) {
                paramname = columns.at(i);
              }
              if (IsNum(columns.at(i))) {
                index = atoi(columns.at(i).c_str());
              }
            }
            double lo = atof(columns.at(2).c_str());
            double up = atof(columns.at(3).c_str());
            double iv = atof(columns.at(4).c_str());

            if (lo < up) {
              paraminfo.AddAParamName(paramname);
              paraminfo.AddALower(lo);
              paraminfo.AddAUpper(up);
              paraminfo.AddAInitialStep(iv);
            } else if (lo - up < DBL_EPSILON) {
              paraminfo.AddAUnOptimiseParamName(paramname);
              paraminfo.AddAUnOptimiseParamValue(lo);
            } else {
              ERR.General("TDCFileReaderParamInfo", __func__, "Upper value:%lf is smaller than Lover value:%lf.\n", up, lo);
            }

          } // if coulmns.size() ==5
        } // else

      } // else
    } //column.size()>2
  } //while


}


