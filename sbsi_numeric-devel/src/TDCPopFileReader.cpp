
using namespace std;

#include <FileReader.h>
#include <ERR.h>

void TDCPopFileReader::Read(string file)
{
  fstream f;
  f.open(file.c_str(), ios::in);

  SetTokenDelimiters("\t");
  SetWhitespaces(" \r");

  if ( !f ) {
    ERR.FileR("FileReaderPop", __func__, file.c_str());
  }

  int set = 0;
  int nset = 0;

  /* the pop file Format should be */

  /* index Cost Fitness etc  */
  /*  param_1 param_2 param_3 ... param_{PARAMNO} Cost fittness etc, */
  /* empty line */
  /* index Cost Fitness etc  */
  /*  param_1 param_2 param_3 ... param_{PARAMNO} Cost fittness etc, */

  int len(0);

  while ( !f.eof() ) {
    string line = GetLine(f);
    vector<string> columns = Tokenize(line);
    if (columns.size() == 1) {
      if (line.find("=") == string::npos) {
        nset++;
        PopSets.resize(nset);
        set = nset - 1;
        len = 0;
        PopSets.at(set).g_info.push_back(line);
      } else {
        PopSets.at(set).g_info.push_back(line);
      }
    } else {
      if (columns.size() > 1) {
        for (size_t i = 0; i < columns.size(); i++) {
          if (IsNum(columns.at(i))) {
            PopSets.at(set).params.push_back(atof(columns.at(i).c_str()));
            len++;
          }
        }
      }
    }
  }
}








