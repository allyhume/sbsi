#include <ERR.h>
#include "UserModelCostFunction.h"

UserModelCostFunction& UserModelCostFunction::setUserModelCostFunction(string uname)
{
     UserModelCostFunction *ucf;
     if(uname == "Conserve"){
       ucf = new Conserve();
       return *ucf;
     } 
     ERR.General("ModelCostFunction",__func__, "Bad ModelCostFunction Type");
}
double Conserve::GetCost(){

  double cost(0);
  Costs.clear();

  /* Done in ModelCostFunction::SetDataSet()

  Model->InputModel();
  SetUnOptimiseParam();
  SetParameters(ParamValues);

  Model->setExpTInit(0);
  Model->setOutInterval(0.01);
  */

   for(size_t i=0; i<dataset_s->size(); i++){

     const DataSet *tmp=GetADataSet(i);

//     DoMyCheck();

     SetDataSet(i);

     Solve();  

     double c =getcost();

     Costs.push_back(c);

     cost+=c;
   }

  return cost;
}
double Conserve::getcost(){

 /* A + B + C = 1000*/

  double cost(0);

  if(TSeries->IsTimeSeriesEmpty()) return GetMAXCost()*1.0e+12;

  vector< vector<double> >results;
  results=TSeries->GetTimeSeries();
  
  cost= (1000 - Model->calVarVec(2,results).front()) * (1000 - Model->calVarVec(2,results).front());

  cost +=(1000 - Model->calVarVec(2, results).back()) * (1000 - Model->calVarVec(2,results).back());

  cost *=0.5;

  return cost;
}

