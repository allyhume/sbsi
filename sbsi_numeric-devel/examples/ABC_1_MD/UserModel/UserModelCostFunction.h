#include <cmath>
using namespace std;

#include<ModelCostFunction.h>

#ifndef __UserModelCostFunction_h__
#define __UserModelCostFunction_h__


class UserModelCostFunction: public ModelCostFunction
{
public:

    static UserModelCostFunction& setUserModelCostFunction(const string name);

};

class Conserve: public UserModelCostFunction
{

public:

	Conserve(){};
	~Conserve(){};

	virtual double GetCost() ;
        virtual double getcost();

};
#endif

