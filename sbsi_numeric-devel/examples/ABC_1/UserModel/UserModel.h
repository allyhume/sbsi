using namespace std;

#ifndef __abc_1_h__
#define __abc_1_h__

#include <cModel.h>

class  abc_1:public cModel {
public: 

  abc_1(){};

   /********** Model has a compartment *********/
   /********** the compartment 0         ********/
   /*                Name; Default 	 		*/
   /*                  Id; Default   	*/
   /*  Spatial dimensions; 3			*/
   /*                Size; 1			*/
   /*            Constant; 1	 	 	*/
   /**********************************************/

   /*********** Model has 3 states. *******/

   void inputModel(void) ;


   /*********** Model has 2 Rules. *******/
   /*********** Model has 2 Reactions. *******/

   void getRHS(double * y_in, double t, double* yout); 

  vector<double> calVarVec(int iv, vector<vector<double> > states); 
  void calCoef(); 
 /************ Parameter list ****************/  
	double k1;
	double k2;
	double k_1;
	double V_flow_1_;
	double V_flow_2_;
	double Default;
};
static cModel* getModel(){return new abc_1();}

#endif
