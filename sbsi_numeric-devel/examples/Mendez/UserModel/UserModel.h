using namespace std;

#ifndef __TestModel_h__
#define __TestModel_h__

#include <cModel.h>

class  TestModel:public cModel {
public: 

  TestModel(){};

   /********** Model has a compartment *********/
   /********** the compartment 0         ********/
   /*                  Id; Compartment_1   	*/
   /*  Spatial dimensions; 3			*/
   /*                Size; 1			*/
   /*            Constant; 1	 	 	*/
   /**********************************************/

   /*********** Model has 8 states. *******/

   void inputModel(void) ;


   /*********** Model has 8 Rules. *******/

   void getRHS(double * y_in, double t, double* yout); 
   void update_assignment_variables(vector<vector<double> > * results);

  vector<double> calVarVec(int iv, vector<vector<double> > states); 
  void calCoef(); 
 /************ Parameter list ****************/  
	double V1;
	double Ki1;
	double ni1;
	double Ka1;
	double na1;
	double k1;
	double V2;
	double Ki2;
	double ni2;
	double Ka2;
	double na2;
	double k2;
	double V3;
	double Ki3;
	double ni3;
	double Ka3;
	double na3;
	double k3;
	double V4;
	double K4;
	double k4;
	double V5;
	double K5;
	double k5;
	double V6;
	double K6;
	double k6;
	double kcat1;
	double Km1;
	double Km2;
	double kcat2;
	double Km3;
	double Km4;
	double kcat3;
	double Km5;
	double Km6;
	double p;
	double s;
	double Compartment_1;
};
static cModel* getModel(){return new TestModel();}

#endif
