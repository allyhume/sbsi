using namespace std;

#ifndef __CirCl_h__
#define __CirCl_h__

#include <cModel.h>

class  CirCl:public cModel {
public: 

  CirCl(){};

   /********** Model has a compartment *********/
   /********** the compartment 0         ********/
   /*                Name; first 	 		*/
   /*                  Id; first   	*/
   /*  Spatial dimensions; 2			*/
   /*                Size; 1			*/
   /*            Constant; 1	 	 	*/
   /**********************************************/

   /*********** Model has 3 states. *******/

   void inputModel(void) ;


   /*********** Model has 3 Rules. *******/

   void getRHS(double * y_in, double t, double* yout); 
   void update_assignment_variables(vector<vector<double> > * results);

  vector<double> calVarVec(int iv, vector<vector<double> > states); 
  void calCoef(); 
 /************ Parameter list ****************/  
	double Vs;
	double K;
	double n;
	double Vm;
	double Km;
	double ks;
	double Vd;
	double Kd;
	double k1;
	double k2;
	double first;
};
static cModel* getModel(){return new CirCl();}

#endif
