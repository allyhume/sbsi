//#include <vector>
//#include <string>

using namespace std;

#ifndef __UserModel_h__
#define __UserModel_h__

#include <cModel.h>

class  UserModel:public cModel {
public: 

  UserModel(){};

  void inputModel(void) ;


  void getRHS(double * y_in, double t, double* yout); 

  vector<double> calVarVec(int iv, vector< vector<double> >); 

  void calCoef(); 

  void setBoundary();

  void setConstraints();

  void setInitialGuess();

  /* parameter list*/
  double ONE;
  double TWO;
  double PI;
  double PT5;
  double PT25;
  double E;
  double lb_x1;
  double ub_x1;
  double lb_x2;
  double ub_x2;
 
  //vector<double> c; // constraints, it will go to ModelArg
};
static cModel* getModel(){return new UserModel();}

#endif
