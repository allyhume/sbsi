#!/bin/sh 

mydir=$PWD
prefix=$1
echo $prefix

result_dir=$mydir/UserModel/results

exec_file=$mydir/${prefix}_KINSOL.exe
model_name=UserModel

functiontol=0.0001
steptol=1.0e-5
mset=1
strategy=1
outfile=Kinsol.dat

#KIN_NONE Full 0 Newton Step
#KIN_LINESEARCH 1 Line Search


echo $exec_file \
	$model_name \
	$functiontol \
	$steptol \
	$mset \
	$strategy \
        $result_dir/$outfile 

##
#execute
##
$exec_file \
	$model_name \
	$functiontol \
	$steptol \
	$mset \
	$strategy \
        $result_dir/$outfile 
 




