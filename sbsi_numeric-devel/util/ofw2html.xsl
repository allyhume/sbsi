<?xml version="1.0"?>


<!--
 This stylesheet converts   an Ofwconfig.xml file to an HTML representation.
 
 The stylesheet can be configured to generate links to the file resources referred
  to in the configuration file, if the ofwconfig file is in the same folder as these
   resources.
   
   To generate links, run this stylesheet with the parameter 'isInConfigFolder' set to true.
   
   To just output the filenames as strings, run this stylesheet with the parameter
     'isInConfigFolder' set to false. This is the default.

 -->
 

<xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
>
<xsl:param name="isInConfigFolder">false</xsl:param>

<!-- Function that surrounds its argument in an '< a href='fname'>fname</a>' -->
<xsl:template name="name2Link">
<xsl:param name="fname" value="'NotAvailable'" />
        <a href="./{$fname}"><xsl:value-of select="$fname"/> </a>
</xsl:template>

<!-- Function that converts its argument string to a link
  if the 'isInConfigFolder' is true, otherwise just outputs the argument unchanged. -->
<xsl:template name="convertToLinkIfConfigFolderElseDefaultString">
<xsl:param name="file" value="'Not Found'" />
     <xsl:choose>
  	 <xsl:when test="$isInConfigFolder='true'">
  	 
        	 	<xsl:call-template name="name2Link">
        	 	  <xsl:with-param name="fname" select="$file"></xsl:with-param>
        	 	</xsl:call-template>	
         	 </xsl:when>
         	 <xsl:otherwise>
         	  <xsl:value-of select="$file"></xsl:value-of>
         	 </xsl:otherwise>
        </xsl:choose>
</xsl:template>



<xsl:template match="/">
  
  <html>
   <head>
    <style type="text/css">
     .small {
     	font-size: 10pt;
     }
     h2,h3 {
     color: navy;
     }
     table {
       border: 1;
       padding: 5px;
       border-collapse: collapse;
     }
     th,td {
       border: 1px solid black;
       padding: 5px;
     }
     .tblColTitle {
       background-color: #9acd32;
     }
     p.centred {
      text-align: center;
     }
    </style>
   </head>
  <body>
     
  	<h1><a name="top"></a> SBSI Numerics configuration </h1>
  	<ul>
  		<li><a href="#solver">Solver Configuration</a></li>
  		<li><a href="#setup">Setup Configuration</a></li>
  		<li><a href="#opt">Optimization Configuration</a></li>
  	</ul>
  	<xsl:apply-templates/> 
  	<p class="centred"><a href="#top">Back to top</a></p>
  </body>
  </html>
</xsl:template>

<xsl:template match="Solver">
   <h2 ><a name="solver"></a>Solver Configuration</h2>
    <table >
    <tr class = "tblColTitle">
		   <th>Argument</th>
		   <th>Value</th>
	</tr>
	<xsl:for-each select="*">
		<tr><td><xsl:value-of select="local-name()"/></td><td><xsl:value-of select="current()"/></td></tr>
	</xsl:for-each>
	

     </table>
</xsl:template>

<xsl:template match="Setup">
   <h2 ><a name="setup"></a>Setup configuration</h2>
   <table >
    <tr class = "tblColTitle">
		   <th>Argument</th>
		   <th>Value</th>
	</tr>
	<tr> <td>MaxCost</td><td><xsl:value-of select="MaxCost"/></td> </tr>
  	<tr> <td>MaxRandomNumberGenerator</td><td><xsl:value-of select="MaxRandomNumberGenerator"/></td> </tr>
  	<tr> <td> SetupType</td><td><xsl:value-of select="SetupType"/></td> </tr>
  	<tr> <td> Initial Parameter File</td><td>
 	<xsl:call-template name="convertToLinkIfConfigFolderElseDefaultString">
        	 	  <xsl:with-param name="file" select="ParameterInitialFile"></xsl:with-param>
      </xsl:call-template>	        
  	</td> </tr>
    </table>
   	<xsl:apply-templates select="ModelCostFunction"/>
</xsl:template>

<xsl:template match="Optimiser">
<h2> <a name="opt"></a>Optimisation configuration</h2>
   <xsl:choose>
    <xsl:when test="OptimiserType = 'DirectSearch'">
   		<p>This is configured to run as a  Direct (simulated annealing) search.</p>
   	</xsl:when>
   	<xsl:when test="count(PSONumGen) = 0 and OptimiserType = 'PGA'">
   		<p>This is configured to run as a  pure Genetic Algorithm search.</p>
   	</xsl:when>
   	<xsl:when test="count(PSONumGen) = 1 and PSONumGen &gt; NumGen">
   		<p>This is configured to run as a  pure Particle Swarm search.</p>
   	</xsl:when>
   	<xsl:when test="count(PSONumGen) = 1 and PSONumGen &lt;= NumGen and PSONumGen &gt;0">
   		<p>This is configured to run as mixed Particle Swarm / Genetic Algorithm search.</p>
   	</xsl:when>
   	<xsl:otherwise><p>This is configured to run as a  pure Genetic Algorithm search.</p></xsl:otherwise>   
   </xsl:choose>
 
  
   <table >
    <tr class = "tblColTitle">
		   <th>Argument</th>
		   <th>Value</th>
	</tr>
	<!-- just write out all simple name/value pairs, ignoring elements with children -->
	<xsl:for-each select="*[not(local-name()='ModelCostFunction') 
	        and not(local-name() = 'ParameterInitialFile')]">
		<tr><td><xsl:value-of select="local-name()"/></td><td><xsl:value-of select="current()"/></td></tr>
	</xsl:for-each>
	
	<tr> 
	<td> Initial Parameter File</td>
	<td>
 	<xsl:call-template name="convertToLinkIfConfigFolderElseDefaultString">
        	 	  <xsl:with-param name="file" select="ParameterInitialFile"></xsl:with-param>
      </xsl:call-template>	        
  	</td>
  	 </tr>
    </table>
   	<xsl:apply-templates select="ModelCostFunction"/>
</xsl:template>

<xsl:template match="ModelCostFunction">
    <h3> Model Cost Function</h3>
    <xsl:if test="count(Weight) = 1">
  			<p><xsl:value-of select="Type"/> has weight: <xsl:value-of select="Weight"/></p>
  	</xsl:if>
	
	  <xsl:if test="Type = 'X2Cost'">
	  <table>
		 <tr class = "tblColTitle">
		   <th>File name</th>
		   <th>Interval</th>
		   <th>Start time</th>
		   <th>Scale type</th>
		   <th>Value type</th>
		   </tr>
         <xsl:for-each select="DataSetConfig">
           <tr>
         	<td> 
        	
  	<xsl:call-template name="convertToLinkIfConfigFolderElseDefaultString">
        	 	  <xsl:with-param name="file" select="FileName"></xsl:with-param>
      </xsl:call-template>	
         	 </td>
         	<td> <xsl:value-of select="Interval"/></td>
         	<td> <xsl:value-of select="DataFileStartTime"/></td>
         	<td> <xsl:value-of select="ScaleType"/></td>
         	<td> <xsl:value-of select="ValueType"/></td>
         	</tr>
      
         </xsl:for-each>
         
         </table>
        </xsl:if>
        <xsl:if test="Type = 'FFT'">
      
	   
	  <table >
		 <tr class = "tblColTitle">
		   <th>File name</th>
		   <th>Interval</th>
		   <th>States</th>
		   </tr>
		  
         <xsl:for-each select="DataSetConfig">
           <tr>
         	<td>   	 <xsl:call-template name="convertToLinkIfConfigFolderElseDefaultString">
        	 	  <xsl:with-param name="file" select="FileName"></xsl:with-param>
      </xsl:call-template>	
  </td>
         	<td> <xsl:value-of select="Interval"/></td>
         	<td><table  class="small">
         	<tr class = "tblColTitle"><th>Target Name</th>
         		<th>Target Period</th>
         		<th>Start Sampling</th>
         		<th>End Sampling</th>
         		<th>Amplitude function</th>
         	</tr>
         	<xsl:apply-templates select="State"/></table></td>
         	</tr>
         </xsl:for-each>
         
         </table>
        </xsl:if>

</xsl:template>
<xsl:template match="State">
<tr>
<xsl:for-each select="*">
	<td><xsl:value-of select="current()"/></td>	
</xsl:for-each>

</tr>
</xsl:template>
 

</xsl:stylesheet>
