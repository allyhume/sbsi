"""A simple script to attempt to obfuscate an SBML file. The need for this
should be now not necessary since this functionality should be added to
SBSIVisual"""
import sys
import xml.dom.minidom

class SymbolFactory:
  """The symbol factory class is a means of giving each name in the
     model file a new name. It maintains a dictionary of all names that
     it has thus-far mapped to new names, such that if we encounter a name
     which has already been mapped we can return the mapped name rather than
     an entirely new name. This means that references in rates to species
     names (for example) will still be correct."""
  def __init__(self):
    """Initialise the symbol factory by setting the count to zero and
       having an empty dictionary. We could also put names that we don't
       wish to obfuscate here (such as perhaps "time")."""
    self.count = 0
    self.dictionary = dict()

  def get_new_symbol(self, orig_string):
    """Implements getting a new symbol, it checks if the given name
       has already been mapped, if so it returns the mapped name, otherwise
       we make a new mapping, add it to the dictionary and return the
       newly mapped name"""
    # Make sure we're not taking into account white space around
    # the actual contents of the string, for example we may have gotten
    # this from <ci> K </ci> so we want to make sure that we are
    # working with the string "K" and not " K ".
    orig_name = orig_string.lstrip().rstrip()
    if orig_name not in self.dictionary:
      symbol = "symbol_" + str(self.count)
      self.count += 1
      self.dictionary[orig_name] = symbol
    else:
      symbol = self.dictionary[orig_name]
    return symbol

def obfuscate_named_attribute(element, attribute):
  """A helper function to obfuscate a given named attribute if
     that attribute is present in the given element"""
  name = element.getAttribute(attribute)
  if attribute and name:
    new_name = symbol_factory.get_new_symbol(name)
    element.setAttribute(attribute, new_name)

def obfuscate_compartment_type(compartment_type):
  """Simple as is function to obfuscate a compartment type definition"""
  obfuscate_named_attribute(compartment_type, "name")
  obfuscate_named_attribute(compartment_type, "id")

def obfuscate_compartment(compartment):
  """Simple obfuscate the names in a compartment definition"""
  obfuscate_named_attribute(compartment, "name")
  obfuscate_named_attribute(compartment, "id")
  
def obfuscate_species(species):
  """Simple obfuscation for a species declaration"""
  obfuscate_named_attribute(species, "name")
  obfuscate_named_attribute(species, "id")
  obfuscate_named_attribute(species, "compartment")

def obfuscate_parameter(parameter):
  """Simple obfuscation of a parameter definition"""
  obfuscate_named_attribute(parameter, "name")
  obfuscate_named_attribute(parameter, "id")

def obfuscate_maths(maths):
  """A function to obfuscate math nodes. Essentially all we're doing is
     descending down through the sub-elements until we come across
     a 'ci' node at which point we symbol-obfuscate its contents"""
  if maths.nodeType == maths.ELEMENT_NODE:
    if maths.tagName == "ci":
      text = maths.firstChild
      old_string = text.data
      new_string = symbol_factory.get_new_symbol(old_string)
      text.data = new_string
    else:
      for child in maths.childNodes:
        obfuscate_maths(child)

def obfuscate_raterule(raterule):
  """A simple as is possible to obfuscate a rate rule"""
  obfuscate_named_attribute(raterule, "variable")
  maths = raterule.getElementsByTagName("math")[0]
  obfuscate_maths(maths)

def obfuscate_init_assign(init_assign):
  """A simple function to obfuscate an initial assignment"""
  obfuscate_named_attribute(init_assign, "symbol")
  maths = init_assign.getElementsByTagName("math")[0]
  obfuscate_maths(maths)

def obfuscate_species_reference(spec_ref):
  """A simple function to obfuscate a <speciesReference> tag"""
  obfuscate_named_attribute(spec_ref, "species")

def obfuscate_kinetic_law(kin_law):
  """A simple function to obfuscate a <kineticLaw> tag"""
  # There should only be one 'maths' element but it doesn't hurt
  # to call it as a possible many maths sub-elements
  obfuscate_list_of(kin_law, "math", obfuscate_maths)

def obfuscate_reaction(reaction):
  """A function to obfuscate a reaction"""
  obfuscate_named_attribute(reaction, "id")
  obfuscate_list_of_list_of (reaction,
                             "listOfReactants",
                             "speciesReference",
                             obfuscate_species_reference)
  obfuscate_list_of_list_of (reaction,
                             "listOfProducts",
                             "speciesReference",
                             obfuscate_species_reference)
  obfuscate_list_of(reaction, "kineticLaw", obfuscate_kinetic_law)


def obfuscate_list_of(element, tag_name, obfuscator):
  """A generic function to obfuscate a list of all the sub-elements of
     a given element which match the given tag name. This is generally
     more forgiving even when there should only be one sub-element with
     the given tag name. It also works if there are no elements of the
     given name. So rather than do:
       t_element = element.getElementsByTagName(tag_name)[0]
       obfuscate_t(t_element)
     you can simply do
     obfuscate_list_of(element, tag_name, obfuscate_t)
     and this covers all three cases, where there are 0, 1 or many
     sub-elements with the given name"""
  tagged_elements = element.getElementsByTagName(tag_name)
  for t_element in tagged_elements:
    obfuscator(t_element)

def obfuscate_list_of_list_of(element, list_name, tag_name, obfuscator):
  """Frequently we have <listOfTAG> <TAG> .. <TAG> .. <TAG> </listOfTag>
     and we wish to obfuscate all of the <TAG> elements with some common
     obfuscator. Unfortunately <listOfTAG> might not be present at all.
     There is the corner case that there may be more than on <listOfTAG>.
     So here is a helper function to apply an obfuscator to all the
     elements of a particular listOf style list"""
  list_ofs = element.getElementsByTagName(list_name)
  for list_of in list_ofs:
    tag_elements = list_of.getElementsByTagName(tag_name)
    for tag_element in tag_elements:
      obfuscator(tag_element)

def obfuscate_model(model):
  """Simple obfuscate the names in the model element and sub-elements.
     Basically the main entry point for obfuscation"""
  obfuscate_list_of_list_of(model, 
                            "listOfCompartmentTypes",
                            "compartmentType", 
                            obfuscate_compartment_type)
  obfuscate_list_of_list_of(model, 
                            "listOfCompartments",
                            "compartment", 
                            obfuscate_compartment)
  obfuscate_list_of_list_of(model, 
                            "listOfSpecies",
                            "species", 
                            obfuscate_species)
  obfuscate_list_of_list_of(model, 
                            "listOfParameters",
                            "parameter", 
                            obfuscate_parameter)
  obfuscate_list_of_list_of(model, 
                            "listOfRules",
                            "rateRule", 
                            obfuscate_raterule)
  obfuscate_list_of_list_of(model,
                            "listOfInitialAssignments",
                            "initialAssignment",
                            obfuscate_init_assign) 
  obfuscate_list_of_list_of(model,
                            "listOfReactions",
                            "reaction",
                            obfuscate_reaction)

def obfuscate_file(filename):
  """Parse in a file as an SBML model, obfuscate it and then print out
     the obfuscated model"""
  dom = xml.dom.minidom.parse(filename)
  obfuscate_model(dom.getElementsByTagName("model")[0])
  print(dom.toxml())
  


def run():
  """Perform the banalities of command-line argument processing and
     then get under way with the proper grunt work of obfuscating the model"""
  # The command line arguments not including this script itself
  arguments    = sys.argv 
  # file names are arguments that don't affect the configuration such
  # as limit=10 or x=k1
  filenames = [ x for x in arguments if '=' not in x and
                  not x.endswith("obfuscate.py") ]
  
  for filename in filenames:
    obfuscate_file(filename)


if __name__ == "__main__":
  symbol_factory = SymbolFactory()
  run()
