"""A simple python script to use gnuplot to plot a csv file.
   It may also accept a separator for example tab rather than
   a comma.
"""
import os
import sys
from subprocess import Popen


def get_options(option_name, arguments):
  """Gets all options of the form <option_name>=<argument> and returns
     the argument parts."""
  option_string = option_name + "="
  option_offset = len (option_string)
  options = [ option[option_offset:] for option in arguments
                if option.startswith(option_string) ]
  return options

def get_single_option(option_name, arguments):
  """The same as 'get_options' but insists on their being only one such,
     if there are no such arguments return the empty string"""
  options = get_options(option_name, arguments)
  if not options:
    return ""
  elif len(options) > 1:
    print ("Conflicting " + option_name + " arguments, choose one, exiting")
    sys.exit(1)
  else:
    return options[0]


def obtain_headers(datafile, arguments):
  """Obtain the column names from the csv file"""
  separator = get_single_option("sep", arguments)
  if not separator:
    separator = "\t"

  csvfile = open(datafile, "rb")
  headrow = csvfile.next()
  while separator not in headrow:
    headrow = csvfile.next()

  headers = headrow.split(separator)

  csvfile.close()
  return (headers, separator)


def set_gnuplot_options(gnuplotfile, arguments):
  """Set up some of the available options in the gnuplot file"""
  title = get_single_option("title", arguments)
  if title:
    gnuplotfile.write("set title \"" + title + "\"\n")

  x_range = get_single_option("xrange", arguments)
  if x_range:
    gnuplotfile.write("set xrange " + x_range + "\n")
  y_range = get_single_option("yrange", arguments)
  if y_range:
    gnuplotfile.write("set yrange " + y_range + "\n")


def create_gnuplot_file(basename, arguments, datafile):
  """From the headers obtained from the csv file, write a
     gnuplot file which will read the csv file and plot a timeseries"""

  (headers, separator) = obtain_headers(datafile, arguments)


  gnufilename = basename + ".gnuplot"
  gnuplotfile = open (gnufilename, "w")

  epsfilename = basename + ".eps"
  gnuplotfile.write("set term post eps color\n")
  gnuplotfile.write("set output \"" + epsfilename + "\"\n")
  gnuplotfile.write("set datafile separator " + repr(separator) + "\n")
  

  set_gnuplot_options(gnuplotfile, arguments)

  columns = get_options("column", arguments)  

  # Just a bit of debugging print(column_names)
  
  # The first column must be printed out a little differently to
  # the others, since we need to separate the lines, so we set
  # the prefix to what it will be for the first line, and then whenever
  # we print one out we just set it to what the prefix should be for
  # any other line (which is to end the previous line and indent).
  quote_data_file = "\"" + datafile + "\"" 
  line_prefix = "plot "
  for i in range (1, len(headers)):
    header = headers[i].lstrip().rstrip()
    if header != "Time" and (not columns or header in columns):    
      line = (line_prefix + quote_data_file + " using 1:" +
              str(i + 1) + " title '" + header + "'")
      # If it's not currently the first line then this will simply
      # set the prefix to what it already is, so no harm done.
      line_prefix = ", \\\n  "
      gnuplotfile.write(line) 

  gnuplotfile.write("\n")
  gnuplotfile.close()
  return (gnufilename, epsfilename)
 

def run ():
  """Simply do all the work"""
  arguments    = [ x for x in sys.argv if not x.endswith(".py") ]
  filenames    = [ x for x in arguments if not '=' in x ]
  datafile     = filenames[0] 

  basename = get_single_option("basename", arguments)
  if not basename:
    basename = os.path.splitext(datafile)[0]

  (gnufilename, epsfilename) = create_gnuplot_file(basename,
                                                   arguments,
                                                   datafile)

  # We now also actually run gnuplot
  gnuplot_command = [ "gnuplot", gnufilename ]
  gnuplot_process = Popen(gnuplot_command)
  gnuplot_process.communicate()

  epstopdf_command = [ "epstopdf", epsfilename ]
  epstopdf_process = Popen(epstopdf_command)
  epstopdf_process.communicate()


if __name__ == "__main__":
  run()
