"""A program to generate gif animations from the log file of a run
   of the sbsi numerics framework using pga or pgapso"""
import sys
from subprocess import Popen

class Individual:
  """A class representing individuals within a genetic population"""
  def __init__(self, number, xvalue, yvalue, zvalue):
    self.number = number
    self.xvalue = xvalue
    self.yvalue = yvalue
    self.zvalue = zvalue

class PlotConfig:
  """A class representing the configuration of the plot, including
     the names of the axes"""
  def __init__(self, xname, yname, zname):
    self.xname = xname
    self.yname = yname
    self.zname = zname

# 0  8.825801e+00 1.133042e-01 T
# k1:	 34.65151202678680
# k2:	 17.62332149744034
# k3:	 18.68497169017792
# F=8.82580085e+00
# 
def parse_param_line(param_line):
  """Parse a parameter line of the form <name>:    <number>"""
  parsed_triple = param_line.partition(":")
  name = parsed_triple[0]
  # colon would be parsed_triple[1]
  rest_string = parsed_triple[2]
  number_string = rest_string.lstrip()
  number = float(number_string)
  return (name, number)

def parse_individual(logfile, plot_config):
  """Parse one individual in one generation"""
  current_line = logfile.next()

  # skip whitespace lines
  while current_line.isspace():
    current_line = logfile.next()

  # Now read in the number, kind of the individual's id within the
  # current generation.
  number_string = current_line.partition (" ")[0] 
  number = int(number_string)
  
  current_line = logfile.next()

  # Now read in the parameter lines
  xvalue_set = False
  yvalue_set = False
  zvalue_set = False
  while ':' in current_line:
    (name, value) = parse_param_line(current_line)
    # note that by not using elifs we allow the same name to be
    # used for two (or three) different axes. Not sure why anyone
    # might want this, but there you go.
    if name == plot_config.xname:
      xvalue = value
      xvalue_set = True
    if name == plot_config.yname:
      yvalue = value
      yvalue_set = True
    if name == plot_config.zname:
      zvalue = value
      zvalue_set = True
    current_line = logfile.next()

  # We now assume that we are staring at the F= line,
  # hence skip to the next line ready to parse the next individual
  # if there is one.
  logfile.next()

  # We can now create the individual assuming that all three of
  # axis variables have been set
  if xvalue_set and yvalue_set and zvalue_set:
    individual = Individual(number, xvalue, yvalue, zvalue)
    return individual
  else:
    print ("values not all set in individual: " + str(number))
    sys.exit(1)


def generation_gif_name(counter):
  """Given the value of the counter generate the name of the gif file"""
  gif_name = "splot-" + str(counter) + ".gif"
  return gif_name
 

def plot_generation(counter, title, generation, plot_config):
  """Create a gif of the plotted values for one generation"""
  gif_name = generation_gif_name(counter)
  csv_name = "generation-" + str(counter) + ".csv"

  gnuplot_file = open("splot.gnuplot", "w")
  gnuplot_file.write("set term gif\n")
  gnuplot_file.write("set output \"" + gif_name + "\"\n")
  if title:
    gnuplot_file.write("set title \"" + title + " generation " + 
                       str(counter) + "\"\n")
  gnuplot_file.write("set xlabel '" + plot_config.xname + "'\n")
  gnuplot_file.write("set ylabel '" + plot_config.yname + "'\n")
  gnuplot_file.write("set zlabel '" + plot_config.zname + "'\n")
  gnuplot_file.write("set xrange [0:1]\n")
  gnuplot_file.write("set yrange [0:1]\n")
  gnuplot_file.write("set zrange [0:1]\n")
  gnuplot_file.write("set ticslevel 0\n")
  gnuplot_file.write("\n")
  gnuplot_file.write("splot \"" + csv_name + "\" using 2:3:4 notitle\n")
  gnuplot_file.close()

  
  csv_file = open(csv_name, "w")
  for individual in generation:
    csv_file.write("8.8, ")
    csv_file.write(str(individual.xvalue) + ", ")
    csv_file.write(str(individual.yvalue) + ", ")
    csv_file.write(str(individual.zvalue))
    csv_file.write("\n")
  csv_file.close()

  gnuplot_command = [ "gnuplot", "splot.gnuplot" ]
  gnuplot_process = Popen(gnuplot_command)
  gnuplot_process.communicate()

def create_animation(limit, output_filename):
  """Create the animation up to the given limit of a generation"""
  animated_gif = open (output_filename, "w")
  gifsicle_command = [ "gifsicle", "--delay=50", "--loop" ]
  for counter in range(0, limit):
    print ("Counter: " + str(counter))
    gifsicle_command.append(generation_gif_name(counter))
  gifsicle_process = Popen(gifsicle_command, stdout=animated_gif)
  gifsicle_process.communicate()


def get_options(option_name, arguments):
  """Gets all options of the form <option_name>=<argument> and returns
     the argument parts."""
  option_string = option_name + "="
  option_offset = len (option_string)
  options = [ option[option_offset:] for option in arguments
                if option.startswith(option_string) ]
  return options

def get_single_option(option_name, arguments):
  """The same as 'get_options' but insists on their being only one such,
     if there are no such arguments return the empty string"""
  options = get_options(option_name, arguments)
  if not options:
    return ""
  elif len(options) > 1:
    print ("Conflicting " + option_name + " arguments, choose one, exiting")
    sys.exit(1)
  else:
    return options[0]


def get_plot_config(arguments):
  """Return the plot configuration based on the arguments"""
  xname = get_single_option("x", arguments)
  if not xname:
    print ("you must specify the x axis variable with x=<variable>")
    sys.exit(1)
  yname = get_single_option("y", arguments)
  if not yname:
    print ("you must specify the y axis variable with y=<variable>")
    sys.exit(1)
  zname = get_single_option("z", arguments)
  if not zname:
    print ("you must specify the z axis variable with z=<variable>")
    sys.exit(1)

  plot_config = PlotConfig(xname, yname, zname)
  return plot_config


def parse_generations(logfile, plot_config):
  """Parse the log file into a list of generations"""
  individuals = []
  try:
    while (True):
      individual = parse_individual(logfile, plot_config)
      individuals.append(individual)
  except StopIteration:
    pass
       
  generations = []
  current_generation = [] # shouldn't be needed as first should 0
  for individual in individuals:
    if individual.number == 0:
      current_generation = []
      generations.append(current_generation)
    current_generation.append(individual)

  return generations


def run ():
  """Do all the work"""
  arguments = sys.argv
  # file names are arguments that don't affect the configuration such
  # as limit=10 or x=k1
  filenames = [ x for x in arguments if '=' not in x and
                  not x.endswith("animate_logfile.py") ]
  # The first filename is the python script in question
  filename = filenames[0]
  logfile = open(filename, "rb")

  plot_config = get_plot_config(arguments)

  generations = parse_generations(logfile, plot_config)

  counter = 0
  limit_string = get_single_option("limit", arguments)
  if limit_string:
    limit = int(limit_string)
    generations = generations[:limit]

  # Note this means that 'title' may be the empty string
  title = get_single_option("title", arguments)

  for generation in generations:
    plot_generation(counter, title, generation, plot_config)
    counter += 1

  output_filename = get_single_option("output", arguments)
  if not output_filename:
    output_filename = "animated.gif"
  create_animation(counter, output_filename)

if __name__ == "__main__":
  run()
