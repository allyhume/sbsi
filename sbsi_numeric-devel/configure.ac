#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_INIT([SBSI_NUMERIC], [3.2], [BUG-REPORT-ADDRESS])
AC_CONFIG_AUX_DIR(config)
AM_INIT_AUTOMAKE

AC_CONFIG_SRCDIR([src/Chi2Cost.cpp])
AC_CONFIG_HEADER([include/config.h])

# Checks for programs.
AC_LANG([C++])
AC_PROG_CXX
AC_PROG_CC
AC_PROG_RANLIB
AC_PROG_INSTALL
#AC_CANONICAL_TARGET
#AC_CANONICAL_HOST
#AC_CANONICAL_BUILD

# Checks for libraries.

dnl check sundial

    AC_ARG_WITH([sundials], AC_HELP_STRING([--with-sundials@<:@=PATH@:>@], [PATH is path to 'sundials']), [WITH_SUNDIALS=$withval], [WITH_SUNDIALS=yes])

    if test "$WITH_SUNDIALS" = "yes"; then
        AC_CHECK_LIB([sundials_cvode], [CVodeCreate], , [AC_MSG_ERROR([Need A Sundials CVODE Library.])])
        AC_CHECK_LIB([sundials_nvecserial], [N_VNew_Serial], , [AC_MSG_ERROR([Need A Sundials CVODE Library.])])
        AC_CHECK_LIB([sundials_kinsol], [KINCreate], , [AC_MSG_ERROR([Need A Sundials CVODE Library.])])
    else
        AC_MSG_CHECKING([sundials])
        for sundials_include in $WITH_SUNDIALS/include $WITH_SUNDIALS; do
                if test -d $sundials_include -a -f $sundials_include/sundials/sundials_nvector.h; then
                        SUNDIALS_INCLUDE="-I$sundials_include"
                fi
        done
        for sundials_lib in $WITH_SUNDIALS/lib $WITH_SUNDIALS/sundials; do
                if test -d $sundials_lib -a -f $sundials_lib/libsundials_cvodes.a; then
                        SUNDIALS_LIB="-L$sundials_lib"
                fi
        done
        if test -n "$SUNDIALS_INCLUDE" -a -n "$SUNDIALS_LIB"; then
                HAVE_LIBSUNDIALS=1
                AC_SUBST(HAVE_LIBSUNDIALS)
                CPPFLAGS="$SUNDIALS_INCLUDE $CPPFLAGS"
                LDFLAGS="$SUNDIALS_LIB $LDFLAGS"
                LIBS="$LIBS -lsundials_nvecserial -lsundials_cvode -lsundials_kinsol"
                AC_MSG_RESULT([$CPPFLAGS $LDFLAGS $LIBS])
        else
                AC_MSG_RESULT([no])
                AC_MSG_ERROR([Need A Sundials Library.])
        fi
   fi

dnl fftw3

    AC_ARG_WITH([fftw3], AC_HELP_STRING([--with-fftw3@<:@=PATH@:>@], [PATH is path to 'fftw3']), [WITH_FFTW3=$withval], [WITH_FFTW3=yes])


    if test "$WITH_FFTW3" = "yes"; then
  AC_CHECK_LIB([fftw3], [fftw_destroy_plan], , [AC_MSG_ERROR([Need A FFTW3 Library.])])
    else
       if test "$WITH_FFTW3" != "no"; then

    AC_MSG_CHECKING([fftw3])
          for fftw3_include in $WITH_FFTW3/include $WITH_FFTW3; do
                  if test -d $fftw3_include -a -f $fftw3_include/fftw3.h; then
                          FFTW3_INCLUDE="-I$fftw3_include"
                  fi
          done
    for fftw3_lib in $WITH_FFTW3/lib $WITH_FFTW3/fft3; do
        if test -d $fftw3_lib -a -f $fftw3_lib/libfftw3.a; then
        FFTW3_LIB="-L$fftw3_lib"
      fi
    done
    if test -n "$FFTW3_INCLUDE" -a -n "$FFTW3_LIB"; then
      HAVE_LIBFFTW3=1
      AC_SUBST(HAVE_LIBFFTW3)
      CPPFLAGS="$FFTW3_INCLUDE $CPPFLAGS"
      LDFLAGS="$FFTW3_LIB $LDFLAGS"
    LIBS="$LIBS -lfftw3"
    AC_MSG_RESULT([$CPPFLAGS $LDFLAGS $LIBS])
    else
    AC_MSG_RESULT([no])
    AC_MSG_ERROR([Need A FFTW3 Library.])
    fi
       else
    AC_MSG_RESULT([Not use FFTW3 Library.])
      fi
    fi

dnl fftw2

    AC_ARG_WITH([fftw2], AC_HELP_STRING([--with-fftw2@<:@=PATH@:>@], [PATH is path to 'fftw2']), [WITH_FFTW2=$withval], [WITH_FFTW2=no])


    if test "$WITH_FFTW2" = "yes"; then
        AC_CHECK_LIB([fftw], [rfftw_create_plan], , [AC_MSG_ERROR([Need A FFTW2 Library.])])
    else
       if test "$WITH_FFTW2" != "no"; then
          AC_MSG_CHECKING([fftw2])
          for fftw2_include in $WITH_FFTW2/include $WITH_FFTW2; do
                if test -d $fftw2_include -a -f $fftw2_include/dfftw.h; then
                        FFTW2_INCLUDE="-I$fftw2_include"
                fi
          done
          for fftw2_lib in $WITH_FFTW2/lib $WITH_FFTW2; do
                if test -d $fftw2_lib -a -f $fftw2_lib/libdfftw.a; then
                        FFTW2_LIB="-L$fftw2_lib"
                fi
          done
          if test -n "$FFTW2_INCLUDE" -a -n "$FFTW2_LIB"; then
                HAVE_LIBFFTW2=1
                echo $HAVE_LIBFFTW2
                AC_SUBST(HAVE_LIBFFTW2)
                AC_DEFINE(ENABLE_FFTW2, 1 ,Use fftw 2.1.5)
                CPPFLAGS="$FFTW2_INCLUDE $CPPFLAGS"
                LDFLAGS="$FFTW2_LIB $LDFLAGS"
                LIBS="$LIBS -ldrfftw -ldfftw "
                AC_MSG_RESULT([$CPPFLAGS $LDFLAGS $LIBS])
          else
                AC_MSG_RESULT([no])
                AC_MSG_ERROR([Need A FFTW2 Library.])
          fi
       else
    AC_MSG_RESULT([Not use FFTW2 Library.])
      fi
    fi

dnl pgapack

   AC_ARG_WITH([pgapack], AC_HELP_STRING([--with-pgapack@<:@=PATH@:>@],
     [PATH is path to 'pgapack']), [WITH_PGAPACK=$withval], [WITH_PGAPACK=yes])

   if test "$WITH_PGAPACK" = "yes"; then
        AC_CHECK_LIB([pgapack], [PGAGetStringLength], , [AC_MSG_ERROR([Need A PGAPACK Library.])])
   else
        AC_MSG_CHECKING([pgapack])
        for pgapack_include in $WITH_PGAPACK/include $WITH_PGAPACK; do
                if test -d $pgapack_include -a -f $pgapack_include/pgapack.h; then
                        PGAPACK_INCLUDE="-I$pgapack_include"
                fi
        done
        for pgapack_lib in $WITH_PGAPACK/lib $WITH_PGAPACK/pgapack; do
                if test -d $pgapack_lib -a -f $pgapack_lib/libpgapack.a; then
                        PGAPACK_LIB="-L$pgapack_lib"
                fi
        done
        if test -n "$PGAPACK_INCLUDE" -a -n "$PGAPACK_LIB"; then
                HAVE_LIBPGAPACK=1
                AC_SUBST(HAVE_LIBPGAPACK)
                CPPFLAGS="$PGAPACK_INCLUDE $CPPFLAGS"
                LDFLAGS="$PGAPACK_LIB $LDFLAGS"
                LIBS="$LIBS -lpgapack "
                AC_MSG_RESULT([yes])
                AC_MSG_RESULT([$CPPFLAGS $LDFLAGS $LIBS])
        else
                AC_MSG_RESULT([no])
                AC_MSG_ERROR([Need A PGAPACK Library.])
        fi
   fi

dnl libsbml

   AC_ARG_WITH([sbml], AC_HELP_STRING([--with-sbml@<:@=PATH@:>@], [PATH is path to 'sbml']),
     [WITH_SBML=$withval], [WITH_SBML=yes])

   if test "$WITH_SBML" = "yes"; then
        AC_CHECK_LIB([sbml], readSBML, , [AC_MSG_WARN([Compile without Sbml Library.])])
   else
        AC_MSG_CHECKING([sbml])
        for sbml_include in $WITH_SBML/include $WITH_SBML; do
                if test -d $sbml_include -a -f $sbml_include/sbml/SBMLDocument.h; then
                        SBML_INCLUDE="-I$sbml_include"
                fi
        done
        for sbml_lib in $WITH_SBML/lib $WITH_SBML/sbml; do
                if test -d $sbml_lib -a -f $sbml_lib/libsbml.a; then
                        SBML_LIB="-L$sbml_lib"
                fi
        done
        if test -n "$SBML_INCLUDE" -a -n "$SBML_LIB"; then
                HAVE_LIBSBML=1
                no_sbml="false"
                AC_SUBST(HAVE_LIBSBML)
                CPPFLAGS="$SBML_INCLUDE $CPPFLAGS"
                LDFLAGS="$LDFLAGS $SBML_LIB "
                LIBS="$LIBS -lsbml "
                AC_MSG_RESULT([$CPPFLAGS $LDFLAGS $LIBS])
dnl                AM_CONDITIONAL([NO_SBML], [test "${no_sbml}" = "true"])
        else
                AC_MSG_RESULT([Compile without SBML])
                no_sbml="true"
        fi
   fi
   AM_CONDITIONAL([NO_SBML], [test "${no_sbml}" = "true"])

# check libxml2

AC_ARG_WITH(libxml2,
  AC_HELP_STRING(
    [--with-libxml2=DIR],
    [Build on top of libxml2 where libxml2 is installed in DIR]
  ),
  [LIBXML2_HOME="$with_libxml2"]
)

echo ${LIBXML2_HOME}

if test "X${LIBXML2_HOME}X" = "XX" ; then
  AC_PATH_PROG(LIBXML2_CONFIG, [xml2-config], [])
else
  echo Freddie
  AC_PATH_PROG(LIBXML2_CONFIG, [xml2-config], [], [${LIBXML2_HOME}/bin:${PATH}])
fi

if test "X${LIBXML2_CONFIG}X" = "XX" ; then
  AC_MSG_ERROR([libxml2 configuration program xml2-config not found.])
fi

AC_MSG_NOTICE([Found libxml2 configuration program ${LIBXML2XX_CONFIG}])
AC_SUBST(LIBXML2_CPPFLAGS, "`${LIBXML2_CONFIG} --cflags`")
AC_MSG_NOTICE([libxml2 compile flags: ${LIBXML2_CPPFLAGS}])
                CPPFLAGS="${LIBXML2_CPPFLAGS} $CPPFLAGS"
AC_SUBST(LIBXML2_LIBS,     "`${LIBXML2_CONFIG} --libs`")
AC_MSG_NOTICE([libxml2 libraries flags: ${LIBXML2_LIBS}])
dnl                LDFLAGS="$LDFLAGS $LIBXML2_LIBS"
                LIBS="$LIBS $LIBXML2_LIBS"


# Checks for header files.

# Checks for typedefs, structures, and compiler characteristics.

# Checks for library functions.
AC_CHECK_LIB([cppunit],[main])
AM_CONDITIONAL(CPPUNIT, test "$ac_cv_lib_cppunit_main" = yes)

AC_CONFIG_FILES(Makefile)
AC_CONFIG_FILES([tests/abc_test/Makefile 
                 tests/def_test/Makefile
                 tests/opt_test/Makefile
                 tests/cppunit/Makefile
                 tests/Makefile
                 util/Makefile
                 examples/Makefile
                 examples/CircadClock/Makefile
                 examples/Mendez/Makefile
                 examples/ABC_1/Makefile
                 examples/ABC_1_MD/Makefile
                 examples/FerTron/Makefile
                 Template/Makefile
                 Template/Model/Makefile
                 sbsi_lib/Makefile])
AC_OUTPUT
