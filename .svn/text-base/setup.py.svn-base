"""
This is an installation helper script for sbsi-numerics.
The intention is that the user need only download and run this script
in order to be able to install and use the sbsi-numerics command-line.
"""

import os
import urllib
from subprocess import PIPE, Popen
import sys
import shutil

# We need to set the environment up as in this script
def create_set_vars_source(install_dir):
  """Return the source for the set-vars script given a default
     installation dir, which defaults to 'install' in the current
     directory if it is not specified"""
  if not install_dir:
    install_dir = "$PWD/install/"
  return """  
#!/bin/sh
if [ $# -gt 0 ] ; then
 PREFIX=$1
else
 PREFIX=""" + install_dir + """
fi

export PATH=${PREFIX}/bin:$PATH # We'll need this for mpicxx etc.
export INCLUDE_PATH=${PREFIX}/include
export LIBRARY_PATH=${PREFIX}/lib
export LD_LIBRARY_PATH=$LIBRARY_PATH
# the mac equivalent of LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH=$LIBRARY_PATH

export CPATH=$INCLUDE_PATH
export LDFLAGS="-L${PREFIX}/lib"
export CFLAGS="-I${PREFIX}/include"
export CXXFLAGS=${CFLAGS}
"""

# We should probably also have a flag to build without the current
# paths available so that we ensure that we will only use what we
# install in the prefix directory.
def add_to_env_variable(var_name, addition, keep_current, separator):
  """Add a value to the given os environment variable"""
  var_value = os.environ.get(var_name, "")
  # I prefer the 2.7 enabled conditional expressions, but this
  # breaks when used with versions of python before 2.7 (or 2.6)
  # So the commented out line is the same as the conditional statement.
  # Perhaps later when we can be relatively sure that noone is still
  # using a python version earlier than 2.7 we can change this.
  # new_var_value = (addition + separator + var_value if
  #                    keep_current and var_value else addition)
  if keep_current and var_value:
    new_var_value = addition + separator + var_value
  else:
    new_var_value = addition
  os.environ[var_name] = new_var_value

def set_environment_variables():
  """This is intended to set the os environment variables to be suitable
     for the compilation of sbsi, in particular the directory to which the
     dependencies are installed is added to various library and include paths
  """
  # This should be settable by the user, basically if it is false
  # then we will not keep the current environment variables and completely
  # overwrite them, this means we will not accidentally use some existing
  # installation of some incorrect version of one of the dependencies
  # If set to true however we will, eg if set to true, LIBRARY_PATH will be
  # set to lib_dir:$LIBRARY_PATH if set to False then LIBRARY_PATH will be
  # set to simply lib_dir
  keep_current = False

  this_dir = os.getcwd()
  prefix_dir = os.path.abspath(os.path.join(this_dir, "install"))
  bin_dir = os.path.join(prefix_dir, "bin")
  lib_dir = os.path.join(prefix_dir, "lib")
  include_dir = os.path.join(prefix_dir, "include")
  os.environ["PREFIX"] = prefix_dir
  # We need to set the path variable for mpicxx
  # note that regardless of keep_current we need to keep the current path
  # otherwise we won't be able to run any programs ;)
  add_to_env_variable("PATH", bin_dir, True, ":")
  add_to_env_variable("INCLUDE_PATH", include_dir, keep_current, ":")
  add_to_env_variable("LIBRARY_PATH", lib_dir, keep_current, ":")
  add_to_env_variable("LD_LIBRARY_PATH", lib_dir, keep_current, ":")
  add_to_env_variable("CPATH", include_dir, keep_current, ":")
  add_to_env_variable("LDFLAGS", "-L" + lib_dir, keep_current, " ")
  add_to_env_variable("CFLAGS", "-I" + include_dir, keep_current, " ")
  add_to_env_variable("CXXFLAGS", "-I" + include_dir, keep_current, " ")

def download_file(url, dfile):
  """ A simple wrapper to download a file since there seems to be
      several different ways that the internet is telling me to do this"""
  urllib.urlretrieve(url, dfile)
  #req = urllib2.urlopen(url)
  #CHUNK = 16 * 1024
  #with open(file, 'wb') as fp:
  #  while True:
  #    chunk = req.read(CHUNK)
  #    if not chunk: break
  #    fp.write(chunk)

def quit_with_error(name):
  """A simple function to tell the user that we are quitting because we
     have failed to install one of the dependencies asked for"""
  print("I'm quitting my attempt to install because")
  print(name)
  print("failed to install correctly")
  sys.exit(1)

class Dependency:
  """A simple class for one dependency, more or less speaks for itself"""
  def __init__ (self, baseurl, archive_name):
    self.baseurl = baseurl
    self.archive_name = archive_name
    self.dep_directory = "deps"
    self.dependency_name = archive_name
    self.configure_flags = []
    # This assumes that the archive is named something like
    # dep.tar.gz and that the resulting directory is simply dep
    # If this is not the case we can simply override this with
    # set_dir_name
    tar_name = os.path.splitext(archive_name)[0]
    self.dir_name = os.path.splitext(tar_name)[0]
    self.install_dir = os.path.join(os.getcwd(), "install")
    # The name used to enable this dependency with the command line
    # this almost certainly wants to be changed with set_command_line_name
    self.command_line_name = archive_name
    self.manual_download_url = baseurl
    # A dictionary mapping other version names which can be selected
    # rather than the default one.
    self.other_versions = dict()

  def set_name(self, name):
    """Set the name of dependency, this name is used for communicating
       to the user what we are doing"""
    self.dependency_name = name

  def set_dir_name(self, name):
    """Set the name of the directory which the archive is unpacked to"""
    self.dir_name = name

  def set_dep_directory(self, dir_name):
    """Set the dependencies directory that we will download the archive
       to and before unpacking it. This is intended to allow a cleaner
       install particularly for those working in the sbsi source"""
    self.dep_directory = dir_name

  def add_another_version(self, name, other_version):
    """Add the definition of another version to the given name.
       This allows us to set up easy aliases for other versions
       of dependencies, for example one can install the 3.4.0 version
       of libsbml, by specifying libsbml=3.4.0 on the command-line.
    """
    self.other_versions[name] = other_version

  def set_archive_location(self, archive_url):
    """Used to set the archive location, essentially so that the user
       need not accept the default version or location of a given
       dependency. So could for example try an upgraded dependency or
       simply use a mirror site if the default one is down.
    """
    # Not quite sure how robust this method is of splitting a url
    # into the baseurl and the filename, but ce la vie.
    url_parts = archive_url.split("/")
    num_parts = len(url_parts)
    archive_name = url_parts[num_parts - 1]
    if num_parts > 1:
      base_url = "/".join(url_parts[0 : num_parts -1])
    else:
      base_url = ""

    self.archive_name = archive_name
    self.baseurl = base_url

  def switch_version(self, version, dir_name):
    """ Allows the selection of a difference version of the dependency
        either by naming a pre-defined (generally earlier) version or
        by the user providing an archive url (and possibly a directory
        which the archive is unpacked to if this is not easily
        extracted from the url).
    """
    if version in self.other_versions:
      preferred_version = self.other_versions[version]
      self.baseurl = preferred_version.baseurl
      self.archive_name = preferred_version.archive_name
      self.dir_name = preferred_version.dir_name
    else:
      self.set_archive_location(version)
      if dir_name:
        self.dir_name = dir_name
      else:
        # This relies on set_archive_location setting self.archive_name
        self.dir_name = os.path.splitext(self.archive_name)[0]

  def set_command_line_name(self, name):
    """Set the command line name, this is the name the user uses to request
       that this dependency be installed"""
    self.command_line_name = name

  def set_configure_flags(self, flags):
    """Set the configure flags used when configuring and installing this
       dependency"""
    self.configure_flags = flags
  def add_configure_flags(self, flags):
    """add to the current set of flags used when configuring and installing
       this dependency"""
    self.configure_flags.extend(flags)

  def set_install_dir(self, install_dir):
    """Explicitly set the installation directory for this dependency"""
    self.install_dir = install_dir

  def set_manual_download_url(self, url):
    """Set the manual download url, which is reported to the user in the
       case that the dependency cannot be downloaded automatically and must
       be downloaded separately by the user"""
    self.manual_download_url = url

  def __check_manual_download(self):
    """Checks if this dependency needs to be manually downloaded and
       if so if it is already downloaded. Returns true if the dependency
       can be installed, that is, either it needn't be manually downloaded
       or it appears to have been downloaded already
    """
    target_path = os.path.join(self.dep_directory, self.archive_name)
    if os.path.exists(self.archive_name) and self.dep_directory:
      shutil.move (self.archive_name, target_path)
    if not self.baseurl and not os.path.exists(target_path):
      print ("The dependency: " + self.command_line_name +
             " cannot be downloaded automatically")
      if self.manual_download_url:
        print ("Please visit: " + self.manual_download_url)
        print ("and download: " + self.archive_name)
      return False
    else:
      return True

  def initial_check(self):
    """Performs preliminary checks, this is called on all requested
       dependencies before any are attempted, such that we can error
       quickly in the case that some dependencies must obviously fail.
       In particular here we are checking that if this dependency must be
       manually downloaded then the archive is present, otherwise explain to
       the user that it must be manually downloaded
    """
    # This check needs to come before the __check_manual_download check
    # because that will look to see if the dependency exists in the
    # dep_directory and that check will fail/crash if no such directory
    # exists
    if self.dep_directory and not os.path.exists(self.dep_directory):
      os.makedirs(self.dep_directory)
    if not self.__check_manual_download():
      sys.exit(1)

  def retrieve(self):
    """If the archive doesn't already exist then download it"""
    target_path = os.path.join(self.dep_directory, self.archive_name)
    # We should probably still check here if the self.dep_directory
    # exists, the check for that is actually in 'initial_check' however
    # it is possible someone has, in the meantime, deleted it.
    if os.path.exists(self.archive_name) and self.dep_directory:
      shutil.move(self.archive_name, target_path)
    elif not os.path.exists(target_path):
      if not self.__check_manual_download():
        quit_with_error(self.dependency_name)
      print ("Downloading " + self.dependency_name)
      if self.baseurl[-1] != "/":
        download_link = self.baseurl + "/" + self.archive_name
      else:
        download_link = self.baseurl + self.archive_name
      download_file(download_link, target_path)

  def unpack(self):
    """Unpack the downloaded archive file into its directory
       unless the directory is already there."""
    current_dir = os.getcwd()
    if self.dep_directory:
      os.chdir(self.dep_directory)

    extension = os.path.splitext(self.archive_name)[1]
    if extension == ".bz2":
      unpack_command = [ "tar", "xjf", self.archive_name ]
    elif extension == ".gz":
      unpack_command = [ "tar", "xzf", self.archive_name ]
    elif extension == ".tar":
      unpack_command = [ "tar", "xf", self.archive_name ]
    elif extension == ".zip":
      unpack_command = [ "unzip", "-o", self.archive_name ]
    else:
      # Okay so in this case we assume that it is actually
      # already a directory and we do not need to unpack it.
      # but this is checked for below
      unpack_command = []

    if not os.path.exists(self.dir_name):
      if not unpack_command:
        print("Unrecognised extension: " + extension)
        quit_with_error(self.dependency_name)
      print ("Unpacking " + self.dependency_name)
      process = Popen(unpack_command, stdout=PIPE)
      (output, error) = process.communicate()
      if process.returncode != 0:
        if output:
          print(output)
        if error:
          print(error)
        quit_with_error(self.dependency_name)
    # Don't forget to switch back to the top level directory
    os.chdir(current_dir)

  def install(self):
    """Actually install the given dependency, this involves the
       whole, configure/make/make install routine"""
    current_dir = os.getcwd()
    target_dir = os.path.join(self.dep_directory, self.dir_name)
    os.chdir(target_dir)

    # We first check if there is a 'configure' script, if not
    # then we check if there is a bootstrap script and if so
    # run that, otherwise we should attempt to run autoconf but
    # for now we are really just doing this for sbsi_numerics, all
    # the other dependencies should have a configure script.
    if not os.path.exists("./configure"):
      print ("Configuring " + self.dependency_name)
      bootstrap_command = [ "./bootstrap" ]
      bootstrap_process = Popen(bootstrap_command, stdout=PIPE, stderr=PIPE)
      bootstrap_process.communicate()

    prefix_flag = "--prefix=" + self.install_dir
    config_command = ["./configure", prefix_flag] + self.configure_flags

    config_process = Popen(config_command, stdout=PIPE)
    (output, error) = config_process.communicate()
    success = config_process.returncode == 0
    if success:
      print ("Installing " + self.dependency_name)
      make_command = [ "make", "install" ]
      make_process = Popen(make_command, stdout=PIPE)
      (output, error) = make_process.communicate()
      success = make_process.returncode == 0
      if not success:
        print(output)
        print(error)
        print("Configure command: " + " ".join(config_command))
        quit_with_error(self.dependency_name)
    else:
      print(output)
      print(error)
      print("Command: " + " ".join(config_command))
      quit_with_error(self.dependency_name)
    # Don't forget to change dir back to the previous directory
    os.chdir(current_dir)
    print("Successfully installed: " + self.dependency_name)

  def retrieve_to_install(self):
    """Perform all the steps necessary to install a given dependency"""
    self.retrieve()
    self.unpack()
    self.install()

class OtherVersion:
  """A very simple class for defining other versions of dependencies.
     This simply stores the information that we must override in the
     original to create an alternative version of a dependency
  """
  def __init__(self, baseurl, archive_name, dir_name):
    self.baseurl = baseurl
    self.archive_name = archive_name
    if dir_name:
      self.dir_name = dir_name
    else:
      self.dir_name = os.path.splitext(archive_name)[0]


# We should have a flag for where we are installing, for
# example, Gnu/Linux, EDDIE or Hector
class Architecture:
  """The class of the architecture we are installing to"""
  def __init__ (self, name):
    self.arch_name = name
  def is_eddie(self):
    """Returns true if we are installing to Eddie"""
    return self.arch_name == "eddie"
  def is_hector(self):
    """Returns true if we are installing to HecToR"""
    return self.arch_name == "hector"


def open_mpi_dep():
  """Calculate the open_mpi dependency"""
  openmpi_base    = "http://www.open-mpi.org/software/ompi/v1.4/downloads/"
  openmpi_archive = "openmpi-1.4.3.tar.bz2"
  openmpi = Dependency(openmpi_base, openmpi_archive)
  openmpi.set_name("open mpi")
  openmpi.set_command_line_name("openmpi")
  return openmpi

def fftw_dep():
  """Calculate the fastest fourier transform in the west dependency"""
  fftw_base = "http://www.fftw.org/"
  fftw_archive = "fftw-3.2.2.tar.gz"
  fftw = Dependency(fftw_base, fftw_archive)
  fftw.set_name ("fastest fourier transform in the west")
  fftw.set_command_line_name("fftw")
  extra_flags = [ "--enable-shared=no",
                  "--enable-static=yes",
                  "--disable-fortran",
                  "CC=gcc",
                  "CXX=g++",
                  "CFLAGS=-O3"
                ]
  fftw.add_configure_flags(extra_flags)
  return fftw

def libxml_dep(architecture):
  """Calculate the libxml dependency"""
  xml_base = "http://xmlsoft.org/sources/"
  # 2-2.7.7 also seems to be available, we could consider upgrading
  xml_archive = "libxml2-2.7.6.tar.gz"
  xml = Dependency(xml_base, xml_archive)
  xml.set_name("libxml2")
  xml.set_command_line_name("libxml2")
  extra_flags = [ "CFLAGS=-O3",
                  "--without-zlib",
                  "--without-python",
                  "--without-readline",
                  "--without-ftp",
                  "--without-catalog",
                  "--without-docbook",
                  "--without-iconv",
                  "--without-schemas",
                  "--with-schematron",
                  "--without-threads",
                ]
  if architecture.is_eddie():
    extra_flags +=  [ "--enable-shared=no",
                      "--enable-static=yes",
                    ]
  elif architecture.is_hector():
    extra_flags += [ "--enable-shared=no",
                     "--enable-static=yes",
                   ]
  else:
    # I have removed the 'enabled-shared=no' flag from here since it caused
    # the compilation to fail on my laptop. Maybe it should also have
    # "--enable-static=yes" which was missing from the readme.
    pass
  xml.add_configure_flags(extra_flags)
  return xml

def make_other_libsbml_version(downloads_base, version_string):
  """All of the other versions of libsbml are easy enough to create
     as they are all the same but for the version string. This is
     a helper function to turn a libsbml version string, such as 3.4.1/
     into an OtherVersion representation.
  """
  version_base = downloads_base + version_string + "/"
  version_dir_name = "libsbml-" + version_string
  version_archive = version_dir_name + "-src.zip"
  other_version = OtherVersion(version_base,
                               version_archive,
                               version_dir_name)
  return other_version

def libsbml_dep():
  """Calculate the libsbml dependency"""
  downloads_base = "http://sourceforge.net/projects/sbml/files/libsbml/"
  sbml_base = downloads_base + "5.0.0/"
  sbml_archive = "libSBML-5.0.0-src.zip"
  sbml = Dependency (sbml_base, sbml_archive)
  sbml.set_name ("libsbml")
  sbml.set_command_line_name("libsbml")
  sbml.set_dir_name("libsbml-5.0.0")
  extra_flags = [ "CXX=g++",
                  "CFLAGS=-O3",
                  "--without-zlib",
                  "--without-bzip2",
                ]
  sbml.add_configure_flags(extra_flags)

  sbml_3_4_0 = make_other_libsbml_version(downloads_base, "3.4.0")
  sbml.add_another_version("3.4.0", sbml_3_4_0)

  sbml_3_4_1 = make_other_libsbml_version(downloads_base, "3.4.1")
  sbml.add_another_version("3.4.1", sbml_3_4_1)

  return sbml

def pgapack_dep():
  """Calculate the dependency on the pga pack library"""
  # We are now using the official version, in our readme it suggests
  # that when compiling for HECToR we should usc "CC=cc" but I'm not sure
  # how critical that is, we'll see when we attempt to install on hector
  #
  # Of course this should be updated to use the official archive from
  # code.google once that is updated.
  pga_pack_base    = "http://www.dcs.ed.ac.uk/pepa/group/"
  pga_pack_archive = "pgapack-1.1.2.tar.bz2"
  pga_pack = Dependency(pga_pack_base, pga_pack_archive)
  pga_pack.set_name ("Parallel Genetic Algorithms Library")
  pga_pack.set_command_line_name("pgapack")
  extra_flags = [ "CC=mpicc",
                  # For some reason this seems to cause configuration to fail
                  # "CFLAGS=\"-O3 -lm\"",
                ]
  pga_pack.add_configure_flags(extra_flags)
  return pga_pack

def sundials_dep():
  """Calculate the sundials dependency"""
  # Note: 2.3.0 does not work, it is missing CVodeSVtolerances.
  #
  # I'm not sure if we could potentially upgrade up to 2.6.0
  # Very annoyingly you can't just provide a link for it, as it asks you
  # to fill out a form, we'll just assume that it is in the directory
  # and I might write a small script to warn about it.
  sundials_base = ""
  sundials_archive = "sundials-2.4.0.tar"
  sundials = Dependency(sundials_base, sundials_archive)
  sundials.set_dir_name("sundials-2.4.0")
  sundials.set_name ("Sundials ODE solvers")
  sundials.set_command_line_name("sundials")
  manual_url_base = "https://computation.llnl.gov/casc/sundials/"
  manual_url = manual_url_base + "download/download.html"
  sundials.set_manual_download_url(manual_url)
  # If you would rather install as a shared library remove
  # enable-shared=no and add -fPIC to CFLAGS
  extra_flags = [ "--enable-shared=no",
                  "CC=gcc",
                  "CXX=g++",
                  "CFLAGS=-O3",
                ]
  sundials.add_configure_flags(extra_flags)
  return sundials

def cppunit_dep():
  """Calculate cppunit as a dependency"""
  cppunit_base = "http://sourceforge.net/projects/cppunit/files/cppunit/1.12.1/"
  cppunit_archive = "cppunit-1.12.1.tar.gz"
  cppunit = Dependency(cppunit_base, cppunit_archive)
  cppunit.set_name ("C++ Unit Testing cppunit")
  cppunit.set_command_line_name("cppunit")
  # Not sure I really need this, or if it should actually be mpic++
  extra_flags = [ "CXX=g++" ]
  cppunit.add_configure_flags(extra_flags)
  return cppunit

def sbsi_dep():
  """Calculate sbsi as a dependency"""
  # Now not an actual dependency but the actual lib sbsi numerics
  # This also has some different options for Hector and Eddie, again
  # check the README to update this file.
  # But again I think that those differences are spurious and unnecessary
  # And also actually for macs, additionally this is a bit special we
  # have to work out whether we are downloading this as a user or as
  # as a developer, essentially are we in the sbsi development directory
  # this we tell by detecting whether we have an sbsi_numeric-devel
  # directory.
  in_source_dir = False
  if os.path.exists("sbsi_numeric-devel"):
    lib_sbsi_base = ""
    lib_sbsi_archive = "sbsi_numeric-devel"
    in_source_dir = True
  else:
    lib_sbsi_base = "http://sourceforge.net/projects/sbsi/files/SBSINumerics/"
    lib_sbsi_archive = "sbsi_numeric-devel-June-2012.tar.bz2"
  lib_sbsi = Dependency(lib_sbsi_base, lib_sbsi_archive)
  lib_sbsi.set_name ("SBSI Numerics")
  lib_sbsi.set_command_line_name("sbsi")
  extra_flags = [ "CXX=mpic++",
                  "CFLAGS=-O3",
                ]
  lib_sbsi.add_configure_flags(extra_flags)
  if in_source_dir:
    lib_sbsi.dep_directory = ""
  return lib_sbsi


# Calculate the list of dependencies
def list_dependencies(architecture):
  """Calculate the list of dependencies, this is all the dependencies from
     which we might filter those that the user wants if that is not all
     of them"""
  dependencies = []

  openmpi = open_mpi_dep()
  dependencies.append(openmpi)

  fftw = fftw_dep()
  dependencies.append(fftw)

  xml = libxml_dep(architecture)
  dependencies.append(xml)

  sbml = libsbml_dep()
  dependencies.append(sbml)

  pga_pack = pgapack_dep()
  dependencies.append(pga_pack)

  sundials = sundials_dep()
  dependencies.append(sundials)

  cppunit = cppunit_dep()
  dependencies.append(cppunit)

  lib_sbsi = sbsi_dep()
  dependencies.append(lib_sbsi)

  return dependencies



def get_options(option_name, arguments):
  """Gets all options of the form <option_name>=<argument> and returns
     the argument parts."""
  option_string = option_name + "="
  option_offset = len (option_string)
  options = [ option[option_offset:] for option in arguments
                if option.startswith(option_string) ]
  return options

def get_single_option(option_name, arguments):
  """The same as 'get_options' but insists on their being only one such,
     if there are no such arguments return the empty string"""
  options = get_options(option_name, arguments)
  if not options:
    return ""
  elif len(options) > 1:
    print ("Conflicting " + option_name + " arguments, choose one, exiting")
    sys.exit(1)
  else:
    return options[0]

def get_architecture(arguments):
  """Calculate the Architecture according to any arch=<arch> flags"""
  arch_name = get_single_option("arch", arguments)
  if arch_name == "":
    architecture = Architecture("general")
  elif arch_name not in [ "eddie", "hector" ]:
    print ("Unrecognised architecture name, leave blank or choose:")
    print ("   eddie")
    print ("   hector")
    print ("Or remove 'arch' flag for general architecture installation")
    sys.exit(1)
  else:
    architecture = Architecture(arch_name)
  return architecture

def get_installation_dir(arguments):
  """Return the installation directory if the user has used the
     'install=<dir> option, otherwise return null
  """
  relative_install_dir = get_single_option ("install", arguments)
  if relative_install_dir:
    install_dir = os.path.abspath(relative_install_dir)
    return install_dir
  return None

def set_installation_dir (install_dir, dependencies):
  """If the user has set the installation directory with an
     install=<dir> argument then use this to override the default
     installation directory in all of the given dependencies
  """
  if install_dir:
    for dep in dependencies:
      dep.set_install_dir(install_dir)

def set_dependency_dir (arguments, dependencies):
  """If the user has set the dependency directory with an
     depsdir=<dir> argument then use this to override the default
     dependency directory in all of the given dependencies"""
  relative_dep_dir = get_single_option("depdir", arguments)
  if relative_dep_dir:
    dep_dir = os.path.abspath(relative_dep_dir)
    for dep in dependencies:
      # Bit of a kludge for lib_sbsi when we are using the one in the
      # current directory because we think we're in the source directory.
      if dep.dep_directory: # or should be if it equals the default.
        dep.set_dep_directory(dep_dir)


def print_help_to_user(dependencies):
  """Print the help information to a requesting or confused user"""
  help_info = """
This script is designed to ease the installation of the sbsi numerics
framework and the associated dependencies.
General usage is:
python setup.py <dependencies>
"""
  extra_help_info = """
The installation directory can be set using the flag install=<directory>
where <directory> can be a relative or absolute path to the directory you
wish to install to. eg
python setup.py all install=/home/myname/install

In order for the sbsi framework to operate the installation directory
must be in certain compiler/library search paths, these can be set
automatically using the 'set-vars.sh' script available at:
https://sourceforge.net/projects/sbsi/files/SBSINumerics/set-vars.sh/download

The architecture that we are installing to can be specified with
python setup.py arch=<arch>
where arch must be either "eddie" or "hector", for all other architectures
leave this blank and it will default to general.

In order to avoid cluttering the current directory a dependencies directory
can be specified with the argument depdir=<dir> as in:
python setup.py depdir=/home/tmp/sbsi_deps
The default is simply, deps, if you want the current directory do "depdir=./"
This directory can be safely removed after installation (unless it is
the same as the install directory). Also one may not wish to delete all
archives especially the sundials archive as this would have to be later
manually downloaded again should you wish to re-install sbsi

Additionally please note that the dependency sundials cannot be downloaded
and as such must be downloaded by the user, this is because the end license
must be agreed. Simply visit:
https://computation.llnl.gov/casc/sundials/download/download.html
and download the version 2.4.0 and leave it in the current directory.

For each dependency name, one can alter the default version. This can be
useful for downloading the dependency archive from a mirror site, or when
attempting to use an upgraded (or older) version for some compatibility
reason. To specify a different version, use
<name>=<archive url> <name>-srcdir=<unpack-directory>
As an example to install version 3.4.0 of libsbml one would execute
the command:
python setup.py libsbml=http://sourceforge.net/projects/sbml/files/libsbml/3.4.0/libsbml-3.4.0-src.zip libsbml-srcdir=libsbml-3.4.0
Below the dependency names are re-listed, and with each some alternative
version shortcuts are presented in parentheses. This means that the above
could be done with the more succinct
python setup.py libsbml=3.4.0
because 3.4.0 is a predefined alternative version name of libsbml.
"""
  print (help_info)
  print ("python setup.py all")
  print ("will build/install all dependencies")
  print ("Alternatively you can list any of the following:")
  for dep in dependencies:
    print (dep.command_line_name)
    for version_name in dep.other_versions:
      print ("    (" + version_name + ")")

  print (extra_help_info)
  for dep in dependencies:
    if dep.other_versions:
      print (dep.command_line_name +
             " has predefined alternative version names:")
      for version_name in dep.other_versions:
        print ("    " + version_name)


def override_version(dependency, arguments):
  """A simple method to check whether there are any command-line
     arguments which override the version of a given dependency
  """
  name = dependency.command_line_name
  version = get_single_option(name, arguments)
  directory = get_single_option(name + "-srcdir", arguments)
  if version:
    dependency.switch_version(version, directory)

def write_out_set_vars_script(install_dir):
  """Write out the set-vars.sh script with the given installation dir"""
  set_vars_file = open ("set-vars.sh", "w")
  set_vars_file.write(create_set_vars_source(install_dir))
  set_vars_file.close()

# Install everything if there are no command line arguments
# or everything mentioned in the command line arguments
def run():
  """Process all the command line arguments and install the calculated
     set of dependencies"""
  record_file = open(".opt_setup", "w")
  for arg in sys.argv:
    record_file.write(arg + " ")
  record_file.write("\n")
  record_file.close()

  # The command line arguments not including this script itself
  arguments    = [ x for x in sys.argv if x != "setup.py" ]

  # Calculate the architecture from the command line arguments
  architecture = get_architecture(arguments)

  # Calculate the list of all dependencies, even those not asked for
  dependencies = list_dependencies(architecture)

  if "help" in arguments or "-help" in arguments or "--help" in arguments:
    print_help_to_user(dependencies)
    sys.exit(0)


  # Set the installation directory if that is specified in the command
  # line arguments
  install_dir = get_installation_dir(arguments)
  set_installation_dir(install_dir, dependencies)

  set_dependency_dir(arguments, dependencies)

  possible_targets = [ "all" ]
  for dep in dependencies:
    possible_targets.append(dep.command_line_name)

  # So for every command line argument we check if it is in the
  # list of possible targets, or if a corresponding version argument
  # is given, so that for example we will install libsbml if either
  # 'libsbml' or 'libsbml=<some-version>' is in the command line.
  # targets then will be the list of all targets requested.
  targets = []
  for argument in arguments:
    # So for example: "libsbml".split("=")[0] ==
    #                 "libsbml=3.4.1".split("=")[0] ==
    #                 "libsbml"
    # So in either case we are getting the list of possible targets.
    arg_name = argument.split("=")[0]
    if arg_name in possible_targets and arg_name not in targets:
      targets.append(arg_name)


  if not targets:
    print ("You must specify at least one dependency")
    print (" or try 'all' to install everything")
    print_help_to_user(dependencies)
    sys.exit(1)

  # Set the environment variables, these won't last past this script
  # however they may be required for the installation of some dependencies
  # in particular sbsi, since they might depend on previous dependencies
  # (obviously sbsi depends on them all)
  set_environment_variables()
  write_out_set_vars_script(install_dir)

  # Calculate the actual set of dependencies which we will install
  if "all" in targets:
    requested_deps = dependencies
  else:
    requested_deps = [ d for d in dependencies
                           if d.command_line_name in targets ]

  # For each dependency we need to check if the user has specified a
  # version for that dependency.
  for dependency in requested_deps:
    override_version(dependency, arguments)

  # First do an initial check for all the dependencies such that any
  # obvious errors can be reported (and fixed) before we start,
  # this usually just means we have to check for any manually
  # downloaded dependencies, so essentially just sundials ;)
  for dep in requested_deps:
    dep.initial_check()

  # Right we've done all we can do in preparation, now it is time to just
  # go ahead and attempt to install all of the dependencies
  for dep in requested_deps:
    dep.retrieve_to_install()

if __name__ == "__main__":

  # The following code doesn't really work instead we need
  # the user to use the 'set-var.sh' script if they are installing
  # to a non-standard location. This is basically because we can
  # set the environment variables but it won't out last the
  # setup.py script.

  # Not sure if this is how we should do this, basically
  # a way to only set the environments and nothing else
  # if "set-environment" in arguments:
  #  set_environment_variables()
  # else:
  run()



