import shutil
import os
import sys
from subprocess import PIPE, Popen
# For now 'run' just assumes that we are wanting to initialise
# a model, but in the future this will be the script for doing
# initialising, running RHS, running optimisation locally and
# running optimisation on clusters which may involve looping behaviour.

def initialise():
  # We assume we're in a directory containing the model file.
  # make the new directory 
  # move/copy the model file into the new directory.
  #model_dir = "my_new_chol_model"
  #os.makedirs(model_dir)

  # we must somehow set up the UserModel directory,
  # check that an appropriate configuration file is there
  # also check that there are appropriate data and header files.
  # Including the InitParam file. 
  model_dir = os.getcwd()
  user_model_dir = os.path.join(model_dir, "UserModel")

  os.makedirs(user_model_dir)
  os.makedirs(os.path.join(user_model_dir, "results"))
  os.makedirs(os.path.join(user_model_dir, "ckpoint"))
  os.makedirs(os.path.join(user_model_dir, "exp_data"))
  
  main_model_c_filename = os.path.join(model_dir, "main_Model.C")
  main_model_c_file = open(main_model_c_filename, "w")
  main_model_c_file.write(main_model_c_source)
  main_model_c_file.close()

def run_sbml2c (model_file):
  sbml2c_command = [ "SBML2C",
                     model_file
                   ]
  sbml2c_process = Popen(sbml2c_command)
  sbml2c_process.communicate()
  
def build_model (model_name):
  includeflag = "-I/disk/scratch/Source/sbsi/sbsi_numerics/trunk/install/include"
  includexmlflag = includeflag + "/libxml2"
  includesbsiflag = includeflag + "/sbsi_numeric"
  first_compile_command = [ "mpic++",
                            "-o", "main_Model.o",
                            "-DWL=32",
                            "-DNO_UCF",
                            includeflag,
                            includexmlflag,
                            includesbsiflag, 
                            "-c", "main_Model.C"
                          ]
  snd_compile_command = [ "mpic++",
                          "-o", "UserModel/UserModel.o",
                          "-DWL=32",
                          "-DNO_UCF",
                          includeflag,
                          includexmlflag,
                          includesbsiflag, 
                          "-c", "UserModel/UserModel.C"
                        ]
  third_compile_command = [ "mpic++",
                            "./main_Model.o",
                            "./UserModel/UserModel.o",
                            "-o", model_name + ".exe",
                            "-lsbsi_numeric",
                            "-lsbml",
                            "-lpgapack",
                            "-lfftw3",
                            "-lsundials_kinsol",
                            "-lsundials_nvecserial",
                            "-lsundials_cvode",
                            "-lxml2",
                           ]
  first_compile_process = Popen(first_compile_command)
  first_compile_process.communicate()

  snd_compile_process = Popen(snd_compile_command)
  snd_compile_process.communicate()

  third_compile_process = Popen(third_compile_command)
  third_compile_process.communicate()




def run_model(model_name, config_file):
  # Not sure about this for other architectures
  model_dir = "./"

  # I used a 'simple-run.sh' which should have its behaviour
  # implemented here instead. 
  import multiprocessing
  num_cores = multiprocessing.cpu_count()

  ofw_error_path = os.path.join(model_dir, "OFW.error")
  # THIS DOES NOT WORK!!
  if os.path.exists(ofw_error_path):
    os.delete(ofw_error_path)
  # Possibly should do the same for OFW.status and 
  # other such files for example logging?

  sub_directory = os.path.join(model_dir, "UserModel")
  model_executable = model_name + ".exe"

  # And then I have to run this command, not sure how I'm going
  # to find out the model id, actually that doesn't matter because
  # I can build up the model executable myself in this script hence
  # it doesn't necessarily have to be the same as that of the model_id
  # mpirun -np $NUM_CORES  $mydir/${BIN} $sub_dir $config_file
  mpirun_command = [ "mpirun",
                     "-np",
                     str(num_cores),
                     model_executable,
                     sub_directory,
                     config_file,
                   ]
  # I'm hopeful that by not setting stdout=PIPE the standard out
  # and standard error will just go straight to the console
  mpirun_process = Popen(mpirun_command, stdout=PIPE)
  mpirun_process.communicate()

  # The UserModel/exp_data directory should contain the InitParam and
  # sbsi{header|data} files.

  # I should worry about how the sbsi{data/header} files are found
  # and named it doesn't seem to relate to the model id and I think it should
  # Ah okay no this is mentioned in the configuration file, that's perfect.
  # And the same for the InitParam file

main_model_c_source ="""
#include <sys/time.h>
#include <mpi.h>          //MAXPATHLEN

#include <Setup.h>
#include <Optimiser.h>
#include <MultiObjCost.h>
#include <CommandLine.h>
#include <ParseConfigFile.h>
#include <ERR.h>

using namespace std;


#ifndef NO_MODEL
#include "UserModel/UserModel.h"
#endif

#ifndef NO_UCF
#include "UserModel/UserCostFunction.h"
#endif



double getnow()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + 1e-6 * tv.tv_usec;
}

void writeBestCostResults( const string fname, const double cost, cModel *model);

void SetMCF(cModel *m, vector<CostArg> &cas, vector<DataSet> &ds, ParamInfo &pi,CostArg &gblca, TimeSeries &ts, ModelCostFunction *mcf);

vector<DataSet> GetUCF_DataSets(UserCostFunctionSet &ucf, string exp_dir);

int main(int argc, char **argv)
{
  /* usage Model_name logfilename loglevel xmlfile */
  if(argc < 3) {
    cout << "Not right usage. Usage: " << argv[0] << " [Model ID] [config.xml file]" << endl;
    exit(8);
  }

  MPI_Init(&argc, &argv);
  int my_rank(0);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  double tstart = getnow();

  CommandLine::is(argc, argv, my_rank);


  /* usage Model_name  xmlfile */ 
  const string MODEL_NAME = CommandLine::arg();
  const string xmlfilename = CommandLine::arg();

  cout << __LINE__ << endl;
  string modeldir = MODEL_NAME + "/";
  string exp_dir = modeldir + "exp_data/";
  string results_dir = modeldir + "results/";
  string ckpoint_dir = modeldir + "ckpoint/";

  /* prepare args */
  cModelArg ModelArg;
  OptArg opt_arg;

  /* set default values */
  opt_arg.setCkpointStartNum(0);
  opt_arg.setPrintBestPop(true);
  opt_arg.MyMutation = NULL;
  opt_arg.MyCrossOver = NULL;
  opt_arg.MyEndOfGen = NULL;

  opt_arg.setResultDir(results_dir);
  opt_arg.setCkpointDir(ckpoint_dir);

  ParseConfigFile parser(modeldir);

  CostArgSet setup_cost_args;
  CostArgSet optimiser_cost_args;
  CostArg GlbCostArg;

  parser.parseConfig(xmlfilename, opt_arg, GlbCostArg, setup_cost_args,
    optimiser_cost_args, ModelArg);
  parser.loadData(setup_cost_args);
  parser.loadData(optimiser_cost_args);


/* EXEC */
  if(my_rank == 0)
    cout << modeldir + parser.Init_Param_File << endl;



  /* read param info file  and set paraminfo */
  FileReader &paraminfo_freader = FileReader::SetFileReader(TabDelimitedColumnParamInfo);
  paraminfo_freader.ClearSets();
  paraminfo_freader.Read(modeldir + parser.Init_Param_File);
  ParamInfo paraminfo = paraminfo_freader.paraminfo;

  /*creat setup for the first parameter sets */
  Setup &setup_param = Setup::SetupParam(opt_arg.getSetupType());

  ModelCostFunction &mcf_x2c = ModelCostFunction::SetModelCostFunction(X2Cost);
  ModelCostFunction &mcf_fft = ModelCostFunction::SetModelCostFunction(FFT);

  /* set CostFunction  */
  /* if there is no costfunctions set to the setup, use the same costfunctions with opt_arg */

#ifndef NO_MODEL
  /* create the model */
  static cModel *model = getModel();
  model->setModelArg(ModelArg);
  model->inputModel();
  model->setFinal(std::max(model->getFinal(), setup_cost_args.maxtime()));
  TimeSeries t_series;
  t_series.SetModel(model);
#endif

  MultiObjCost *cf = new MultiObjCost();

  if(!setup_cost_args.X2C_cost_arg.empty() || !setup_cost_args.FFT_cost_arg.empty() || !setup_cost_args.UserCostFunctions.empty() ) cf->Init();

#ifndef NO_MODEL

  if(!setup_cost_args.X2C_cost_arg.empty()) {
     SetMCF(model,setup_cost_args.X2C_cost_arg, setup_cost_args.X2C_dataset, paraminfo, GlbCostArg,t_series,&mcf_x2c);
    cf->AddObjective(mcf_x2c); 
  }
  if(!setup_cost_args.FFT_cost_arg.empty()) {
     SetMCF(model,setup_cost_args.FFT_cost_arg, setup_cost_args.FFT_dataset, paraminfo, GlbCostArg,t_series,&mcf_fft);
    cf->AddObjective(mcf_fft);
  }
#endif 

#ifndef NO_UCF
   vector<UserCostFunction*> ucf_p;
   vector<DataSet> UCF_DataSets;

  if(!setup_cost_args.UserCostFunctions.empty()) {
   for(size_t i=0; i<setup_cost_args.UserCostFunctions.size(); i++){
   UserCostFunction &ucf=UserCostFunction::setUserCostFunction(setup_cost_args.UserCostFunctions.at(i).type);
#ifdef SBSI_TEST
    vector<string> allpname = paraminfo.GetParamName();
    int anum(20);
    vector<string> pname;
    for(int j=0; j<anum; j++) pname.push_back(allpname.at(i*anum+j));
    ucf.SetParamNames(pname);
#else
    ucf.SetModel(model);
    ucf.SetGlbCostArg(GlbCostArg);
    ucf.SetCostArgs(setup_cost_args.X2C_cost_arg);

    /* set dataset from file name */
    UCF_DataSets=GetUCF_DataSets(setup_cost_args.UserCostFunctions.at(i), parser.exp_dir);
    if(!UCF_DataSets.empty()) ucf.SetDataSets(&UCF_DataSets);
    ucf.SetTimeSeries(&t_series);
    ucf.SetSolver();
#endif
    ucf_p.push_back(&ucf);
    cf->AddObjective(*ucf_p.back());
   }
  }
#endif 


  if(!paraminfo.IsParamNameEmpty())  cf->SetParamNames(paraminfo.GetParamName());



  /* Global CostArg to CostFunction */
    cf->SetGlbCostArg(GlbCostArg);

  setup_param.Init();

  setup_param.SetParamInfo(&paraminfo);
  setup_param.SetOptArg(&opt_arg);

  if(opt_arg.getSetupType() == ReadinFile) {
    ostringstream paramreadinfile;
    if(opt_arg.getCkpointStartNum() == 0) {
      paramreadinfile << opt_arg.getCkpointDir() << "/" << parser.paramreadinfile;
    }
    else {
      paramreadinfile << opt_arg.getCkpointDir() << "/BestPop." << opt_arg.getCkpointStartNum() << ".ga";
    }
    setup_param.SetFileName(paramreadinfile.str());
  }

  setup_param.SetCostFunction(cf);
  setup_param.run();


  /*optimiser */
#ifndef NO_MODEL
  model->setFinal(std::max(model->getFinal(), optimiser_cost_args.maxtime()));
#endif

  vector<ParamSet> paramset = setup_param.paramset;

  Optimiser &myoptimiser = Optimiser::SetOptimiser(opt_arg.getOptimiserType());

  myoptimiser.Init(&opt_arg, paraminfo, &paramset);

  delete cf;
  cf = new MultiObjCost();
  cf->Init();

  cf->SetGlbCostArg(GlbCostArg);

  /*
   * Note:
   * If there is no setting costfunctions for the optimiser,  user the same costfunction and 
   *  the same dataset as the setup.
   * If there is any setting, use its optimiser setting costfunctions and datasets 
   */

  if(optimiser_cost_args.X2C_cost_arg.empty() || optimiser_cost_args.FFT_cost_arg.empty() || optimiser_cost_args.UserCostFunctions.empty() ) {
    
#ifndef NO_MODEL
     if(!setup_cost_args.X2C_cost_arg.empty()) {
        SetMCF(model,setup_cost_args.X2C_cost_arg, setup_cost_args.X2C_dataset, paraminfo, GlbCostArg,t_series,&mcf_x2c);
        cf->AddObjective(mcf_x2c);
     }

     if(!setup_cost_args.FFT_cost_arg.empty()) {
       SetMCF(model,setup_cost_args.FFT_cost_arg, setup_cost_args.FFT_dataset, paraminfo, GlbCostArg,t_series,&mcf_fft);
     cf->AddObjective(mcf_fft);
     }
#endif
#ifndef NO_UCF
   ucf_p.clear();
   UCF_DataSets.clear();
  if(!setup_cost_args.UserCostFunctions.empty()) {
    vector<string> allpname = paraminfo.GetParamName();
   for(size_t i=0; i<setup_cost_args.UserCostFunctions.size(); i++){
   UserCostFunction &ucf=UserCostFunction::setUserCostFunction(setup_cost_args.UserCostFunctions.at(i).type);
#ifdef SBSI_TEST
    int anum(20);
    vector<string> pname;
    for(int j=0; j<anum; j++) pname.push_back(allpname.at(i*anum+j));
    ucf.SetParamNames(pname);
#else
    ucf.SetModel(model);
    ucf.SetParamNames(allpname);
    ucf.SetGlbCostArg(GlbCostArg);
    ucf.SetCostArgs(setup_cost_args.X2C_cost_arg);
    /* set dataset from file name */
    UCF_DataSets=GetUCF_DataSets(setup_cost_args.UserCostFunctions.at(i), parser.exp_dir);
    if(!UCF_DataSets.empty())ucf.SetDataSets(&UCF_DataSets);
    ucf.SetTimeSeries(&t_series);
    ucf.SetSolver();
#endif
    ucf_p.push_back(&ucf);
    cf->AddObjective(*ucf_p.back());
   }
  }
#endif
  } else{
#ifndef NO_MODEL
  if(!optimiser_cost_args.X2C_cost_arg.empty()) {
        SetMCF(model,optimiser_cost_args.X2C_cost_arg, optimiser_cost_args.X2C_dataset, paraminfo, GlbCostArg,t_series,&mcf_x2c);
        cf->AddObjective(mcf_x2c);
  }
  if(!optimiser_cost_args.FFT_cost_arg.empty()) {
        SetMCF(model,optimiser_cost_args.FFT_cost_arg, optimiser_cost_args.FFT_dataset, paraminfo, GlbCostArg,t_series,&mcf_fft);
        cf->AddObjective(mcf_fft);
  }
#endif
#ifndef NO_UCF
   ucf_p.clear();
   UCF_DataSets.clear();
  if(!optimiser_cost_args.UserCostFunctions.empty()) {
      vector<string> allpname = paraminfo.GetParamName();
      for(size_t i=0; i<optimiser_cost_args.UserCostFunctions.size(); i++){
      UserCostFunction &ucf=UserCostFunction::setUserCostFunction(optimiser_cost_args.UserCostFunctions.at(i).type);
  #ifdef SBSI_TEST
      int anum(20);
      vector<string> pname;
      for(int j=0; j<anum; j++) pname.push_back(allpname.at(i*anum+j));
      ucf.SetParamNames(pname);
  #else
      ucf.SetModel(model);
      ucf.SetParamNames(allpname);
      ucf.SetGlbCostArg(GlbCostArg);
      ucf.SetCostArgs(optimiser_cost_args.X2C_cost_arg);
      /* set dataset from file name */
      UCF_DataSets=GetUCF_DataSets(optimiser_cost_args.UserCostFunctions.at(i), parser.exp_dir);
      if(!UCF_DataSets.empty())ucf.SetDataSets(&UCF_DataSets);
      ucf.SetTimeSeries(&t_series);
      ucf.SetSolver();
  #endif
      ucf_p.push_back(&ucf);
      cf->AddObjective(*ucf_p.back());
    }
  }
#endif
 }

  
  if(!paraminfo.IsParamNameEmpty())  cf->SetParamNames(paraminfo.GetParamName());

  myoptimiser.SetCostFunction(cf);

  double opt_tstart = getnow();

  /* Optimiser Run */
  int iter = myoptimiser.Run();

  /* Finish Run */

  if(my_rank == 0) {

    double opt_elapsed = getnow() - opt_tstart;

    cout << "elapsed time for Optimiser " << opt_elapsed << "sec" << endl;

    int cNumGen = myoptimiser.optarg->getCkpointStartNum() + iter;

    string result_dir = myoptimiser.optarg->getResultDir();

    vector<double> b_param = myoptimiser.GetTheBest();

    CostArgSet cost_args;

    if(optimiser_cost_args.X2C_cost_arg.empty() && optimiser_cost_args.FFT_cost_arg.empty()) {
     cost_args=setup_cost_args;
    }else cost_args=optimiser_cost_args;

    if(!cost_args.X2C_dataset.empty()){
     mcf_x2c.SetDataSets(&cost_args.X2C_dataset);
    }else mcf_fft.SetDataSets(&cost_args.FFT_dataset);

    if(!cost_args.X2C_cost_arg.empty()){
      mcf_x2c.SetParameters(b_param);
    }else {
    if(!cost_args.FFT_cost_arg.empty())
      mcf_fft.SetParameters(b_param);
    }

    for(size_t i = 0; i < cost_args.X2C_dataset.size(); i++) {

      mcf_x2c.SetDataSet(i);

      string dataSetname=cost_args.X2C_dataset[i].GetSetName();

      dataSetname.resize(dataSetname.rfind("."));
#ifdef SBSI_TEST
      cout.precision(8);
      cout << "========================= Result for the dataset: " << dataSetname << 
              " =========================" << endl;
      cout << "\\tCostValue is " << mcf_x2c.getcost() << endl;
#else
      ostringstream b_cost_fname;
      b_cost_fname << result_dir << "BestCost_" << dataSetname << ".txt." << cNumGen;
      writeBestCostResults( b_cost_fname.str(), mcf_x2c.getcost(), model);
     
      ostringstream t_series_fname;
      t_series_fname << parser.timeseries_dir <<  parser.timeseries_fname << "_" << dataSetname << "_" <<  cNumGen << ".dat";

      t_series.WriteToSBSIDataFormat(t_series_fname.str());
#endif
    }


    for(size_t i = 0; i < cost_args.FFT_dataset.size(); i++) {

      mcf_fft.SetDataSet(i);
		
      string dataSetname = cost_args.FFT_dataset[i].GetSetName();

      dataSetname.resize(dataSetname.rfind("."));
#ifdef SBSI_TEST
      cout << "========================= Result for the dataset: " << dataSetname << 
              " =========================" << endl;
      cout << "\\tCostValue is " << mcf_fft.getcost() << endl;
#else
      ostringstream b_cost_fname;
      b_cost_fname << result_dir << "BestCost_FFT_" << dataSetname << ".txt." << cNumGen;
     
      writeBestCostResults( b_cost_fname.str(), mcf_fft.getcost(), model);

      ostringstream t_series_fname;
      t_series_fname << parser.timeseries_dir <<  "FFT_" << parser.timeseries_fname << "_" << dataSetname << "_" <<  cNumGen << ".dat";

      t_series.WriteToSBSIDataFormat(t_series_fname.str());
#endif
    }


  }

  int stat(0);
  MPI_Initialized(&stat);
  if(stat) MPI_Finalize();
  
  if(my_rank == 0)
  {
    double elapsed = getnow() - tstart;
    cout << "finish jobs; elapsed " << elapsed << "sec" << endl;

  }

  return 0;
}

void writeBestCostResults(  const string fname , const double cost,  cModel *model) {

	ofstream bfp(fname.c_str());

	if(!bfp) ERR.FileW("Result Out", __func__, fname.c_str());
	
	bfp.precision(8);
	
	bfp << "CostValue is " <<cost << endl;

        vector<string> paramNames = model->getParamNames();
	
	for(unsigned int p_i = 0; p_i < paramNames.size(); p_i++) 
        bfp << paramNames[p_i] << ":\\t" << model->getParamByName(paramNames[p_i]) << ";\\n" ;
	
	
}

void SetMCF(cModel *m, vector<CostArg> &cas, vector<DataSet> &ds, ParamInfo &pi,CostArg &gblca, TimeSeries &ts, ModelCostFunction *mcf){
    mcf->Init(m, cas, pi.GetParamName());
    if(!pi.IsUnOptimiseParamNameEmpty())
         mcf->SetUnOptimiseParameters(pi.GetUnOptimiseParamName(),pi.GetUnOptimiseParamValue());
    mcf->SetDataSets(&ds);
    mcf->SetGlbCostArg(gblca);
    mcf->SetTimeSeries(&ts);
    mcf->SetSolver();
}

vector<DataSet> GetUCF_DataSets(UserCostFunctionSet &ucfs, string exp_dir){

  vector<DataSet> ds;

  if(!ucfs.datafilename.empty() || !ucfs.hdrfilename.empty()){
              FileReaderType expfilereadtype = TabDelimitedColumnData;
              FileReader &exp_file_reader = FileReader::SetFileReader(expfilereadtype);
              TDCHeaderFileReader hdr;
          for(vector<string>::iterator f_i = ucfs.datafilename.begin(); f_i != ucfs.datafilename.end(); f_i++){
              exp_file_reader.Sets.clear();
              exp_file_reader.Read(exp_dir + *f_i);
              ds.push_back(exp_file_reader.Sets.back());
          }
             size_t n = ds.size();
             ds.resize(n + ucfs.hdrfilename.size());
          for(size_t f_i=0; f_i<ucfs.hdrfilename.size(); f_i++){
               ds.at(n+f_i).SetSetName(ucfs.hdrfilename.at(f_i));
               hdr.SetDataSet(&(ds.at(n+f_i)));
               hdr.Read(exp_dir + ucfs.hdrfilename.at(f_i));
          }
    }
  return ds;

}
"""

main_model_rhs_c_source = """
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <ctype.h>
#include <unistd.h>
#include <sys/param.h>//MAXPATHLEN
#include <mpi.h>
using namespace std;

#include <cModelArg.h>
#include <cModel.h>
#include <Solver.h>
#include <CVODESolver.h>

#include "UserModel/UserModel.h"

int main(int argc,char **argv)
{
  /************************************************************/
  /*                      SKELTON                             */
  /*                                                          */
  /* Part 1 ; INPUT                                           */                         
  /*    1) set FileReader Type for Exp. data and its filename */
  /*    2) set cModel_arg                                      */
  /*            i) set CostFunction Type;                     */
  /*           ii) set Solver Type                            */
  /*          iii) set cModelCostFunctionType                  */
  /*                                                          */
  /* Part 2; EXECUTE                                          */
  /*    1) Create cModel                                       */
  /*    2) Calculate RHS of the Model                          */
  /*    2) Write the result of Timeseris to the file          */
  /************************************************************/
  MPI_Init(&argc, &argv);
  int my_rank(0);
  MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);

   /* set default*/

   /* read argv */

   int argv_i=0;

   char *model_name=argv[++argv_i];
   string MODEL_NAME = model_name;

   /*make model_dir under current dir */

   char currentdir[MAXPATHLEN];

   getcwd(currentdir,MAXPATHLEN);

   string modeldir=currentdir;


   modeldir+="/";
   modeldir+=MODEL_NAME;
   modeldir+="/";

   //chdir(modeldir.c_str());

   getcwd(currentdir,MAXPATHLEN);


  /* read set up numbers */

  int tmp_int;
  double tmp_double;

  char *scan_int_str="%d";
  char *scan_double_str="%lf";

  /* Input cModel_arg */
  cModelArg model_arg;

  if(sscanf(argv[++argv_i],scan_double_str,&tmp_double)!=1){
   printf("ERROR: t_final scannging argv[%d]\\n ",argv_i);
   exit(8);
  }
   model_arg.Final=tmp_double;

  if(my_rank==0) printf("t_final %f\\n",model_arg.Final);

  if(sscanf(argv[++argv_i],scan_double_str,&tmp_double)!=1){
   printf("ERROR: t_init scannging argv[%d]\\n ",argv_i);
   exit(8);
  }
  model_arg.Init=tmp_double;

  if(sscanf(argv[++argv_i],scan_int_str,&tmp_int)!=1){
   printf("ERROR: maxTimes scannging argv[%d]\\n ",argv_i);
   exit(8);
  }
  model_arg.MaxTimes=tmp_int;

  if(sscanf(argv[++argv_i],scan_double_str,&tmp_double)!=1){
   printf("ERROR: interval scannging argv[%d]\\n ",argv_i);
   exit(8);
  }
  model_arg.Interval=tmp_double;

  if(sscanf(argv[++argv_i],scan_double_str,&tmp_double)!=1){
   printf("ERROR: ouput interval scannging argv[%d]\\n ",argv_i);
   exit(8);
  }
  model_arg.OutputInterval=tmp_double;

  if(sscanf(argv[++argv_i],scan_double_str,&tmp_double)!=1){
   printf("ERROR: atol scannging argv[%d]\\n ",argv_i);
   exit(8);
  }
  model_arg.Atol=tmp_double;

  if(sscanf(argv[++argv_i],scan_double_str,&tmp_double)!=1){
   printf("ERROR: reltol scannging argv[%d]\\n ",argv_i);
   exit(8);
  }
  model_arg.Reltol=tmp_double;


  /* Set the TimeSeries file name*/
  char * t_seriesfilename=argv[++argv_i];
  ostringstream oss_tfout;
  oss_tfout << t_seriesfilename  << "_RHS.dat";

  string t_series_fname= oss_tfout.str();



 /*************************EXECUTE ****************************************/



  /* create the model */
  static cModel   *model=getModel();
  
  model->setModelArg(model_arg);
  model->inputModel();

  /* set TimeSeries */
  TimeSeries t_series;
  t_series.SetModel(model);

  /* set Solver */

  CVODESolver  cvodesolver;
  Solver *solver = &(cvodesolver);
  solver->SetModel(model);
  solver->SolveCreate();
  solver->SolveInit();
  solver->SetTimeSeries(&t_series); 
  model->calCoef();
  solver->SolveInput();
  if(solver->Solve()){
     if(my_rank==0) t_series.WriteToSBSIDataFormat(t_series_fname);
  }

  int stat(0);
       MPI_Initialized(&stat);
        if(stat)MPI_Finalize();

  if(my_rank==0) cout << "finish jobs" << endl;

  
}
"""


if __name__ == "__main__":
  arguments = sys.argv
  if "init" in arguments:
    initialise()  
  if "sbml2c" in arguments:
  # Clearly the model file must somehow be specified in the command
  # line arguments.
    run_sbml2c ("simple-model.xml")
  if "run" in arguments:
    # Again obviously these arguments should be passable on the command
    # line.
    run_model("my_model_name", "ofwconfig.xml")
  if "build-model" in arguments:
    # Once again obviously this model name should be in the arguments
    build_model("my_model_name")

