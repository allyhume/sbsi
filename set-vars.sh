  
#!/bin/sh
if [ $# -gt 0 ] ; then
 PREFIX=$1
else
 PREFIX=$PWD/install/
fi

export PATH=${PREFIX}/bin:$PATH # We'll need this for mpicxx etc.
export INCLUDE_PATH=${PREFIX}/include
export LIBRARY_PATH=${PREFIX}/lib
export LD_LIBRARY_PATH=$LIBRARY_PATH
# the mac equivalent of LD_LIBRARY_PATH
export DYLD_LIBRARY_PATH=$LIBRARY_PATH

export CPATH=$INCLUDE_PATH
export LDFLAGS="-L${PREFIX}/lib"
export CFLAGS="-I${PREFIX}/include"
export CXXFLAGS=${CFLAGS}
