"""Run a bunch of tests, assume one test for each sub-directory"""
import shutil
import os
import sys
from subprocess import PIPE, Popen
import xml.dom.minidom

def obfuscate_file(filename):
  """Parse in a file as an SBML model, obfuscate it and then print out
     the obfuscated model"""
  dom = xml.dom.minidom.parse(filename)
  obfuscate_model(dom.getElementsByTagName("model")[0])
  print(dom.toxml())
  

def run_test(test):
  """Given a test element, perform the given test"""
  command_elements = test.getElementsByTagName("Command")
  for command_element in command_elements:
    # In reality of course I must parse in the text of the Command element
    # Don't forget the test.xml should also run SBML2C
    # command_words  = [ "source", "test-simple-run.sh" ]
    # process = Popen(command_words)
    # process.communicate()
    text_element = command_element.firstChild
    command_text = text_element.data.lstrip().rstrip()
    command = command_text.split()
    process = Popen(command, stdout=PIPE)
    (output, error) = process.communicate()
    if process.returncode != 0:
      print("command: " + command_text + " failed to execute successfully")

  # So then there may be a bunch of files that we expect to be
  # created and what we expect their contents to be.
  # If the bug is simply that there is a seg fault then we might
  # have no files to test against. However we should probably
  # report such tests such that when we go green, we should add
  # the gold standard as something of a regression test.

def run_test_directory(current_dir, subdir):
  """Enter the test directory and read the test.xml file"""
  os.chdir(subdir)
  
  dom   = xml.dom.minidom.parse("test.xml")
  tests = dom.getElementsByTagName("Test")
  for test in tests:
    run_test(test)
  
  # Don't forget to switch back to the original directory
  os.chdir(current_dir)

def run():
  """Perform the banalities of command-line argument processing and
     then get under way with the proper grunt work of running tests"""
  # The command line arguments not including this script itself
  # arguments    = sys.argv 

  # Grab all files in the current directory which are themselves directories
  current_dir = os.getcwd()
  subdirs = [name for name in os.listdir(current_dir)
               if os.path.isdir(os.path.join(current_dir, name))]

  for subdir in subdirs:
    run_test_directory(current_dir, subdir)


if __name__ == "__main__":
  run()
