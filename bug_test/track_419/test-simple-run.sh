#!/bin/sh

# PREFIX=
INCLUDES="-I$PREFIX/include -I$PREFIX/include/libxml2 -I$PREFIX/include/sbsi_numeric"
LIBS="-L$PREFIX/lib"


if [ $# -gt 0 ] ; then
 CONFIG_FILE=$1
else
 CONFIG_FILE=ofwconfig.xml
fi

if [$# -gt 1 ] ; then
  MODEL_NAME=$2
else
  MODEL_NAME=model
fi

BIN_NAME=${MODEL_NAME}.exe

mpic++ -o main_Model.o ${INCLUDES} ${LIBS} -DWL=32 "-DNO_UCF" -c  main_Model.C
mpic++ -o UserModel/UserModel.o ${INCLUDES} ${LIBS} -DWL=32 "-DNO_UCF" -c UserModel/UserModel.C
mpic++ ./main_Model.o ./UserModel/UserModel.o  -o ${BIN_NAME} ${INCLUDES} ${LIBS} -lsbsi_numeric -lsbml -lpgapack -lfftw3 -lsundials_kinsol -lsundials_nvecserial -lsundials_cvode  -lxml2
./simple-run.sh ${MODEL_NAME} ${CONFIG_FILE} 
