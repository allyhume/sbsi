"""A simple python script to perform a packaging of sbsi_numerics.
This actually checks out the svn, performs a bootstrap and then
creates a tar archive of the sbsi source. Additionally it then
builds the manual and copies that ready to be uploaded to source forge.
"""
import shutil
import os
import sys
from subprocess import PIPE, Popen

def get_options(option_name, arguments):
  """Gets all options of the form <option_name>=<argument> and returns
     the argument parts."""
  option_string = option_name + "="
  option_offset = len (option_string)
  options = [ option[option_offset:] for option in arguments
                if option.startswith(option_string) ]
  return options

def get_single_option(option_name, arguments):
  """The same as 'get_options' but insists on their being only one such,
     if there are no such arguments return the empty string"""
  options = get_options(option_name, arguments)
  if not options:
    return ""
  elif len(options) > 1:
    print ("Conflicting " + option_name + " arguments, choose one, exiting")
    sys.exit(1)
  else:
    return options[0]

def svn_checkout(package_dir):
  """Perform the checkout of the svn sources"""
  repo_root = "https://sbsi.svn.sourceforge.net/svnroot/sbsi/"
  repo_addr = repo_root + "sbsi_numerics/trunk/sbsi_numeric-devel"
  svn_command = [ "svn", 
                  "co",
                  repo_addr,
                  package_dir
                ]  
  svn_process = Popen(svn_command)
  svn_process.communicate()
  if svn_process.returncode != 0:
    print ("Could not successfully execute svn checkout")
    print ("Maybe the server is down?")
    print ("Try running the svn command yourself:")
    print (svn_command)
    sys.exit(1)

def run_bootstrap(package_dir):
  """Run the bootstrap script in the sbsi source directory to
     create the configure script, ready for it to be packaged up"""
  current_dir = os.getcwd()
  os.chdir(package_dir)
  bootstrap_command = [ "./bootstrap" ]
  bootstrap_process = Popen(bootstrap_command, stdout=PIPE)
  bootstrap_process.communicate()
  if bootstrap_process.returncode != 0:
    print ("Could not bootstrap sbsi")
    print ("Could still be a workable distribution")
    print ("But try running it yourself")
    sys.exit(1)
  os.chdir(current_dir)

 

def tarball_sbsi_dir(package_dir):
  """Create a bz2ed tar archive of the 
     sbsi_numeric-devel<version> directory"""
  archive_name = package_dir + ".tar.bz2"
  tar_command = [ "tar", "cjf", archive_name, package_dir ]
  tar_process = Popen(tar_command)
  tar_process.communicate()
  if tar_process.returncode != 0:
    print ("tar command has failed;")
    print ("   perhaps you do not have the tar command installed?")
    sys.exit(1)

def build_manual(package_dir, version):
  """change to the documentation's manual directory and run
     pdflatex (3 times to get the cross references correct) to
     create the manual and then move and rename it appropriately"""
  doc_dir    = "doc/manual"
  manual_dir = os.path.join (package_dir, doc_dir)
  current_dir = os.getcwd()
  os.chdir(manual_dir)

  # In order to get the version correct we must update the version
  # tex file, this is a simple text file which is included within
  # the main tex file.
  version_file = open("SBSI-NumericsVersion.tex", "w")
  version_file.write(version)
  version_file.close()

  pdflatex_command = [ "pdflatex", "SBSI-NumericsUserManual" ]
  pdflatex_process1 = Popen(pdflatex_command)
  pdflatex_process1.communicate()
  if pdflatex_process1.returncode != 0:
    print ("pdflatex command failed, perhaps you don't have it installed?")
    sys.exit(1)
  pdflatex_process2 = Popen(pdflatex_command)
  pdflatex_process2.communicate()
  if pdflatex_process2.returncode != 0:
    print ("pdflatex command failed, perhaps you don't have it installed?")
    sys.exit(1)
  pdflatex_process3 = Popen(pdflatex_command)
  pdflatex_process3.communicate()
  if pdflatex_process3.returncode != 0:
    print ("pdflatex command failed, perhaps you don't have it installed?")
    sys.exit(1)

  # switch back to the original directory and copy the pdf file
  # to there whilst at the same time renaming it to the given version.
  new_pdf_name = "SBSI-NumericsUserManual-" + version + ".pdf"
  os.chdir(current_dir)
  manual_name = "SBSI-NumericsUserManual.pdf"
  manual_path = os.path.join(manual_dir, manual_name)
  shutil.copy(manual_path, new_pdf_name)
 
def run():
  """Simply do the brunt of the work"""
  arguments = sys.argv
  
  version = get_single_option("version", arguments)
  if not version:
    print ("You must specify the version string with: version=<vstring>")
    sys.exit(1)

  # The sandbox directory so everything is kept away from the
  # actual sources and such that
  sandbox_dir = "package_sandbox"
  # The package directory which will be what we eventually tar up
  package_dir = "sbsi_numeric-devel-" + version
  package_dir_path = os.path.join(sandbox_dir, package_dir)
  if os.path.exists(package_dir_path):
    print ("Package directory already exists")
    print (package_dir)
    print ("Please remove and try again")
    sys.exit(1)

  if not os.path.exists(sandbox_dir):
    os.makedirs(sandbox_dir)
  
  os.chdir(sandbox_dir)

  # Check out the svn repository
  svn_checkout(package_dir)

  # Run the bootstrap command to produce the configure command
  run_bootstrap(package_dir)

  # tarball up the latest and we're done.
  tarball_sbsi_dir(package_dir) 

  # Now build the manual in the directory and move the resulting
  # manual to outside the directory, notice that we do this after
  # tarballing up so the built manual is not included in the tarball.
  build_manual(package_dir, version) 

  # Not sure if I can programmatically upload the file to
  # source forge, but that would be quite cool.
  
  # I could run grep on setup.py and check that the version number
  # is there?
  print ("Now you have to upload the archive to source forge")
  print ("Archive: " + package_dir_path + ".tar.bz2")
  print ("Also upload the manual")
  print ("Don't forget to update setup.py!")    
  print ("  In sbsi_dep, set lib_sbsi_archive to:")
  print ("  \"sbsi_numeric-devel-" + version + ".tar.bz2\"")
  print ("Also upload setup.py")
  sys.exit(0)


if __name__ == "__main__":
  run ()

